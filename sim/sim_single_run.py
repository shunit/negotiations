#! /usr/bin/env python
'''
Created on May 2, 2012

@author: eyal
'''
from exp.core.Loads import gen_random_sine_const_sum, loadupto20
from sim.SimsScan import SimsScan, guests_generator
from etc.TimeNow import get_time_now
import mom

do_optimize = False

n = 8
alpha = 1
mem_ratio = 0.5
base_mem = 1150
mem_max_perf = 1400
rounds = 100
T_load = 600
T_auction = T_load / 10

#funcs, funcs_info = gen_random_sine_const_sum(n, T_avg = T_load, **loadupto20)
funcs_info = "simple linear"
funcs = lambda i: lambda t: funcs[i](t * T_auction)
loads = [funcs(i) for i in range(n)]

adviser_args = {"name": "AdviserProfit",
                "profiler": "../doc/profiler-memcached-10vms.xml",
                "base_mem": base_mem, "entry": "throughput",
                "memory_delta": 10, "rev_func": "lambda x: 10*x",
                "load_smooth": 0, "memory_smooth": 1,
                "load_d": 2, "memory_d": 10}
no_costs = False

def sim_single_run():
    mom.Adviser.NoCosts = no_costs
    sim = SimsScan("single-run-%s" % get_time_now())

    info = {"n":n, "alpha":alpha, "mem_ratio":mem_ratio, "base_mem": base_mem,
            "mem_max_perf":mem_max_perf, "rounds":rounds, "T_load":T_load,
            "T_auction":T_auction, "loads": funcs_info,
            "adviser_args": adviser_args}

    sim.save_to_file(info, "info")

    sim.run(guests_generator(n, adviser_args, loads, T_load), mem_max_perf, rounds,
            mem_ratio, mem_ratio, 1, alpha, alpha, 1, do_optimized = do_optimize)

    import sim_output
    sim_output.sim_dir = sim.get_test_dir()
    sim_output.draw_exp(mem_ratio, alpha)

if __name__ == '__main__':
    sim_single_run()
