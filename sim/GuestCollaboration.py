from mom.Comm.GuestServerWithPort import GuestServerWithPort
from mom.Negotiators.Negotiator import Negotiator
from mom.Comm.Proposals import DECREASE_Q
import time
import threading
import logging

# Each guest's listening port will be composed of the base port and
# the guest id.
BASE_PORT = 2187

# The maximal time to strike a deal between two agents, in seconds.
# After this timeout, both the client thread and the server thread should
# finish running.
#ROUND_TIMEOUT = 12
ROUND_TIMEOUT = 1

logger = logging.getLogger("Collaboration")

def client_logic(guest_id, guests):
    '''
    @param guest_id - a number(int) representing the id of the guest.
                      It also represents the guest's index in <guests>.
    @param guests - a list of Guest entities.
    '''
    end_time = time.time() + ROUND_TIMEOUT
    while time.time() < end_time:
        proposal = guests[guest_id].policy.negotiator.get_message_to_send(COMPENSATE_SMALLER_BID)
        if proposal is None:
            continue
        receiver = guests[proposal.receiver_id]
        receiver.Prop('nego_comm').communicate(proposal.as_dictionary())
    guests[guest_id].policy.negotiator.round_end()
    logger.debug("finished client " + str(guest_id))

def server_logic(guest):
    '''
    @param guest - a Guest entity instance that has a policy and 
                   a nego_comm property. (negotiation communicator,
                   from which the ip and port will be extracted.)
    '''
    end_time = time.time() + ROUND_TIMEOUT
    host = guest.Prop('nego_comm').client.host
    port = guest.Prop('nego_comm').client.port
    server = GuestServerWithPort(guest, guest.policy, host, port)
    server.serve_forever()
    time.sleep(int(end_time-time.time()))
    server.shutdown()
    logger.debug("finished server "+ str(guest.name))
         

def collaboration(guests):
    threads_registry = []
    logger.debug("Creating Guest threads for %d guests..." % len(guests))
    for guest in guests:
        logger.debug("Client thread for guest " + guest.name)
        client_t = threading.Thread(target = client_logic, name = "Thread-" + guest.name,
                                    kwargs = {"guest_id": int(guest.name), "guests": guests})
        logger.debug("Server thread for guest " + guest.name)
        server_t = threading.Thread(target = server_logic, name = guest.name, kwargs = {"guest": guest})
        threads_registry.append(client_t)
        threads_registry.append(server_t)
        client_t.start()
        server_t.start()
    for t in threads_registry:
        t.join()
    logger.debug("Finished collaboration round.")

