import pymprog as mprog

def optimized_allocation(max_mem, max_bw, val, verbose = True):
    # val: a list of lists of arrays, each array represents a valuation function of a guest
    # for the possible memory values and bandwidth values. (memory and bandwidth have been divided into chunks).
    # mem and bw will be the number of chunks.
    mem = len(val[0])
    bw = len(val[0][0])
    mprog.beginModel()
    bidders = range(len(val))
    # list of possible memory and bandwidth assignments to a bidder
    memrange = range(mem)
    bwrange = range(bw)

    # each guest can be assigned with each memory and bandwidth quantity
    # iprod is the cartesian product
    assignments = mprog.iprod(bidders, memrange, bwrange)
    # alloc[i,m,bw] == 1 iff guest i is assigned with memrange[m] and bwrange[w]
    # this creates a group of variables that can get 1 or 0 and have names: alloc[i,m,w]
    alloc = mprog.var(assignments, "allocation", kind = int, bounds = (0, 1))
    # sum of all allocation for all guests must be less than the total memory and bandwidth
    mprog.st(sum(alloc[i, m, w] * m for i, m, w in assignments) <= max_mem,
             "limit allocation to total memory")
    mprog.st(sum(alloc[i, m, w] * w for i, m, w in assignments) <= max_bw,
         "limit allocation to total bandwidth")
    # each bidder can be assigned only with exactly 1 memory and bandwidth
    for i in bidders:
        mprog.st(sum(alloc[i, m, w] for m in memrange for w in bwrange) == 1)
    # the total valuation is the sum of the valuation that were allocated. if
    # guest i was allocated with memrange[m], from alloc[i,:] only
    # alloc[i,m] == 1, so for each guest we will sum only val[i][m][w]
    # corresponding to the allocation
    mprog.maximize(sum(val[i][m][w] * alloc[i, m, w]
                       for i, m, w in assignments), "social welfare")

    mprog.solve()

    # collect the allocation that each guest got after the solution
    ret_alloc = [[0,0] for i in bidders]
    for i in bidders:
        cur = [(m,w) for m in memrange for w in bwrange if mprog.evaluate(alloc[i, m, w]) == 1]
        assert len(cur) == 1
        ret_alloc[i][0] = cur[0][0] # memory
        ret_alloc[i][1] = cur[0][1] # bandwidth

    if verbose:
        for i in bidders:
            print 'bidder %i got: (%i/%i) memory and (%i/%i) bandwidth' % (i, ret_alloc[i][0], max_mem, ret_alloc[i][1], max_bw)
    mprog.endModel()

    return ret_alloc

if __name__ == "__main__":
    from exp.core.Loads import LoadConst
    from mom.Adviser import get_adviser
    import numpy as np
    dt = 2
    mems = range(600, 2200, 50)
    bws = range(5000, 50000, 1000)
    
    base_mem = "600"
    base_bw = "5000" # 50 Mbit
    profiler_file = "doc/profiler-fake-network-linear-mult.xml"
    profiler_entry = "hits_rate"
    load_interval = 10
    revenue_func_str = 'lambda x:x'
    adv = [0] * 11
    for i in xrange(1,11):
        load = LoadConst(i) # was 6
        adv[i] = get_adviser(name = 'AdviserProfit',
                      profiler = profiler_file,
                      entry = profiler_entry,
                      rev_func=revenue_func_str,
                      base_mem = base_mem,
                      base_bw = base_bw,
                      memory_delta = "50",
                      bw_delta = "1000")

    load_funcs = [LoadConst(i) for i in xrange(1, 11)]
    for t in range(10):
        vals1 = []
        perfs = []
        for lf in load_funcs:
            load = int(lf(t * dt))
            perf = []
            for m in mems:
                length = len(bws)
                perf.append(adv[t+1].perf([load,\
                                            m,\
                                            np.asarray(bws)]))
            perfs.append(perf)
            vals1.append(adv[t+1].V_func(perf))

    # 4480 / 200 = 22, 199500 / 5000 = 39
        print str(optimized_allocation(4480 / 50, 199500 / 1000, vals1))
