'''
Created on Dec 25, 2011

@author: eyal
'''
from exp.core.OutputProperties import POL
from simulate import simulate
from sim.simulate import Guest
from sim.SimsScan import SimsScan
import random
from exp.core import Loads
from mom import Profiler
import numpy as np
from mom.Adviser import get_adviser

n = 20
base_mem = 500
saturation_mem = 700
auction_period = 1
rounds = 10
memory_percent = 0.75
profiler_file = "/home/eyal/moc/doc/profiler-static-memcached-500mb.xml"
profiler_entry = "hits_rate"

name = "static_memcached_valuation_compare-m-%.2f" % memory_percent

def guests_generator(advisers_and_load_list):
    return [Guest(str(i), get_adviser(**adviser),
                  base_mem = adviser["base_mem"],
                  load_func = load)
            for i, (adviser, load) in enumerate(advisers_and_load_list)]

class SimValationCompare(SimsScan):
    def run(self,
            rev_func_dict,
            load_func_list,
            base_adviser_params,
            saturation_mem,
            rounds,
            show = False):

        num_guests = len(load_func_list)

        for name, revs in rev_func_dict.iteritems():

            print "simulating: %s" % (name)

            advisers = []
            for i in range(num_guests):
                adviser = base_adviser_params.copy()
                adviser['rev_func'] = revs[i]
                advisers.append(adviser)

            guests = guests_generator(zip(advisers, load_func_list))

            sim_res = simulate(guests,
                               alpha = 1.0,
                               mem_available = int(saturation_mem * num_guests * memory_percent),
                               rounds = rounds)

            self.save_to_file(sim_res, "sim-%s" % name)

def shuffeled(a):
    b = a[:]
    random.shuffle(b)
    return b

if __name__ == "__main__":
    sim = SimValationCompare(name)
    adviser_args = {"name": "AdviserProfit",
                    "profiler": profiler_file,
                    "base_mem": base_mem, "entry": profiler_entry,
                    "memory_delta": 10, "load_smooth": 0, "memory_smooth": 0,
                    "load_d": 2, "memory_d": 10}

    loads = [Loads.LoadConstant(i) for i in map(int, np.ceil(np.linspace(1, 10, n)))]
    pareto_numbers = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 4, 5, 9, 2097], float)
    assert len(pareto_numbers) == n

    numbers = np.linspace(1, 100, n)

    prf = Profiler.fromxml(profiler_file, (0, 0), (1, 10))
    norm = float(prf.original_data[profiler_entry][-1][-1])

    rev_funcs = {
                 "straight":          ["lambda x: 1000 * (x / %f)" % (norm)] * n,
                 "pareto-random":     ["lambda x: %f * (x / %f)" % (i, norm) for i in shuffeled(pareto_numbers)],
                 "pareto-increasing": ["lambda x: %f * (x / %f)" % (i, norm) for i in sorted(pareto_numbers)],
                 "pareto-decreasing": ["lambda x: %f * (x / %f)" % (i, norm) for i in sorted(pareto_numbers, reverse = True)],
                 "linear-random":     ["lambda x: %f * (x / %f)" % (i, norm) for i in shuffeled(numbers)],
                 "linear-increasing": ["lambda x: %f * (x / %f)" % (i, norm) for i in sorted(numbers)],
                 "linear-decreasing": ["lambda x: %f * (x / %f)" % (i, norm) for i in sorted(numbers, reverse = True)],
                 "x^2": ["lambda x: x ** 2"] * n,
                 "x^4": ["lambda x: x ** 4"] * n,
                 "x^6": ["lambda x: x ** 6"] * n,
                 "x^8": ["lambda x: x ** 8"] * n,
                 }

#    for name, funcs in rev_funcs.iteritems():
#        for f in funcs:
#            prf.draw_smoothing(profiler_entry, 1, 0, range(1, 11), range(base_mem, saturation_mem, 50), f)
#
#    import pylab as pl
#    pl.show()

    info = {"n": n, "base_mem": base_mem, "saturation_mem": saturation_mem,
            "auction_period":auction_period, "adviser_args": adviser_args,
            "loads": map(lambda f: f.info, loads), "rev_funcs": rev_funcs,
            "rounds": rounds}

    sim.save_to_file(info, "info")
    sim.run(rev_funcs, loads, adviser_args, saturation_mem, rounds)
