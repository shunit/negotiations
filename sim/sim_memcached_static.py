'''
Created on Jan 3, 2012

@author: eyal
'''
import pylab as pl

if __name__ == '__main__':
    loads = [25]#[10, 15, 20, 40, 60, 100]
    types = len(loads) * ['ProfitAdviser']

    smooths = [0, 2, 10]
    for smooth in smooths:
        guests = []
        for i in range(len(types)):
            guests.append(gen_memcached_guest(types[i], loads[i], 5))

        plot_guests_graphs(guests, range(400, 3000), loads)
#    pl.legend(["kern size: " + str(i) for i in smooths], loc = 'best')

#    simulate(guests, max_time = max_time, alpha = alpha,
#                           total_mem_func = lambda t: tot_mem, show = show)

    pl.show()



