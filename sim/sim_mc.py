#! /usr/bin/env python
'''
Created on Mar 25, 2012

@author: eyal
'''
from SimsScan import SimsScan
import exp.core.Loads
from sim.SimsScan import guests_generator

n = 10
base_mem = 600
rounds = 1000
Tauction = 12
Tload = 200
load_funcs = [exp.core.Loads.LoadConst(i + 1) for i in range(n)]
max_mem = n * 2000
min_mem = n * base_mem
host_mem = 500
mem_diff = 54



print "max oc: %.1f" % (float(max_mem + host_mem + n * mem_diff) / (min_mem + host_mem + n * mem_diff))

name = "sim-mc"

def doTestMemcached():
    sim = SimsScan(name)

    adviser_args = {"name": "AdviserProfit",
                    "profiler": "doc/profiler-mc-spare-50-satur-2000.xml",
                    "base_mem": base_mem, "entry": "hits_rate",
                    "memory_delta": 10, "rev_func": "lambda x:1000*x"}

    chooser = lambda i: lambda t: load_funcs[i](t)
    loads = [chooser(i) for i in range(n)]

    info = {"n": n, "base_mem": base_mem, "min_mem": min_mem, "max_mem":max_mem,
            "Tauction": Tauction, "Tload": Tload, "adviser_args": adviser_args,
            "loads": map(lambda f: f.info, load_funcs),
            "rounds": rounds, "host_mem": host_mem, "mem_diff": mem_diff}

    sim.save_to_file(info, "info")
    sim.run(guests_generator(n, adviser_args, loads, Tload),
            min_mem = min_mem,
            max_mem = max_mem,
            rounds = rounds,
            Tauction = Tauction,
            ocs = [1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 2.99],
#             alphas = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
            alphas = [1.0],
            p0 = 0,
            host_mem = host_mem,
            mem_diff = mem_diff,
            do_optimized = True,
            static_optimization = True
            )

if __name__ == '__main__':
    doTestMemcached()
