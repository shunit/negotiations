'''
Created on Mar 5, 2012

@author: eyal
'''
import os
from exp.core.OutputProperties import HOST, MON, POL
import pylab as pl
import numpy as np
from etc.plots import draw_n_save, pl_apply_defaults
from mom import Profiler
from exp.exp_output import sync_files

pl_apply_defaults()

percent = 0.5
#test_name = "memcached_valuation"
#sim_name = "memcached_valuation_compare-m-%.2f-10" % percent
#profiler_file = "/home/eyal/moc/doc/profiler-memcached-spare-50-win-100k.xml"
#profiler_field = "hits_rate"

#test_name = "static_memcached_valuation"
#sim_name = "static_memcached_valuation_compare-m-%.2f-1" % percent
#profiler_file = "/home/eyal/moc/doc/profiler-static-memcached-500mb.xml"
#profiler_field = "hits_rate"

sim_name = "mc_valuation_compare-m-%.2f-1" % percent
profiler_file = "/home/eyal/moc/doc/profiler-mc-spare-50-satur-2000.xml"
profiler_field = "hits_rate"

localdir = "/home/eyal/moc-output/sim-dshad4/" + sim_name
remotedir = "%s/moc-output/sim/%s" % (os.path.expanduser("~"), sim_name)

size = (20, 12)
t0 = 0
start = 8
end = 12

colors = ["r", "g", "b", "gray"]

pol_time = lambda lst: [t - lst[POL]['time'][0] for t in lst[POL]['time']]

def exp_plot(data, funcs, labels, markers, linestyles, _ymax = None, _ymin = None):
    ret_y = {}
    guests = sorted(set(data.keys()).difference(set((HOST,))))
    n = len(guests) + 1
    sp = 1
    maxy = [0] * len(funcs)
    axss = []
    lines = []

    # draw total load in first subplot
    min_size = 10000000
    _xmin = 100000000
    _xmax = -1
    for vm in guests:
        min_size = min(min_size, len(data[vm][POL]["load"]))
    sum_load = np.zeros(min_size)
    for vm in guests:
        sum_load += np.array(data[vm][POL]['load'][:min_size])

    for vm in guests:
        lines = []
        pl.subplot(n - 1, 1, sp)
        axs = [pl.gca()]
        for i in range(len(funcs) - 1):
            axs.append(pl.twinx(axs[0]))
        axss.append(axs)
        axl = pl.twinx(axs[0])
        try:
            y = []
            x = []
            for i in range(len(funcs)):
                xfunc, yfunc = funcs[i]
                xi = xfunc(data[vm])
                yi = yfunc(data[vm])
                xi = xi[-len(yi):]
                _xmin = min(_xmin, xi[0])
                _xmax = max(_xmax, xi[-1])
                maxy[i] = max(maxy[i], max(yi))
                x.append(xi)
                y.append(yi)
            ret_y.setdefault("test", []).extend(y[0])
        except Exception as ex:
            continue

        for i in range(len(y)):
#             l = axs[i].plot(x[i], y[i], markersize = 4, alpha = 0.5,
             l = axs[i].plot(x[i], y[i], markersize = 4, alpha = 1.0,
                            linestyle = linestyles[i],
                            marker = markers[i],
                            c = colors[i],
                            label = "%s-%i" % (labels[i], sp))
             lines.append(l[0])

        try:
            axl.plot(pol_time(data[vm]),
                     data[vm][POL]['load'], linestyle = "--",
                     color = "k", label = "load-%i" % sp)
        except KeyError:
            pass
        axl.set_ylim((0, 20))
        axs[0].set_ylabel(vm)

        axs[0].grid(True)

        if sp != n - 1:
            for ax in axs:
                ax.get_xaxis().set_ticklabels([])
        sp += 1

    if len(maxy) > 2:
        y2max = max(maxy[1:])
        for i in range(1, len(maxy)):
            maxy[i] = y2max

    for axs in axss:
        for i in range(len(funcs)):
            axs[i].set_ylim((0 if _ymin is None else _ymin,
                             (maxy[i] * 1.1) if _ymax is None else _ymax))
            axs[i].set_xlim((_xmin, _xmax))

    pl.legend(lines, [l.get_label() for l in lines])
    try:
        axss[-1][0].set_xlabel("round")
    except IndexError:pass
    return ret_y

def draw_exp(name, data):
    read_dir = localdir
    results_file = "sim-%s" % name
    write_dir = os.path.join(read_dir, "plots")
    if not os.path.exists(write_dir):
        os.mkdir(write_dir)

    for p in ["auction_mem", "ties"]:  # , 'p_in_min_mem', 'p_out_max_mem']:
        pl.clf()
        pl.plot([x - data[HOST][POL]["time"][0] for x in data[HOST][POL]["time"]],
                data[HOST][POL][p], color = "g",
                marker = "None", alpha = 0.5, label = "bla", linestyle = "-")
        pl.legend(loc = "best")
        pl.grid(True)
        pl.ylim((0, pl.ylim()[1]))
        draw_n_save(os.path.join(write_dir, "host-%s-%s.png" % (p, name)), size)

    pl.clf()
    exp_plot(data,
             [(pol_time, lambda lst: lst[POL]['control_mem']),
              (pol_time, lambda lst: lst[POL]['perf'])],
             ["memory", "throughput"], ["None", "None"], ["--", "-"])
    draw_n_save(os.path.join(write_dir, "mem-perf-%s.png" % (name)), size)

    # memory view
    pl.clf()
    exp_plot(data,
             [(pol_time, lambda lst: lst[POL]['control_mem'])],
             ["memory"], ["None"], ["-"])
    draw_n_save(os.path.join(write_dir, "memory-%s.png" % (name)), size)

    # bills
    pl.clf()
    exp_plot(data,
             [(pol_time, lambda lst: lst[POL]['control_bill'])],
             ["bill"], ["o"], ["-"])
    draw_n_save(os.path.join(write_dir, "bill-%s.png" % (name)), size)

    pl.clf()
    exp_plot(data,
             ((pol_time, lambda lst: lst[POL]['p']),
              (pol_time, lambda lst: [x[0][0] for x in lst[POL]['rq']]),
              (pol_time, lambda lst: [x[0][1] for x in lst[POL]['rq']])),
             ["p", "r", "q"], ["None", "^", "v"], ["-"] * 3)
    draw_n_save(os.path.join(write_dir, "prq-%s.png" % (name)), size)


def draw_sim(data_dict, info):
    rev = {}
    perf = {}
    profit = {}

    prf = Profiler.fromxml(profiler_file, profiler_field)
    perf_func = np.vectorize(lambda l, m: prf.interpolate((l, m)))

    for name, data in data_dict.iteritems():
        vms = map(str, sorted([int(g) for g in data.keys() if g != HOST]))

        # calculate static-divided performance:

        memory = np.array(data[HOST][POL]['auction_mem'])
        for g in vms:
            memory += np.array(data[g][POL]['mem_base'])
        guest_mem = memory / len(vms)
        divided_perf = 0
        divided_rev = 0

        funcs = map(eval, info['rev_funcs'][name])
        for g in vms:
            rev_func = np.vectorize(funcs[int(g)])
            load = np.array(data[g][POL]['load'])
            p = perf_func(load, guest_mem)
            r = rev_func(p)

            divided_perf += np.sum(p)
            divided_rev += np.sum(r)

        # get the revenue and profit of the guest:
        rev[name] = sum([sum(data[g][POL]["rev"]) for g in vms]) / divided_rev
        perf[name] = sum([sum(data[g][POL]["perf"]) for g in vms]) / divided_perf
        profit[name] = sum([sum(data[g][POL]["profit"]) for g in vms])

    tests = sorted(rev.keys(), key = lambda name: rev[name])

    pl.gcf()

    ax = pl.gca()
    for i, name in enumerate(tests):
#         ax.bar(i - 0.2, rev[name], 0.4, 0, color = 'b', alpha = 0.5, linewidth = 0)
        ax.bar(i - 0.2, rev[name], 0.4, 0, color = 'b', alpha = 1.0, linewidth = 0)
        ax.text(i, min(rev[name] + 0.05, 9.5), "%.2f" % (rev[name]),
                fontsize = 6,
                horizontalalignment = 'center',
                backgroundcolor = "white")

    ax.plot([-0.5, len(tests) - 0.5], [1, 1], color = 'r')
    pl.xlim([-0.5, len(tests) - 0.5])
    pl.ylim([0, min(10, pl.ylim()[1])])
    pl.xticks(range(len(tests)))
    pl.gca().set_xticklabels([t.replace("-", "\n") for t in tests])
    ax.grid(True, axis = "y")

    draw_n_save(os.path.join(localdir, "valuation-%s-%.2f.eps" % (sim_name, percent)), (10, 3))

    pl.clf()
    ax = pl.gca()

    for i, name in enumerate(tests):
#         ax.bar(i - 0.2, perf[name], 0.4, 0, color = 'b', alpha = 0.5)
        ax.bar(i - 0.2, perf[name], 0.4, 0, color = 'b', alpha = 1.0)

    pl.xlim([-0.5, len(tests) - 0.5])
    pl.gca().set_xticks(range(len(tests)))
    pl.gca().set_xticklabels([t.replace("-", "\n") for t in tests])
    pl.legend([pl.bar(0, 0, 0, 0, color = 'b')[0]], ['Performance'], loc = "best")
    ax.set_ylabel("Hits")
    ax.grid(True, axis = "y")
    draw_n_save(os.path.join(localdir, "performance-%s-%.2f.eps" % (sim_name, percent)), (10, 3))

if __name__ == '__main__':

    if not os.path.exists(localdir): os.mkdir(localdir)
    sync_files(remotedir, localdir, [])

    data = {}

    for f in os.listdir(localdir):
        if f.endswith("png") or not f.startswith("sim-"):
            continue
        d = eval(file(os.path.join(localdir, f), "r").readline())
        name = f[f.find("-") + 1:]
        data[name] = d

    info = eval(file(os.path.join(localdir, 'info'), "r").readline())

    draw_sim(data, info)

    for name, d in data.iteritems():
        draw_exp(name, d)

    data = {}

    for f in os.listdir(localdir):
        if f.endswith("png") or not f.startswith("sim-"):
            continue
        d = eval(file(os.path.join(localdir, f), "r").readline())
        name = f[f.find("-") + 1:]
        data[name] = d

    info = eval(file(os.path.join(localdir, 'info'), "r").readline())

    draw_sim(data, info)

    for name, d in data.iteritems():
        draw_exp(name, d)
