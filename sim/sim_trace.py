'''
Created on Apr 7, 2013

@author: eyal
'''

from numpy import array
import numpy as np
import pylab as pl

vm_num = 13
vm_show = 4

fields = ['load', 'last_won_p', 'control_bill', 'control_mem', 'perf', 'r', 'q',
          'profit', 'rev', 'p', 'mem_base', 'time', 'p_target_mem', 'adviser_time']

path = "/home/eyal/moc-output/exp-dshad4/mcd-10G-0/"
sim_data = eval("".join(file(path + "ginseng_results.txt")))
exp_time = eval("".join(file(path + "time")))
exp_load = eval("".join(file(path + "load")))
exp_perf = eval("".join(file(path + "perf")))
exp_val = eval("".join(file(path + "val")))

field = ['perf'] * 2

if __name__ == '__main__':


    vms = ["vm-%i" % (i + 1) for i in range(vm_num)]  #[v for v in data[vm_num] if v != HOST]

    fig, axs = pl.subplots(nrows = vm_show, ncols = 1, sharex = True, sharey = True)
    twins = []

    for i in range(vm_show):
        d = sim_data[vm_num][vms[i]]
        l1 = axs[i].plot(np.array(d["time"]) / 60., np.array(d[field[0]]),
                         color = 'b', drawstyle = "steps-post", linewidth = 3)[0]
        axs[i].set_ylabel(field[0])
        ax = axs[i].twinx()
        twins.append(ax)
        l2 = ax.plot(
                     #np.array(d["time"]) / 60., np.array(d[field[1]]),
                     np.array(exp_time[vm_num][vms[i]]) / 60., np.array(exp_perf[vm_num][vms[i]]),
                     color = 'r', drawstyle = "steps-post", linewidth = 3)[0]
        ax.set_ylabel(field[1])
    pl.legend([l1, l2], field, loc = 'lower right')

    maxy = max([ax.get_ylim()[1] for ax in twins])
    miny = min([ax.get_ylim()[0] for ax in twins])
    for ax in twins:
        ax.set_ylim((miny, maxy))
#        ax.set_ylim(axs[0].get_ylim())

    pl.show()
