#! /usr/bin/env python
'''
Created on Mar 25, 2012

@author: eyal
'''
import numpy as np
import exp.core.Loads
from sim.SimsScanP0 import guests_generator
from sim.SimsScanP0 import SimsScanP0

# number of guests
number_of_guests = 10
# base mem for all the guests. This value is taken from the minimum memory used by the profiler.
base_mem = 600
# base bandwidth for all the guests. This value is taken from the minimum bandwidth used by the profiler.
base_bw = 5000 #Kbit 
# number of auction rounds to be simulated (usually per oc, here there is only one oc)
#rounds = 1000
rounds = 10
# time for a single memory auction round
Tauction_mem = 12
# time for a single bandwidth auction round
Tauction_bw  = 2
#length of a benchmark round in seconds
Tload = 10
#
sature_dict_mem = dict((i, 2000) for i in range(number_of_guests + 1))
# maximum memory for auction
max_mem = sum([sature_dict_mem[i + 1] for i in range(number_of_guests)])
# minimum memory for auction
min_mem = number_of_guests * base_mem
#
sature_dict_bw = dict((i, 50000) for i in range(number_of_guests + 1))
# maximum bandwidth for auction
max_bw = sum([sature_dict_bw[i + 1] for i in range(number_of_guests)])
# minimum bandwidth for auction
min_bw = number_of_guests * base_bw
# memory that the host keeps for himself. taken into account when calculating the memory for sale
host_mem = 500
# bandwidth that the host keeps for himself. taken into account when calculating the bandwidth for sale
host_bw = 1000    
# difference between allocation and what the guest actually sees
mem_diff = 54
# the bandwidth available for auction
# auction_bw = 100 * 1000 - host_bw # 100Mbit - host_bw
# 
max_Tauction = max(Tauction_bw, Tauction_mem)
# The points in time when data is written to the logs. 
logging_times = range(max_Tauction, 10 * max_Tauction, max_Tauction)  # T = T_load / T_auction (= T_load)

print "max oc: %.1f" % (float(max_mem + host_mem + number_of_guests * mem_diff) / (min_mem + host_mem + number_of_guests * mem_diff))

name = "sim-p0-bilinear-mcd-bw"

def doTestMemcached():
    sim = SimsScanP0(name)

    adviser_args = {"name": "AdviserProfit",
                    "profiler": "doc/profiler-fake-network-linear-mult.xml",
                    "entry": "hits_rate", "rev_func": "lambda x:1000*x"}

    for T in logging_times:
        #functions to represent the load for each guest
        load_funcs = []
        for i in range(number_of_guests / 2):
            load_funcs.append(exp.core.Loads.LoadBinary(i + 1, number_of_guests - i, T, 0))
            load_funcs.append(exp.core.Loads.LoadBinary(number_of_guests - i, i + 1, T, 0))

        chooser = lambda i: lambda t: load_funcs[i](t)
        loads = [chooser(i) for i in range(number_of_guests)]

        info = {"number_of_guests": number_of_guests, "base_mem": base_mem, "min_mem": min_mem, "max_mem":max_mem,
                "Tauction_mem": Tauction_mem, "Tauction_bw": Tauction_bw, "Tload": Tload,
                "adviser_args": adviser_args, "loads": map(lambda f: f.info, load_funcs),
                "rounds": rounds, "host_mem": host_mem, "mem_diff": mem_diff, "min_bw" : min_bw, "max_bw" : max_bw}

        sim.save_to_file(info, "info-T=%i" % T)

        res = sim.run(
                guests_generator(number_of_guests, adviser_args, loads, Tload),
                min_mem = min_mem,
                max_mem = max_mem,
                rounds = rounds,
                Tauction_mem = Tauction_mem,
                Tauction_bw = Tauction_bw,
                ocs = [2],
                alpha = 1.0,
                p0s = [0],
                host_mem = host_mem,
                host_bw  = host_bw,
                mem_diff = mem_diff,
                min_bw = min_bw,
                max_bw = max_bw,
                #optimize = False
                )
        sim.save_to_file(res[0][0], "sim-T-%02d" % (T))

if __name__ == '__main__':
    doTestMemcached()

