#! /usr/bin/env python
'''
Created on Mar 25, 2012

@author: eyal
'''
from SimsScan import SimsScan
import exp.core.Loads
from sim.SimsScan import guests_generator
from sim.SimResultsProcess import get_tools
import numpy as np
from sim.sim_mcd import sat_dict
from mom.Plotter import HOST

oc = 1.5
name = "dynamic-mcd-oc-%.2f" % oc

n = 10
base_mem = 600
host_mem = 500
mem_diff = 54
rounds = 1000
Tload = 200
Tauction = 12
# alphas = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
alphas = [1.0]
Ts = range(1, 10) + range(10, 50, 5) + range(50, 201, 50)  # T = T_load / T_auction (= T_load)

do_not_measure = ("poa", "waste")

adviser_args = {"name": "AdviserProfit",
                "profiler": "doc/profiler-memcached-inside-spare-50-win-500k.xml",
                "base_mem": base_mem, "entry": "hits_rate",
                "memory_delta": 10, "rev_func": "lambda x:x"}

if __name__ == "__main__":
    sim = SimsScan(name)
    best_a = []
    results = dict((name, []) for name in get_tools().keys())
    for name in do_not_measure:
        del results[name]
    for T in Ts:
        print "step T:", T

        assert n <= 10
        load_funcs = []
        max_mem = 0
        for i in range(n / 2):
            load_funcs.append(exp.core.Loads.LoadBinary(i + 1, 10 - i, T, 0))
            load_funcs.append(exp.core.Loads.LoadBinary(10 - i, i + 1, T, 0))
            max_mem += sat_dict[i + 1] + sat_dict[10 - i]

        max_mem *= 1024

        min_mem = n * base_mem

        print "max oc: %.1f" % (float(max_mem + host_mem + n * mem_diff) / (min_mem + host_mem + n * mem_diff))

        chooser = lambda i: lambda t: load_funcs[i](t)
        loads = [chooser(i) for i in range(n)]

        info = {"n": n, "base_mem": base_mem, "min_mem": min_mem, "max_mem":max_mem,
                "adviser_args": adviser_args, "loads": map(lambda f: f.info, load_funcs),
                "rounds": rounds}

        sim.save_to_file(info, "info-T=%i" % T)
        res = sim.run(guests_generator(n, adviser_args, loads, Tload),
                      min_mem = min_mem,
                      max_mem = max_mem,
                      rounds = rounds,
                      Tauction = Tauction,
                      ocs = [oc],
                      alphas = alphas,
                      host_mem = host_mem,
                      mem_diff = mem_diff,
                      do_optimized = True,
                      static_optimization = False
                      )
        for lst in results.values():
            lst.append([])
        for i, a in enumerate(alphas):
            data = res[0][i]

            tools = get_tools()
            for name in do_not_measure:
                del tools[name]

            for k, v in data.iteritems():
                for tool in tools.itervalues():
                    if k == HOST:
                        tool.process_host(v)
                    else:
                        tool.process_guest(v)
            for name, tool in tools.iteritems():
                results[name][-1].append(tool.finish())

    results.update({
          "alphas": alphas,
          "Ts": Ts,
          "n": n,
          "base_mem": base_mem,
          "rounds": rounds,
          })
    sim.save_to_file(results, "all-results")

