'''
Created on Dec 23, 2011

@author: eyal
'''
import pylab as pl
import os
# from exp.deprecated.OutputViews import SplitPlotView
from etc.plots import nice_surf, draw_n_save, pl_apply_defaults

sim_dir = "../../moc-output/sim-dshad4/memcached/gen_random_sine_const_sum-2012-05-12-14:33:33"

def plot_sims(res, sim_dir):
    pl_apply_defaults()
    out_dir = os.path.join(sim_dir, "plots")
    try: os.mkdir(out_dir)
    except OSError: pass

    alpha = res['alpha']
    memratio = res['memratio']

    for measure in set(res.keys()).difference(set(['alpha', 'memratio'])):
        z = res[measure]
        nice_surf(alpha, memratio, z, z_label = measure)
    #    pl.suptitle(("\n%s.png" % (measure)).capitalize())
        pl.xlabel("reclaim factor")
        pl.ylabel("% max-memory")
        draw_n_save(os.path.join(out_dir, "sim-%s.eps.png" % measure), (5, 4))
    #for measure in ['waste-normalized']:
    #    pl.clf()
    #    alpha_errorbar(alpha, res[measure], measure, "(avg +/- stdev)")
    #    draw_n_save(os.path.join(out_dir,
    #                             "alpha-%s.png.png" % measure))

if __name__ == "__main__":
    res = eval(file(os.path.join(sim_dir, "results"), "r").readline().strip())
    plot_sims(res, sim_dir)


#def get_overall(res, measure):
#    overall_data = []
#    for a in range(len(alpha)):
#        overall_data.append([])
#        for test in tests:
#            overall_data[-1].extend(pl.transpose(res[test][measure])[a])
#
#    overall_data = pl.transpose(overall_data)
#    return overall_data

#for measure in ['waste-normalized']:
#    pl.clf()
#    alpha_errorbar(alpha, get_overall(res, measure),
#                   measure, "Overall Advisers (avg +/- stdev)")
#    draw_n_save(os.path.join(output_dir, subdir,
#                             "alpha-overall-%s" % (measure)))
#
## draw overall bill min bill max graph
#pl.clf()
#bill_min = get_overall(res, 'bill-min')
#bill_max = get_overall(res, 'bill-max')
#alpha_errorbar(alpha, bill_max, linestyle = 'b', fillcolor = 'blue')
#pl.hold(True)
#alpha_errorbar(alpha, bill_min, linestyle = 'r', fillcolor = 'red')
#pl.ylabel('Bill')
#pl.title('Overall Advisers Bill Limits Distribution')
#pl.legend(['Maximum', 'Minimum'])
#pl.hold(False)
#draw_n_save(os.path.join(output_dir, subdir,
#                         "alpha-overall-bill-min-bill-max"))

# draw diff functions
#for name1, name2 in comperisons:
#    diff = {}
#    for key in set(results[tests[name1]].keys()).difference(set(['alpha', 'memratio', 'utility-total', 'utility-min', 'utility-max', 'utility-sum-min'])):
#        diff[key] = pl.np.array(results[tests[name1]][key]) - pl.np.array(results[tests[name2]][key])
#
#    alpha = results[tests[name1]]['alpha']
#    memratio = results[tests[name1]]['memratio']
#
#    for measure in diff.keys():
#        alpha_memrat_surf(alpha, memratio, diff[measure],
#                          "\n%s difference of\n(%s) - (%s)" % (measure, tests[name1], tests[name2]))
#        draw_n_save(os.path.join(output_dir, subdir,
#                                 "diff-%s-%s-%s" % (tests[name1], tests[name2], measure)))
