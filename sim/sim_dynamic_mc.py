#! /usr/bin/env python
'''
Created on Mar 25, 2012

@author: eyal
'''
from SimsScan import SimsScan
import exp.Loads
from sim.SimsScan import guests_generator
from sim.SimResultsProcess import get_tools
import numpy as np
from mom.Plotter import HOST
from sim.sim_dynamic_mcd import alphas
import time

# number of guests
number_of_guests = 10
# over commitment factor - TODO: find out how exactly that works
#probably: (max_memory-min_memory)/oc = memory to sell
oc = 2
#output file name
simulation_name = "dynamic_mc-oc-%.2f_" % oc +  time.strftime("%Y-%m-%d-%X")
#probably used to calculate max_mem. max_mem=sum(sature_dict)
sature_dict = dict((i, 2000) for i in range(11))
# base mem for all the guests. TODO: enable different base mems (make it a dict like sature_dict)
base_mem = 600
# number of auction rounds to be simulated (usually per oc, here there is only one oc)
#rounds = 1000
rounds = 10
# alphas - depracated, now always equals 1. 
# TODO maybe use this mechanism for affine maximizer
# alphas = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
alphas = [1.0]
# The points in time when data is written to the logs. 
logging_times = range(1, 10) + range(10, 50, 5) + range(50, 301, 50)  # T = T_load / T_auction (= T_load)
# difference between allocation and what the guest actually sees
mem_diff = 54
# memory that the host keeps for himself. taken into account when calculating the memory for sale
host_mem = 500
# time for a single auction round
Tauction = 12
#length of a benchmark round in seconds
Tload = 10

# Collaboration Predicate: in which rounds will collaboration happen.
def should_collaborate(round_number):
    #Collaboration will only happen in round 2.
    return (round_number == 2)

adviser_args = {"name": "AdviserProfit",
                "profiler": "doc/profiler-mc-spare-50-satur-2000.xml",
                "base_mem": base_mem, "entry": "hits_rate",
                "memory_delta": 10, "rev_func": "lambda x:1000*x"}

if __name__ == "__main__":
    sim = SimsScan(simulation_name)
    best_a = []
    results = dict((name, []) for name in get_tools().keys())
    for T in logging_times:
        print "step T:", T

        assert number_of_guests <= 10
        load_funcs = []
        max_mem = 0
        for i in range(number_of_guests / 2):
            load_funcs.append(exp.Loads.LoadBinary(i + 1, 10 - i, T, 0))
            load_funcs.append(exp.Loads.LoadBinary(10 - i, i + 1, T, 0))
        max_mem = sum(sature_dict.values())
        min_mem = number_of_guests * base_mem
        chooser = lambda i: lambda t: load_funcs[i](t)
        loads = [chooser(i) for i in range(number_of_guests)]

        info = {"number_of_guests": number_of_guests, "base_mem": base_mem, "min_mem": min_mem, 
                "max_mem":max_mem, "adviser_args": adviser_args, 
                "loads": map(lambda f: f.info, load_funcs),
                "rounds": rounds}

        sim.save_to_file(info, "info-T=%i" % T)
        res = sim.run(guests_generator(number_of_guests, adviser_args, loads, Tload),
                      min_mem = min_mem,
                      max_mem = max_mem,
                      rounds = rounds,
                      Tauction_mem = Tauction,
                      Tauction_bw = 0, #currently unused
                      ocs = [oc],
                      alphas = alphas,
                      host_mem = host_mem,
                      mem_diff = mem_diff,
                      p0 = 0,
                      collaborate_pred = should_collaborate)
        for lst in results.values():
            lst.append([])
        for i, a in enumerate(alphas):
            data = res[0][i]

            tools = get_tools()
            for k, v in data.iteritems():
                for tool in tools.itervalues():
                    if k == HOST:
                        tool.process_host(v)
                    else:
                        tool.process_guest(v)
            for name, tool in tools.iteritems():
                results[name][-1].append(tool.finish())

    results.update({
          "alphas": alphas,
          "Logging Times": logging_times,
          "number of guests": number_of_guests,
          "base_mem": base_mem,
          "rounds": rounds,
          })
    sim.save_to_file(results, "all-results")

