'''
Created on Dec 25, 2011

@author: eyal
'''
import os
from simulate import simulate
from sim.simulate import Guest
from mom.Adviser import get_adviser
from threading import Thread

def confirm_moc_path():
    if all((d != "moc" for d in os.getcwd().split("/"))):
        raise OSError("Bad startup location, must be in moc dir")
    while os.path.split(os.getcwd())[1] != "moc":
        os.chdir("..")
    return os.getcwd()

def guests_generator(n, adviser_args, load_funcs, load_interval):
    return lambda: [Guest(str(i), get_adviser(**adviser_args),
                          base_mem = adviser_args["base_mem"],
                          base_bw = adviser_args["base_bw"],
                          load_func = load_funcs[i],
                          load_interval = load_interval)
            for i in range(n)]

class SimsScanP0:
    def __init__(self, name):
        self._name = name
        self.__dir = None

    def get_test_dir(self):
        moc_dir = confirm_moc_path()
        out_dir = os.path.join(os.path.split(moc_dir)[0], "moc-output", "sim")
        if self.__dir is None:
            dr = os.path.join(out_dir, self._name + "-")
            i = 0
            while True:
                if not os.path.exists(dr + str(i)):
                    dr += str(i)
                    break
                i += 1
            os.mkdir(dr)
            self.__dir = dr

        return self.__dir

    def draw_n_save(self, name, size = (10, 16)):
        from etc.plots import draw_n_save
        draw_n_save(os.path.join(self.get_test_dir(), name + ".png"), size)

    def save_to_file(self, obj, name):
        fd = open(os.path.join(self.get_test_dir(), name), "w")
        fd.write(str(obj))
        fd.close()
    '''
    def show_perf_func(self, func, tot_mem, d_mem):
        import pylab as pl
        pl.clf()
        pl.hold(True)
        for i in range(len(func)):
            x = []
            y = []
            for mem in range(0, tot_mem + d_mem, d_mem):
                x.append(mem)
                y.append(func[i](mem))

            pl.plot(x, y, label = 'guest %i' % i)
        pl.legend(loc = 'lower right')
        pl.xlabel("Memory")
        pl.ylabel("Performance")
        pl.grid(True)
        pl.hold(False)
        pl.show()
    '''
    def run(self, guests_generator,
            min_mem, max_mem,
            rounds, Tauction_mem, Tauction_bw,
            ocs, alpha, p0s,
            host_mem, host_bw, mem_diff,
            min_bw, max_bw,
            optimize = (False, 10, 1000),
            show = False):

        """
        @param optimize: use False for not doing optimization, else give a
            tuple, corresponding to a boolean that determines if the simulation is static, memory granularity, and bandwidth(to spare time)
        """
        guests = guests_generator()
        n = len(guests)

        res = []
        for oc in ocs:  # MEMORY LOOP
            res.append([])

            wasted_mem = host_mem + n * mem_diff
            memory = int(float(wasted_mem + max_mem) / oc) - wasted_mem
            if memory < min_mem:
                print "memory %i, minimum %i" % (memory, min_mem)
                return []


            wasted_bw = host_bw
            bw = int(float(wasted_bw + max_bw) / oc) - wasted_bw
            if bw < min_bw:
                print "bandwidth %i, minimum %i" % (bw, min_bw)
                return []

            print 'Running with %d memory and %d bandwidth for auction' % (memory, bw)
            # Calculate allocations without going into the auction,
            # instead all is done via simple Integer Programming.
            # TODO: this isn't functioning for multi-resource - should we make this code flow obsolete?
            if optimize != False:
                from sim_optimized import sim_optimize
                opt_sim_res = sim_optimize(guests = guests,
                                           rounds = rounds,
                                           total_mem = memory,
                                           total_bw  = bw,
                                           min_vm_mem = 600,
                                           max_vm_mem = 2200,
                                           min_vm_bw = 5000,
                                           max_vm_bw = 50000,
                                           d_mem = guests[0].adviser.d_mem,
                                           d_bw  = guests[0].adviser.d_bw,
                                           dt = 0 if optimize[0] else min(Tauction_mem, Tauction_bw)
                                           )
                self.save_to_file(opt_sim_res, "opt-oc-%.2f" % oc)

            for p0 in p0s:  # P0 LOOP

                print "step: oc: %.2f, p0: %.2f" % (oc, p0)
                guests = guests_generator()

                sim_res = simulate(guests,
                                   alpha = alpha,
                                   p0 = p0,
                                   mem_available = memory,
                                   bw_available = bw,
                                   rounds = rounds,
                                   Tauction_mem = Tauction_mem,
                                   Tauction_bw = Tauction_bw)

                # add optimization results
                if optimize:
                    assert not sim_res.has_key('opt')
                    sim_res['opt'] = {}
                    for name, opt_res in opt_sim_res.items():
                        sim_res['opt'][name] = {}
                        for key, val in opt_res.items():
                            sim_res['opt'][name][key] = val

                self.save_to_file(sim_res, "sim-oc-%.2f-p0-%.2f" % (oc, p0))

                res[-1].append(sim_res)

            # END ALPHA LOOP
        # END MEMORY LOOP

        return res

