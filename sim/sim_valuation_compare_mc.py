'''
Created on Dec 25, 2011

@author: eyal
'''
from exp.core import Loads
from mom import Profiler
import numpy as np
from sim.sim_valuation_compare import SimValationCompare, shuffeled
from sim.pareto import pareto_distributed_numbers, \
    bounded_pareto_distributed_numbers
import os

n = 20
base_mem = 800
saturation_mem = 2000
auction_period = 1
rounds = 10
memory_percent = 0.5
profiler_file = "%s/workspace/moc/doc/profiler-mc-spare-50-satur-2000.xml" % os.path.expanduser("~")
profiler_entry = "hits_rate"

name = "mc_valuation_compare-m-%.2f" % memory_percent

if __name__ == "__main__":
    sim = SimValationCompare(name)
    adviser_args = {"name": "AdviserProfit",
                    "profiler": profiler_file,
                    "base_mem": base_mem, "entry": profiler_entry,
                    "memory_delta": 10}

    loads = [Loads.LoadConstant(10) for i in map(int, np.ceil(np.linspace(1, 10, n)))]
    pareto_numbers = bounded_pareto_distributed_numbers(1, 10000, 1.36, n)
    pareto_numbers = 10 * np.array(pareto_numbers, float)

    numbers = 10 * np.linspace(1, 1000, n)

    assert len(pareto_numbers) == n

    rev_funcs = {
             "straight":          ["lambda x: 100000 * x"] * n,
             "pareto-random":     ["lambda x: %f * x" % (i) for i in shuffeled(pareto_numbers)],
             "pareto-increasing": ["lambda x: %f * x" % (i) for i in sorted(pareto_numbers)],
             "pareto-decreasing": ["lambda x: %f * x" % (i) for i in sorted(pareto_numbers, reverse = True)],
             "linear-random":     ["lambda x: %f * x" % (i) for i in shuffeled(numbers)],
             "linear-increasing": ["lambda x: %f * x" % (i) for i in sorted(numbers)],
             "linear-decreasing": ["lambda x: %f * x" % (i) for i in sorted(numbers, reverse = True)],
             "x^2": ["lambda x: x ** 2"] * n,
             "x^4": ["lambda x: x ** 4"] * n,
             "x^6": ["lambda x: x ** 6"] * n,
             "x^8": ["lambda x: x ** 8"] * n,
             }

    info = {"n": n, "base_mem": base_mem, "saturation_mem": saturation_mem,
            "auction_period":auction_period, "adviser_args": adviser_args,
            "loads": map(lambda f: f.info, loads), "rev_funcs": rev_funcs,
            "rounds": rounds}

    sim.save_to_file(info, "info")
    sim.run(rev_func_dict = rev_funcs,
            load_func_list = loads,
            base_adviser_params = adviser_args,
            rounds = rounds,
            sum_saturation = saturation_mem * n)
