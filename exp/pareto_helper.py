# Sample n values from pareto CDF.
import argparse
import numpy as np

def reverse_pareto_cdf(y, alpha):
  return 1.0/((1-y)**(1/alpha))

def evenly_sample_from_pareto_cdf(alpha=1.36, n=10):
  y_values = [i*1.0/n+0.5/n for i in range(n)]
  x_values = [reverse_pareto_cdf(y,alpha) for y in y_values]
  return x_values

def randomly_sample_from_pareto(alpha=1.36, n=10):
  return [1 + np.random.pareto(alpha) for i in range(n)]


def randomly_sample_from_lognormal(gibrat=2.68, n=10):
  sigma = 1/((2*(gibrat**2))**0.5)
  return [1 + np.random.lognormal(mean=0, sigma = sigma) for i in range(n)]

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Sample n values from pareto CDF.')
  parser.add_argument("n", type=int, default=10, help='How many values to sample. Default is 10.')
  parser.add_argument("alpha", type=float, default=1.36, help='The alpha paramter of the pareto distribution. Default is 1.36.')
  parser.add_argument("--random", action='store_true', help='Sample randomly (other option is sample evenly)')
  parser.add_argument("--rep", type=int, default=1, help='How many times to repeat the sampling. Default is 1.')

  args = parser.parse_args()
  if args.random:
    print "Randomize %i times" % args.rep
    for i in range(args.rep):
        vals = sorted(randomly_sample_from_pareto(args.alpha, args.n), reverse=True)
        print "[" + ", ".join(["%f" % i for i in vals]) + "],"
  else:
    print "Evenize"
    print sorted(evenly_sample_from_pareto_cdf(args.alpha, args.n), reverse=True)
  
