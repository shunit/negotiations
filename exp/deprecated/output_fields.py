'''
Created on Mar 5, 2012

@author: eyal
'''
import types
from exp.core.OutputProperties import diff_from_min, MonitorProp, PolicyProp, nrmlz

hfields = {"pol-time"    : PolicyProp('time', func = diff_from_min),
           "auc-mem" : PolicyProp('auction_mem', 'auc-mem'),
           'results_round_mem' : PolicyProp('results_round_mem', 'round-res'),
           'auc-round' : PolicyProp('auction_round', 'round', tp = types.IntType),
           'ties' : PolicyProp('ties', 'not_tie', func = lambda y: [100 if yi else 0 for yi in y]),

           'mon-time' : MonitorProp('time', func = diff_from_min),
           'mem_avail' : MonitorProp('mem_available', 'host mem'),
           'mem_free' : MonitorProp('mem_free', 'mem free'),
           'swap_out': MonitorProp('swap_out', 'swap out'),
           'swap_in' : MonitorProp('swap_in', 'swap in'),
           'anon_pages' : MonitorProp('anon_pages', 'anon pages', tp = types.IntType),
           'mem_unused' : MonitorProp('mem_unused', 'unused mem', tp = types.IntType),
           }

gfields = {'pol-time' : PolicyProp('time', func = diff_from_min),
           'rq' : PolicyProp('rq'),
           'p' : PolicyProp('p'),
           'mem_base' : PolicyProp('mem_base', 'base mem'),
           'control-mem' : PolicyProp('control_mem'),
           'control-bill' : PolicyProp('control_bill', 'bill'),
           'profit' : PolicyProp('profit'),
           'mem_extra': PolicyProp('mem_extra', 'mem extra'),
           'mem-curr' : PolicyProp('curr_mem', 'mem curr'),
           'mem-without' : PolicyProp('mem_without', 'mem without'),
           'mem-free' : PolicyProp('mem_free', 'mem free'),

           "p_min_low": PolicyProp('p_min_low'),
           "p_min_up": PolicyProp('p_min_up'),
           "choose": PolicyProp('choose', func = lambda y: [(yi * 10) for yi in y]),
           "load": PolicyProp('load', 'load'),
           "pss": PolicyProp('pss'),
           "last_won_p": PolicyProp('last_won_p'),
           "cost_prediction": PolicyProp('cost_prediction'),

           "perf": MonitorProp('perf', 'perf', func = lambda y: [yi['throughput'] if yi != 0 else 0 for yi in y]),
           'mon-time' : MonitorProp('time', func = diff_from_min),
           'mem-avail' : MonitorProp('mem_available', 'mem'),
           'mem-free' : MonitorProp('mem_free', 'mem free'),
           'app-perf' : MonitorProp('app_perf', 'app perf', nrmlz(700)),
           'app-load' : MonitorProp('load', 'load'),
           'app-rev' : MonitorProp('app_rev', 'app rev'),
           'swap-out': MonitorProp('swap_out', 'swap out'),
           'swap-in' : MonitorProp('swap_in', 'swap in'),
           'major-fault' : MonitorProp('major_fault', 'major fault', tp = types.IntType),
           'minor-fault' : MonitorProp('minor_fault', 'minor fault', tp = types.IntType),
           'host-minor-faults' : MonitorProp('host_minor_faults', 'host minor faults', tp = types.IntType),
           'host-major-faults' : MonitorProp('host_major_faults', 'host major faults', tp = types.IntType),
           'bid_min' : MonitorProp('bid_min', 'bid min'),
           'bid_mem' : MonitorProp('bid_mem', 'bid mem'),
           'bid-price' : MonitorProp('bid_p', 'bid p'),
           'bid-round' : MonitorProp('bid_round', 'bid round'),
           'rss': MonitorProp('rss')}
