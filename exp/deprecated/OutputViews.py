'''
Created on Oct 30, 2011

@author: eyal
'''
import pylab as pl
import types
from exp.deprecated.output_fields import hfields, gfields
from mom.Plotter import HOST, MON, POL
import os

class View():
    def __init__(self, h_props, g_props):
        self.t0 = None
        self.h_props = h_props
        self.g_props = g_props

    def draw(self, res):
        raise NotImplementedError

    def _find_t0(self, res):
        try:
            if len(res[HOST][POL]['time']) == 0:
                return None
        except KeyError:
            return None
        t0 = res[HOST][MON]['time'][0]
        for name in res.keys():
            for tp in res[name].keys():
                t0 = min([t0, res[name][tp]['time'].values()])
        self.t0 = t0

    def _all_guests(self, res):
        guests = set(res.keys())
        guests.remove(HOST)
        return sorted(list(guests))

def_host_table = [hfields[prop] for prop in (
                                             "pol-time",
                                             "auc-round"
                                             )]
def_guest_table = [gfields[prop] for prop in
                   (
                    'mem_base',
                    'rq',
                    'p',
#                    'r',
                    'control-mem',
                    'control-bill'
                    )]

class TableView(View):
    def __init__(self, h_props=def_host_table, g_props=def_guest_table,
                 sep='|'):
        View.__init__(self, h_props, g_props)

    def draw(self, res):
        if res == {}:
            return []
        try:
            num_values = len(res[HOST][POL]['time'])
        except KeyError:
            num_values = 0

        if num_values == 0:
            return []

        def_width = 10

        columns = []
        for prop in self.h_props:
            columns.append(prop.get_column(res[HOST]))

        for guest in self._all_guests(res):
            columns.append([guest.ljust(def_width)] +
                           num_values * [' >'.ljust(def_width)])
            for prop in self.g_props:
                columns.append(prop.get_column(res[guest]))

        # print header row
        lines = []
        sz = min(len(column) for column in columns)
        for i in range(sz):
            lines.append("|".join(column[i] for column in columns))
        return lines


def_host_plot = [hfields[prop] for prop in (
                                            "auc-mem",
                                            )]
def_guest_plot = [gfields[prop] for prop in
                  (
                   'mem-avail',
#                   'rq',
                   'p',
                   'app-load',
                   'app-perf'
                   )]

class PlotView(View):
    def __init__(self, h_props=def_host_plot, g_props=def_guest_plot):
        View.__init__(self, h_props, g_props)
#        pl.ion()
#        pl.draw()

    def draw(self, res):
        if self.t0 is None:
            self._find_t0(res)

        pl.subplot(111)
        pl.cla()

        # plot host's data
        for prop in self.h_props:
            line = prop.add_plot(res[HOST], self.t0)

        # plot guests' data
        for prop in self.g_props:
            for guest in self._all_guests(res):
                line = prop.add_plot(res[guest], self.t0, label_pre=guest[2:] + "-")

        pl.legend(loc='lower left')
        pl.xlabel('time [sec]')
        pl.grid(True)
        pl.draw()

class SplitPlotView(View):
    def __init__(self, h_props=def_host_plot, g_props=def_guest_plot):
        View.__init__(self, h_props, g_props)
#        pl.ion()
#        pl.draw()

    def draw(self, res, color, xlabel='time [sec]'):
        if self.t0 is None:
            self._find_t0(res)
            if self.t0 is None: return

        subplots = len(self._all_guests(res))
        curr_plot = 1

        # plot host's data
        if len(self.h_props) > 0:
            subplots += 1
            pl.subplot(subplots, 1, curr_plot)
            curr_plot += 1
            i = 0
            for prop in self.h_props:
                prop.add_plot(res[HOST], self.t0, color=color,
                              marker = str(pl.Line2D.filled_markers[i]))
                i += 1
            pl.legend()
            pl.grid(True)
            pl.ylabel(HOST)
            pl.gca().get_xaxis().set_ticklabels([])

        # plot guests' data
        for guest in self._all_guests(res):
            pl.subplot(subplots, 1, curr_plot)
            ax1 = pl.gca()
            ax2 = pl.twinx(ax1)
            curr_plot += 1
            i = 0
            for prop in self.g_props:
                prop.add_plot(res[guest], self.t0, color=color, ax=ax1,
                              marker=str(pl.Line2D.filled_markers[i]))
                i = 1 + 1 if i < len(pl.Line2D.filled_markers) - 1 else 0
            pl.ylabel(guest)
            pl.grid()
            if curr_plot != subplots + 1:
                pl.gca().get_xaxis().set_ticklabels([])

        pl.xlabel(xlabel)
        pl.grid(True)

if __name__ == "__main__":
    import sys, signal
    num = -1
    if sys.argv.__len__() > 1:
        num = int(sys.argv[1])
    fd = open('../../moc-output/result_log', 'r')
    line = fd.readlines()[num]
    fd.close()
    results = eval(line.split('>')[1])
    tv = TableView()
    for line in tv.draw(results):
        print line

    pv = SplitPlotView()
#    import threading
#    threading.Thread(target = pv.draw, args = [results]).start()
    pv.draw(results)
    signal.pause()



