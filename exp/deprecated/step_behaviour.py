#! /usr/bin/env python
'''
Created on Oct 21, 2011

@author: eyal
'''

import time
from threading import Thread, Semaphore
from mom.LogUtils import LogUtils
import logging
from exp.prog.Programs import get_host_program
import pylab
from datetime import datetime
from exp.remote.remote_bidder import remote_bidder_quit_server_port

remote_testbed_location = "/home/eyal/moc/remote_testbed.py"
vm_name = "vm-1"
mem_vals = [300, 1200]
load = 600
prog_args = "PyMemoryConsumer" #"CMemoryConsumer " + str(load)
mem_interval = 120
sampling_interval = 5

class Sampler(Thread, Terminatable):
    def __init__(self, interval):
        Thread.__init__(self, name="Sampler")
        Terminatable.__init__(self)
        self.results = {'time': [], 'load': [], 'rev': [],
                        'perf': [], 'mem': []}
        self.sem = Semaphore()
        self.interval = interval
        self.start_time = datetime.now()
        self.start()

    def run(self):
        while self.should_run:
            res = eval(response_client(vm_name, remote_bidder_quit_server_port, "collect"))
            if res['load'] != load:
                continue
            res['time'] = (datetime.now() - self.start_time).seconds
            print "Got results: " + str(res)
            for k in self.results.keys():
                self.results[k].append(res.get(k, -1))
            time.sleep(self.interval)

    def get_results(self):
        return self.results

def main():
    import libvirt
    LogUtils(logging.INFO)
    prog = get_host_program(*prog_args)

    if vm_name != "localhost":
        conn = libvirt.open('qemu:///system')
        dom = conn.lookupByName(vm_name)
        if not dom.isActive():
            dom.create()
            dom.resume()
            time.sleep(5)

        dom.setMemory((mem_vals[0]) << 10)
        time.sleep(5)

        ssh_conn = ssh.Connection(vm_name, username="eyal", password="1234")
        trd = Thread(target=ssh_conn.execute,
                     args = [" ".join([remote_testbed_location] + prog.command_args)],
                     name="Remote-Testbed")
        trd.start()
    else:
        import remote_testbed
        trd = Thread(target=remote_testbed.remote_testbed,
                     args=[prog.get_command_args()], name="Remote-Testbed")
        trd.start()

    time.sleep(1)

    # Set program load
    prog.apply_load(vm_name, load)

    sampler = Sampler(sampling_interval)

    for mem in mem_vals:
        print "Set Memory: " + str(mem)
        if vm_name != "localhost":
            dom.setMemory((mem) << 10)
        time.sleep(mem_interval)

    sampler.terminate()
    # End remote testbed process:
    client(vm_name, remote_bidder_quit_server_port, "finish")
    if vm_name != "localhost":
        ssh_conn.close()

    results = sampler.get_results()
    print results

    ax1 = pylab.subplot(111)
    pylab.title("Step Behaviour of: " + prog_args.split()[0] +
                ", Load: " + str(load))
    ax1.plot(results['time'], results['mem'], 'b')
    ax1.set_ylabel('mem', color='b')
    ax1.set_ylim([0, max(results['mem']) * 1.1])
    for tick in ax1.get_yticklabels():
        tick.set_color('b')

    ax2 = ax1.twinx()
    ax2.plot(results['time'], results['app_perf'], 'r')
    ax2.set_ylabel('perf [MB/sec]', color='r')
    ax2.set_ylim([0, max(results['app_perf']) * 1.1])
    for tick in ax2.get_yticklabels():
        tick.set_color('r')

    pylab.show()

if __name__ == "__main__":
    main()
