#! /usr/bin/env python

import signal
import sys
from exp.prog.Programs import get_guest_program
from mom.LogUtils import LogUtils
# from exp.deprecated.QuitServer import QuitServer, send_quit_request
import logging

from resource import RLIMIT_CORE, RLIM_INFINITY, setrlimit

port = 1924

# def signal_quit(signum = None, frame = None):
# 	send_quit_request("localhost", port)

def set_core_unlimited():
    setrlimit(RLIMIT_CORE, (RLIM_INFINITY, RLIM_INFINITY))

def remote_prog(verbosity, *prog_args):
    LogUtils(verbosity)
    logger = logging.getLogger("RemoteProgram")
    prog = get_guest_program(args=prog_args)

    # 	def signal_quit(self, signum = -1, frame = None):
    # 		prog.terminate()
    #
    # 	try:
    # 		signal.signal(signal.SIGINT, signal_quit)
    # 		signal.signal(signal.SIGTERM, signal_quit)
    # 	except ValueError:
    # 		pass  # Not on main thread
    #
    # 	quit_server = QuitServer(port, prog.terminate,
    # 							 timeout=None, name="ProgQuitServer")
    try:
        logger.info("Starting program: %s", prog.name)
        # 		quit_server.serve_forever()
        set_core_unlimited()
        out, err = prog.start()
        logger.info("Program ended")
        logger.info("== stdout:\n%s\n== stderr:\n%s", out, err)
    finally: pass
    # 		quit_server.shutdown()

if __name__ == "__main__":
    remote_prog(*sys.argv[1:])
