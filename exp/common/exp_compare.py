#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: eyal
'''
import os
from mom.Collectors.HostMemory import HostMemory
from exp.core.VMDesc import get_vm_desc
from mom.LogUtils import LogUtils
from exp.core.ExpMachine import ExpMachineProps
from exp.core.Experiment import confirm_moc_path
from exp.core.Experiment import Experiment

all_memory = 10000

def exp_compare(name, n, prog_args, bm_args, advisers_args,
                bid_collection_interval_mem, calculation_interval_mem, notification_interval_mem,
                rounds_mem, funcs, load_interval, base_mem,
                host_mem, output_path, saturation_mem=None,
                verbosity="info", exclude=[], auction_mem=0, val_switch_func=None,
                bidder_conf="doc/bidder.conf"):

    LogUtils(verbosity)
    confirm_moc_path()

    vm_names = ["vm-%i" % (i + 1) for i in range(n)]
    if n == 0:
        vm_names = ["localhost"]
        n = 1

    mem_tot = HostMemory(None).collect()['mem_available']

    duration_mem = rounds_mem * (bid_collection_interval_mem + calculation_interval_mem + notification_interval_mem)  # seconds

    duration = duration_mem
    info = {"n": n, "host_mem": host_mem, "auction_mem": auction_mem,
            "exp_duration_mem": duration_mem,
            "bid_collection_interval_mem": bid_collection_interval_mem,
            "calculation_interval_mem": calculation_interval_mem,
            "notification_interval_mem": notification_interval_mem,
            "funcs": [f.info for f in funcs],
            "load_interval": load_interval, "rounds_mem": rounds_mem,
            "val_switch_func": map(lambda f: f.info, val_switch_func),
            "adviser_args": advisers_args, "prog": prog_args, "bidder_conf" : bidder_conf}

    # save test information file
    file(os.path.join(output_path, "info"), "w").write(str(info))

    # make the vms description dictionary
    vms = {}
    for i, name in enumerate(vm_names):
        if not isinstance(prog_args, list) or not isinstance(prog_args[0], list):
            vms[name] = ExpMachineProps(advisers_args[i], prog_args, bm_args,
                                        funcs[i], load_interval, get_vm_desc(name),
                                        val_switch_func=val_switch_func[i] if val_switch_func is not None else None,
                                        bidder_conf = bidder_conf)
        else:  # mixed programs
            vms[name] = ExpMachineProps(advisers_args[i], prog_args[i], bm_args[i],
                                        funcs[i], load_interval[i], get_vm_desc(name),
                                        val_switch_func=val_switch_func[i] if val_switch_func is not None else None,
                                        bidder_conf = bidder_conf)

    # GINSENG TEST
    def exp_ginseng():
        moc_args = ("MocAuctioneer", (host_mem,
                                      [bid_collection_interval_mem, calculation_interval_mem, notification_interval_mem],
                                      auction_mem))

        for i, name in enumerate(vm_names):
            vms[name]['desc']['base_mem'] = base_mem
            vms[name]['desc']['max_mem'] = saturation_mem  #all_memory

        exp_out = os.path.join(output_path, "ginseng")
        if not os.path.exists(exp_out):
            os.mkdir(exp_out)

        Experiment(
                   moc_args = moc_args,
                   vms_desc = vms,
                   output_path = exp_out,
                   duration = duration,
                   verbosity = verbosity
                   ).start()

    # DIVIDED STATIC TEST
    def exp_static_divided():
        mem_guest = int((mem_tot - host_mem) / n)

        for name in vm_names:
            vms[name]['desc']['base_mem'] = mem_guest
            vms[name]['desc']['max_mem'] = saturation_mem

        exp_out = os.path.join(output_path, "divided")
        if not os.path.exists(exp_out):
            os.mkdir(exp_out)

        Experiment(
                   moc_args = ("MocStatic",),
                   vms_desc = vms,
                   output_path = exp_out,
                   duration = duration,
                   verbosity = verbosity
                   ).start()

    # HOST SWAPPING TEST
    def exp_host_swapping():
        for name in vm_names:
            vms[name]['desc']['base_mem'] = all_memory
            vms[name]['desc']['max_mem'] = all_memory

        exp_out = os.path.join(output_path, "host-swapping")
        if not os.path.exists(exp_out):
            os.mkdir(exp_out)

        Experiment(
                   moc_args = ("MocStatic",),
                   vms_desc = vms,
                   output_path = exp_out,
                   duration = duration,
                   verbosity = verbosity
                   ).start()

    # HINTED HOST SWAPPING TEST
    def exp_hinted_host_swapping():
        for name in vm_names:
            vms[name]['desc']['base_mem'] = saturation_mem
            vms[name]['desc']['max_mem'] = saturation_mem

        exp_out = os.path.join(output_path, "hinted-host-swapping")
        if not os.path.exists(exp_out):
            os.mkdir(exp_out)

        Experiment(
                   moc_args = ("MocStatic",),
                   vms_desc = vms,
                   output_path = exp_out,
                   duration = duration,
                   verbosity = verbosity
                   ).start()

    # MOM TEST
    def exp_mom():
        if n <= 5:
            exp_out = os.path.join(output_path, "mom")
            if not os.path.exists(exp_out):
                os.mkdir(exp_out)

            for name in vm_names:
                vms[name]['desc']['base_mem'] = base_mem
                vms[name]['desc']['max_mem'] = all_memory


            Experiment(
                       moc_args = ("MocMomBalloon",),
                       vms_desc = vms,
                       output_path = exp_out,
                       duration = duration,
                       verbosity = verbosity
                       ).start()

    # HINTED MOM TEST
    def exp_hinted_mom():
        exp_out = os.path.join(output_path, "hinted-mom")
        if not os.path.exists(exp_out):
            os.mkdir(exp_out)

        for name in vm_names:
            vms[name]['desc']['base_mem'] = base_mem
            vms[name]['desc']['max_mem'] = saturation_mem

        Experiment(
                   moc_args = ("MocMomBalloon",),
                   vms_desc = vms,
                   output_path = exp_out,
                   duration = duration,
                   verbosity = verbosity
                   ).start()
    if "ginseng" not in exclude:
            exp_ginseng()
    if "hinted-mom" not in exclude:
        exp_hinted_mom()
    if "divided" not in exclude:
        exp_static_divided()
    if "hinted-host-swapping" not in exclude:
        exp_hinted_host_swapping()

    print "Output dir: %s" % output_path



