'''
Created on 22/07/2014

@author: Liran Funaro
'''
import bisect

class FunctionOfTime(object):
    '''
    An functor for time to value function
    '''

    # Time units
    SECONDS = 1
    MINUTES = 60
    HOURS = MINUTES * 60
    DAYS = HOURS * 24

    UNITS_SHORT = {
               SECONDS: "sec.",
               MINUTES: "min.",
               HOURS: "hrs.",
               DAYS: "days"
               }

    def __init__(self, value_duration_tuples = [],
                 input_time_unit = SECONDS,
                 representation_time_unit = SECONDS,
                 function_input_time_unit = SECONDS,
                 function_output_time_unit = SECONDS,
                 value_units = "",
                 representation = None,
                 output_with_interval_time = False,
                 ):
        '''
        Constructor
        '''
        self.input_time_unit = input_time_unit
        self.representation_time_unit = representation_time_unit

        self.function_input_time_unit = function_input_time_unit
        self.function_output_time_unit = function_output_time_unit

        self.value_units = value_units
        self.representation = representation

        self.output_with_interval_time = output_with_interval_time

        self.times = []
        self.values = []

        self.next_time = 0

        for value, duration in value_duration_tuples:
            self.addValue(value, duration)

    def addValue(self, value, duration):
        assert duration > 0

        duration_in_seconds = duration * self.input_time_unit

        self.times.append(self.next_time)
        if self.output_with_interval_time:
            self.values.append((value,duration_in_seconds/self.function_output_time_unit))
        else:
            self.values.append(value)

        self.next_time += duration_in_seconds

    def getValueSeconds(self, time):
        i = bisect.bisect_right(self.times, time)
        if i > 0:
            return self.values[i-1]
        else:
            return self.values[0]

    def getMinValue(self):
        return min(self.values)

    def getMaxValue(self):
        return max(self.values)

    def getMaxTime(self):
        return self.next_time

    def __call__(self, time):
        return self.getValueSeconds(time * self.function_input_time_unit)

    def __repr__(self):
        if not self.representation:
            ret = ", ".join( ["%s%s-%s%s" % (
                            float(self.times[i]) / self.representation_time_unit,
                            self.UNITS_SHORT[self.representation_time_unit],
                            self.values[i],
                            self.value_units)
                          for i in range(len(self.times))] )
        else:
            ret = self.representation

        ret = ret % dict(
                         duration_h = float(self.getMaxTime())/float(self.HOURS),
                         duration_m = float(self.getMaxTime())/float(self.MINUTES),
                         duration_s = self.getMaxTime(),
                         max_value = self.getMaxValue(),
                         min_value = self.getMinValue()
                         )

        return "'%s'" % ret

class StepFunctionOfTime(FunctionOfTime):

    def __init__(self, mem_base_mb, mem_step_mb, step_times,
                 input_time_unit = FunctionOfTime.SECONDS,
                 representation_time_unit = FunctionOfTime.SECONDS,
                 value_units = "MB",
                 representation = None
                 ):

        FunctionOfTime.__init__(self,
                                [],
                                input_time_unit,
                                representation_time_unit,
                                FunctionOfTime.SECONDS,
                                FunctionOfTime.SECONDS,
                                value_units,
                                representation,
                                False)

        for i, duration in enumerate(step_times):
            self.addValue(mem_base_mb if i % 2 == 0 else mem_step_mb, duration)

