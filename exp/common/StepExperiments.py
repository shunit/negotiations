#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: eyal
'''
import itertools
from exp.common.MultiExperiments import MultiExperiments
from exp.common.FunctionOfTime import StepFunctionOfTime, FunctionOfTime

class StepExperiments(MultiExperiments):

    def __init__(self, name, vm_name,
                 step_timings, loads, load_intervals,
                 mem_bases, mem_steps,
                 guest_prog, host_prog,
                 guest_prog_kwargs, host_prog_kwargs,
                 guest_swappiness,
                 start_cpu = None, cpu_count = None,
                 verbosity = "info", on_finish = None, cmd_args = []):
        '''
        Parameters
        -------------------
        name : experiment name
        vm_name : the virtual machine to work on

        step_timings : at what time to create each step (up/down) (list of lists)
        loads : the loads to apply to the benchmark (list)
        load_intervals : for how long to apply a load (list)

        mem_bases : the base memory of the VM (list)
        mem_steps : the additional memory to allocate at step up (list)

        guest_prog : the guest program class
        host_prog : the host program class

        guest_prog_kwargs : the guest program arguments (dict of lists)
        host_prog_kwargs : the host program arguments (dict of lists)

        guest_swappiness : the swappiness of the guest machine

        start_cpu : DEPRACATED
        cpu_count : DEPRACATED

        verbosity : the logger verbosity

        on_finish : function to run when each experiment finished

        cmd_args : the command line arguments that run this experiment (for doc)
        '''

        step_timings = step_timings if isinstance(step_timings, list) else [[step_timings]]
        if not isinstance(step_timings[0], list):
            step_timings = [step_timings]

        mem_bases =  self._assertList(mem_bases)
        mem_steps =  self._assertList(mem_steps)

        memory_functions = self.get_memory_functions(step_timings, mem_bases, mem_steps)

        loads = self._assertList(loads)

        load_functions = self.get_load_functions(loads, min(load_intervals))

        MultiExperiments.__init__(self, self.TYPE_STEP_TEST, name, vm_name,
                                  memory_functions, load_functions,
                                  load_intervals, guest_prog, host_prog,
                                  guest_prog_kwargs, host_prog_kwargs,
                                  guest_swappiness, start_cpu, cpu_count,
                                  verbosity, on_finish, cmd_args)

    @classmethod
    def get_memory_functions(cls, step_timings, mem_bases, mem_steps):
        memory_functions = []

        for step_times, mem_base, mem_step in itertools.product(step_timings, mem_bases, mem_steps):
            memory_functions.append( StepFunctionOfTime(mem_base, mem_step, step_times, representation_time_unit = StepFunctionOfTime.MINUTES) )

        return memory_functions

    @classmethod
    def get_bandwidth_functions(cls, step_timings, bw_bases, bw_steps):
        bandwidth_functions = []

        for step_times, bw_base, bw_step in itertools.product(step_timings, bw_bases, bw_steps):
            bandwidth_functions.append( StepFunctionOfTime(bw_base, bw_step, bw_times, representation_time_unit = StepFunctionOfTime.MINUTES) )

        return bandwidth_functions

    @classmethod
    def get_load_functions(cls, loads, duration):
        load_functions = []

        for load in loads:
            load_functions.append( FunctionOfTime([(load, duration)]) )

        return load_functions
