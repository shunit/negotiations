'''
Created on May 5, 2013

@author: eyal
'''
from sim.pareto import pareto_distributed_numbers

shapes_mc = ["linear", "secondorder"]  # "piecewise60"]
shapes_mcd = ["linear", "secondorder", "piecewise1400"]
shapes_mixed = ["linear"] #, "secondorder", "secondorder"]
#shapes_mixed = ["linear", "secondorder"]
#distributions = ["ones", "pareto", "1rich", "3rich"]
distributions = ["pareto"]

shape_str = {
    "piecewise60": "lambda p:p*%f*(1 if p<60 else 1000)",
    "piecewise1400": "lambda p:p*%f*(1 if p<1400 else 1000)",
    "secondorder": "lambda p:%f*p*p",
    "linear": "lambda p:%f*p"
    }

def get_valuation(n, shape, dist, factor = 1, individual_factors = []):
    cs = [1] * n
    if dist == "pareto":
        cs = pareto_distributed_numbers(1, 1000, 1.36, n)
        cs = sorted(cs, reverse = True)
    elif dist == "1rich":
        cs[0] = 1000
    elif dist == "3rich":
        cs[0] = 3000
        cs[1] = 2000
        cs[2] = 1000

    ret = []
    for i, c in enumerate(cs):
        scaling_factor = float(factor)
        if i < len(individual_factors):
            scaling_factor *= individual_factors[i]
        ret.append(shape_str[shape] % (c * scaling_factor))
    return ret
