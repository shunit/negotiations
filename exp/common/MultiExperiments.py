#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from exp.core.VMDesc import get_vm_desc
from exp.core.ExpMachine import ExpMachineProps
import os
from mom.LogUtils import LogUtils
from exp.core.Experiment import confirm_moc_path, Experiment
import logging
import itertools
from etc.Settings import Settings
from datetime import datetime,timedelta
import re
from etc.HostMachine import HostMachine
from contextlib import closing
from etc.decorators import dict_represented

@dict_represented
class MultiExperiments(object):
    # just dummy adviser args
    DUMMY_ADVISER = {'name': 'AdviserProfit',
                     'profiler': 'doc/profiler-linear.xml',
                     'advice_entry': 'throughput',
                     'rev_func': 'lambda x: x',
                     }

    TYPE_STEP_TEST = "step-test"
    TYPE_TESTBED = "testbed"
    TYPE_BATCH = "exp-batch"

    def __init__(self,
                 exp_type,
                 name,
                 vm_names,
                 guest_prog,
                 host_prog,
                 guest_master_image,
                 guest_max_vcpus,
                 verbosity = "info",
                 on_start = None, on_finish = None,
                 cmd_args = []):
        '''
        Parameters
        -------------------
        exp_type : experiment type
        name : experiment name
        vm_names : the virtual machines to work on (list)

        guest_prog : the guest program class
        host_prog : the host program class

        guest_master_image: the master image to use
        guest_max_vcpus: the cpus to allocate to the machines

        verbosity : the logger verbosity

        on_start : function to run before each experiment is started
        on_finish : function to run when each experiment is finished

        cmd_args : the command line arguments that run this experiment (for doc)
        '''

        LogUtils(verbosity)
        self.exp_type = exp_type
        self.name = name
        self.vm_names = self._assertList(vm_names)
        self.vm_descs = dict( [ (vm, get_vm_desc(vm)) for vm in self.vm_names ])

        self.output_path, self.out_path = self.get_output_path()
        self.logger = logging.getLogger( "%s-%s" % (self.__class__.__name__, self.exp_type) )

        self.experiments = []

        self.verbosity = verbosity

        self.on_start = on_start
        self.on_finish = on_finish

        self.cmd_args = cmd_args

        self.revision_information = Settings.revision_info()

        self.guest_prog = guest_prog
        self.host_prog = host_prog

        self.guest_master_image = guest_master_image
        self.guest_max_vcpus = guest_max_vcpus

        self.host_machine = HostMachine()

        self.memory_functions = set()
        self.load_functions = set()
        self.load_intervals = set()
        self.guest_swappiness = set()
        self.guest_image_cow = set()
        self.guest_prog_kwargs = {}
        self.host_prog_kwargs = {}

    def add_experiments(self,
                        memory_functions,
                        load_functions,
                        load_intervals,
                        guest_swappiness,
                        guest_image_cow,
                        guest_prog_kwargs,
                        host_prog_kwargs
                        ):
        '''
        memory_functions: time->memory functions (FunctionOfTime class) (list)
        load_functions : time->load functions (FunctionOfTime class) (list)
        load_intervals : for how long to apply a load (list)
        guest_swappiness : the swappiness of the guest machine (list)
        guest_image_cow : use cow on the master image or copy (list)

        guest_prog_kwargs : the guest program arguments (dict of lists)
        host_prog_kwargs : the host program arguments (dict of lists)
        '''

        exp = dict(
                 memory_functions = self._assertList(memory_functions),
                 load_functions = self._assertList(load_functions),
                 load_intervals = self._assertList(load_intervals),
                 guest_swappiness = self._assertList(guest_swappiness),
                 guest_image_cow = self._assertList(guest_image_cow),
                 guest_prog_kwargs = self._assertDictOFList(guest_prog_kwargs),
                 host_prog_kwargs = self._assertDictOFList(host_prog_kwargs),
                 )

        self.experiments.append(exp)

        self.memory_functions.update( map(str, exp["memory_functions"]) )
        self.load_functions.update( map(str, exp["load_functions"]) )
        self.load_intervals.update( map(str, exp["load_intervals"]) )
        self.guest_swappiness.update( map(str, exp["guest_swappiness"]) )
        self.guest_image_cow.update( map(str, exp["guest_image_cow"]) )

        for param, list_value in exp["guest_prog_kwargs"].iteritems():
            self.guest_prog_kwargs.setdefault(param, set()).update( map(str, list_value) )

        for param, list_value in exp["host_prog_kwargs"].iteritems():
            self.host_prog_kwargs.setdefault(param, set()).update( map(str, list_value) )

    @classmethod
    def _assertList(cls, parameter):
        return parameter if isinstance(parameter, list) else [parameter]

    @classmethod
    def _assertDictOFList(cls, parameter):
        assert isinstance(parameter, dict)
        for arg in parameter:
            parameter[arg] = cls._assertList(parameter[arg])

        return parameter

    def get_output_path(self):
        moc_dir = confirm_moc_path()
        moc_dir_base = os.path.split(moc_dir)[0]
        out_base_dir = os.path.join(moc_dir_base, Settings.output_dir(), self.exp_type)
        out_base_name = self.name + "-%i"

        i = 0
        output_path = out_path = None

        while not out_path or os.path.exists(out_path):
            output_path = out_base_name % i
            out_path = os.path.join(out_base_dir, output_path)
            i += 1

        # Create sub-directories also
        os.makedirs(out_path)
        return output_path, out_path

    @classmethod
    def dict_product(cls, kwargs):
        for items in itertools.product(*kwargs.itervalues()):
            yield dict(zip(kwargs, items))

    def get_relavent_parameters(self, memory_function, load_function, load_interval, guest_prog_args, host_prog_args, guest_swappiness, guest_image_cow):
        ret = {}

        def add_arg_to_list_if_iteratable(param_name, iteratable_parm, single_param):
            if len(iteratable_parm) > 1:
                ret[param_name] = single_param

        def add_dict_arg_to_list_if_iteratable(kwargs, prog, all_kwargs):
            for arg in kwargs:
                add_arg_to_list_if_iteratable("%s(%s)" % (prog.__name__, arg), all_kwargs[arg], kwargs[arg])

        add_arg_to_list_if_iteratable("Memory", self.memory_functions, memory_function)
        add_arg_to_list_if_iteratable("Load", self.load_functions, load_function)
        add_arg_to_list_if_iteratable("Load Interval", self.load_intervals, load_interval)

        add_arg_to_list_if_iteratable("Guest Swappiness", self.guest_swappiness, guest_swappiness)
        add_arg_to_list_if_iteratable("Guest Image Cow", self.guest_image_cow, guest_image_cow)

        add_dict_arg_to_list_if_iteratable(guest_prog_args, self.guest_prog, self.guest_prog_kwargs)
        add_dict_arg_to_list_if_iteratable(host_prog_args, self.host_prog, self.host_prog_kwargs)

        return ret

    def get_long_title(self, relavent_parameters):
        ret = []

        for key in relavent_parameters:
            ret.append( "%s-%s" % (key, relavent_parameters[key]))

        return ','.join(ret) if len(ret) > 0 else "Single Experiment"

    def get_short_title(self, relavent_parameters):
        re_quotes = re.compile("'|\"")
        ret = []

        for key in relavent_parameters:
            ret.append( re_quotes.sub("", str(relavent_parameters[key])) )

        return ','.join(ret) if len(ret) > 0 else "single"

    def iterate_experiments(self):
        available_vms = set(self.vm_names)

        vms_mem_functions = {}

        vms_desc = {}
        duration = 0

        long_titles = []
        short_titles = []

        def prepare_yield_experiment():
            if duration <= 0:
                return None

            exp_start_time = datetime.now()
            exp_estimated_end_time = exp_start_time + timedelta(seconds = duration)

            sub_exp_name = "-".join(short_titles)
            exp_out_path = os.path.join(self.out_path, sub_exp_name)
            os.makedirs(exp_out_path)

            exp_info = dict(
                   type = self.exp_type,
                   name = self.name,
                   exp_name = self.output_path,
                   sub_exp_name = sub_exp_name,

                   guest_prog = self.guest_prog.__name__,
                   host_prog = self.host_prog.__name__,

                   guest_prog_kwargs = guest_prog_kwargs,
                   host_prog_kwargs = host_prog_kwargs,

                   short_titles = short_titles,
                   long_titles = long_titles,

                   relavent_parameters = relavent_parameters,
                   time = dict( start = str(exp_start_time),
                                estimated_end = str(exp_estimated_end_time))
                    )

            return dict(
                       moc_args=("MocStatic", [], dict(func_mem = vms_mem_functions)),
                       vms_desc = vms_desc,
                       duration = duration,
                       output_path = exp_out_path,
                       extra_info = exp_info,
                       verbosity = self.verbosity)

        for experiment_args in self.experiments:
            for memory_function, load_function, load_interval, guest_prog_kwargs, host_prog_kwargs, guest_swappiness, guest_image_cow in itertools.product(
                                    experiment_args["memory_functions"],
                                    experiment_args["load_functions"],
                                    experiment_args["load_intervals"],
                                    self.dict_product(experiment_args["guest_prog_kwargs"]),
                                    self.dict_product(experiment_args["host_prog_kwargs"]),
                                    experiment_args["guest_swappiness"],
                                    experiment_args["guest_image_cow"]):
                #vm = available_vms.pop()

                relavent_parameters = self.get_relavent_parameters(memory_function,
                                                 load_function,
                                                 load_interval,
                                                 guest_prog_kwargs,
                                                 host_prog_kwargs,
                                                 guest_swappiness,
                                                 guest_image_cow)

                long_title = self.get_long_title(relavent_parameters)
                long_titles.append( long_title )
                short_title = self.get_short_title(relavent_parameters)
                short_titles.append( short_title )

                guest_prog_args = self.guest_prog(**guest_prog_kwargs).command_args
                host_prog_args = self.host_prog(**host_prog_kwargs).command_args

                for vm in available_vms:
                    vms_mem_functions[vm] = memory_function

                    vms_desc[vm] = self.get_machine_props(
                                            vm,
                                            long_title,
                                            memory_function,
                                            load_function,
                                            load_interval,
                                            guest_prog_args,
                                            host_prog_args,
                                            guest_swappiness,
                                            guest_image_cow)

                cur_duration = max(memory_function.getMaxTime(), load_function.getMaxTime())
                # Account for the time between loads (assume about 10 sec. between loads)
                approx_load_runs = (cur_duration / load_interval) + 1
                cur_duration += approx_load_runs * 10

                duration = max(duration, cur_duration)

                if len(available_vms) == 0:
                    exp_kwargs = prepare_yield_experiment()
                    if exp_kwargs is not None:
                        yield exp_kwargs
                        available_vms = set(self.vm_names)
                        vms_mem_functions = {}
                        vms_desc = {}
                        duration = 0

                        long_titles = []
                        short_titles = []

        exp_kwargs = prepare_yield_experiment()
        if exp_kwargs is not None:
            yield exp_kwargs

    def print_experiments(self):
        for exp_kwargs in self.iterate_experiments():
            print exp_kwargs

    def start(self):
        self.write_info(self.out_path)

        self.host_machine.begin_experiment(self)

        for exp_kwargs in self.iterate_experiments():
            self.run_experiment(exp_kwargs)

        self.host_machine.end_experiment(self)

    def run_experiment(self, exp_kwargs):
        try:
            print exp_kwargs.keys()
            with closing( Experiment(**exp_kwargs) ) as exp_obj:
                try:
                    if self.on_start and hasattr(self.on_start, '__call__'):
                        self.on_start(**exp_obj.as_dict);
                except Exception as e:
                    self.logger.error("on_start() execution failed: %s", e)

                exp_error = None
                try:
                    exp_obj.start()
                except Exception as e:
                    exp_error = e
                    self.logger.error("Experiment.start() execution failed: %s", e)

                try:
                    if self.on_finish and hasattr(self.on_finish, '__call__'):
                        self.on_finish(exp_error = exp_error, **exp_obj.as_dict);
                except Exception as e:
                    self.logger.error("on_finish() execution failed: %s", e)
        except Exception as e:
            self.logger.error("Experiment uncaught error: %s", e, exc_info=1)

    def get_machine_props(self, vm_name, title, memory_function,
                          load_function,
                          load_interval, guest_prog_args, host_prog_args,
                          guest_swappiness, guest_image_cow):
        return ExpMachineProps(
                    adviser_args = self.DUMMY_ADVISER,
                    prog_args = guest_prog_args,
                    bm_args = host_prog_args,
                    load_func = load_function,
                    desc = self.vm_descs[vm_name],
                    load_interval = load_interval,
                    base_mem = memory_function.getMinValue(),
                    max_mem = memory_function.getMaxValue() + 128,
                    swappiness = guest_swappiness,
                    title = title,
                    use_image_cow = guest_image_cow,
                    master_image = self.guest_master_image,
                    max_vcpus = self.guest_max_vcpus
                    )

#     def get_experiment_class(self):
#         if self.vm_name == "localhost":
#             import tests.LocalExperiment
#             return tests.LocalExperiment.ReallyLocalExperiment
#         else:
#             return exp.core.Experiment.Experiment

    @property
    def as_dict(self):
        return dict(
                    name = self.name,
                    vm_names = self.vm_names,
                    experiments = self.experiments,

                    guest_prog = self.guest_prog.__name__,
                    host_prog = self.host_prog.__name__,

                    guest_master_image = self.guest_master_image,
                    guest_max_vcpus = self.guest_max_vcpus,

                    runtime_information = dict(
                          start_time = str(datetime.now()),
                          revision = self.revision_information,
                          cmd_args = self.cmd_args,
                          cmd_line = " ".join(self.cmd_args)
                          )
                       )
