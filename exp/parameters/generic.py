'''
Created on 14/12/2014

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from etc.NamedParameters import NamedParameters, SingleParameterGroup,\
    ListSingleParameterGroup, ListMultiParameterGroup
from exp.common.MultiExperiments import MultiExperiments

GENERIC_PARAMETERS = {
        "exp_type" : SingleParameterGroup(dict(
            batch = MultiExperiments.TYPE_BATCH,
            step = MultiExperiments.TYPE_STEP_TEST,
            bed = MultiExperiments.TYPE_TESTBED
         ), "batch"),

        "vm_names" : ListSingleParameterGroup(dict(
            [("vm%s" % i, "vm-%s" % i) for i in range(1,13)]
        ), "vm-1"),

        "guest_swappiness" : ListMultiParameterGroup(dict(
            swap_all = [1, 10, 20, 30, 40, 50, 60],
            swap_lowest = [1],
            swap_low = [10,20],
            swap_medium = [30, 40],
            swap_high = [50, 60],
            swap_highest = [100],
        ), "swap_lowest"),

        "guest_image_cow" : ListSingleParameterGroup(dict(
            image_cow = True,
            image_copy = False,
        ), "image_cow"),

        "master_image" : SingleParameterGroup(dict(
            memcached_image = "memcahed-master.qcow2",
            postgres_image = "postgres-master.qcow2",
            moc_image = "moc-master.qcow2",
            guest_program_default = None,
        ), "guest_program_default"),

        "max_vcpus" : SingleParameterGroup(dict(
            vcpus_10 = 10,    
            vcpus_8 = 8,    
            vcpus_6 = 6,
            vcpus_4 = 4,
            vcpus_3 = 3,
            vcpus_2 = 2,
            vcpus_1 = 1,
        ), "vcpus_4")
    }

def get_generic_parameters():
    return NamedParameters(GENERIC_PARAMETERS)