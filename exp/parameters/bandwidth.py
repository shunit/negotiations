'''
Created on 23/3/2015

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from exp.common.FunctionOfTime import FunctionOfTime
from etc.NamedParameters import NamedParameters, ListSingleParameterGroup

def StaticBandwidth_FunctionOfTime_Min(allocation, name):
    return FunctionOfTime([allocation], FunctionOfTime.MINUTES, FunctionOfTime.HOURS, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS, "Mbps", representation = name)

def step_functtion_post_action(experiments_params):
    for p in experiments_params:
            if p["step_timings"]:
                bandwidth_functions = p.get_group("bandwidth_functions")
                for bw_func in StepExperiments.get_bandwidth_functions(p["step_timings"], p["bw_bases"], p["bw_steps"]):
                    bandwidth_functions.include_param(str(bw_func), bw_func)

                p.get_group("step_timings").restart()
                p.get_group("bw_bases").restart()
                p.get_group("bw_steps").restart()

BANDWIDTH_FUNCTIONS = {
            'bw_1Gb_short'  : StaticBandwidth_FunctionOfTime_Min((10**6, 1), "1024Mbps"),
            'bw_512Mb'      : StaticBandwidth_FunctionOfTime_Min((1<<19, 30), "512Mbps"),
            'bw_256Mb'      : StaticBandwidth_FunctionOfTime_Min((1<<18, 30), "256Mbps"),
            'bw_128Mb'      : StaticBandwidth_FunctionOfTime_Min((1<<17, 30), "128Mbps"),
            'bw_64Mb'       : StaticBandwidth_FunctionOfTime_Min((1<<16, 30), "64Mbps"),
            'bw_32Mb'       : StaticBandwidth_FunctionOfTime_Min((1<<15, 30), "32Mbps"),
            'bw_16Mb'       : StaticBandwidth_FunctionOfTime_Min((1<<14, 30), "16Mbps"),
            'bw_8Mb'        : StaticBandwidth_FunctionOfTime_Min((1<<13, 30), "8Mbps"),
            'bw_4Mb'        : StaticBandwidth_FunctionOfTime_Min((1<<12, 30), "4Mbps"),
            'bw_2Mb'        : StaticBandwidth_FunctionOfTime_Min((1<<11, 30), "2Mbps"),
            'bw_1Mb'        : StaticBandwidth_FunctionOfTime_Min((1<<10, 30), "1Mbps"),

            "bw_constant"   : FunctionOfTime([((2**18 - 4000) / 3.0, 3*60)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "Kb", "Constant 256Mbit divided by 3"),

            "bw_dual"       : FunctionOfTime([(10**6,10)] + [(m,1) for m in xrange(1<<10, 1<<18 | 1<<16, 1<<10)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "Kb", "Increasing bandwidth from 2Mbps to 128Mbps(~11 hrs."),

            "bw_big"       : FunctionOfTime([(10**6,10)] + [(m,1) for m in xrange(1<<12, 10**6, 1<<12)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "Kb", "Increasing bandwidth from 2Mbps to 128Mbps(~11 hrs."),


            "bw_increase"   : FunctionOfTime([(1<<19,10)] + [(m,1) for m in xrange(1<<10,1<<17,1<<10)] + [(m,10) for m in xrange(1<<15,1<<17,1<<14)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "Kb", "Increasing bandwidth from 1Mbps to 128Mbps (190 min.)"),

            "bw_decrease"   : FunctionOfTime([(1<<17 + 1<<16,30)] + [(m,10) for m in xrange(1<<17,1<<15,-1<<14)] + [(m,10) for m in xrange(1<<15,1<<12,-1<<12)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "Kb", "Increasing bandwidth from 1Mbps to 128Mbps (190 min.)")
        }

BANDWIDTH_PARAMETERS = NamedParameters({
        "bandwidth_functions" : ListSingleParameterGroup(BANDWIDTH_FUNCTIONS, "bw_1Gb_short"),

        # Step-test parameters
        "step_timings" : ListSingleParameterGroup(dict(
            # Exp. duration: 3 min + 3 min, + 3 min = 9 min
            step_short = [180,180,180],

            # Exp. duration: 3 h + 3 h, + 3 h = 9 hours
            step_long = [10800, 10800, 10800],

            # Exp. duration: 9 hours (single step)
            step_single_low = [32400],

            # Exp. duration: 9 hours (single step)
            step_single_high = [10, 32400]
        ), show = False),

        "bw_bases" : ListSingleParameterGroup(dict(
            bw_base_low = 1<<10,
            bw_base_medium1 = 1<<13,
            bw_base_medium2 = 1<<14,
            bw_base_medium3 = 1<<15,
            bw_base_high = 1<<16
        ), "bw_base_high", show = False),

        "bw_steps" : ListSingleParameterGroup(dict(
            bw_step_low = 1<<10,
            bw_step_medium = 1<<12,
            bw_step_high = 1<<14
        ), "bw_step_high", show = False),
        }, post_actions = [step_functtion_post_action])
