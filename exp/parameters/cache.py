'''
Created on 14/4/2015

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from exp.common.FunctionOfTime import FunctionOfTime
from etc.NamedParameters import NamedParameters, ListSingleParameterGroup

CACHE_FUNCTIONS = dict(
            # For testing
            cache_static_short = FunctionOfTime([((0,19),1)],
                    FunctionOfTime.MINUTES, FunctionOfTime.MINUTES,
                    representation = "Static - all the cache (1 min.)"),

            # Static allocation
            cache_static_all = FunctionOfTime([((0,19),30)],
                                FunctionOfTime.MINUTES, FunctionOfTime.MINUTES,
                                representation = "Static - all the cache (30 min.)"),
        )

CACHE_PARAMETERS = NamedParameters({
        "cache_functions" : ListSingleParameterGroup(CACHE_FUNCTIONS, "cache_static_all"),
        })