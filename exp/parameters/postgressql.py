'''
Created on Jul 27, 2014

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''

import sys

from etc.NamedParameters import NamedParameters, \
    ListSingleParameterGroup, ListMultiParameterGroup
from exp.common.FunctionOfTime import FunctionOfTime
from exp.parameters.memory import MEMORY_PARAMETERS
from exp.parameters.load import LOAD_PARAMETERS
from exp.parameters.generic import get_generic_parameters

def StaticMemory_FunctionOfTime_Hours(allocation, name):
    return FunctionOfTime([allocation], FunctionOfTime.HOURS, FunctionOfTime.HOURS, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS, "MB", representation = name)

def get_postgressql_parameters():
    ret = get_generic_parameters()
    ret.add_groups(MEMORY_PARAMETERS)
    ret.add_groups(LOAD_PARAMETERS)

    ret.add_groups({
        "guest_prog_kwargs" : NamedParameters(dict(
            max_connections = ListMultiParameterGroup(dict(
                pg_max_conn_highest = [120],
                pg_max_conn_default = [60],
                pg_max_conn_high = [120,110,100],
                pg_max_conn_medium = [80,60,40,20],
                pg_max_conn_low = [5, 10],
            ), "pg_max_conn_default"),

            shared_buffers_MB = ListSingleParameterGroup(dict(
                pg_shared_buff_default = 24,

                pg_shared_buff_very_large = 128,
                pg_shared_buff_large = 64,
                pg_shared_buff_medium = 32,
                pg_shared_buff_small = 16,
            ), "pg_shared_buff_default"),

            temp_buffers_MB = ListSingleParameterGroup(dict(
                pg_temp_buff_large = 8,
                pg_temp_buff_medium = 5,
                pg_temp_buff_small = 1,
            ), "pg_temp_buff_small"),

            work_mem_MB = ListSingleParameterGroup(dict(
                # Amount of memory for sorts (in MB)
                pg_work_mem_default = 1, # 1MB (postgres defaults)

                pg_work_mem_high = 4,    # 4MB
                pg_work_mem_medium = 2,  # 2MB
            ), "pg_work_mem_default"),

            autovacuum = ListSingleParameterGroup(dict(
                pg_vacuum_on = True,
                pg_vacuum_off = False,
            ), "pg_vacuum_off"),

            checkpoint_timeout = ListSingleParameterGroup(dict(
                # Maximum time between automatic WAL checkpoints, in seconds
                pg_checkpoint_timeout_default  = 300,  # 5min (postgres default)

                pg_checkpoint_timeout_high     = 900,  # 15min
                pg_checkpoint_timeout_medium   = 600,  # 10min
                pg_checkpoint_timeout_low      = 60,   # 1min
            ), "pg_checkpoint_timeout_default"),

            checkpoint_completion_target = ListSingleParameterGroup(dict(
                # Specifies the target of checkpoint completion, as a fraction of total time between checkpoints.
                pg_checkpoint_comp_trg_default = 0.5, # postgres default
                pg_checkpoint_comp_trg_high    = 0.9, # recommended for large checkpoint segments
                pg_checkpoint_comp_trg_medium  = 0.7,
                pg_checkpoint_comp_trg_low     = 0.3, # not recommended
            ), "pg_checkpoint_comp_trg_high"),

            checkpoint_mem_static_MB = ListSingleParameterGroup(dict(
                # Specifies the target of checkpoint completion, as a fraction of total time between checkpoints.
                pg_checkpoint_mem_high          = (1<<12), # 4G
                pg_checkpoint_mem_medium        = (1<<11), # 2G
                pg_checkpoint_mem_low           = (1<<9),  # 1/2 G
            ), "pg_checkpoint_mem_high"),

            checkpoint_mem_ratio = ListMultiParameterGroup(dict(
                # Ratio between total memory to the checkpoint buffer size (checkpoint/memory)
                pg_checkpoint_ratio_0_10    = [0.1],
                pg_checkpoint_ratio_0_15    = [0.15],
                pg_checkpoint_ratio_0_20    = [0.2],
                pg_checkpoint_ratio_0_25    = [0.25],
                pg_checkpoint_ratio_0_30    = [0.3],
                pg_checkpoint_ratio_0_35    = [0.35],
                pg_checkpoint_ratio_0_40    = [0.4],
                pg_checkpoint_ratio_0_45    = [0.45],
                pg_checkpoint_ratio_0_50    = [0.5],
                pg_checkpoint_ratio_0_55    = [0.55],
                pg_checkpoint_ratio_0_60    = [0.6],
                pg_checkpoint_ratio_0_65    = [0.65],
                pg_checkpoint_ratio_0_70    = [0.7],

                pg_checkpoint_ratio_all     = [ 0.1 + i*0.05 for i in xrange(0,12)],
                pg_checkpoint_ratio_medium  = [0.3,0.35,0.4,0.45,0.5],
                pg_checkpoint_ratio_very_low     = [ 0.01 + i*0.01 for i in xrange(0,10)],
            ), "pg_checkpoint_ratio_0_50"),

            checkpoint_mem_change_threshold = ListSingleParameterGroup(dict(
                # Ratio between total memory to the checkpoint buffer size (checkpoint/memory)
                pg_checkpoint_mem_change_threshold_small = 0.01,
                pg_checkpoint_mem_change_threshold_medium = 0.05,
                pg_checkpoint_mem_change_threshold_large = 0.1,
            ), "pg_checkpoint_mem_change_threshold_medium"),
        )),

        "host_prog_kwargs" : NamedParameters(dict(
            connection_per_transaction = ListSingleParameterGroup(dict(
                # Create new connection for each transaction
                pgbench_cpt = True,
                pgbench_no_cpt = False,
            ), "pgbench_no_cpt"),

            output_log = ListSingleParameterGroup(dict(
                # Create log files
                pgbench_log = True,
                pgbench_no_log = False,
            ), "pgbench_no_log"),

            threads_count = ListSingleParameterGroup(dict(
                # Create log files
                pgbench_threads_max = 150,  # More than enough
                pgbench_threads_high = 20,
                pgbench_threads_medium = 12,
                pgbench_threads_low = 6
            ), "pgbench_threads_max"),
        ))
    })

    return ret

if __name__ == '__main__':
    ''' Unit Test '''
    params = get_postgressql_parameters()
    params.inquire_parameters(sys.argv[1:])

    print "Parameters:\n%s\n" % params
