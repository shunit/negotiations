'''
Created on 14/12/2014

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from exp.common.FunctionOfTime import FunctionOfTime
from etc.NamedParameters import ListSingleParameterGroup, NamedParameters
from exp.common.StepExperiments import StepExperiments

def StaticMemory_FunctionOfTime_Hours(allocation, name):
    return FunctionOfTime([allocation], FunctionOfTime.HOURS, FunctionOfTime.HOURS, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS, "MB", representation = name)

def StaticMemory_FunctionOfTime_Min(allocation, name):
    return FunctionOfTime([allocation], FunctionOfTime.MINUTES, FunctionOfTime.HOURS, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS, "MB", representation = name)

def step_functtion_post_action(experiments_params):
    for p in experiments_params:
            if p["step_timings"]:
                memory_functions = p.get_group("memory_functions")
                for mem_func in StepExperiments.get_memory_functions(p["step_timings"],  p["mem_bases"], p["mem_steps"]):
                    memory_functions.include_param(str(mem_func), mem_func)

                p.get_group("step_timings").restart()
                p.get_group("mem_bases").restart()
                p.get_group("mem_steps").restart()

MEMORY_FUNCTIONS = {
            # For testing
            "mem_6.5G_long" : StaticMemory_FunctionOfTime_Hours((6144+512, 15), "6.5GB (15 Hrs.)"),
            "mem_4G_short" : StaticMemory_FunctionOfTime_Min((4096, 1), "4GB (1 Min.)"),

            # Static memory
            "mem_12G" : StaticMemory_FunctionOfTime_Min((12288, 30), "12GB"),
            "mem_9G" : StaticMemory_FunctionOfTime_Min((9216, 30), "9GB"),
            "mem_8G" : StaticMemory_FunctionOfTime_Min((8192, 30), "8GB"),
            "mem_7.5G" : StaticMemory_FunctionOfTime_Min((7168+512, 30), "7.5GB"),
            "mem_7G" : StaticMemory_FunctionOfTime_Min((7168, 30), "7GB"),
            "mem_6.5G" : StaticMemory_FunctionOfTime_Min((6144+512, 30), "6.5GB"),
            "mem_6G" : StaticMemory_FunctionOfTime_Min((6144, 30), "6GB"),
            "mem_5.5G" : StaticMemory_FunctionOfTime_Min((5120+512, 30), "5.5GB"),
            "mem_5G" : StaticMemory_FunctionOfTime_Min((5120, 30), "5GB"),
            "mem_4.5G" : StaticMemory_FunctionOfTime_Min((4096+512, 30), "4.5GB"),
            "mem_4G" : StaticMemory_FunctionOfTime_Min((4096, 30), "4GB"),
            "mem_3.5G" : StaticMemory_FunctionOfTime_Min((3072+512, 30), "3.5GB"),
            "mem_3G" : StaticMemory_FunctionOfTime_Min((3072, 30), "3GB"),
            "mem_2.5G" : StaticMemory_FunctionOfTime_Min((2048+512, 30), "2.5GB"),
            "mem_2G" : StaticMemory_FunctionOfTime_Min((2048, 30), "2GB"),
            "mem_1.5G" : StaticMemory_FunctionOfTime_Min((1024+512, 30), "1.5GB"),
            "mem_1G" : StaticMemory_FunctionOfTime_Min((1024, 30), "1GB"),
            "mem_900M" : StaticMemory_FunctionOfTime_Min((896, 30), "896MB"),
            "mem_700M" : StaticMemory_FunctionOfTime_Min((768, 30), "768MB"),
            "mem_600M" : StaticMemory_FunctionOfTime_Min((640, 30), "640MB"),
            "mem_500M" : StaticMemory_FunctionOfTime_Min((512, 30), "512MB"),

            "mem_increase" : FunctionOfTime([(2048+512,90)] + [(m,60) for m in xrange(512,1024,128)] + [(m,60) for m in xrange(1024,6500,512)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "MB", "Increasing memory from 500M to 6G (%(duration_h) hrs.)"),

            "mem_increase_slow" : FunctionOfTime([(4096,90)] + [(m,20) for m in xrange(512,1024,128)] + [(m,20) for m in xrange(1024,6500,512)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "MB", "Increasing memory from 500M to 6G (6.5 hrs.)"),
            "mem_decrease" : FunctionOfTime([(6144+512,90)] + [(m,60) for m in xrange(6144,1024,-512)] + [(m,60) for m in xrange(1024,500,-128)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "MB", "Decreasing memory from 6G to 500M (%(duration_h)s hrs.)"),

            "mem_decrease_increase" : FunctionOfTime([(6144+512,90)] + [(m,40) for m in xrange(6144,1024,-512)] + [(m,40) for m in xrange(1024,500,-128)] + [(m,100) for m in xrange(512, 1024, 128)] + [(m,100) for m in xrange(1024,6144+1024,512)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS,
                                            "MB", "Decreasing then increasing memory from 6G to 500M (%(duration_h)s hrs.)"),

            "mem_test_sample" : FunctionOfTime([(1868, 90)], FunctionOfTime.MINUTES,
                                            FunctionOfTime.MINUTES, FunctionOfTime.SECONDS,
                                            FunctionOfTime.SECONDS, "MB",
                                            "test 1868 MB (%(duration_h)s hrs.)"),
        }

MEMORY_PARAMETERS = NamedParameters({
        "memory_functions" : ListSingleParameterGroup(MEMORY_FUNCTIONS, "mem_4G"),

        # Step-test parameters
        "step_timings" : ListSingleParameterGroup(dict(
            # Exp. duration: 3 min + 3 min, + 3 min = 9 min
            step_short = [180,180,180],

            # Exp. duration: 3 h + 3 h, + 3 h = 9 hours
            step_long = [10800, 10800, 10800],

            # Exp. duration: 9 hours (single step)
            step_single_low = [32400],

            # Exp. duration: 9 hours (single step)
            step_single_high = [10, 32400]
        ), show = False),

       "mem_bases" : ListSingleParameterGroup(dict(
            mem_base_low = 256,
            mem_base_medium1 = 512,
            mem_base_medium2 = 512+128,
            mem_base_medium3 = 512+256,
            mem_base_high = 1024
        ), "mem_base_high", show = False),

        "mem_steps" : ListSingleParameterGroup(dict(
            mem_step_low = 1024,
            mem_step_medium = 4096,
            mem_step_high = 8192
        ), "mem_step_high", show = False),
    }, post_actions = [step_functtion_post_action])
