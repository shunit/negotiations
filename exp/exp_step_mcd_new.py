'''
Created on Apr 1, 2012

@author: eyal
'''
import exp.core.Loads
from exp.prog.Programs import Guest_Memcached, Host_Memcached
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_out_dir
import os
from exp.core.VMDesc import get_vm_desc
from exp.core.ExpMachine import ExpMachineProps
from exp.core.Experiment import Experiment


frequency = 40 * 60 # how often do we switch memory amount?
step_count = 3#4 # how many steps do we want? (step = low-high-low)

steps = [600, 1200, 2000]

class Configuration(object):
    def __init__(self, prog_args, bm_args, base0, saturation_mem,
                 profiler_file, profiler_entry, revenue_func_str,
                 load, mem_interval, steps, repetitions):
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.base0 = base0
        self.saturation_mem = saturation_mem
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.revenue_func_str = revenue_func_str
        self.mem_interval = mem_interval # how often do we switch memory step?
        self.steps = steps # what are the steps?
        self.repetitions = repetitions # how many times will we repeat?

        self.steps.sort()

        for i in reversed(self.steps[0:len(self.steps) - 1]):
            self.steps.append(i)

        self.steps *= self.repetitions

        self.adviser = {'name': 'AdviserProfit',
                        'profiler': profiler_file,
                        'advice_entry': profiler_entry,
                        'rev_func': revenue_func_str,
                        'base_mem': base0, 'memory_delta': 10} #,
#                        'alloc_diff': alloc_diff}

    def mem_function(self):
        return 'lambda t: (%s)[(int(t / %i)) %% len(%s)]' % (self.steps, self.mem_interval, self.steps)

    def duration(self):
        return len(self.steps) * self.mem_interval

    def load_interval(self):
        ret = self.duration() - 3 * 60
        assert ret > 0, "experiment isn't long enough"
        return self.duration() - 3 * 60

configs = Configuration(
        prog_args = Guest_Memcached(
                       spare_mem = 50,
                       init_mem_size = 10,
                       update_interval = 1
#                       alloc_diff = alloc_diff
                       ).command_args,
        bm_args = Host_Memcached(
                       cmd_get_percent = 0.3,
                       keys_dist = [(249, 249, 1.0)],
                       vals_dist = [(1024, 1024, 1.0)],
                       win_size = "500k",
                       ).command_args,
        base0 = 600,
        saturation_mem = 2000,
        profiler_file = "doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
        profiler_entry = "hits_rate",
        revenue_func_str = 'lambda x:x',
        load = exp.core.Loads.LoadConstant(8), #8),
        mem_interval = frequency,
        steps = steps,
        repetitions = step_count,
        )

if __name__ == '__main__':
    LogUtils("debug")
    name = "step-mcd"
    out_dir = set_out_dir("step", name)
    n = 1
    vm_names = ["vm-%i" % (i + 1) for i in range(n)]
    vms = {}

    host_mem = 500

    mem_funcs = {}

    for name, conf in zip(vm_names, [configs]):
        vms[name] = ExpMachineProps(
                        adviser_args = conf.adviser,
                        prog_args = conf.prog_args,
                        bm_args = conf.bm_args,
                        load_func = conf.load,
                        load_interval = conf.load_interval(),
                        desc = get_vm_desc(name)
                        )
        vms[name]['desc']['base0'] = conf.base0
        vms[name]['desc']['max_mem'] = conf.saturation_mem

        mem_funcs[name] = conf.mem_function()

    exp_out = os.path.join(out_dir, "mcd-step-%i-load-%i" % (configs.mem_interval, configs.load(0)))
    os.makedirs(exp_out)

    moc_args = ("MocStatic", [], dict(func_mem=mem_funcs))

    Experiment(
               moc_args = moc_args,
               vms_desc = vms,
               out_dir = exp_out,
               duration = conf.duration(),
               start_cpu = 1,
               join_timeout = 30,
               verbosity = "debug",
               ).start()
