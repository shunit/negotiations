'''
Created on Mar 8, 2012

@author: eyal
'''
import logging
import time
from subprocess import Popen, PIPE
from mom.Collectors.Collector import parse_float_list, parse_int_list, parse_float, parse_int, parse_string
from etc.PySock import Client
import socket
from exp.prog.MemoryConsumer import MemoryConsumerServer
import tempfile
from threading import Thread
from etc.shell import SSHCommunicator, TimeoutError
import re
from getpass import getuser
from ast import literal_eval

class BenchmarkError(Exception): pass

class BenchmarkProgram():
    def __init__(self, name, ip, alias = None):
        self.ip = ip
        self.name = "%s-%s" % (alias if alias is not None else ip, name)
        self.logger = logging.getLogger(self.name)
        self.prog = None
        self.should_run = True

    def consume(self, load, duration, cpu_pin):
        """
        blocking
        consumes load for a duration running benchmark on CPU number cpu_pin in the range: [0,N-1]
        return a dict of performance.
        raises BenchmarkError when benchmark fails
        """
        raise NotImplementedError

    def getCpuCount(self):
        """
        If the benchmark needs more than 1 CPU, it should override this method
        """
        return 1

    def popen(self, args, cpu_pin = None, timeout = None):
        '''
        execute command and stream the output (optional timeout, cpu pinning)
        '''

        if cpu_pin is not None:
            if not isinstance(cpu_pin,list):
                cpu_pin = [cpu_pin]

            cpus_bit_vector = 0

            for cpu in cpu_pin:
                cpus_bit_vector |= ( 1 << int(cpu) )

            args = ["taskset" ,"-a", hex( cpus_bit_vector )] + list(args)
        self.prog = Popen(args, stdout = PIPE, stderr = PIPE)

        def communicate(out):
            '''
            communicate with the created Popen object:
            pause until response from the child
            '''
            output, error = self.prog.communicate()
            out.append(output)
            out.append(error)

        out = []
        trd = Thread(target = communicate, args = (out,))
        trd.start()
        trd.join(timeout)

        if trd.is_alive():
            self.prog.kill()
            self.prog = None
            raise BenchmarkError, "benchmark stuck"

        self.prog = None
        # out[0] - stdout
        # out[1] - stderr
        return out[0].strip(), out[1].strip()

    def terminate(self):
        self.should_run = False
        if self.prog is not None:
            try:
                self.prog.kill()
            except OSError:
                pass
        self.prog = None

    @staticmethod
    def getFields():
        return set()

class MemoryConsumerBenchmark(BenchmarkProgram):
    def __init__(self, **kwargs):
        BenchmarkProgram.__init__(self, "MC-Benchmark", **kwargs)

    def consume(self, load, duration, cpu_pin):
        timeout = duration * 2 if duration != 1 else 100
        client = Client(self.ip, MemoryConsumerServer.port, timeout = timeout,
                        name = "%s-MC-Benchmark-client" % self.ip)
        start = time.time()
        try:
            msg = client.send_recv("{'load':%i,'duration':%i}" % (load, duration))
        except socket.error as err:
            raise BenchmarkError("Error in communication: %s" % err)
        finally:
            client.close()

        dt = time.time() - start

        try:
            dct = eval(msg)
        except SyntaxError:
            raise BenchmarkError("Error while parsing response: %s" % msg)

        if (dt < duration):
            time.sleep(duration - dt)

        return dct

    @staticmethod
    def getFields():
        return set(['throughput', 'hits_rate', 'test_time'])

class MemSlap04406(BenchmarkProgram):
    def __init__(self, keys_dist, vals_dist, cmd_get_percent, win_size, seed=0,
                 **kwargs):
        '''
        keys_dist, vals_dist - key/value distribution
        cmd_get_percent - gets percent (from total requests)
        win_size - window size
        '''
        BenchmarkProgram.__init__(self, "memslap", **kwargs)

        self.config_file = tempfile.NamedTemporaryFile()
        self.config_file.write("\n".join(
                        ["key"] +
                        ["%i %i %.1f" % tpl for tpl in keys_dist] +
                        ["value"] +
                        ["%i %i %.1f" % tpl for tpl in vals_dist] +
                        ["cmd",
                        "0 %.1f" % (1 - cmd_get_percent),
                        "1 %.1f" % cmd_get_percent]))
        self.config_file.flush()
        self.win_size = win_size
        #self.logger.info("config file: %s", self.config_file.name)
        # configure random seed
        if 'seed' in kwargs:
            self.seed = int(kwargs['seed'])
        else:
            self.seed = seed
        #self.logger.info("random seed: %d", self.seed)

    def consume(self, load, duration, cpu_pin):
        '''
        load
        duration
        cpu_pin
        '''
        from exp.prog.Programs import Memcached
        if load == 0:
            return dict((prop, 0) for prop in self.getFields())
        memcachedaddr = "%s:%i" % (self.ip, Memcached.memcached_port)
        #self.logger.info("starting: load: %i, executions: %i", load, duration)

# testing the new version of memslap (1.0.17), where the similar one is called mem*a*slap
#        args = ["memslap",
        args = ["memaslap",
                "--servers=%s" % memcachedaddr,
                "--concurrency=%i" % load,
                "--time=%is" % duration,
                "--win_size=%s" % self.win_size,
                "--stat_freq=5s", # we get stats every 5s
                "--cfg_cmd=%s" % self.config_file.name,
                "--reconnect",
                ]
        if self.seed > 0:
            args.append("--seed=%i" % self.seed)

        ms_output, ms_error = self.popen(args, cpu_pin)  #blocking

        # in case of terminate() was invoked:
        if not self.should_run:
            return {}

        # check for error:
        if len(ms_error) > 0:
            self.logger.warn("Memslap ended with error output:\n%s", ms_error)
            raise BenchmarkError, "memslap error: %s" % ms_error

        output = ms_output

        # first, get a dictionary with lists of the data, and process it
        all_matches = {}
        all_matches['test_time'] = parse_float_list("Run time: (.*)s Ops", output)
        all_matches['samples_num'] = parse_int_list("Ops: (.*) TPS", output)
        all_matches['throughput'] = parse_float_list("TPS: (.*) Net", output)
        all_matches['get_total'] = parse_int_list("cmd_get: (.*)\n", output)
        all_matches['get_misses'] = parse_int_list("get_misses: (.*)\n", output)

        all_matches['measure_duration'] = []
        for t in all_matches['test_time']:
            all_matches['measure_duration'].append(t)

        # sanity check - all input are of the same length
        input_len = -1
        for k in all_matches.keys():
            if input_len == -1:
                input_len = len(all_matches[k])
            elif input_len != len(all_matches[k]):
                raise BenchmarkError, ("failed to parse memslap's %s: %s" %
                                       (k, ms_output))

        # substract things up (so we get the differential)
        for i in range(len(all_matches['test_time']) - 1, 0, -1):
            all_matches['measure_duration'][i] -= all_matches['measure_duration'][i - 1]
            all_matches['samples_num'][i] -= all_matches['samples_num'][i - 1]
            all_matches['get_total'][i] -= all_matches['get_total'][i - 1]
            all_matches['get_misses'][i] -= all_matches['get_misses'][i - 1]

        # calculate the hits (total, percent and rate)
        all_matches['get_hits_total'] = []
        all_matches['get_hits_percent'] = []
        all_matches['hits_rate'] = []
        all_matches['miss_rate'] = []
        for time, total, misses in zip(all_matches['measure_duration'], all_matches['get_total'], all_matches['get_misses']):
            hits = total - misses
            all_matches['get_hits_total'].append(hits)
            all_matches['get_hits_percent'].append(float(hits) / total if total else 0)
            all_matches['hits_rate'].append(float(hits) / time if time else 0)
            all_matches['miss_rate'].append(float(misses) / time if time else 0)

        # now we can nicely put it in an array of dictionaries
        res = []
        for i in range(len(all_matches['hits_rate'])):
            res_data = {}
            for key in all_matches:
                res_data[key] = all_matches[key][i]
            res.append(res_data)
        '''
        res['test_time'] = parse_float("Run time: (.*)s Ops", output)
        res['samples_num'] = parse_int("Ops: (.*) TPS", output)
        res['throughput'] = parse_float("TPS: (.*) Net", output)

        get_total = parse_int("cmd_get: (.*)\n", output)
        get_misses = parse_int("get_misses: (.*)\n", output)
        get_hits = (get_total - get_misses
                    if None not in [get_total, get_misses] else 0)

        res['get_hits_percent'] = (float(get_hits) / get_total
                                   if get_total and get_misses != None else 0)
        res['get_hits_total'] = get_hits
        res["get_total"] = get_total
        res["hits_rate"] = float(get_hits) / res['test_time']

        failed = [k for k, v in res.iteritems() if v is None]
        if failed:
            raise BenchmarkError, ("failed to parse memslap's %s: %s" %
                                   (failed, ms_output))
        else:
            self.logger.debug("Ended with %s" % str(res))
        '''
        return res

    @staticmethod
    def getFields():
        return set(['test_time', 'samples_num', 'throughput',
                    'get_hits_percent', 'get_hits_total', 'get_total',
                    "hits_rate", 'measure_duration'
                    ])

class RemoteMemslap(BenchmarkProgram):
    def __init__(self, keys_dist, vals_dist, cmd_get_percent, win_size, seed, remote_ip, **kwargs):
        BenchmarkProgram.__init__(self, "remote-memslap", **kwargs)
        self.keys_dist = keys_dist
        self.vals_dist = vals_dist
        self.cmd_get_percent = cmd_get_percent
        self.win_size = win_size
        self.seed = seed
        self.ssh = None
        self.net_delay_set = False
        # the ip of the machine from which we run the benchmark
        self.remote_ip = remote_ip if remote_ip != "None" else self.ip

    def set_net_delay(self):
        # Add to guest network delay emulation
        # since local loopback is for send and receive, the delay is twice as it
        # is defineded.
        # http://japhr.blogspot.co.il/2011/07/network-emulation-in-linux.html
        delay = 1  #ms
        while True:
            try:
                delay_cmd = "tc qdisc add dev lo root netem delay %.1fms" % (delay * 0.5)
                ssh = SSHCommunicator(delay_cmd,
                        "root", self.ip, name = "network-delayer", verbose = False)
                err = ssh.communicate(timeout = 60)[1]
            except TimeoutError:
                err = "Timeout Error"
            if len(err) == 0 or err.find("File exists") != -1:
                break

            self.logger.info("Error setting netem in guest: %s", err)
            time.sleep(5)

    def consume(self, load, duration, cpu_pin):
        #if not self.net_delay_set:
        #    self.set_net_delay()
        #    self.net_delay_set = True
        out = ""
        if load == 0:
            return dict((prop, 0) for prop in self.getFields())

        user = ex = ""
        if self.remote_ip != self.ip:
            user = getuser()
            ex = "PYTHONPATH=/home/%s/workspace/moc python -O /home/%s/workspace/moc/exp/prog/remote_memslap.py" % (user, user)
        else:
            # when running on the guest machine the moc directory is at /root/moc
            user = 'root'
            ex = "PYTHONPATH=/%s/moc python -O /%s/moc/exp/prog/remote_memslap.py" % (user, user)

        cmd = [ex, (self.keys_dist, self.vals_dist, self.cmd_get_percent,
                    self.win_size, self.seed, self.ip, load, duration)]

        self.logger.debug("command is: %s", str(cmd))
        self.ssh = SSHCommunicator(cmd,
                              user = user,
                              ip = self.remote_ip,
                              name = "memslap-ssh",
                              verbose = True)

        try:
            start = time.time()
            timeout = duration * 2 if duration != 1 else 100
            out, err = self.ssh.communicate(timeout = timeout)  # blocking
            if duration == 1:
                self.logger.info("Benchmark took: %.1fs, requested: %is", time.time() - start, duration)
            self.ssh = None
            if len(err) > 0:
                if duration == 1:
                    self.logger.info("Benchmark ended with error: %s of length %d", str(err), len(err))
                raise BenchmarkError(err)
        except TimeoutError as ex:
            raise BenchmarkError(ex)
        self.logger.info("\nout: %s\nerr: %s", out, err)
        return eval(out)

    def terminate(self):
        if self.ssh is None:
            return
        self.ssh.terminate()

    def getFields(self):
        return set(['test_time', 'samples_num', 'throughput',
                    'get_hits_percent', 'get_hits_total', 'get_total',
                    "hits_rate"
                    ])


class ApacheBench(BenchmarkProgram):
    def __init__(self, **kwargs):
        BenchmarkProgram.__init__(self, "ab", **kwargs)

    def consume(self, load, duration, cpu_pin):
        if load == 0:
            return dict((prop, 0) for prop in self.getFields())

        dt_address = "http://%s:8080/daytrader/scenario" % self.ip
        self.logger.debug("starting: load: %i, executions: %i" % (load, duration))

        args = map(str, ("ab", "-c", load, "-t", duration, dt_address))
        output, ab_error = self.popen(args, cpu_pin)  # blocking

#        self.logger.debug("\n".join(["=== ab out:", output[0],
#                                     "=== ab err:", output[1]]))
        res = {}
        res['req_sec'] = parse_float("Requests per second:\s+(.*) \[\#/sec\]", output)
        res['ms_req'] = parse_float("Time per request:\s+(.*) \[ms\] \(mean\)", output)
        res['ms_req_across'] = parse_float("Time per request:\s+(.*) \[ms\] \(mean, across all concurrent requests\)", output)
        res['percent95'] = parse_int("95%\s(.*)", output)
        res['test_time'] = parse_float("Time taken for tests:\s+(.*) seconds", output)
        if any((x is None for x in res.values())):
            self.logger.warn("failed to parse ab output: %s", output)
            raise BenchmarkError, "failed to parse ab output: %s" % output
        else:
            self.logger.debug("Ended with %s", str(res))
        return res

    @staticmethod
    def getFields():
        return set(['req_sec', 'ms_req', 'ms_req_across', 'percent95',
                    'test_time'])


if __name__ == "__main__":
    h = RemoteMemslap(
            cmd_get_percent = 0.3,
            keys_dist = ((249, 249, 1),),
            vals_dist = ((1024, 1024, 1),),
            win_size = "100k",
            seed=14,
            remote_ip = "csl-tapuz26"
            )

    print h.consume(4, 2, 0, test = True)

#===============================================================================
# PostgresSQL
#===============================================================================

class PostgresSQLBenchmark(BenchmarkProgram):
    # Sample Output
    # ======================================================
    # starting vacuum...end.
    # transaction type: TPC-B (sort of)
    # scaling factor: 100
    # query mode: simple
    # number of clients: 60
    # number of threads: 20
    # duration: 120 s
    # number of transactions actually processed: 17117
    # tps = 142.297915 (including connections establishing)
    # tps = 156.115797 (excluding connections establishing)
    # statement latencies in milliseconds:
    #     0.002530    \set nbranches 1 * :scale
    #     0.000541    \set ntellers 10 * :scale
    #     0.000563    \set naccounts 100000 * :scale
    #     0.000639    \setrandom aid 1 :naccounts
    #     0.000468    \setrandom bid 1 :nbranches
    #     0.000512    \setrandom tid 1 :ntellers
    #     0.000490    \setrandom delta -5000 5000
    #     0.141871    BEGIN;
    #     2.222018    UPDATE pgbench_accounts SET abalance = abalance + :delta WHERE aid = :aid;
    #     0.230132    SELECT abalance FROM pgbench_accounts WHERE aid = :aid;
    #     0.263907    UPDATE pgbench_tellers SET tbalance = tbalance + :delta WHERE tid = :tid;
    #     0.251859    UPDATE pgbench_branches SET bbalance = bbalance + :delta WHERE bid = :bid;
    #     0.219017    INSERT INTO pgbench_history (tid, bid, aid, delta, mtime) VALUES (:tid, :bid, :aid, :delta, CURRENT_TIMESTAMP);
    #     2.457115    END;

    fields = {
              "scaling_factor" :                dict(function = parse_int,      regexp = "scaling factor: ([0-9]+)"),
              "query_mode" :                    dict(function = parse_string,   regexp = "query mode: (.+)"),
              "number_of_clients" :             dict(function = parse_int,      regexp = "number of clients: ([0-9]+)"),
              "number_of_threads" :             dict(function = parse_int,      regexp = "number of threads: ([0-9]+)"),
              "duration" :                      dict(function = parse_int,      regexp = "duration: ([0-9]+) s"),
              "transactions_count" :            dict(function = parse_int,      regexp = "number of transactions actually processed: ([0-9]+)"),
              "tps_with_connections_time" :     dict(function = parse_float,    regexp = "tps = ([0-9]*\.?[0-9]+) \(including connections establishing\)"),
              "tps_without_connections_time" :  dict(function = parse_float,    regexp = "tps = ([0-9]*\.?[0-9]+) \(excluding connections establishing\)"),

              "query01_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\set nbranches 1 \* :scale"),
              "query02_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\set ntellers 10 \* :scale"),
              "query03_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\set naccounts 100000 \* :scale"),
              "query04_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\setrandom aid 1 :naccounts"),
              "query05_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\setrandom bid 1 :nbranches"),
              "query06_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\setrandom tid 1 :ntellers"),
              "query07_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*\\setrandom delta -5000 5000"),
              "query08_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*BEGIN;"),
              "query09_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*UPDATE pgbench_accounts SET abalance = abalance \+ :delta WHERE aid = :aid;"),
              "query10_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*SELECT abalance FROM pgbench_accounts WHERE aid = :aid;"),
              "query11_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*UPDATE pgbench_tellers SET tbalance = tbalance \+ :delta WHERE tid = :tid;"),
              "query12_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*UPDATE pgbench_branches SET bbalance = bbalance \+ :delta WHERE bid = :bid;"),
              "query13_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*INSERT INTO pgbench_history \(tid, bid, aid, delta, mtime\) VALUES \(:tid, :bid, :aid, :delta, CURRENT_TIMESTAMP\);"),
              "query14_latency" :               dict(function = parse_float,    regexp = "[ \t]*([0-9]*\.?[0-9]+)[ \t]*END;"),
              };

    def __init__(self, threads_count = 1, connection_per_transaction = True,
                 output_log = False, measure_frequency=0, **kwargs):
        self.threads_count = threads_count
        self.connection_per_transaction = connection_per_transaction
        self.output_log = output_log
        self.measure_frequency = measure_frequency

        BenchmarkProgram.__init__(self, "PostgresSQL-pgbench", **kwargs)
        self.logger.debug("measure frequency: %d", self.measure_frequency)

    def getCpuCount(self):
        return min(2, self.threads_count)

    def consume(self, load, duration, cpu_pin):
        '''
        load
        duration
        cpu_pin
        '''
        if load == 0:
            return dict((prop, 0) for prop in self.getFields())
        self.logger.info("starting: load: %i, load duration: %i", load, duration)

        # "The number of clients must be a multiple of the number of threads"
        cur_threads_count = self.threads_count
        if cur_threads_count > load: cur_threads_count = load
        while load % cur_threads_count != 0: cur_threads_count -= 1

        args = ["pgbench",
                "-h", str(self.ip),             # Host Name
                "-U", "postgres",               # Username
                "-r",                           # Report latencies
                "-T", str(duration),            # Duration
                "-c", str(load),                # Number of clients
                "-j", str(cur_threads_count)]  # Number of threads

        if self.connection_per_transaction:
            args += ["-C"]              # Reconnect for each transaction
        if self.output_log:
            args += ["-l"]              # Output log to working directory

        ms_output, ms_error = self.popen(args, cpu_pin)  #blocking

        # in case of terminate() was invoked:
        if not self.should_run:
            return {}

        # check for error:
        if len(ms_error) > 0 and not re.search("starting vacuum\.\.\..*end\.", ms_error, re.DOTALL):
            self.logger.warn("pgbench ended with error output:\n%s", ms_error)
            raise BenchmarkError, "pgbench error: %s" % ms_error

        self.logger.debug("output: %s", ms_output)
        res = self.parseOutput(ms_output)
        ret = res
        if (self.measure_frequency > 0) and (self.measure_frequency < duration):
            ret = []
            samples_num = duration / self.measure_frequency
            self.logger.debug("number of samples: %d", samples_num)
            for s in reversed(range(samples_num + 1)):
                # not changing anything, so a shallow copy is enough
                ret.append(dict(res))
                ret[-1]['test_time'] = max(0, duration - self.measure_frequency * s)
        return ret

    @classmethod
    def getFields(cls):
        return set(cls.fields.keys())

    @classmethod
    def parseOutput(cls, output):
        res = {}
        for field_key in cls.fields:
            function = cls.fields[field_key]["function"]
            regexp = cls.fields[field_key]["regexp"]

            res[field_key] = function(regexp, output)

        return res

#===============================================================================
# Phoronix Test Suite
#===============================================================================

class PhoronixTestSuiteBenchmark(BenchmarkProgram):
    def __init__(self, test, test_parameters = {}, **kwargs):
        self.test = test
        self.test_parameters = test_parameters

        self.test_name = self.test.split("/")[-1]

        self.preset_options = []
        for key, value in self.test_parameters.iteritems():
            self.preset_options.append("%s.%s=%s" % (self.test_name, key, value))

        if self.preset_options:
            self.preset_options_str = "PRESET_OPTIONS='%s' " % (";".join(self.preset_options))
        else:
            self.preset_options_str = ""

        self.cmd = self.preset_options_str + "phorinix-test-suite python_results.run " + self.test

        BenchmarkProgram.__init__(self, "PhoronixTestSuiteBenchmark-%s" % self.test, **kwargs)

    def getCpuCount(self):
        # Does not need its own CPU. Will use system CPUs.
        return 0;

    def consume(self, load, duration, cpu_pin):
        self.logger.info("Apply load: %i (not-really-applicable) for min-duration: %i", load, duration)

        args = ["ssh",
                "root@%s" % str(self.ip),
                self.cmd]

        ms_output, ms_error = self.popen(args, cpu_pin)  #blocking

        # in case of terminate() was invoked:
        if not self.should_run:
            return {}

        # check for error:
        if len(ms_error) > 0:
            self.logger.warn("Test %s ended with error output:\n%s", self.test, ms_error)
            raise BenchmarkError, "Test %s error: %s" % (self.test, ms_error)

        res = self.parse_output(ms_output)

        # The actual output of the test
        self.logger.debug(res[0])

        # The output parameters (dict) of the test
        return res[1]

    @staticmethod
    def getFields(cls):
        return {"result","scale","title","arguments","arguments-description",
                "estimated-run-time","all-results","avg","min","max", "std-dev"}

    @staticmethod
    def parse_output(output):
        res = literal_eval(output)

        if not isinstance(res, list):
            res = [res]

        for i, item in enumerate(res):
            if isinstance(item, str):
                res[i] = item.strip()

        return res
