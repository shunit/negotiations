'''
Created on Jun 25, 2012

@author: eyal
'''
from mom.Collectors.HostMemory import HostMemory
from etc.TerminatableThread import TerminatableThread
import time
import logging
from mom.Comm.Messages import MessageTargetMemory
from etc.PySock import Client
from mom.Comm.GuestServer import GuestServer

class DynamicMemCtrl(TerminatableThread):

    def __init__(self, check_interval_time, name = "DynamicMemCtrl"):
        TerminatableThread.__init__(self, name = name)
        self.mem_monitor = HostMemory(None)
        self.logger = logging.getLogger(name)
        self.check_interval_time = check_interval_time

        self.msg = MessageTargetMemory()
        self.guest_server_client = Client("localhost", GuestServer.port_mem, 10,
                             "DynamicMemCtrlClient")

    def change_mem_func(self, mem_total, mem_usage, mem_cache_and_buff):
        '''
        This function should be overridden
        Gets target_memory in MB, and should change the size of the memory
        of the program to that target memory.
        '''
        raise NotImplementedError

    @property
    def current_mem(self):
        '''
        This property should be overridden
        returns the current memory of program
        '''
        raise NotImplementedError

    def run(self):
        # main control loop
        # update target memory according to the total memory
        self.logger.info("Started")
        while self.should_run:
            # collect current statistics
            try:
                data = self.mem_monitor.collect()

                mem_available = data["mem_available"]
                mem_unused = data["mem_unused"]
                cache_and_buff = data["cache_and_buff"]

                usage = mem_available - mem_unused
            except Exception as ex:
                self.logger.error("Error getting data on available memory: %s", ex)
            else:
                try:
                    # take the minimum from the host's hint and current allocation,
                    # so we won't take more memory than available when memory is
                    # growing, and we will free memory when shrinkage is about to
                    # happen.
                    target = self.msg.communicate(self.guest_server_client)["target_mem"]
                    if target is None:
                        raise ValueError("Missing target memory")

                    hint = int(target)
                    total = min(mem_available, hint) if hint > 0 else mem_available

                    self.logger.info("Got hint %i, available: %i", hint, mem_available)

                except Exception as ex:
                    self.logger.warn("Error getting hint on target mem: %s", ex)
                    total = mem_available

                self.logger.info("State: {total: %i, usage: %i}", total, usage)

                try:
                    self.change_mem_func(total, usage, cache_and_buff)
                except Exception as ex:
                    self.logger.error("Failed to update the program memory: %s",ex)

            time.sleep(self.check_interval_time)

        self.logger.info("Ended")
