'''
Created on Apr 10, 2013

@author: eyal
'''
import sys
from exp.prog.BenchmarkPrograms import MemSlap04406, BenchmarkError
import logging 
#from exp.remote.remote_prog import set_core_unlimited

if __name__ == '__main__':
    params = " ".join(sys.argv[1:]).strip()

    keys_dist, vals_dist, cmd_get_percent, win_size, seed, machine_ip, load, duration = eval(params)

    memslap = MemSlap04406(keys_dist, vals_dist, cmd_get_percent, win_size, 
                           seed = seed, ip = machine_ip)

    try:
#        set_core_unlimited()
        ret = memslap.consume(load, duration, cpu_pin = 0)
    except BenchmarkError as ex:
        sys.stderr.write(str(ex))
        sys.exit(1)

    sys.stdout.write(str(ret))
    sys.exit(0)




