//#include <sys/types.h>
//#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h> // memset
#include <unistd.h> // access, usleep
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h> // SIGINT, SIGTERM
#include <netdb.h>

#define MB (1<<20)
#define CONTROL_PORT 9892
#define BUFSZ 256
#define MAX_MB 1024

sem_t sem, quit_sem;
unsigned int load = 0;
int quit = 0;

void signal_quit() {
	sem_wait(&quit_sem);
	quit = 1;
	sem_post(&quit_sem);
}

int should_run() {
	sem_wait(&quit_sem);
	int val = quit;
	sem_post(&quit_sem);
	return !val;
}

double time_diff(struct timespec *start, struct timespec *end)
{
	struct timespec temp;
	temp.tv_sec = end->tv_sec - start->tv_sec;
	temp.tv_nsec = end->tv_nsec - start->tv_nsec;
	if (temp.tv_nsec < 0) {
		--temp.tv_sec;
		temp.tv_nsec += 1000000000;
	}
	return (double)temp.tv_sec + ((double)temp.tv_nsec) / 1000000000;
}

unsigned int get_load() {
	unsigned int val;
	sem_wait(&sem);
	val = load;
	sem_post(&sem);
	return val;
}

void set_load(unsigned int val) {
	sem_wait(&sem);
	load = val;
	sem_post(&sem);
}

void write_chunk(void* where) {
	memset(where, 0, MB);
}

sem_t stats_sem;
int stats_num = 0, stats_mb = 0;
double stats_load_avg = 0, stats_sec = 0;

void add_stats(double dt, int dmb, int load) {
	sem_wait(&stats_sem);
	int n = stats_num;
	stats_load_avg = (float)(n * stats_load_avg + load) / (n + 1);
	stats_num++;
	stats_sec += dt;
	stats_mb += dmb;
	sem_post(&stats_sem);
}

void get_stats(int *load, double *perf) {
	sem_wait(&stats_sem);
    if (stats_num == 0) {
        *load = get_load();
        *perf = 0;
	}else {
        *load = stats_load_avg;
        *perf = (float)(stats_mb) / stats_sec;
        stats_num = 0;
        stats_sec = 0;
        stats_mb = 0;
        stats_load_avg = 0;
	}
    sem_post(&stats_sem);
}

unsigned long realloc_chunks(void** chunks, unsigned int mb) {
	static unsigned long last_n = 0;
	unsigned long chunk, nchunks = mb;

	if (last_n < nchunks) {
		for (chunk = 0; chunk < nchunks; ++chunk) {
			if (chunk >= last_n) {
				chunks[chunk] = (void*)malloc(MB);
				if (!chunks[chunk]) {
					perror("couldn't allocate chunks\n");
					exit(1);
				}
			}
			write_chunk(chunks[chunk]);
		}
	}
	else if (nchunks < last_n) {
		for (chunk = 0; chunk < last_n ; ++chunk) {
			(chunk < nchunks) ? write_chunk(chunks[chunk]) : free(chunks[chunk]);
		}
	}
	last_n = nchunks;
	return nchunks;
}

int sock_receive(int sockfd, char* msg) {
	int i = 0, n;
	while (i < BUFSZ-1) {
		n = read(sockfd, &msg[i], (BUFSZ - i) * sizeof(char));
		if (n < 0)
			perror("server error reading from socket");
		i += n;
		if (msg[i] == '\n' || msg[i] == '\0') {
			break;
		}
	}
	msg[i] = '\0';
	return i;
}

int sock_setup() {
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		perror("problem starting control server");
		return -1;
	}
	// set SO_REUSEADDR on a socket to true (1):
	int optval = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	// set timout
	struct timeval timeout;
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;
	setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
	setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
	return sockfd;
}

int sock_listen(int port) {
	struct sockaddr_in serv_addr;
	int sockfd = sock_setup();
	if (sockfd <= 0) {
		return -1;
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	// bind
	if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		close(sockfd);
		sockfd = 0;
		perror("server bind");
	}
	if (listen(sockfd, 5) < 0) {
		close(sockfd);
		sockfd = 0;
		perror("server listen");
	}
	return sockfd;
}

void session(int sockfd) {
	char msg[BUFSZ];
	if (sock_receive(sockfd, msg) < 0) {
		printf("Problem receiving message\n");
		return;
	}
	printf("Received massage: %s\n", msg);
	if (strncmp(msg, "finish", 6) == 0) {
		printf("Got quit message\n");
		signal_quit();
	}
	else if (strncmp(msg, "setload", 7) == 0) {
		unsigned int new_mb = atof(&msg[8]);
		printf("Got new load: %u\n", new_mb);
		set_load(new_mb);
	}
	else if (strncmp(msg, "get_report", 10) == 0) {
		printf("Got report request\n");
		int load;
		double perf;
		get_stats(&load, &perf);
		char str[BUFSZ];
		sprintf(str, "%d,%f\n", load, perf);
		if (write(sockfd, str, strlen(str)) < 0) {
			perror("Error sending performance");
			close(sockfd);
			return;
		}
		printf("reported: %s\n", str);
	}
	else {
		printf("Unknown massage: %s\n", msg);
	}
	close(sockfd);
}

void* control_server(void *input) {
	int sockfd = 0;
	while(should_run()) {
		if (sockfd <= 0) {
			sockfd = sock_listen(CONTROL_PORT);
			if (sockfd <= 0) {
				sleep(1);
				continue;
			}
		}
		struct sockaddr_in cli_addr;
		socklen_t clilen = sizeof(cli_addr);
		int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if (newsockfd <= 0) {
			close(sockfd);
			sockfd = 0;
			perror("server accept error");
			continue;
		}
		session(newsockfd);
	}
	close(sockfd);
	return 0;
}

int main(int argc, char *argv[]) {
	unsigned long max_mb = MAX_MB;
	if (argc >= 2) {
		max_mb = atoi(argv[1]);
	}
	sem_init(&sem, 0, 1);
	sem_init(&stats_sem, 0, 1);
	sem_init(&quit_sem, 0, 1);

	set_load(0);

	// allocate chunks
	void** chunks = (void**)malloc(max_mb * sizeof(void*));

	// initiate quit mechanism
	signal(SIGINT, signal_quit);
	signal(SIGTERM, signal_quit);

	// start server
	pthread_t server_thread;
	pthread_create(&server_thread, NULL, control_server, NULL);

	// start the test
	struct timespec start, now;
	unsigned long i, nchunks;
	i = -1;
	while(should_run()) {
		unsigned int load = get_load();
		nchunks = realloc_chunks(chunks, load);

		double dt, sec = 0;
		while(should_run()) {
			i++;
			if (nchunks == 0) {
				sleep(1);
				break;
			}
			if (i >= nchunks) i = 0;
			// do work
			clock_gettime(CLOCK_REALTIME, &start);
			write_chunk(chunks[i]);
			clock_gettime(CLOCK_REALTIME, &now);
			// sleep a little bit
			dt = time_diff(&start, &now);
			usleep(300000);
			add_stats(dt, 1, load);
			if (load != get_load()) break;
		}
	}
	// free and close resources
	realloc_chunks(chunks, 0);
	free(chunks);
	return 0;
}
