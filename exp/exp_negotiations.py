"""
Created on Aug 31, 2015

@author: alexbousso
"""
import exp.core.Loads
from exp.prog.Programs import Guest_Memcached, Host_Memcached, \
    Guest_MemoryConsumer, Host_MemoryConsumer
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_output_path
import os
from exp.core.VMDesc import get_vm_desc
from exp.core.ExpMachine import ExpMachineProps
from exp.core.Experiment import Experiment
from exp.core.Loads import LoadBinary
from mom.Collectors.HostMemory import HostMemory

t_auction = [3, 1, 8]
duration = 30 * sum(t_auction)
# duration = 180 * 60
oc = 3
frequency = [60]  # [600, 360, 180, 20]


# alloc_diff = 0

class Configuration(object):
    def __init__(self, prog_args, bm_args, base_mem, saturation_mem,
                 profiler_file, profiler_entry, revenue_func_str,
                 load, load_interval, vm_number, val_switch_func=None, max_vcpus=1, swappiness=100,
                 max_total_side_payment=0.0, min_compensation=1.0):
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem  # The memory quantity for which the profiler function saturates
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.revenue_func_str = revenue_func_str
        self.val_switch_func = val_switch_func
        self.max_vcpus = max_vcpus
        self.swappiness = swappiness

        self.adviser = {'name': 'AdviserNegotiations',
                        'profiler': profiler_file,
                        'advice_entry': profiler_entry,
                        'rev_func': revenue_func_str,
                        'base_mem': base_mem, 'memory_delta': 10,
                        'vm_number': vm_number,
                        'num_of_guests': 2,
                        'comm_timeout': 2,  #, # TODO(alexbousso): Think about this parameter
                        'max_total_side_payment': max_total_side_payment,
                        'min_compensation': min_compensation,
                        'enable_negotiations': True}
#                        'alloc_diff': alloc_diff}

configs = {}  # config set pre frequency
for f in frequency:
    configs[f] = [
        Configuration(  # MCD
                        prog_args=Guest_Memcached(
                            spare_mem=50,
                            init_mem_size=10,
                            update_interval=1
                        ).command_args,
                        bm_args=Host_Memcached(
                            cmd_get_percent=0.3,
                            keys_dist=[(249, 249, 1.0)],
                            vals_dist=[(1024, 1024, 1.0)],
                            win_size="500k",
                        ).command_args,
                        base_mem=600,
                        saturation_mem=2500,
                        profiler_file="doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
                        profiler_entry="hits_rate",
                        load=exp.core.Loads.LoadBinary(v1=0, v2=1, T=f, T0=0),
                        load_interval=duration - 3 * 60,
                        revenue_func_str=['lambda x:0.5*x', 'lambda x:1.1*x'],
                        val_switch_func=LoadBinary(v1=0, v2=1, T=2 * f, T0=0),
                        vm_number=1,
                        max_total_side_payment=0.8,
                        min_compensation=0.5,
                        ),
        Configuration(  # MC
                        prog_args=Guest_MemoryConsumer(
                            spare_mem=50,
                            saturation_mem=2000,
                            update_interval=1,  # 0.1,
                            sleep_after_write=0.1,
                        ).command_args,
                        bm_args=Host_MemoryConsumer().command_args,
                        base_mem=600,
                        saturation_mem=2500,
                        profiler_file='doc/profiler-mc-spare-50-satur-2000-tapuz21.xml',
                        profiler_entry="hits_rate",
                        load=exp.core.Loads.LoadConstant(10),
                        load_interval=5,
                        revenue_func_str=['lambda x:5*x', 'lambda x:10*x'],
                        val_switch_func=LoadBinary(v1=0, v2=1, T=f, T0=0),
                        vm_number=2,
                        max_total_side_payment=0.8,
                        min_compensation=0.5,
                        )
    ]

if __name__ == '__main__':
    LogUtils("debug")
    name = "negotiations"
    output_path = set_output_path("exp", name)
    for f, config in configs.iteritems():
        n = len(config)
        vm_names = ["vm-%i" % (i + 1) for i in range(n)]
        vms = {}

        min_mem = sum([c.base_mem for c in config])
        max_mem = sum([c.saturation_mem for c in config])
        # TODO: Why is auction_mem = max_mem - min_mem, and not the system's memory - min_mem?
        auction_mem = int((max_mem - min_mem) / oc)
        total_mem = HostMemory(None).collect()["mem_available"]
        host_mem = 500
        print "Overcommitment: %.2f\nTotal memory: %i\nAuction memory: %i\nHost memory: %i\nFrequency: %i" % (
        oc, total_mem, auction_mem, host_mem, f)

        for name, conf in zip(vm_names, config):
            vms[name] = ExpMachineProps(
                adviser_args=conf.adviser,
                prog_args=conf.prog_args,
                bm_args=conf.bm_args,
                load_func=conf.load,
                load_interval=conf.load_interval,
                desc=get_vm_desc(name),
                val_switch_func=conf.val_switch_func,
                max_vcpus=conf.max_vcpus,
                swappiness=conf.swappiness,
            )
            vms[name]['desc']['base_mem'] = conf.base_mem
            vms[name]['desc']['max_mem'] = conf.saturation_mem  # total_mem

        p0 = 0.0
        print "batch starting with p0 = %.2f and frequency = %i" % (p0, f)
        exp_out = os.path.join(output_path, "p0-%.2f-freq-%i" % (p0, f))
        os.mkdir(exp_out)
        moc_args = ("MocAuctioneer", (host_mem, t_auction, auction_mem))
        Experiment(
            moc_args=moc_args,
            vms_desc=vms,
            output_path=exp_out,
            duration=duration,
            verbosity="debug",
        ).start()
