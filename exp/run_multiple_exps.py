import sys
sys.path.append("/home/shunita/moc")
import os
import exp.pareto_helper as ph
from subprocess import call
import numpy as np
import pandas
from pprint import pprint

#OV_AT = [(0.1,0.1)]*5
REPETITIONS = 15

OV_AT=[(0.1,0.1)]

DRY_RUN = False
WORKING_DIR = "/home/shunita/moc/"
#EXP_SCRIPT = WORKING_DIR + "exp/exp_coalition_many_guests_mc.py"
EXP_SCRIPT = WORKING_DIR + "exp/exp_coalition_many_guests_memcached.py"
GRAPH_SCRIPT = WORKING_DIR + "out/SW_graph_generator.py"
OUTPUT_FILE = "/home/shunita/tmp/mixed_env_4_changes_memcached_insiders_2710_start.csv"

def geometric_mean(negparams_df):
    x = []
    rounds = set(negparams_df["round"].values)
    for i in rounds:
        product = 1
        guests_num = 0
        round_i = negparams_df.loc[negparams_df["round"] == i]
        for j in (round_i["bill_wo_coalition"]-round_i["bill"]):
            product*=j
            guests_num+=1
        x.append(pow(product,1.0/guests_num))
    return np.mean(x)

def find_stable_round(negparams_df):
    if DRY_RUN:
        return 0
    last_round = negparams_df["round"].max()
    last_coalition = set(negparams_df.loc[negparams_df["round"] == last_round].vm_number)
    for r in range(last_round, -1, -1):
        guests_in_coalition = set(negparams_df.loc[negparams_df["round"] == r].vm_number)
        if last_coalition != guests_in_coalition:
            return r

def data_coalition_per_guest(negparams_df):
    rics = {}
    profits = {}
    for i in range(1,11):
        rics[i] = 0
        profits[i] = []

    for i in set(negparams_df["round"].values):
        round_i = negparams_df.loc[negparams_df["round"] == i]
        for g in round_i["vm_number"]:
            rics[g]+=1
        for g in round_i["vm_number"].values:
            guest = round_i.loc[round_i["vm_number"] == g]
            profit = (guest["bill_wo_coalition"]-guest["bill"]).values[0]
            profits[g].append(profit)

    for i in range(1,11):
        profits[i] = np.mean(profits[i])
    return rics, profits

def run_cmd(cmd_str):
    if DRY_RUN:
        final_cmd_str = 'echo "%s"' % cmd_str
    else:
        final_cmd_str = cmd_str
    call(final_cmd_str, shell=True)

if DRY_RUN:
    print "Dry run: about to run %d experiments" % (REPETITIONS*len(OV_AT))
if not DRY_RUN:
    f = open(OUTPUT_FILE, "w")
    f.write("exp_id,ov,at,valuations,stable_round, avg_profit,avg_profit_stable,geometric_mean,geometric_mean_stable,"+
            "ric1,ric2,ric3,ric4,ric5,ric6,ric7,ric8,ric9,ric10,"+
            "profit1,profit2,profit3,profit4,profit5,profit6,profit7,profit8,profit9,profit10\r\n")
    f.close()
# pareto 1.36 valuations
valuation_sets = [[2.452007, 1.529264, 1.512367, 1.464463, 1.414464, 1.366215, 1.271573, 1.258867, 1.222780, 1.103894],
                  [10.729670, 4.334625, 2.635574, 2.033083, 2.031084, 1.912476, 1.432569, 1.334670, 1.215737, 1.195154],
                  [3.662201, 2.954508, 2.724356, 2.110818, 1.398487, 1.361839, 1.316922, 1.263199, 1.221837, 1.111510],
                  [15.254605, 6.699919, 2.682537, 2.628321, 1.448605, 1.436226, 1.349103, 1.286640, 1.140074, 1.020414],
                  [10.923103, 7.869019, 6.043720, 5.034991, 3.701090, 3.130627, 2.480853, 2.463591, 1.403566, 1.136985]]

valuation_sets_10 = [#[18.067364, 9.545946, 5.849115, 4.216838, 3.778930, 2.343592, 1.968208, 1.841472, 1.808812, 1.408815],
                     [9.221481, 3.091974, 2.868166, 2.848148, 2.736943, 2.610075, 2.334122, 1.751624, 1.470887, 1.125221],
                     [11.952399, 4.165540, 3.972204, 3.486773, 2.415298, 1.527582, 1.322430, 1.253589, 1.095108, 1.012303],
                     [4.847068, 2.046923, 1.381472, 1.376244, 1.337104, 1.274357, 1.109668, 1.074225, 1.020254, 1.003595],
                     [6.702371, 2.854401, 2.363483, 2.216423, 1.563732, 1.555590, 1.420093, 1.342293, 1.243625, 1.095554],
                     [2.452007, 1.529264, 1.512367, 1.464463, 1.414464, 1.366215, 1.271573, 1.258867, 1.222780, 1.103894],
                    # [10.729670, 4.334625, 2.635574, 2.033083, 2.031084, 1.912476, 1.432569, 1.334670, 1.215737, 1.195154],
                     [3.662201, 2.954508, 2.724356, 2.110818, 1.398487, 1.361839, 1.316922, 1.263199, 1.221837, 1.111510],
                    # [15.254605, 6.699919, 2.682537, 2.628321, 1.448605, 1.436226, 1.349103, 1.286640, 1.140074, 1.020414],
                    # [10.923103, 7.869019, 6.043720, 5.034991, 3.701090, 3.130627, 2.480853, 2.463591, 1.403566, 1.136985],
                     [10.315041, 3.392282, 1.863920, 1.855561, 1.731026, 1.432961, 1.320261, 1.166809, 1.067714, 1.037517],
                     [5.638862, 3.608985, 3.544568, 3.311139, 2.883530, 2.573234, 2.178350, 1.945268, 1.657465, 1.095579],
                     [10.771824, 5.562753, 4.968284, 3.841721, 2.952462, 2.206856, 1.159877, 1.108661, 1.058556, 1.039648],
                     [4.875342, 4.399321, 4.034680, 2.613515, 2.134818, 2.038830, 1.608642, 1.462665, 1.443692, 1.088154],
                     [4.113474, 3.502848, 2.577196, 2.391485, 2.185031, 1.836948, 1.826977, 1.215959, 1.132660, 1.072360],
                     [7.416537, 7.359972, 4.944052, 4.002625, 2.078023, 2.013369, 1.780168, 1.667001, 1.405638, 1.034953],
                     [7.672754, 2.065051, 1.710576, 1.674090, 1.473158, 1.292571, 1.140271, 1.108293, 1.106181, 1.084636],
                     [5.720345, 2.948571, 1.280346, 1.239558, 1.236972, 1.218382, 1.158429, 1.102493, 1.022993, 1.020690],
                    # [73.060148, 3.908782, 2.683075, 2.398720, 2.239473, 1.946706, 1.305083, 1.217589, 1.160467, 1.009319],
                     [6.271149, 2.932920, 2.649056, 2.648928, 2.379417, 2.198676, 1.377239, 1.245653, 1.212983, 1.113948],
]
# pareto 1.1 valuations
#valuation_sets = [[4.216470, 3.737545, 3.526213, 3.387943, 1.698870, 1.506198, 1.500770, 1.252180, 1.141526, 1.014696],
#                  [4.932826, 3.373990, 2.682979, 1.386416, 1.310077, 1.297183, 1.262187, 1.168099, 1.137644, 1.013077],
#                  [26.591485, 2.564922, 2.084027, 1.584389, 1.514949, 1.427680, 1.316512, 1.156232, 1.141248, 1.105561],
#                  [14.324765, 7.402853, 4.392012, 3.803069, 2.891742, 2.723101, 2.666334, 1.926195, 1.888409, 1.629723],
#                  [15.537413, 7.715586, 1.862389, 1.711844, 1.706820, 1.609052, 1.563405, 1.400091, 1.243103, 1.044936]]

# pareto 1.02 valuations
#valuation_sets = [[8.802162, 3.970484, 2.242041, 2.141975, 1.965931, 1.894461, 1.721067, 1.349500, 1.211452, 1.087339],
#                  [8.518436, 7.566558, 6.747786, 3.788178, 2.834589, 2.210021, 2.185143, 1.826368, 1.437256, 1.030750],
#                  [4.417844, 4.071452, 3.897768, 3.394412, 2.042930, 1.693579, 1.430498, 1.031827, 1.030115, 1.004273],
#                  [5.069177, 2.075139, 1.984187, 1.615046, 1.238864, 1.122596, 1.119204, 1.117895, 1.087132, 1.062128],
#                  [6.106214, 4.306308, 2.302306, 1.796027, 1.790700, 1.622792, 1.621592, 1.427476, 1.329052, 1.061895]]

for i in range(REPETITIONS):
    valuations = valuation_sets_10[i]
    val_string = " ".join(["%.5f"%val for val in valuations])
    for ov_at in OV_AT:
        #exp_str = "python %s --offer_value=%s --accept_threshold=%s --valuations %s " % (EXP_SCRIPT, ov_at[0], ov_at[1], val_string)
        # Random neogtiation profile
        exp_str = "python %s --random_neg_params --valuations %s" % (EXP_SCRIPT, val_string)
        run_cmd(exp_str)
        exp_dirs = os.listdir("/home/shunita/moc-output/exp/")
        exp_id = max([int(d.split("-")[1]) for d in exp_dirs if d.startswith("coalitions-")])
        
        gen_graphs_str = "python %s --exp_ids %s --num_guests 10" % (GRAPH_SCRIPT, exp_id)
        run_cmd(gen_graphs_str)
        df = pandas.read_csv("/home/shunita/moc-output/exp/coalitions-%s/negparams.csv" % exp_id)
        r = find_stable_round(df) + 2
        rounds_per_guest, profits_per_guest = data_coalition_per_guest(df)
        ric_str = ",".join([str(rounds_per_guest[i]) for i in range(1, 11)])
        profit_str = ",".join([str(profits_per_guest[i]) for i in range(1, 11)])
        stable = df.loc[df["round"]>=r]
        if not DRY_RUN:
            f = open(OUTPUT_FILE, "a")
            f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\r\n" % (exp_id, ov_at[0], ov_at[1], val_string, r, 
                                           np.mean(df["bill_wo_coalition"]-df["bill"]), 
                                           np.mean(stable["bill_wo_coalition"]-stable["bill"]),
                                           geometric_mean(df), geometric_mean(stable), ric_str, profit_str))
            f.close()


