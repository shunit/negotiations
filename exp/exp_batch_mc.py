'''
Created on Apr 1, 2012

@author: eyal
'''
from exp.common.exp_batch import batch
from exp.prog.Programs import Guest_MemoryConsumer, Host_MemoryConsumer
from exp.core import Loads
from exp.common.valuations import get_valuation, distributions, shapes_mc

shape = shapes_mc[0]
dist = distributions[0]

spare = 50
saturation = 2000
base0 = 600
init_mem = 600
T = 200
vms_num = range(4, 15 + 1, +1)
mid_load = 6
max_load = 10
auction_ts = [3, 1, 8]
exp_time = 60  #min

prog_args = Guest_MemoryConsumer(
                 spare_mem = spare,
                 saturation_mem = saturation,
                 update_interval = 0.5,
                 sleep_after_write = 0.1,
                 ).command_args
bm_args = Host_MemoryConsumer().command_args

loads = {}
for n in vms_num:
    loads[n] = []
    m = n / 2
    for i in range(m):
        a = int(float(max_load - mid_load - 1) / (m - 1) * (i)) + 1
        T0 = T / m * i
        loads[n].append(Loads.LoadBinary(v1 = mid_load - a, v2 = mid_load + a,
                                         T = T, T0 = T0))
        loads[n].append(Loads.LoadBinary(v1 = mid_load + a, v2 = mid_load - a,
                                         T = T, T0 = T0))
    if n % 2 != 0:
        loads[n].append(Loads.LoadConstant(val = mid_load))

# make the vms description dictionary
valuations = {}
for n in vms_num:
    valuations[n] = get_valuation(n, shape, dist, factor = 1000)

def the_exp():
    batch(
          vms_num = vms_num,
          alpha = 1.0,
          p0 = 0.0,
          start_cpu = 1,
          base0 = base0,
          init_mem = init_mem,
          prifiler_file = ('doc/profiler-mc-spare-%s-satur-%s.xml' % (spare, saturation)),
          profiler_entry = 'hits_rate',
          revenue_func_dict = valuations,
          bid_collection_interval = auction_ts[0],
          calculation_interval = auction_ts[1],
          notification_interval = auction_ts[2],
          rounds = exp_time * 60 / sum(auction_ts),
          load_funcs_dict = loads,
          load_interval = 10,
          prog_args = prog_args,
          bm_args = bm_args,
          host_mem = 500,
          saturation_mem = saturation,
          verbosity = "info",
          name = "mc-" + shape + "-" + dist
          )

if __name__ == '__main__':
    the_exp()
