#! /usr/bin/env python
'''
Created on May 20, 2014

@author: Liran Funaro
'''
from matplotlib import use
from etc.NamedParameters import parse_parameters
from etc.Settings import Settings
use('pdf')
from exp.common.MultiExperiments import MultiExperiments
from etc.publish import report_kwargs, SERVER_CSM, publish_result, SERVER_T2
from exp.parameters.postgressql import get_postgressql_parameters
from out.common.EvaluationOutput import EvaluationOutputPlotter

from exp.prog.Programs import Guest_PostgresSQL, Host_PostgresSQL
import sys
from out.common.views import *

PGBENCH_VIEWS = [VIEW_MEMORY_SUMMERY, VIEW_MEMORY_SWAP, VIEW_SQL_EVALUATION,
             VIEW_SQL_PERFORMANCE, VIEW_SQL_PERF_FOR_MEM, VIEW_SQL_PERF_FOR_MEM_COMPARE,
             VIEW_SQL_QUERIES, VIEW_CPU ]

USERS_EMAILS = {
                "alexbousso": "alexbousso@gmail.com"
                }

USERS_SERVERS = {
                 "fonaro": SERVER_CSM,
                 }

def publish_for_current_user():
    user_server = USERS_SERVERS.get(Settings.username().lower(), None)
    if user_server is not None:
        return publish_result("research/all-outputs", user_server)

def report_to_current_user(subject, **kwargs):
    user_server = USERS_SERVERS.get(Settings.username().lower(), None)
    user_email = USERS_EMAILS.get(Settings.username().lower(), None)
    if user_server is not None and user_email is not None:
        return report_kwargs(user_email, subject, user_server, **kwargs)

def lirans_on_start(**kwargs):
    publish_for_current_user()

def lirans_on_finish(**kwargs):
    # Extract some data
    exp_error = kwargs["exp_error"]
    exp_type = kwargs["type"]
    exp_name = kwargs["exp_name"]
    sub_exp_name = kwargs["sub_exp_name"]

    if not exp_error:
        subject = "Experiment success: %s - %s - %s" % (exp_type, exp_name, sub_exp_name)

        # Plot
        try:
            EvaluationOutputPlotter(exp_type, exp_name, PGBENCH_VIEWS).plotAll()
        except Exception as e:
            kwargs["plot_error"] = str(e)
    else:
        subject = "Experiment failed: %s - %s - %s" % (exp_type, exp_name, sub_exp_name)

    # Publish
    link = publish_for_current_user()
    if link is not None:
        kwargs["link"] = '%s/%s/%s/%s' % ( link, exp_type, exp_name, sub_exp_name )

    report_to_current_user(subject, **kwargs)

if __name__ == '__main__':
    experiments_params = get_postgressql_parameters().parse(sys.argv[1:])

    if experiments_params:
        mexp = MultiExperiments(
                 name = "psql",

                 exp_type = experiments_params[0]["exp_type"],
                 vm_names = experiments_params[0]["vm_names"],

                 guest_prog = Guest_PostgresSQL,
                 host_prog = Host_PostgresSQL,

                 guest_master_image = experiments_params[0]["master_image"],
                 guest_max_vcpus = experiments_params[0]["max_vcpus"],

                 verbosity = "info",

                 on_start = lirans_on_start,
                 on_finish = lirans_on_finish,

                 cmd_args = sys.argv
                 )

        for p in experiments_params:
            mexp.add_experiments(
                 memory_functions = p["memory_functions"],
                 load_functions = p["load_functions"],
                 load_intervals = p["load_intervals"],
                 guest_swappiness = p["guest_swappiness"],

                 guest_prog_kwargs = p["guest_prog_kwargs"],
                 host_prog_kwargs = p["host_prog_kwargs"],
                 guest_image_cow = p["guest_image_cow"],
                 )

        user_input = raw_input("Continue? (y/n): ")
        if user_input == 'y':
            mexp.start()
    else:
        print "No parameters available for an experiment."
