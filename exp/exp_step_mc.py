#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: eyal
'''
from exp.prog.Programs import Guest_MemoryConsumer, Host_MemoryConsumer
from exp.common.StepExperiments import StepExperiments

if __name__ == '__main__':
    prog_args = Guest_MemoryConsumer(
                 spare_mem = 50,
                 saturation_mem = 2000,
                 update_interval = 0.1,
                 sleep_after_write = 0.1,
                 ).command_args

    bm_args = Host_MemoryConsumer().command_args

    StepExperiments(
                    name = "step-MC",
                    vm_name = "vm-1",

                    step_timings = [[100, 100, 100]],
                    load_interval = [10],
                    loads = [1, 5, 10],

                    mem_bases = [500],
                    mem_steps = [1000],

                    prog_args = prog_args,
                    bm_args = bm_args,

                    start_cpu = 3,
                    verbosity = "info"
                    ).start()

