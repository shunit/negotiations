"""
Created on September 2017

@author: shunita
An experiment with 10 guests running Memcached and coalition advisers.
"""
import sys
sys.path.append("/home/shunita/moc")
import exp.core.Loads
from exp.prog.Programs import Guest_Memcached, Host_Memcached, \
    Guest_MemoryConsumer, Host_MemoryConsumer
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_output_path
import os
from exp.core.VMDesc import get_vm_desc
from exp.core.ExpMachine import ExpMachineProps
from exp.core.Experiment import Experiment
from exp.core.Loads import LoadBinary, LoadMultiStep
from mom.Collectors.HostMemory import HostMemory
import argparse
import random
import time

t_auction = [3, 1, 8]
# Define duration by rounds:
auction_rounds = 10
duration = auction_rounds * sum(t_auction)
# It's also possible to define it by seconds (minutes)
# duration = 180 * 60

oc = 3

# For an experiment where everyone gets saturation mem
#oc = 1

frequency = [60]  # [600, 360, 180, 20]

BASE_MEM = 600

ADVISER = "AdviserCoalitions"
#ADVISER = "AdviserProfitEstimatorAge"

ENABLE_NEGOTIATIONS = True
LEAVE_IF_SMALLER_Q = False
FEAR_COALITION_BREAK = True
VERY_CAREFUL = True
OFFER_VALUE = 0.2
ACCEPT_THRESHOLD = 0.15
SHAPLEY = True
DRAW_PER_PARTNER = False
GUARD_OFFERS = True
NUM_GUESTS = 10

# alloc_diff = 0

class Configuration(object):
    def __init__(self, prog_args, bm_args, base_mem, saturation_mem,
                 profiler_file, profiler_entry, revenue_func_str,
                 load, load_interval, vm_number, val_switch_func=None, 
                 max_vcpus=1, swappiness=100, adviser_class_name='', 
                 offer_value=0.5, accept_threshold=0.5, 
                 leave_coalition_if_smaller_q=LEAVE_IF_SMALLER_Q, 
                 fear_coalition_break=FEAR_COALITION_BREAK,
                 very_careful=VERY_CAREFUL, shapley=SHAPLEY):
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem  # The memory quantity for which the profiler function saturates
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.revenue_func_str = revenue_func_str
        self.val_switch_func = val_switch_func
        self.max_vcpus = max_vcpus
        self.swappiness = swappiness

        self.adviser = {#'name': 'AdviserCoalitions',
                        'name': adviser_class_name,
                        'profiler': profiler_file,
                        'advice_entry': profiler_entry,
                        'rev_func': revenue_func_str,
                        'base_mem': base_mem, 
                        'memory_delta': 10,
                        'vm_number': vm_number,
                        'num_of_guests': NUM_GUESTS,
                        'comm_timeout': 2,
                        'enable_negotiations': ENABLE_NEGOTIATIONS,
                        'offer_value': offer_value,
                        'accept_threshold': accept_threshold,
                        'leave_coalition_if_smaller_q': leave_coalition_if_smaller_q,
                        'fear_coalition_break': fear_coalition_break,
                        'very_careful': very_careful,
                        'draw_per_partner': DRAW_PER_PARTNER,
                        'guard_offers': GUARD_OFFERS,
                        'shapley': shapley,
                        #'alloc_diff': alloc_diff,
            }

def fill_configs(offer_value, accept_threshold, valuations):
    if offer_value is None or accept_threshold is None:
        OVs = []
        ATs = []
        for i in range(NUM_GUESTS):
            #ov = random.uniform(0.1, 0.5)
            ov = random.uniform(0,1)
            #at = random.uniform(0, min(0.5,1-ov))
            at = random.uniform(0, 1-ov)
            OVs.append(ov)
            ATs.append(at)
    else:
        OVs = [offer_value for i in range(NUM_GUESTS)]
        ATs = [accept_threshold for i in range(NUM_GUESTS)]
    print "Negotiation Params: %s" % zip(OVs,ATs)
    time.sleep(2)
    vals = sorted(valuations, reverse=True)
    valsets = [random.sample((valuations), len(valuations)) for i in range(4)]
    configs = {}  # config set pre frequency
    for f in frequency:
        configs[f] = [Configuration(# MCD
                          prog_args=Guest_Memcached(
                              spare_mem=50,
                              init_mem_size=10,
                              update_interval=1
                          ).command_args,
                          bm_args=Host_Memcached(
                              cmd_get_percent=0.3,
                              keys_dist=[(249, 249, 1.0)],
                              vals_dist=[(1024, 1024, 1.0)],
                              win_size="500k",
                          ).command_args,
                          base_mem=BASE_MEM,
                          saturation_mem=1600,
                          profiler_file="doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
                          profiler_entry="hits_rate",
                          load=exp.core.Loads.LoadConstant(1),
                          load_interval=5,
                          # 4 changes
                          revenue_func_str=['lambda x:%s*x' % vals[i], 'lambda x:%s*x' % vals[NUM_GUESTS - i - 1]],
                          val_switch_func=LoadBinary(v1=0, v2=1, T=(sum(t_auction)*auction_rounds/4), T0=0),
                          
                          # 4 shuffles
                          #revenue_func_str=['lambda x:%s*x' % valsets[j][i] for j in range(4)],
                          #val_switch_func=LoadMultiStep(steps=[0,1,2,3], step_duration=[10*sum(t_auction)]*4, offset=0),
                          
                          # Stable valuations
                          #revenue_func_str=['lambda x:%s*x' % vals[i]],
                          #val_switch_func = LoadBinary(v1=0, v2=1, T=2*f, T0=0),
                          vm_number=i+1,
                          adviser_class_name=ADVISER,
                          offer_value=OVs[i],
                          accept_threshold=ATs[i],
                          leave_coalition_if_smaller_q=LEAVE_IF_SMALLER_Q)
        for i in range(NUM_GUESTS)]


    return configs

if __name__ == '__main__':
    LogUtils("debug")
    name = "coalitions"
    output_path = set_output_path("exp", name)
    parser = argparse.ArgumentParser(description='run a mc experiment with 5-10 guests.')
    parser.add_argument('--offer_value', dest='offer_value', type=float, default=OFFER_VALUE,
                        help='set offer value of all guests. should be between 0 and 0.5.')
    parser.add_argument('--accept_threshold', dest='accept_threshold', type=float, default=ACCEPT_THRESHOLD,
                        help='set accept threshold of all guests. should be between 0 and 0.5 if you want them to cooperate.')
    parser.add_argument("--random_neg_params", dest="random_neg_params", action="store_true")
    parser.set_defaults(random_neg_params=False)
    parser.add_argument("--valuations", nargs='+', type=float, dest='valuations',
                        help='the factors for valuations of the guests. one for each guest, e.g., --valuations 1.233 3.456 ... 10.888.')
    args = parser.parse_args()
    at = args.accept_threshold
    ov = args.offer_value
    if args.random_neg_params:
        ov = None
        at = None
    valuations = args.valuations
    if not valuations:
        # Pareto 1.36 evenly sampled
        valuations = [1.03843591957319, 1.1269323570200953, 1.235568188113226, 
                      1.3726623074248687, 1.552064577279704, 1.7988328032346368, 
                      2.1639343706305816, 2.7713486779613903, 4.0347371833282075, 9.049815029644986]
    configs = fill_configs(ov, at, valuations)
    #print [c.revenue_func_str for c in configs[60]]
    #sys.exit()
    for f, config in configs.iteritems():
        n = len(config)
        vm_names = ["vm-%i" % (i + 1) for i in range(n)]
        vms = {}

        min_mem = sum([c.base_mem for c in config])
        max_mem = sum([c.saturation_mem for c in config])
        auction_mem = int((max_mem - min_mem) / oc)
        total_mem = HostMemory(None).collect()["mem_available"]
        host_mem = 500
        print "Overcommitment: %.2f\nTotal memory: %i\nAuction memory: %i\nHost memory: %i\nFrequency: %i" % (
            oc, total_mem, auction_mem, host_mem, f)

        for name, conf in zip(vm_names, config):
            vms[name] = ExpMachineProps(
                adviser_args=conf.adviser,
                prog_args=conf.prog_args,
                bm_args=conf.bm_args,
                load_func=conf.load,
                load_interval=conf.load_interval,
                desc=get_vm_desc(name),
                val_switch_func=conf.val_switch_func,
                max_vcpus=conf.max_vcpus,
                swappiness=conf.swappiness,
            )
            vms[name]['desc']['base_mem'] = conf.base_mem
            vms[name]['desc']['max_mem'] = conf.saturation_mem  # total_mem

        p0 = 0.0
        print "batch starting with p0 = %.2f and frequency = %i" % (p0, f)
        exp_out = os.path.join(output_path, "p0-%.2f-freq-%i" % (p0, f))
        os.mkdir(exp_out)
        moc_args = ("MocAuctioneer", (host_mem, t_auction, auction_mem))
        Experiment(
            moc_args=moc_args,
            vms_desc=vms,
            output_path=exp_out,
            duration=duration,
            verbosity="debug",
        ).start()
