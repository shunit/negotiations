'''
Created on Apr 1, 2012

@author: eyal
'''
from exp.common.exp_batch import batch
from exp.prog.Programs import Guest_Memcached, Host_Memcached
from exp.core import Loads
from exp.common.valuations import get_valuation, distributions, shapes_mcd

'''
base0 - base guest memory (MB) (might be futile)
init_mem - initial guest memory (MB)
spare - memcached
window_size - memcached
vms_num - number of VMs (as list, can be initialized with range() )
mid_load - average load
max_load - maximal load
T - half period time (maximal load duration) in seconds
exp_time - experiment duration (minutes)
auction_ts - auction times: [bid collection duration, calculation time, notification time]
'''
spare = 50
base0 = 600
init_mem = 600
window_size = "500k"
vms_num = [8]
# TODO: reach 23 machines benchmark
#vms_num = range(8, 23 + 1, 1)
mid_load = 6
max_load = 10
#T = 1000  #sec
T = 1000
exp_time = 60  #min
auction_ts = [3, 1, 8]

# program arguments (what's running inside the guest)
prog_args = Guest_Memcached(
            spare_mem = spare,
            init_mem_size = 10,
            update_interval = 1,
            ).command_args

# benchmark arguments (that makes the load)
bm_args = Host_Memcached(
            cmd_get_percent = 0.3,
            keys_dist = [(249, 249, 1.0)],
            vals_dist = [(1024, 1024, 1.0)],
            win_size = window_size,
            ).command_args

# generates a dictionary of loads:
# for each number of machines, there's an array with the loads
loads = {}
for n in vms_num:
    loads[n] = []
    m = n / 2
    for i in range(m):
        a = int(float(max_load - mid_load - 1) / (m - 1) * (i)) + 1
        T0 = T / m * i
        loads[n].append(Loads.LoadBinary(v1 = mid_load - a, v2 = mid_load + a,
                                         T = T, T0 = T0))
        loads[n].append(Loads.LoadBinary(v1 = mid_load + a, v2 = mid_load - a,
                                         T = T, T0 = T0))
    if n % 2 != 0:
        loads[n].append(Loads.LoadConstant(val = mid_load))

def the_exp():
    for shape in shapes_mcd:
        for dist in distributions:
            if (shape in ("piecewise1400", "linear")):
#            if (shape in ("linear")):
                print "skipping: %s, %s" % (shape, dist)
                continue

            batch(
                  vms_num = vms_num,
                  alpha = 1.0,
                  p0 = 0.3, #[0, 0.3],
                  start_cpu = 1,
                  base0 = base0,
                  init_mem = init_mem,
                  prifiler_file = 'doc/profiler-memcached-inside-spare-50-win-500k-tapuz21.xml',
                  profiler_entry = 'hits_rate',
                  revenue_func_dict = dict((n, get_valuation(n, shape, dist, factor = 1))
                                           for n in vms_num),
                  bid_collection_interval = auction_ts[0],
                  calculation_interval = auction_ts[1],
                  notification_interval = auction_ts[2],
                  rounds = exp_time * 60 / sum(auction_ts),
                  load_funcs_dict = loads,
                  load_interval = 200,
                  prog_args = prog_args,
                  bm_args = bm_args,
                  host_mem = 500,
                  saturation_mem = 2500,
                  verbosity = "debug",
                  name = "mcd-test16-" + shape + "-" + dist,
                  exclude = ["hinted-mom", "divided", "hinted-host-swapping"]
                  )

if __name__ == '__main__':
    the_exp()
