'''
Created on Jul 27, 2012

@author: eyal
'''
import threading
# from exp.QuitServer import send_quit_request
from mom.Comm.Messages import MessageExpHintLoad, EchoMessage, MessageError, MessageNextValuation
import time
import numpy as np
from etc.Machine import Machine
from exp.prog.Programs import get_host_program, get_guest_program_master_image
from etc.PySock import Client
from mom.Comm.GuestServer import GuestServer
import os
from exp.remote import remote_prog
from exp.remote import remote_bidder
from mom.Plotter import Plotter, PERF
from etc.TerminatableThread import Terminatable
from subprocess import Popen
from etc.Settings import Settings
import ConfigParser

class ExpMachineProps(dict):
    def __init__(self, adviser_args, prog_args, bm_args,
                 load_func, load_interval, desc, val_switch_func=None,
                 base_mem = None, max_mem = None, cpu_count=None,
                 swappiness = 100, title = None, use_image_cow = True,
                 master_image = None, max_vcpus = 1, bidder_conf = "doc/bidder.conf",
                 vm_class="default"):
        """
        Parameters
        ----------
        advisor_args : the advisor
        prog_args : the program
        bm_args : the benchmark
        load_func : determine the load as a function of t (from the start of the experiment)
        load_interval : determine the granularity of the load change
        base_mem : the virtual machine base memory
        max_mem : the virtual machine max memory
        val_switch_func : guest valuation as a function of time (None if valuation doesn't change)
        cpu_count : DEPRACATED
        swappiness : the swappiness value of the guest machine
        title : the title of the machine (not the name)
        use_image_cow: create the machine image using the master as a backing image (COW)
        master_image: the filename of the master image to use for this machine
        max_vcpus: set the maximum cpus that is allocated to this machine (will change permanently)
        vm_class: the class of the machine (to load the right descriptor)
        """

        dict.__init__(self,
                      (
                       ("adviser_args", adviser_args),
                       ("bm_args", bm_args),
                       ("prog_args", prog_args),
                       ("load_func", load_func),
                       ("load_interval", load_interval),
                       ("val_switch_func", val_switch_func),
                       ("desc", desc),
                       ("swappiness", swappiness),
                       ("title", title),
                       ("use_image_cow", use_image_cow),
                       ("master_image", master_image),
                       ("max_vcpus", max_vcpus),
                       ("bidder_conf", bidder_conf),
                       ("vm_class", vm_class)
                       )
                      )
        if base_mem: self["desc"]["base_mem"] = base_mem
        if max_mem: self["desc"]["max_mem"] = max_mem

class ExpMachine(Machine, Terminatable):
    def __init__(self, vm_name, conn, output_dir,
                 cpu_pin = None, swappiness = 60, title = None,
                 use_image_cow = True, master_image = None,
                 max_vcpus = 1, **props):
        '''
        output_dir: the directory to write the program/bidder output to
        '''

        # do_restart - a threading.Event, that can be set when something went wrong
        # and restart of experiment is needed.
        if master_image is None:
            master_image = get_guest_program_master_image(props['prog_args'])

        # Fallback if fail to get it from the guest program
        if master_image is None:
            master_image = "moc-master.qcow2"

        Machine.__init__(self, vm_name, conn, cpu_pin, swappiness, title,
                         use_image_cow, master_image, max_vcpus)
        Terminatable.__init__(self)

        self.adviser_args = props['adviser_args']
        self.prog_args = props['prog_args']
        self.bm_args = props['bm_args']
        self.load_func = props['load_func']
        self.load_func_vectorize = np.vectorize(self.load_func)
        self.desc = props['desc']  # override Machine.desc
        self.vm_class = props['vm_class']
        self.load_interval = props['load_interval']
        self.bidder_conf = props['bidder_conf']

        self.verbosity = "info"
        if "verbosity" in props:
            self.verbosity = props['verbosity']

        # valuation functions switcher
        self.last_valuation = None
        self.val_switch_func = None
        self.val_switch_string = None
        if 'val_switch_func' in props:
            self.val_switch_string = props['val_switch_func']
            self.val_switch_func = eval(self.val_switch_string) \
                if isinstance(self.val_switch_string, str) \
                else self.val_switch_string

        self.logger.debug("valuation switch function: %s", str(self.val_switch_string))

        self.plotter = Plotter(vm_name, PERF)
        self.output_dir = output_dir

        self.logger.debug("ip is: %s", str(self.ip))
        self._bm = get_host_program(args = self.bm_args, ip = self.ip, alias = self.name)
        # this client is used only to update the advser about load/valuation changes
        # it shouldn't attempt to upadte anything but the adviser
        # [BP]: TODO: We currently run 2 advisers - one for each remote bidder.
        #       We should unify the 2 advisers by using a single remote_bidder that
        #       runs select on 2 sockets. 
        #       As a temporary fix we send valuation switch messages to both advisers.
        self._client = {}
        for auction_type, port in GuestServer.ports.iteritems():
            self._client[auction_type] = Client(self.ip, port, timeout = 10,
                name = "%s-exp-client-%s" % (self.name, auction_type))


        self._ssh_bidder = []
        self._ssh_prog = None

        self._is_ready_for_experiment = False

    @property
    def as_dict(self):
        return dict(Machine.as_dict.fget(self),
                    adviser_args = self.adviser_args,
                    prog_args = self.prog_args,
                    bm_args = self.bm_args,
                    load_func = self.load_func,
                    load_interval = self.load_interval,
                    val_switch_func=self.val_switch_string,
                    bm_cpus = self.get_benchmark_cpus(),
                    vm_class=self.vm_class
                    )

    def get_benchmark_cpu_count(self):
        return self._bm.get_benchmark_cpu_count()

    def set_benchmark_cpus(self, bm_cpus):
        self._bm.set_benchmark_cpus(bm_cpus)

    def get_benchmark_cpus(self):
        return self._bm.get_benchmark_cpus()

    def start_domain(self):
        if self.dom.isActive():
            return
        base_mem = int(self.desc['base_mem'])
        # set initial and limit memories in the libvirt XML file
        self.logger.debug("base_mem: %i", base_mem)
        self.set_libvirt_mem(base_mem, int(self.desc['max_mem']))
        Machine.start_domain(self)
        self.set_mem(base_mem) # set the initial memory (not done anymore with set_libvirt_mem)

    def init_experiment(self):
        config = ConfigParser.SafeConfigParser()
        config.read(self.bidder_conf)
        try:
            self.logger.info("Initiating experiment on VM...")
            self.start_domain()
            self.logger.info("Waiting for SSHd...")
            self.wait_for_ssh_server()
            self.logger.info("SSH is alive")
            self.set_vm_props()
            self.log_info()
            self.logger.info("Copy ginseng code...")
            self.copy_code()
            self.logger.info("Disabling Cron...")
            self.disable_cron()

            # starting program and bidder
            self.logger.info("Starting remote program...")
            self.prog_start()
            self.logger.info("Initiating valuation switcher...")
            self.valuation_switch_init()
            self.logger.info("Waiting for remote program initiation...")
            self.prog_wait_for_init()
            self.logger.info("Program initiated, Starting bidder...")
            if config.has_section("mem"):
                self.bidder_start("mem")
                self.bidder_wait_for_init("mem")
            self.logger.info("Bidder initiated")
            self.logger.info("Initiating clients")
            self.init_client()
        except Exception as e:
            self.logger.error("Failed to initiate experiment: %s",e)
        else:
            self._is_ready_for_experiment = True

    def is_ready_for_experiment(self):
        return self._is_ready_for_experiment

    def end_experiment(self):
        self._is_ready_for_experiment = False
        self.terminate()
        self.logger.info("Ending valuation switch...")
        self.valuation_switch_end()
        self.logger.info("Ending benchmark...")
        try:
            self.benchmark_end()
        except Exception as e:
            self.logger.error("caught an exception! %s", e)
        self.logger.info("Waiting for remote bidder...")
        self.bidder_join(20)
        self.logger.info("Waiting for remote program...")
        self.prog_join(20)
        self.logger.info("Getting coredumps...")
        self.get_coredumps()

    def get_coredumps(self):
        cmd = " ".join(
            ("scp -r",
             "%s@%s:%s" % (self.desc["user"], self.name, "/root/core_dumps"),
             "%s/coredumps-%s" % ("%s/moc-output" % Settings.user_home(), self.name))
            )
        Popen(cmd, shell = True, stdout = None, stderr= None).communicate()

    def valuation_switch_init(self):
        if self.val_switch_func is None:
            self._val_switch_thread = None
            return

        self._val_switch_thread = threading.Thread(target = self.__valuation_thread,
                                                   name = "%s-ValuationSwitchThread" % self.name)

    def valuation_switch_end(self):
        if self._val_switch_thread is not None and self._val_switch_thread.is_alive():
            self._val_switch_thread.join(30)

    def benchmark_end(self):
        # end the __bm_thread loop
        for k, c in self._client.iteritems():
            c.close()

        if self._bm is not None:
            self._bm.terminate()
            if self._bm_thread.is_alive():
                self._bm_thread.join(self.load_interval * 3 / 2)

    def run_benchmark(self, start_time):
        self._bm_thread = threading.Thread(target = self.__benchmark_thread,
                name = "%s-BenchmarkThread" % self.name)

        self.start_time = start_time
        if self._val_switch_thread is not None:
            self._val_switch_thread.start()
        self._bm_thread.start()

    def __benchmark_thread(self):
        while self.should_run:
            t = int(time.time() - self.start_time)
            try:
                self.__apply_load(t, self.load_interval)
            except:
                if self.should_run:
                    raise

    def __valuation_thread(self):
        if self.val_switch_func is None:
            return
        while self.should_run:
            t = int(time.time() - self.start_time)
            curr_valuation = int(self.val_switch_func(t))
            try:
                if self.last_valuation is None:
                    self.last_valuation = curr_valuation
                elif self.last_valuation != curr_valuation:
                    for k, c in self._client.iteritems():
                        self.logger.debug("next valuation for client %s", k)
                        MessageNextValuation().communicate(c)
                    self.last_valuation = curr_valuation
            except Exception as err:
                if self.should_run:
                    self.logger.error("While sending valuation switch: %s", err)

            time.sleep(3)

    def __apply_load(self, t, duration):
        """
        blocking
        """
        # calculate the average load from the last time it was applied to the
        # current time
        # start = t / self.load_interval * self.load_interval
        # times = np.arange(start, start + self.load_interval)
        # load = int(np.average(self.load_func_vectorize(times)))
        load = int(self.load_func(t))

        self.logger.debug("current load for %s is: %i", self.name, load)

        # hint the client:
        try:
            for k, c in self._client.iteritems():
                MessageExpHintLoad(load=load).communicate(c)
        except Exception as err:
            if self.should_run:
                self.logger.error("While sending load hint: %s", err)

        # actually applying load:
        start = time.time()
        perf = self._bm.benchmark(load, duration)  #blocking
        end = time.time()

        actual_duration = end - start

        # log performance
        if isinstance(perf, list):
            if len(perf) == 1:
                perf = perf[0]

        if isinstance(perf, list):
            samples_count = len(perf)
            for i, sample in enumerate(perf):
                self.plotter.plot({'load': load,
                                   'perf': sample,
                                   'time': start + sample['test_time'],
                                   'duration': actual_duration})
        else:
            self.plotter.plot({'load': load,
                               'perf': perf,
                               'time': start,
                               'duration': actual_duration})
            self.plotter.plot({'load': load,
                               'perf': perf,
                               'time': end,
                               'duration': actual_duration})
            '''
            self.plotter.plot({'load': load,
                               'perf': perf,
                               'time': 0.5 * (start + end),
                               'duration': actual_duration})
            '''

        # sleep if test ended up too fast
        if actual_duration < duration:
            time.sleep(duration - actual_duration)

    def disable_cron(self):
        ssh_cron = self.ssh("service", "cron", "stop",
                                  name = "Disable cron on guest machine")

        ssh_cron.communicate()  # short blocking

        if not ssh_cron.err:
            self.logger.info("Cron disabled: %s", ssh_cron.out)
        else:
            self.logger.error("Fail to disable cron: out=%s, err=%s", ssh_cron.out, ssh_cron.err)

    def __prog_start(self):
        executable = self.local_path('exp/remote/remote_prog.py')
        remote_prog_args = [self.verbosity] + list(self.prog_args)
        args = [executable] + remote_prog_args
        out_file = os.path.join(self.output_dir, 'prog-%s' % self.name)

        self.logger.debug("starting remote program: %s",
                          " ".join(args))
        self._ssh_prog = self.ssh(*args, name = self.name + "-Remote-Program",
                                  output_file = out_file)

        self._ssh_prog.communicate()  # blocking

        if self.should_run:
            raise RuntimeError("Program crashed before its time: %s" % remote_prog_args)


    def prog_start(self):
        if not self.should_run:
            return
        threading.Thread(target = self.__prog_start).start()

    def prog_wait_for_init(self):
        sleep_time = 10  # 10 seconds before checking if ready (again)
        timeout_to_init = 60 * 5 # 5 minutes timeout

        count = 0
        max_count = timeout_to_init / sleep_time

        while self.should_run:
            self.logger.info("Waiting %s sec. for program to start...", sleep_time)
            time.sleep(sleep_time)

            if self._bm.test_benchmark():
                return

            count += 1
            if count >= max_count:
                raise Exception("Program not started after timeout (%s sec.)" % timeout_to_init)

    def __bidder_start(self, auction_type):
        self.logger.info("__bidder_start: %s" % auction_type)
        executable = self.local_path('exp/remote/remote_bidder.py')
        remote_bidder_args = [self.desc['moc_dir'], self.verbosity,
                              self.prog_args[0], self.bidder_conf, str(self.adviser_args)]
        args = [executable] + remote_bidder_args
        out_file = os.path.join(self.output_dir, 'bidder-%s-%s' % (self.name, auction_type))

        while True:
            self._ssh_bidder.append(self.ssh(*args, name = self.name + "-Remote-Bidder-" + auction_type,
                                        output_file = out_file))

            self._ssh_bidder[-1].communicate()  # blocking

            # detect ssh problem
            if file(out_file).readline().find("No route to host") != -1:
                self.logger.warn("SSH problem when starting bidder. restarting bidder in 5 seconds...")
                time.sleep(5)
                continue
            break

        if self.should_run:
            raise RuntimeError("Bidder crashed before its time")

    def bidder_start(self, auction_type):
        if not self.should_run:
            return
        threading.Thread(target = ExpMachine.__bidder_start, args = (self, auction_type)).start()

    def bidder_wait_for_init(self, auction_type):
        count = 0
        time_to_init = 60
        timeout = 10
        msg = EchoMessage()
        client = Client(self.ip, GuestServer.ports[auction_type], timeout, "BidderInitClient")
        try:
            while self.should_run:
                start = time.time()
                try:
                    ret = msg.communicate(client)
                    if ret.has_key("ack") and ret["ack"] == True:
                        return
                except MessageError:
                    pass
                count += 1
                if count >= time_to_init / timeout:
                    self.logger.info("Bidder not started yet")
                time.sleep(max(0, timeout - (time.time() - start)))
        finally:
            client.close()

    def init_client(self):
        for k, c in self._client.iteritems():
            self.logger.debug("Initializing client for %s", k)
            try:
                c.connect()
            except Exception as e:
                self.logger.warn("Failed to initialize client %s: %s", k, e)
                c.close()
                self._client.pop(auction_type, None)

    def terminate_ssh(self, ssh_trd, timeout):
        if ssh_trd is None:
            return
        if ssh_trd.is_alive():
            ssh_trd.terminate()
            ssh_trd.join(timeout)

    def prog_join(self, timeout = None):
        self.terminate_ssh(self._ssh_prog, timeout)

    def bidder_join(self, timeout = None):
        [self.terminate_ssh(_ssh_bidder, timeout) for _ssh_bidder in self._ssh_bidder]

class LocalExpMachine(ExpMachine):
    def prog_start(self):
        remote_prog_args = [self.verbosity] + list(self.prog_args)
        self._ssh_prog = threading.Thread(
                    target = remote_prog.remote_prog,
                    args = remote_prog_args,
                    name = self.name + "-Remote-Program")
        self._ssh_prog.start()

    def bidder_start(self):
        remote_bidder_args = [
                self.desc['moc_dir'], self.verbosity, self.prog_args[0]] + \
                str(self.adviser_args).split()
        self._ssh_bidder = threading.Thread(
                    target = remote_bidder.remote_bidder,
                    args = remote_bidder_args,
                    name = self.name + "-Remote-Bidder")
        self._ssh_bidder.start()

    def prog_join(self, timeout):
        self._ssh_prog.join(timeout)

    def bidder_join(self, timeout):
        self._ssh_bidder.join(timeout)

    def init_experiment(self): return
    def is_ready_for_experiment(self): return True
    def end_experiment(self): return
    def copy_code(self): return
