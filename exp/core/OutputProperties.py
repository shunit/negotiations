'''
Created on Mar 5, 2012

@author: eyal
'''
import types
import pylab as pl
from mom.Plotter import POL, MON

def_func = lambda y: y
def nrmlz(val = 1): return lambda y: [(yi / max(y) * val if max(y) != 0 else 0) for yi in y]
diff_from_min = lambda y: [yi - min(y) for yi in y]

class Prop:
    def __init__(self, var_name, disp_name = None, func = def_func,
                 tp = types.FloatType, width = 10):
        self.var_name = var_name
        self.disp_name = disp_name if disp_name is not None else var_name
        self.func = func
        self.line = None

        self.width = width
        if tp == types.FloatType:
            self.frmt = "%" + str(width / 2) + "." + str(width / 2 - 1) + "f"
        else:
            self.frmt = "%" + ('%i' % width) + "i"

    def get_column(self, res):
        raise NotImplementedError

    def add_plot(self, res, t0, marker = "s", color = "r", label_pre = "", ax = None):
        raise NotImplementedError

    def _get_column(self, res):
        column = []
        column.append(self.disp_name.ljust(self.width))
        try:
            values = self.func(res[self.var_name].values())
        except KeyError:
            return []
        column.extend([(self.frmt % val).rjust(self.width) for val in values])
        return column

    def _get_plot_arrays(self, res, t0):
        t = []
        y = []
        for i in range(len(res['time'])):
            try:
                if res[self.var_name].has_key(i):
                    ti = res['time'][i] - t0
                    yi = res[self.var_name][i]
                    if yi is None:
                        continue
                    t.append(ti)
                    y.append(yi)
            except KeyError:
                pass
        try: y = self.func(y)
        except Exception: pass
        return t, y

class PolicyProp(Prop):
    def get_column(self, res):
        return self._get_column(res.get(POL, {}))

    def add_plot(self, res, t0, marker = "-s", color = "r", label_pre = "", ax = None):
        if t0 is not None and res.has_key(POL):
            t, y = self._get_plot_arrays(res[POL], t0)
        else:
            t = []
            y = []

        if ax is None: ax = pl
        ax.plot(t, y, marker, c = color,
                label = label_pre + self.disp_name,
#                 linestyle = 'steps-post', alpha = 0.5,
                 linestyle = 'steps-post', alpha = 1.0,
                markersize = 8)

class MonitorProp(Prop):
    def get_column(self, res):
        return self._get_column(res[MON])

    def add_plot(self, res, t0, marker = "-s", color = "r", label_pre = "", ax = None):
        if t0 is not None and res.has_key(POL):
            t, y = self._get_plot_arrays(res[MON], t0)
        else:
            t = []
            y = []
        if ax is None: ax = pl
#        ax.plot(t, y, marker, c = color, label = label_pre + self.disp_name, alpha = 0.5)
        ax.plot(t, y, marker, c = color, label = label_pre + self.disp_name, alpha = 1.0)
