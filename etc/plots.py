'''
Created on Oct 25, 2011

@author: eyal
'''
import pylab as pl
import numpy as np
from matplotlib.ticker import FormatStrFormatter
from matplotlib.artist import ArtistInspector

def pl_apply_defaults(override = {}, fontsize = 8):
    params = {'backend': 'ps',
              'axes.labelsize': fontsize,
              'text.fontsize': fontsize,
              'legend.fontsize': fontsize,
              'font.size' : fontsize,
              'xtick.labelsize': fontsize,
              'ytick.labelsize': fontsize,
              'text.usetex': False,
              'ps.fonttype': 42,
              'pdf.fonttype': 42}
    params.update(override)
    pl.rcParams.update(params)

def draw_n_save(name, size = None, num = None):
    pl.draw()
    if num is not None and size is not None:
        size = (size[0], size[1] * num)

    if size is not None:
        pl.Figure.set_size_inches(pl.gcf(), size)

    pl.savefig(name)
    print "drew figure %s" % (name)

def getSubplotAxes(main_axe, count):
    main_axe.grid(True)
    subplot_axes = [pl.twinx(main_axe) for _ in range(count)]

    for i, axe in enumerate(subplot_axes):
        axe.spines['right'].set_position(('outward', 40 * i))
        axe.grid(False)

    all_axes = [main_axe] + subplot_axes

###############################################################################
# Caused appearance of white line at the top/bottom section of the plot (Liran)
###############################################################################
#     for axe in all_axes:
#         for loc in ['bottom', 'top']:
#                 axe.spines[loc].set_position(('outward', 10))
###############################################################################

    return all_axes

plot_available_properties = [
        "agg_filter",                       # unknown
        "alpha",                            # float (0.0 transparent through 1.0 opaque)
        "animated",                         # [True | False]
        "antialiased", "aa",                # [True | False]
        "axes",                             # an Axes instance
        "clip_box",                         # a matplotlib.transforms.Bbox instance
        "clip_on",                          # [True | False]
        "clip_path",                        # [ (Path, Transform) | Patch | None ]
        "color", "c",                       # any matplotlib color
        "contains",                         # a callable function
        "dash_capstyle",                    # ['butt' | 'round' | 'projecting']
        "dash_joinstyle",                   # ['miter' | 'round' | 'bevel']
        "dashes",                           # sequence of on/off ink in points
        "drawstyle",                        # ['default' | 'steps' | 'steps-pre' | 'steps-mid' | 'steps-post']
        "figure",                           # a matplotlib.figure.Figure instance
        "fillstyle",                        # ['full' | 'left' | 'right' | 'bottom' | 'top' | 'none']
        "gid",                              # an id string
        "label",                            # string or anything printable with '%s' conversion.
        "linestyle", "ls",                  # ['-' | '--' | '-.' | ':' | 'None' | ' ' | ''] and any drawstyle in combination with a linestyle, e.g., 'steps--'.
        "linewidth", "lw",                  # float value in points
        "lod",                              # [True | False]
        "marker",                           # the shape of a point (e.g "^" for upper triangle)
        "markeredgecolor", "mec",           # any matplotlib color
        "markeredgewidth", "mew",           # float value in points
        "markerfacecolor", "mfc",           # any matplotlib color
        "markerfacecoloralt", "mfcalt",     # any matplotlib color
        "markersize", "ms",                 # float
        "markevery",                        # unknown
        "path_effects",                     # unknown
        "picker",                           # float distance in points or callable pick function fn(artist, event)
        "pickradius",                       # float distance in points
        "rasterized",                       # [True | False | None]
        "sketch_params",                    # unknown
        "snap",                             # unknown
        "solid_capstyle",                   # ['butt' | 'round' | 'projecting']
        "solid_joinstyle",                  # ['miter' | 'round' | 'bevel']
        "transform",                        # a matplotlib.transforms.Transform instance
        "url",                              # a url string
        "visible",                          # [True | False]
        "xdata",                            # 1D array
        "ydata",                            # 1D array
        "zorder",                           # any number
    ]

def getSublotXLables(subplot_xy, max_y, axes, sp_axes, plot_kwargs):
    labels = []
    lines = []

    for i, xy in enumerate(subplot_xy):
        # update max y
        if len(xy[0]) == 0 or len(xy[1]) == 0:
            continue
        max_y[axes[i]] = max(max_y[axes[i]], max(xy[1]))

        # draw the line
        ax = sp_axes[axes[i]]
        if "color" not in plot_kwargs[i]:
            plot_kwargs[i]["color"] = pl.cm.jet(float(i + 1) / len(subplot_xy))  # @UndefinedVariable

#         if "drawstyle" not in plot_kwargs[i]:
#             plot_kwargs[i]["drawstyle"] = "steps-post"

        function_args = {k:plot_kwargs[i][k] for k in plot_kwargs[i] if k in plot_available_properties}
        lines.append(ax.plot(xy[0], xy[1], **function_args)[0])
        labels.append(plot_kwargs[i]["legend_label"])

    return (labels, lines)

def getJoinedArgs(relavent_args, key):
    matching_args = [kw[key] if kw.has_key(key) else None for kw in relavent_args]
    if not matching_args: return None

    # Assert that all of the units match (don't mix and match)
    assert matching_args.count(matching_args[0]) == len(matching_args)

    return matching_args[0]


def setSubplotsYLabels(plot_index, sp_axes, plot_kwargs, axes):
    # set axes y labels
    for ax_ind, ax in enumerate(sp_axes):
        relavent_args = [kw for i, kw in enumerate(plot_kwargs) if axes[i] == ax_ind]
        unit = getJoinedArgs(relavent_args, "units")
        yscale = getJoinedArgs(relavent_args, "yscale")

        if unit == None: unit = "Not Specified"

        labels = set([kw["y_label"].replace("_", " ") if kw.has_key("y_label") else None for kw in relavent_args])
        labels_str = ' / '.join(labels)

        final_label = "%s [%s]" % (labels_str, unit)

        ax.set_ylabel(final_label, labelpad = 2)

        if yscale: ax.set_yscale(yscale)

def multifunction(data, axes, plot_kwargs, xlim = None,
                  ylim = None, ylabels = None, x_label = "Time [m]"):
    '''

    ylabels : deprecated
    '''
    axes_count = max(axes)
    subplots_count = len(data)

    # Set the maximum of all the axes to 1
    max_y = [1] * (axes_count + 1)

    all_axes = []
    lines = []

    for plot_i, subplot_xy in enumerate(data):
        '''
        Plot each plot array in different row
        '''
        pl.subplot(subplots_count, 1, plot_i + 1)
        main_axe = pl.gca()

        sp_axes = getSubplotAxes(main_axe, axes_count)
        all_axes.append(sp_axes)

        labels, lines = getSublotXLables(subplot_xy, max_y, axes, sp_axes, plot_kwargs)

        setSubplotsYLabels(plot_i, sp_axes, plot_kwargs, axes)

        # show only last plot x ticks
        if plot_i < subplots_count - 1:
            for ax in sp_axes:
                ax.get_xaxis().set_ticklabels([])


    max_y_extra_factor = 1.01
    x0 = 0.06
    x1 = 0.86
    y0 = 0.11
    y1 = 0.93
    space = 0.01
    shrink_factor = 0.01
#    h = (y1 - y0) / subplots_count
    h = (y1 - y0 - (subplots_count - 1) * shrink_factor) / subplots_count
#    h = 0.04
    # make all axes in all subplots with the same limits
    for ii, sp_axes in enumerate(all_axes):
        for i, ax in enumerate(sp_axes):
            ax.set_position([x0, y0 + (subplots_count - 1 - ii) * (h + space), x1 - x0, h])
            ax.set_ylim((0 , max_y[i] * max_y_extra_factor))
            if xlim:
                ax.set_xlim(*xlim)
            if ylim:
                ax.set_ylim(*ylim)
#            if i == 0:
#                ax.set_ylim(0, 60)
#            if i == 1:
#                ax.set_ylim(0, 2)

#        ticks = sp_axes[0].get_xticks()
#        sp_axes[0].set_xticks(np.arange(min(ticks), max(ticks), 1.0))

    #legend_cols = len(axes) / len(plot_kwargs)
    #if legend_cols == 1: legend_cols = len(plot_kwargs)

    sp_axes[-1].legend(lines, [l.replace("_", " ") for l in labels],
              loc = 'best',
              #bbox_to_anchor = (0.5, 0.05),
              #bbox_transform = pl.gcf().transFigure,
              #ncol = legend_cols,
              #frameon = True
              )
    if all_axes:
        all_axes[-1][0].set_xlabel(x_label, labelpad = 2)
#        ticks = all_axes[-1][0].get_xticks()
#        all_axes[-1][0].set_xticks(np.arange(min(ticks), max(ticks), 1.0))

def group_by_x(x, ys):
    g = []
    for j in range(len(ys)):
        g.append({})
    for i in range(len(x)):
        for j in range(len(ys)):
            try:
                g[j].setdefault(x[i], []).append(ys[j][i])
            except IndexError:
                g[j].setdefault(x[i], []).append(0)
    return g

def plot_mean(ax, ay, az,
            title = None,
            x_label = "X", y_label = "Y", legend_label = "Z",
            normalize = False, ignore_zeros = True, marker = None, xlim = None):

    x, y = group_by_x(ax, (ay, az))
    if 0 in (len(x), len(y)):
        return
    i = 0
    num = len(x.keys())
    maxy = max(y.values())
    legend = sorted(x.keys(), reverse = True)
    ax1 = pl.gca()
    xx = []
    for i in range(len(legend)):
        l = legend[i]
        if normalize:
            y[l] = [float(x) / maxy for x in y[l]]
        vals, = group_by_x(x[l], (y[l],))
        xx.append([])
        yy = []
        for xi in sorted(vals.keys()):
            if ignore_zeros:
                yy.append(np.average([v for v in vals[xi] if v != None]))
            else:
                yy.append(np.average(vals[xi]))
            xx[-1].append(xi)

        yy = np.array(yy)
        ax1.plot(xx[-1], yy,
                label = "%s: %s" % (legend_label, legend[i]),
                c = pl.cm.jet(1 - float(i) / len(legend)),  # @UndefinedVariable
                marker = str(pl.Line2D.filled_markers[i]) if marker is None else marker,
                markersize = 4, alpha = 1, linewidth = 1)
    ax1.legend(loc = "best")
    ax1.grid(True)

    if xlim is not None:
        pl.xlim(xlim)

    pl.xlabel(x_label)
    ax1.set_ylabel(y_label)
    pl.ylim((0, pl.ylim()[1]))

def plot_mean_and_stdev(ax, ay, az,
            x_label = "X", y_label = "Y", z_label = "Z",
            normalize = False):

    x, y = group_by_x(ax, (ay, az))
    i = 0
    num = len(x.keys())
    maxy = max(y.values())
    legend = sorted(x.keys(), reverse = True)
    ax1 = pl.gca()
    ax2 = pl.twinx(ax1)
    xx = []
    yerr = []
    for i in range(len(legend)):
        l = legend[i]
        if normalize:
            y[l] = [float(x) / maxy for x in y[l]]
        vals, = group_by_x(x[l], (y[l],))
        xx.append([])
        yerr.append([])
        yy = []
        for xi in sorted(vals.keys()):
            yy.append(np.average(vals[xi]))
            xx[-1].append(xi)
            yerr[-1].append(np.std(vals[xi]))

        yy = np.array(yy)
        ax1.plot(xx[-1], yy,
                label = "%s: %s" % (x_label, legend[i]),
                linewidth = 2, c = pl.cm.jet(1 - float(i) / len(legend)),  # @UndefinedVariable
                marker = str(pl.Line2D.filled_markers[i]), markersize = 8, alpha = 1)
    ax1.legend(loc = "best")
    ax1.grid(True)

    for i in range(len(legend)):
        yyerr = np.array(yerr[i])
        xxx = xx[i]
        ax2.plot(xxx, yyerr, label = "", ls = "dashed",
                linewidth = 2, c = pl.cm.jet(1 - float(i) / len(legend)),  # @UndefinedVariable
                marker = str(pl.Line2D.filled_markers[i]), markersize = 8, alpha = 1)

    pl.xlabel(y_label)
    ax1.set_ylabel(z_label)
    ax2.set_ylabel("stdev")
    pl.ylim((0, pl.ylim()[1]))

def subplotsdefered(ax, ay, az,
             x_label = "X", y_label = "Y",
             normalize = False):
    up = {'x':[ax[0]], 'y':[ay[0]], 'z':[az[0]]}
    down = {'x':[], 'y':[], 'z':[]}

    curr = up
    for i in range(1, min([len(ax), len(ay), len(az)])):
        if ay[i - 1] < ay[i]:
            curr = up
        elif ay[i - 1] > ay[i]:
            curr = down
        curr['x'].append(ax[i])
        curr['y'].append(ay[i])
        curr['z'].append(az[i])

    xu, yu = group_by_x(up['x'], (up['y'], up['z']))
    xd, yd = group_by_x(down['x'], (down['y'], down['z']))
    i = 0
    num = len(xu.keys())
    maxy = max(yu.values() + yd.values())
    legend = sorted(xu.keys(), reverse = True)
    for i in range(len(legend)):
        l = legend[i]
        if normalize:
            yu[l] = [float(x) / maxy for x in yu[l]]
            yd[l] = [float(x) / maxy for x in yd[l]]
        pl.subplot(num, 1, i + 1)
        if i != num - 1: pl.gca().get_xaxis().set_ticklabels([])

        pl.plot(xu[l], yu[l], "--sr", label = 'increase',
                   marker = str(pl.Line2D.filled_markers[0]),
                   markersize = 8, alpha = 0.5, linestyle = "none")

        pl.plot(xd[l], yd[l], "--sb", label = 'decrease',
                   marker = str(pl.Line2D.filled_markers[0]),
                   markersize = 8, alpha = 0.5, linestyle = "none")

        pl.ylabel("%s: %s" % (x_label, l))
        pl.grid(True)
    pl.xlabel(y_label)
    pl.subplot(len(legend), 1, 1)

def plot_scatter(ax, ay, az,
             x_label = "X", y_label = "Y",
             normalize = False, legend = True, xlim = None):

    marker_size = 5
    marker_alpha = 0.3

    up = {'x':[ax[0]], 'y':[ay[0]], 'z':[az[0]]}
    down = {'x':[], 'y':[], 'z':[]}

    curr = up
    for i in range(1, min([len(ax), len(ay), len(az)])):
        if ay[i - 1] < ay[i]:
            curr = up
        elif ay[i - 1] > ay[i]:
            curr = down
        curr['x'].append(ax[i])
        curr['y'].append(ay[i])
        curr['z'].append(az[i])

    xu, yu = group_by_x(up['x'], (up['y'], up['z']))
    xd, yd = group_by_x(down['x'], (down['y'], down['z']))
    i = 0
    num = len(xu.keys())
    maxy = max(yu.values() + yd.values())
    legend = sorted(xu.keys(), reverse = True)

    for i in range(len(legend)):
        l = legend[i]
        if normalize:
            yu[l] = [float(x) / maxy for x in yu[l]]
            yd[l] = [float(x) / maxy for x in yd[l]]

        pl.plot([], [],
                c = pl.cm.jet(1 - float(i) / len(legend)),  # @UndefinedVariable
                label = 'load:%i' % l, marker = "x", markeredgewidth = 3,
                markersize = marker_size, alpha = marker_alpha, linestyle = "none")

        pl.plot(xu[l], yu[l],
                c = pl.cm.jet(1 - float(i) / len(legend)),  # @UndefinedVariable
                marker = "^",
                markersize = marker_size, alpha = marker_alpha, linestyle = "none")



    if legend:
        pl.legend(loc = "best")

    if xlim is not None:
        pl.xlim(xlim)

    try:
        for i in range(len(legend)):
            l = legend[i]
            pl.plot(xd[l], yd[l], "--sb",
                    c = pl.cm.jet(1 - float(i) / len(legend)),  # @UndefinedVariable
                    marker = "v",
                    markersize = marker_size, alpha = marker_alpha, linestyle = "none")
    except KeyError:
        pass

    pl.ylim((0, pl.ylim()[1]))
    pl.ylabel("%s" % (y_label))
    pl.grid(True)
    pl.xlabel(x_label)

def plot_up_down(ax, ay, az,
             x_label = "X", y_label = "Y",
             normalize = False, has_legend = True, xlim = None):

    marker_size = 5
    marker_alpha = 0.3

    up = {'x':[ax[0]], 'y':[ay[0]], 'z':[az[0]]}
    down = {'x':[], 'y':[], 'z':[]}

    curr = up
    for i in range(1, min([len(ax), len(ay), len(az)])):
        if ay[i - 1] < ay[i]:
            curr = up
        elif ay[i - 1] > ay[i]:
            curr = down
        curr['x'].append(ax[i])
        curr['y'].append(ay[i])
        curr['z'].append(az[i])

    xu, yu = group_by_x(up['x'], (up['y'], up['z']))
    xd, yd = group_by_x(down['x'], (down['y'], down['z']))
    i = 0
    num = len(xu.keys())
    maxy = max(yu.values() + yd.values())
    legend = sorted(xu.keys(), reverse = True)
    for i in range(len(legend)):
        l = legend[i]
        color = pl.cm.jet(1 - float(i) / len(legend))  # @UndefinedVariable
        if normalize:
            yu[l] = [float(x) / maxy for x in yu[l]]
            yd[l] = [float(x) / maxy for x in yd[l]]

        x_up_and_down = []
        y_up_and_down = []


        try:
            all_x = np.array(xu[l])
            all_y = np.array(yu[l])
            xx = sorted(list(set(xu[l])))
            yy = [np.average([y for y in all_y[all_x == x] if y is not None])
                  for x in xx]

            x_up_and_down.extend(xx)
            y_up_and_down.extend(yy)

            top = xx[-1], yy[-1]

            pl.plot(xx, yy, c = color,
                    label = 'load:%i up' % l,
                    marker = "^",
                    ms = marker_size, alpha = marker_alpha,
                    ls = "None")
        except KeyError:
            pass  # if no up

        try:
            all_x = np.array(xd[l])
            all_y = np.array(yd[l])
            xx = sorted(list(set(xd[l])), reverse = True)
            yy = [np.average([y for y in all_y[all_x == x] if y is not None])
                  for x in xx]

            x_up_and_down.extend(xx)
            y_up_and_down.extend(yy)

            xx.insert(0, top[0])
            yy.insert(0, top[1])

            pl.plot(xx, yy, c = color,
                    label = 'load:%i down' % l,
                    marker = "v",
                    ms = marker_size, alpha = marker_alpha,
                    ls = "None")
        except KeyError:
            pass  # if no down

        pl.plot(x_up_and_down, y_up_and_down, c = color, marker = "None",
                ls = "-")

    if has_legend:
        pl.legend(loc = 0)

    if xlim is not None:
        pl.xlim(xlim)

    pl.ylabel("%s" % (y_label))
    pl.grid(True)
    pl.xlabel(x_label)

def nice_surf(x, y, z, z_label = "", zlim = None,
              cbar_pos = None,
              cbar_formatter = "%i",
              cbar_ticks = None):
    pl.clf()
    plot = pl.contourf(x, y, z, levels = cbar_ticks, extend = "both", cmap = pl.cm.bone)  # @UndefinedVariable
    lines = pl.contour(plot, x, y, z, levels = cbar_ticks, cmap = pl.cm.jet, extend = "both", linewidths = 1)  # @UndefinedVariable
    pl.gca().xaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    pl.gca().yaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    pl.gca().set_xticks([0.1, 0.4, 0.7, 1.0])
    pl.grid(True)
    cbar = pl.gcf().colorbar(plot, shrink = 0.8, aspect = 10, ticks = cbar_ticks, format = cbar_formatter)
    cbar.add_lines(lines)
    if cbar_pos is not None:
        cbar.ax.set_position(cbar_pos)
    cbar.ax.set_ylabel(z_label)

def nice_surf2(x, y, z,
              cbar_formatter = "%.2f", cbar_pos = None,
              z_label = None):
    pl.clf()
    plot = pl.contourf(x, y, z,
                       100,  #levels = np.linspace(cbar_ticks[0], cbar_ticks[-1], 100, endpoint = True),
                       extend = "both", cmap = pl.cm.jet, antialiased = True)  # @UndefinedVariable
#    lines = pl.contour(plot, x, y, z, levels = cbar_ticks, extend = "both", colors = "k", linewidths = 1)  # @UndefinedVariable
    pl.gca().xaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    pl.gca().yaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    pl.grid(True)
    cbar = pl.gcf().colorbar(plot, shrink = 0.8, aspect = 10, format = cbar_formatter,
#                             ticks = cbar_ticks
                             )
#    cbar.add_lines(lines)
    if cbar_pos is not None:
        cbar.ax.set_position(cbar_pos)
    if z_label is not None:
        cbar.ax.set_ylabel(z_label)

def filled_errorbar(x, y, yerr, linestyle = 'k', fillcolor = 'b'):
    y = pl.np.array(y)
    yerr = pl.np.array(yerr)
    pl.fill_between(x, y + yerr, y - yerr, alpha = 0.5, facecolor = fillcolor)
    pl.plot(x, y, linestyle)

def plot_guests_graphs(guests, mem_range, loads, entry):
    pl.clf()
    ax1 = pl.gca()
    ax2 = pl.twinx(ax1)
    for i in range(len(guests)):
        prflr = guests[i].adviser.profiler
        perf = [prflr.interpolate([loads(i), mem], entry) for mem in mem_range]
        p = [p / m if m != 0 else 0 for p, m in zip(perf, mem_range)]
        l1 = ax1.plot(mem_range, perf)  # , label = 'guest %i, load: %i' % (i, loads[i]))
        l2 = ax2.plot(mem_range, p, "--")

    ax1.grid(True)
    ax1.legend(["%i" % i for i in range(len(guests))], loc = "best")
    ax1.set_xlabel('Memory [MB]')
    ax1.set_ylabel('Throughput [req/sec]')
    ax2.set_ylabel('P from zero, [req/sec-MB]')
