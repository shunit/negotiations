'''
Publish your output result folder to your web server (including indexing)

Usage:
Publish.py <public_remote_dir> [, <server>]

public_remote_dir: the remote directory on your public pages to sync the files to (omitting the .www/)
server: the server to publish (if using different username than use username@server)

@author Liran Funaro <fonaro@cs.technion.ac.il>
'''

import subprocess
import sys
from etc.Settings import Settings
import os
from scripts.index import FolderIndex
import re
import tempfile
from etc.shell import run, run_on_server
SERVER_CSM = "cs"
SERVER_TX = "tx"
SERVER_T2 = "t2"

SERVER_DATA_CS = ("csm", ".www", "http://cs.technion.ac.il/~%(username)s")
SERVER_DATA_TX = ("tx.technion.ac.il", "public_html", "http://tx.technion.ac.il/~%(username)s")
SERVER_DATA_T2 = ("t2.technion.ac.il", "public_html", "http://t2.technion.ac.il/~%(username)s")

SERVERS = {
    "csm" : SERVER_DATA_CS,
    "cs" : SERVER_DATA_CS,
    "csm.technion.ac.il" : SERVER_DATA_CS,
    "cs.technion.ac.il" : SERVER_DATA_CS,

    "tx": SERVER_DATA_TX,
    "tx.technion.ac.il": SERVER_DATA_TX,

    "t2": SERVER_DATA_T2,
    "t2.technion.ac.il": SERVER_DATA_T2
    }

FILES_TO_PUBLISH = [ "index.html",
                    "info", "exp-out",
                    "*.conf", "*.rules", "*.xml",
                    "bidder-*", "prog-*",
                    "*.data", "*.csv",
                    "*.pdf", "*.png", "*.jpg", "*.jpeg", "*.gif"]

INFO_TO_HTML_TEMPLATE = (
     '<div style="margin-left: 2em;">\n'
     '<span style="font-weight:bold; color: #800080; display: inline;">%(key)s:</span>\n'
     '<span style="color: #0000FF;">%(value)s</span>\n'
     '</div>\n')

def remote_sync(src_path, dst_path,
                src_server = None, dst_server = None,
                include = [], exclude = [],
                include_subfolders = True,
                delete = True, delete_excluded = False,
                show_progress = True, verbose = False,
                wait = True):
    '''
    Remote sync files
    ===========================================================================
    src_path : the source path to sync from
        (remote if src_server is specified)
    dst_path : the destination path to sync into
        (remote if dst_server is specified)

    src_server : the remote server to sync from (optional)
    dst_server : the remote server to sync into (optional)

    include : files to include in sync (optional).
        If this is specified but not exclude pattern is specified, then only
        include those files and nothing else.
    exclude : file to exclude from sync (optional).
        If not specified but include is specified, then everything will be
        excluded but the include files.

    include_subfolders : to include sub-folders (optional, default: True)
    delete: delete files that don't exist on the sending side
        (optional, default: True)
    delete_excluded : also delete excluded files on the receiving side
        (optional, default: False)

    show_progress : Show progress during transfer (optional, default: True)
    verbose : Increase verbosity (optional, default: False)
    '''
    # What to copy:
    #  -r, --recursive             recurse into directories
    #      --exclude=PATTERN       Exclude files matching PATTERN
    #      --include=PATTERN       Don't exclude files matching PATTERN
    #
    # How to copy it:
    #  -e, --rsh=COMMAND           Specify rsh replacement
    #
    # Destination options:
    #  -a, --archive               Archive mode
    #  -z, --compress              Compress file data during the transfer
    #      --delete                Delete files that don't exist on the sending side
    #      --delete-excluded       also delete excluded files on the receiving side
    #      --progress              Show progress during transfer
    #
    # Misc Others:
    #  -v, --verbose               Increase verbosity
    args = ["rsync", "-az"]

    if include_subfolders:
        args += ["-r"]

    if verbose:
        args += ["-v"]

    if src_server or dst_server:
        args += ["-e", "ssh"]

    if include and not exclude:
        exclude = ["*"]

    if include_subfolders and "*" in exclude:
        include = ["*/"] + list(include)

    for include_file in include:
        args += ["--include", include_file]

    for exclude_file in exclude:
        args += ["--exclude", exclude_file]

    if delete:
        args += ["--delete"]

    if delete_excluded:
        args += ["--delete-excluded"]

    if show_progress:
        args += ["--progress"]

    if not src_path.endswith("/"):
        src_path += "/"

    if not dst_path.endswith("/"):
        dst_path += "/"

    if src_server:
        args += [ "%s:%s" % (src_server, src_path) ]
    else:
        args += [src_path]

    if dst_server:
        args += [ "%s:%s" % (dst_server, dst_path) ]
    else:
        args += [dst_path]

    p_rsync = subprocess.Popen(args)
    if wait:
        p_rsync.wait()

def get_server(server_id):
    try:
        server_name, server_public_folder, server_public_address = SERVERS[server_id]
    except:
        server_name = server_id
        server_public_folder = "."
        server_public_address = server_id

    return server_name, server_public_folder, server_public_address

def report(to, subject, message, server = SERVER_CSM):
    ''' Report experiment information via email using mail-server '''
    new_subject = re.sub("(\"|<|>)", r"\\\1", subject)

    server_name, _,_ = get_server(server)

    _, tmp_fpath = tempfile.mkstemp()
    try:
        with open(tmp_fpath, "wb") as f:
            f.write(message)

        run(["scp",f.name,"%s:%s" % (server_name, "exp-report")])
    finally:
        os.remove(tmp_fpath)

    run_on_server('cat exp-report | mutt -e "set content_type=text/html" -s "%s" %s' % (new_subject, to), server_name)

def report_kwargs(to, subject, server = SERVER_CSM, **kwargs):
    '''
    Report experiment information via email using mail-server
    ===========================================================================
    to, subject, server
    **kwargs : the args specified will be presented in the mail in a pretty form
    '''
    message = FolderIndex.infoToHtmlWithTemplate("Experiment Information", kwargs, INFO_TO_HTML_TEMPLATE)
    report(to, subject, message, server)

def publish_result(public_remote_dir = "research/all-outputs", server = SERVER_CSM):
    '''
    Publish your output result folder to your web server (including indexing)
    ===========================================================================
    public_remote_dir : the address to publish the outputs (without the .www/)
    server : the server to publish
    '''
    server_name, server_public_folder, server_public_address = get_server(server)
    server_public_address = server_public_address % dict(username = Settings.username())

    local_dir = os.path.join(Settings.user_home(),Settings.output_dir()) + "/"
    public_remote_dir = public_remote_dir.strip("/")
    remote_dir = os.path.join(server_public_folder, public_remote_dir) + "/"

    index_path = os.path.join("home",public_remote_dir)

    FolderIndex(FILES_TO_PUBLISH).writeIndexFile(index_path, local_dir)

    remote_sync(
                src_path = local_dir,
                dst_path = remote_dir,
                dst_server = server_name,
                include = FILES_TO_PUBLISH,
                include_subfolders = True,
                delete = True,
                delete_excluded = False,
                show_progress = False,
                verbose = False)

    return os.path.join(server_public_address, public_remote_dir)

if __name__ == '__main__':
    print publish_result(*sys.argv[1:])
