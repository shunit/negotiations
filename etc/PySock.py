# Ginseng
# Copyright (C) 2012 Eyal Posener, Orna Agmon Ben Yehuda,
# Technion - Israel Institute of Technology
#
# Ginseng is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ginseng is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ginseng.  If not, see <http://www.gnu.org/licenses/>.

from SocketServer import StreamRequestHandler, TCPServer
import socket
import logging
import threading
from etc.ping import ping


class RequestHandlerFactory(object):
    """
    Creates a StreamRequestHandler object with override timeout value.
    Implements the handle() method such that it read commands until
    connection is closed, and pass them to the server.process method.
    if server.process returns not None object, it will be returned as
    a reply to client.
    On any error, connection will be closed.
    """
    def __new__(cls, timeout):
        """
        creates a Handler class with specific timeout.
        """
        class _RequestHandler(StreamRequestHandler):
            def setup(self):
                StreamRequestHandler.setup(self)
                # set thread name
                threading.current_thread().name = self.server.name + "-Handler"

            def handle(self):
                """
                Created by a request
                """
                while True:  # MAIN SERVER THREAD LOOP

                    msg = None
                    try:
                        # read line
                        msg = self.rfile.readline()
                        # if connection closed (got EOF), break the handle loop
                        if not msg:
                            break
                        msg = msg.strip()
                        if msg != "MessageExpHintLoad(load=0)":
                            self.server.logger.debug("Received message: %s", msg)
                    except socket.error as err:
                        self.server.logger.warn(
                            "Error receiving message: %s", err)
                        continue  # we can't reply if socket error accrued

                    # process message in derived class method process()
                    reply = self.server.process(msg)

                    # send reply if necessary (if it is not None)
                    if reply is not None:
                        try:
                            reply = str(reply).strip()
                            self.wfile.write(reply + '\n')
                            self.wfile.flush()
                            if msg != "MessageExpHintLoad(load=0)":
                                self.server.logger.debug("Sent reply: %s", reply)
                        except socket.error as err:
                            self.server.logger.warn(
                                "Error sending reply %s: %s", reply, err)

        # set timeout
        _RequestHandler.timeout = timeout
        # return the created class
        return _RequestHandler


class ServerError(socket.error): pass


class TcpThreadedServer(TCPServer):
    allow_reuse_address = True
    request_queue_size = 20

    def __init__(self, host, port, timeout = None, name = None):
        '''
        Initialize a server thread.
        @param host, port: Address is host:port, use host = "" for a INADDR_ANY
            network server
            INADDR_ANY - when receiving, receive from all possible interfaces with this port
                when sending, send with default ip.
        @param timeout: timeout of session, will raise TimeoutError in process()
            method any time a message will not be received with the specified
            timeout.
            use timeout=None for infinite timeout,
            use timeout=0 to make process non-blocking.
        @param name: thread's and logger's name, use None for default name.
        '''
        if name is None:
            name = "%s:%s" % (host, port)
        self.name = name

        self.logger = logging.getLogger(name)
        self.poll_interval = 0.5
        self.__trd_lock = threading.Lock()
        self.__trd = None
        self.requests_lock = threading.Lock()
        self.requests = {}

        try:
            TCPServer.__init__(self, (host, port),
                               RequestHandlerFactory(timeout))
        except socket.error as err:
            raise ServerError("Starting server with: host - %s, port %s : %s" % (host,port,err))

    def process(self, msg):
        """
        Should be overridden
        process a message
        msg - string containing a received message
        return - if returns a not None object, it will be sent to the client.
        """
        raise NotImplemented

    def serve_forever(self, poll_interval = 0.5):
        """
        Start server in a thread as a non blocking function
        """
        self.poll_interval = poll_interval
        with self.__trd_lock:
            if self.__trd is not None:
                return
            self.__trd = threading.Thread(
                target = TCPServer.serve_forever,
                args = [self, self.poll_interval],
                name = "Server-" + self.name)
            self.__trd.start()

    def shutdown(self):
        with self.__trd_lock:
            if not self.__trd:
                return
        TCPServer.shutdown(self)
        for request, trd in self.requests.items():
            self.logger.debug("Shutting down request: %s", request)
            try:
                self.shutdown_request(request)
                if trd.is_alive():
                    trd.join()
            except: pass  # thread might be killed during iterations
        self.server_close()
        self.join()

    def join(self, timeout = None):
        with self.__trd_lock:
            if not self.__trd or not self.__trd.is_alive():
                return
            trd = self.__trd
        trd.join(timeout)

    def is_alive(self):
        with self.__trd_lock:
            return self.__trd.is_alive()

    ## MODIFIED THREADING MIXING

    def __process_request_trd(self, request, client_address):
        try:
            self.finish_request(request, client_address)
        except Exception as err:
            self.logger.warn("Request error: %s", err)
        finally:
            self.shutdown_request(request)

    def process_request(self, request, client_address):
        """
        Calls finish_request() to create an instance of the RequestHandlerClass.
        If desired, this function can create a new process or thread to handle
        the request; the ForkingMixIn and ThreadingMixIn classes do this.
        """
        assert not self.requests.has_key(request)

        t = threading.Thread(target = self.__process_request_trd,
                             args = (request, client_address))
        t.start()
        with self.requests_lock:
            self.requests[request] = t

    def close_request(self, request):
        with self.requests_lock:
            if not request in self.requests:
                return
            TCPServer.close_request(self, request)
            del self.requests[request]


class ClientError(socket.error): pass


class Client():
    def __init__(self, host, port, timeout = None, name = None,
                 verbose = True):
        '''
        Initialize a client for TcpThreadedServer
        @param host, port: ip and port of server to connect to.
        @param timeout: timeout of session, will raise TimeoutError in
            send_recv() method any time a message will not be received with
            the specified timeout.
            use timeout = None for infinite timeout,
            use timeout = 0 to make process non - blocking.
        @param name: name of client, for logging purposes.
        '''
        self.host = host
        self.port = port
        self.verbose = verbose
        self.sock = None
        self.__lock = threading.RLock()
        self.timeout = timeout
        if name is None:
            name = "Client-%s:%s" % (host, port)
        self.name = name
        self.logger = logging.getLogger(name)

    def __del__(self):
        self.close()

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def close(self):
        with self.__lock:
            if self.sock is not None:
                self.sock.close()
                self.logger.debug("Connection Closed")
            self.sock = None

    def connect(self):
        """
        connect to server if not already connected
        """
        with self.__lock:
            # if connected, return
            if self.sock is not None:
                return
            # try and connect:
            try:
                self.sock = socket.create_connection((self.host, self.port),
                                                     self.timeout)
                self.logger.debug("Connected")
            except socket.error as err:
                self.sock = None
                raise ClientError("Creating connection problem: %s" % err)

    def set_timeout(self, timeout):
        with self.__lock:
            self.timeout = timeout
            if self.sock is None:
                return
            self.sock.settimeout(timeout)

    def send_recv(self, msg, recv_len = 4096):
        """
        Send a message and receive a reply.
        steps:
            verify / connect to server
            send message
            receive reply if recv_len > 0
        """
        msg = str(msg)
        reply = None

        with self.__lock:
            # make sure the client is connected
            self.connect()
            # send message
            try:
                try:
                    self.sock.sendall(msg + "\n")
                    self.logger.debug("Sent message: %s" % msg)
                except socket.error as err:
                    raise ClientError("Error sending message: %s" % err)

                # receive reply
                if recv_len > 0:
                    try:
                        reply = self.sock.recv(recv_len)
                        # Removes white space from beginning and end of string
                        reply = reply.strip()
                        if len(reply) == 0:
                            raise socket.error("Got empty string")
                        self.logger.debug("Received reply: %s" % reply)
                    except socket.error as err:
                        raise ClientError("Error receiving reply for: %s,  %s" %
                                          (msg, err))

                    # return reply as str
                    assert isinstance(reply, str)
                    return reply
            except Exception as err:
                # close connection in send-recv only when error accrued
                logging.exception("")
                self.close()
                ping(self.host, self.logger)  # test if the host is alive
                if self.verbose:
                    self.logger.warn("ERROR: connection lost, closing client: %s",
                                     err)
                raise ClientError("Closing: %s" % err)

    def send(self, msg):
        """
        Send a msg without waiting for reply
        """
        self.send_recv(msg, recv_len = 0)


if __name__ == "__main__":
    from mom.LogUtils import LogUtils
    import time
    LogUtils("debug")

    class EchoServerExample(TcpThreadedServer):
        def __init__(self):
            TcpThreadedServer.__init__(self, "localhost", 1234, None, "Server")

        def process(self, data):
            print "EchoServer Got: " + data
            return str.upper(data)

    echo = EchoServerExample()
    echo.serve_forever()
    clients = []
    for i in range(10):
        clients.append(Client("localhost", 1234, timeout=6,
                              name="client-%i" % i))
        response = clients[-1].send_recv("hi %i" % i)
        print "Client received: " + response
    for c in clients:
        c.close()
    echo.shutdown()

    # timeout testing...
    class DelayServerExample(TcpThreadedServer):
        def __init__(self):
            TcpThreadedServer.__init__(self, "localhost", 1234, None, "Server")

        def process(self, data):
            time.sleep(5)
            return str.upper(data)

    delay = DelayServerExample()
    delay.serve_forever()
    client = Client("localhost", 1234, timeout=2)
    try:
        reply = client.send_recv("hi")
        print reply
    except ClientError as e:
        print e
    client.close()
    delay.shutdown()


    # race testing
    def say_hi(handler, name):
        msg = "ik ben %s" % name
        try:
            print "%s: %s" % (name, msg)
            reply = handler.send_recv(msg)
            print "server to %s: %s" % (name, reply)
        except ClientError as e:
            print "server to %s: %s" % (name, e)
    echo = EchoServerExample()
    echo.serve_forever()
    client = Client("localhost", 1234, timeout=5, name="shared-client")
    print client
    threads = []
    for i in range(3):
        name = "nummer %i" % (i + 1)
        t = threading.Thread(target=say_hi, args=(client, name), name=name)
        threads.append(t)
        t.start()

    for t in threads:
        print "joining thread %s" % t.name
        t.join()
    client.close()
    echo.shutdown()