class HistoryWindow(object):
    '''
    a FIFO history window with a limited size
    '''

    def __init__(self, window_size):
        assert window_size >= 0, "window size must be non-negative"
        self.hist = []
        self.window_size = window_size

    def add(self, value):
        self.hist.insert(0, value)
        if len(self.hist) > self.window_size:
            self.hist.pop()

    def clear(self):
        self.hist = []

    def dump(self):
        return self.hist
    
    def len(self):
        return len(self.hist)
    
    def __getitem__(self, i):
        return self.hist[i]

class NumericHistoryWindow(HistoryWindow):
    '''
    a specialized history window for numeric classes
    can calculate the weighted average of the history, where the weights
    depend on the index of the elements (no weight function => same weight
    for all)
    '''

    def __init__(self, window_size, weight_function = None):
        HistoryWindow.__init__(self, window_size)

        assert weight_function is None or hasattr(weight_function, "__call__"), "weight function must be callable or none"
        if weight_function is None:
            self.weight_function = lambda x: 1
        else:
            self.weight_function = weight_function

    def average(self):
        if len(self.hist) == 0:
            return 0
        else:
            weights = [self.weight_function(i) for i in range(len(self.hist))]
            return sum([h * w for h, w in zip(self.hist, weights)]) / sum(weights)

    def min(self):
        if self.len() == 0:
            return 0
        else:
            return min(self.hist)

    def max(self):
        if self.len() == 0:
            return 0
        else:
            return max(self.hist)

if __name__ == '__main__':
    h = HistoryWindow(0)
    print h.dump()
    h.add(3)
    print h.dump()

    h = HistoryWindow(2)
    print h.dump()
    h.add(1)
    print h.dump()
    h.add(3)
    print h.dump()
    h.add(5)
    print h.dump()

    h = NumericHistoryWindow(2)
    print h.dump(), h.average()
    h.add(1)
    print h.dump(), h.average()
    h.add(3)
    print h.dump(), h.average()
    h.add(5)
    print h.dump(), h.average()

    h = NumericHistoryWindow(6, lambda i: 1./2**i)
    h.add(1)
    h.add(2)
    h.add(4)
    print h.dump(), h.average()
    h.add(3)
    print h.dump(), h.average()
    h.add(3)
    print h.dump(), h.average()
    h.add(5)
    print h.dump(), h.average()
    h.add(0)
    print h.dump(), h.average()

    h.clear()
    print h.dump(), h.average()
    h.add(4)
    h.add(2)
    h.add(1)
    print h.dump(), h.average()
    print h[0], h[1], h[2], h[-1]
