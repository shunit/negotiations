'''
Created on 21 ���� 2014

@author: fonaro
'''

class FunctionRepresentaion(object):
    '''
    Allow to add string representation to a function (i.e. tostring)
    '''

    def __init__(self, func, representation):
        '''
        Constructor
        '''
        self.func = func
        self.representation = representation

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)

    def __repr__(self):
        return "'%s'" % self.representation

def FunctionRepr(representation):
    def wrapper(func):
        return FunctionRepresentaion(func, representation)
    return wrapper

