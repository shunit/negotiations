'''
Created on Aug 30, 2011

@author: eyal
'''
from subprocess import Popen, PIPE
import time
import os
from exp.core.VMDesc import get_vm_desc, get_vm_ip
import logging
from etc.shell import run, SSHCommunicator, TimeoutError
import getpass
import xml.etree.ElementTree as et

from etc.FaultyFlag import FaultyFlag
from __builtin__ import isinstance
from etc.decorators import dict_represented

try:
    import libvirt
    from libvirt import libvirtError
except ImportError:
    pass

def xml_change_code(curr_kb, max_kb):
    import sys
    from xml.dom import minidom
    # read the xml
    filename = sys.argv[1]
    content = "".join(l.strip() for l in file(filename, "r"))
    # parse
    doc = minidom.parseString(content)
    # apply change
    doc.getElementsByTagName("memory")[0].firstChild.data = str(max_kb)
    doc.getElementsByTagName("currentMemory")[0].firstChild.data = str(curr_kb)

    # save xml back again
    file(filename, "w").write(doc.toxml())

def check_libvirt_mem_state(vm_name):
    from xml.dom import minidom
    content = "".join(run("virsh dumpxml %s" % vm_name).out.split("\n"))
    doc = minidom.parseString(content)
    max_mem = int(doc.getElementsByTagName("memory")[0].firstChild.data) >> 10
    curr_mem = int(doc.getElementsByTagName("currentMemory")[0].firstChild.data) >> 10
    return curr_mem , max_mem

class MachineException(Exception): pass

@dict_represented
class Machine(FaultyFlag):
    IMAGES_FOLDER = "/var/lib/libvirt/images"

    def __init__(self, vm_name, conn, cpu_pin = None, swappiness = 60,
                 title = None, use_image_cow = True,
                 master_image = "moc-master.qcow2", max_vcpus = 1):
        '''
        vm_name: the virtual machine name/alias
        conn: a libvirt connection - can be obtained using "libvirt.open('qemu:///system')"
        cpu_pin: a list of cpus to pin this machine to
        swappiness: the swappiness parameter for the guest OS in the VM
        title: a title for the machine for documentation purpose
        use_image_cow: use copy-on-write mechanism - instead of full copy of the master image
        master_image: the filename of the master image to use for this machine (should reside on IMAGES_FOLDER)
        max_vcpus: set the maximum cpus that is allocated to this machine (will change permanently)
        '''

        FaultyFlag.__init__(self)
        self.name = vm_name
        self.logger = logging.getLogger("Machine-" + self.name)

        self.start_time = None
        self.dom = None
        self.desc = get_vm_desc(self.name)
        self.set_guest_cpus(cpu_pin)
        self.ip = get_vm_ip(self.name)
        self.curr_mem_mb = 0
        self.swappiness = swappiness if 0 <= swappiness <= 100 else 60
        self.title = title
        self.use_image_cow = use_image_cow
        self.master_image = master_image

        self.conn = conn

        try:
            self.dom = conn.lookupByName(vm_name)
        except Exception, er:
            if self.name == "localhost": return
            raise MachineException("Bad VM name: %s, %s" % (vm_name, er))

        self.update_max_vcpus(max_vcpus)

    @property
    def as_dict(self):
        return dict(
                    name = self.name,
                    descriptor = self.desc,
                    swappiness = self.swappiness,
                    guest_cpus = self.guest_cpus,
                    title = self.title,
                    use_image_cow = self.use_image_cow,
                    master_image = self.master_image,
                    max_vcpus = self.max_vcpus
                    )

    def get_xml_desc(self):
        if self.name == "localhost":
            return

        return self.dom.XMLDesc(libvirt.VIR_DOMAIN_XML_UPDATE_CPU)

    def update_max_vcpus(self, max_vcpus):
        if self.name == "localhost":
            return

        self.max_vcpus = max_vcpus

        desc_xml = et.fromstring(self.get_xml_desc())
        vcpu = desc_xml.find("vcpu")
        if vcpu is None:
            vcpu = desc_xml.makeelement("vcpu", {})
            desc_xml.insert(1,vcpu)

        if "current" in vcpu.attrib:
            vcpu.attrib.pop("current")

        vcpu.text = str(max_vcpus)

        self.conn.defineXML(et.tostring(desc_xml))

    def set_cpu_count(self, cpu_count):
        if self.name == "localhost":
            return

        self.dom.setVcpus(cpu_count)

    def get_cpu_count(self):
        if self.name == "localhost":
            return

        return self.dom.info()[3]

    def get_guest_cpus(self):
        return self.guest_cpus

    def set_guest_cpus(self, cpu_pin):
        if cpu_pin:
            if not isinstance(cpu_pin, list):
                cpu_pin = [cpu_pin]

            self.guest_cpus = cpu_pin
        else:
            self.guest_cpus = None

        if self.dom and self.dom.isActive():
            self.apply_cpus_pin()

    def apply_cpus_pin(self):
        if self.name == "localhost":
            return

        if not self.guest_cpus:
            return

        cpu_vector_len = len(self.dom.vcpus()[1][0])

        if len(self.guest_cpus) == self.get_cpu_count():
            '''If we have vcpu per cpu, we can allocate them directly'''
            for i, cpu in enumerate(self.guest_cpus):
                self.dom.pinVcpu(i, tuple(x == cpu for x in range(0,cpu_vector_len+1)))
        else:
            '''If we have different amount of vcpus, we allocate them collectively'''
            cpus_tuple = tuple(x in self.guest_cpus for x in range(0,cpu_vector_len+1))

            for i in range(self.get_cpu_count()):
                self.dom.pinVcpu(i, cpus_tuple)

        # Command syntax: virsh vcpupin <vm-name> <vcpu> <real-cpu>
        # popen = run('virsh vcpupin %s %i %i' % (self.name, i, cpu))
        # if len(popen.err) > 0:
        #    raise MachineException("Failed set CPU to %s, reason: %s" %
        #                           (self.name, popen.err))

    def set_mem(self, mb):
        kb = mb << 10
        try:
            self.dom.setMemory(kb)
            self.curr_mem_mb = mb
            self.logger.info("Memory set to %i MB", mb)
        except libvirtError as err:
            self.logger.error("Couldn't set memory to %iMB, %s", mb, err)
            self.setFaulty()

    def set_libvirt_mem(self, curr_mb, max_mb, retry = 0):
        self.dom.setMaxMemory(max_mb << 10)
#        self.dom.setMemory(curr_mb << 10)
#
#          self.logger.warn("Using workaround for setMaxMemory")
#          if retry >= 3:
#              self.logger.warn("Didn't made the change in 3 retries")
#              return
#          curr_kb = curr_mb << 10
#          max_kb = max_mb << 10
#          # convert xml_change_code to string
#          code = inspect.getsource(xml_change_code).split("\n")[1:]
#          # remove tabs and change kb variable to the value
#          code = "\n".join((line[4:] for line in code)
#                  ).replace("max_kb", str(max_kb)).replace("curr_kb", str(curr_kb))
#          # write to temporary file the code
#          with tempfile.NamedTemporaryFile("w") as f:
#              f.write(code)
#              f.flush()
#              popen = run("virsh edit %s" % self.name, env = {"EDITOR": "python " + f.name})
#
#          acurr, amax = check_libvirt_mem_state(self.name)
#          if acurr != curr_mb or amax != max_mb:
#              self.logger.warn("XML Changes didn't work:\n"
#                               "max %i (instead of: %i)\n"
#                               "curr %i (instead of: %i)\n"
#                               "error: %s",
#                               amax, max_mb, acurr, curr_mb, popen.err)
#              self.set_libvirt_mem(curr_mb, max_mb, retry + 1)
#          else:
#              self.logger.info("Changed to: curr: %i, max: %i",
#                               acurr, amax)

    def reset_image(self):
        master_image_path = os.path.join(self.IMAGES_FOLDER, self.master_image)
        vm_image_path = os.path.join(self.IMAGES_FOLDER, "%s.qcow2" % self.name)

        # evil hack - remove me when bug is fixed
        run("sudo chown %s:libvirtd %s" % (getpass.getuser(), vm_image_path))

        if self.use_image_cow:
            self.logger.info("Creating VM image (master as backing copy - COW)")
            run("qemu-img create -b %s -f qcow2 %s" % (master_image_path, vm_image_path))
        else:
            self.logger.info("Creating VM image (full copy of master)")
            run("cp -f %s %s" % (master_image_path, vm_image_path))

    def start_domain(self):
        # if domain active- destroy it
        if self.dom.isActive():
            self.logger.warn("Machine was activated. Restarting machine.")
            self.dom.destroy()

        self.reset_image()

        # start the domain
        try:
            self.dom.create()
            self.dom.resume()
        except Exception as e:
            raise MachineException("Failed to start: %s" % e)
#            self.setFaulty()

        # pin the CPU
        self.apply_cpus_pin()
        self.logger.info("Started on CPUs %s", self.guest_cpus)

    def destroy_domain(self):
        if not self.dom:
            return
        try:
            self.dom.destroy()
            self.logger.info("Destroyed")
        except libvirt.libvirtError:
            self.logger.error("Couldn't destroy VM")
            pass

    def ssh(self, *cmd, **kwargs):
        if len(cmd) == 1:
            cmd = cmd[0].split()
        # set nice for remote programs
        # add PYTHONPATH and "python -O" prefix for python scripts
        if any(map(lambda s: s.find(".py") != -1, map(str, cmd))):
            python_cmd = ["PYTHONPATH=%s" % self.desc['moc_dir'], "nice", "-20", "python", "-O"]
            cmd = python_cmd + list(cmd)
        else:
            cmd = ["nice", "-20"] + list(cmd)
        return SSHCommunicator(cmd, self.desc['user'], self.ip, **kwargs)

    def wait_for_ssh_server(self):
        time_limitiation = 60
        ssh_timeout = 20
        count = 0

        while True:
            start = time.time()
            try:
                out = self.ssh("echo", "1",
                               name = "Test-%s" % self.name,
                               verbose = False).communicate(timeout = ssh_timeout)[0]
                if out.strip() == "1":
                    return
            except: pass
            count += 1
            if count >= time_limitiation / ssh_timeout:
                self.logger.info("SSH not ready yet")
            time.sleep(max(0, ssh_timeout - (time.time() - start)))

    def set_vm_props(self):
        props = [
                 'vm.min_free_kbytes=0',
                 'vm.overcommit_memory=1',  # prevent OOM error for process
                 'vm.swappiness=%s' % self.swappiness,
#                 'vm.mmap_min_addr=0',
#                 'vm.vfs_cache_pressure=0',
                 ]

        sysctl = "sysctl " + " ".join(props)
        while True:
            try:
                err = self.ssh(sysctl, name = "SSH-VMPropsSet", verbose = False).communicate(timeout = 60)[1]
            except TimeoutError:
                err = "Timeout"
            if err is not None and len(err) > 0:
                self.logger.info("Problem setting sysctl")
                time.sleep(5)
            else:
                return

    def log_info(self):
        fields = [
              'vm.min_free_kbytes',
              'vm.mmap_min_addr',
              'vm.overcommit_memory',
              'vm.swappiness',
              'vm.lowmem_reserve_ratio',
              ]

        vm = self.ssh("sysctl " + " ".join(fields),
                       name = "SSH-VMPropsGet", verbose = False
                       ).communicate(timeout = 60)[0]
        try:
            vm_stats = dict(row.split(" = ")
                            for row in vm.strip().split("\n"))
            self.logger.info(
                "VM Info:\n" +
                "min free mb: %i\n" % (int(vm_stats["vm.min_free_kbytes"]) >> 10) +
                "mmap_min_addr: %i\n" % int(vm_stats["vm.mmap_min_addr"]) +
                "overcommit_memory: %i\n" % int(vm_stats["vm.overcommit_memory"]) +
                "swappiness: %i" % int(vm_stats["vm.swappiness"])
                )
        except:
            self.logger.warn("Error getting sysctl info, cannot confirm correctness")
            self.setFaulty()

    def local_path(self, local):
        return os.path.join(self.desc['moc_dir'], local)

    def copy_code(self):
        while True:
            cmd = " ".join(
                ("rsync -azW --delete --exclude '*.pyc' --exclude '*.pyo'",
                 "%s/moc" % os.path.expanduser("~"),
                 "%s@%s:%s" % (self.desc["user"], self.ip, self.local_path("..")))
                )
            ret = Popen(cmd, shell = True,
                        stdout = PIPE, stdin = PIPE, stderr = PIPE).communicate()[1]
            if ret is not None and len(ret.strip()) > 0:
                self.logger.warn("Problem copy code, retrying...\n%s", ret)
                time.sleep(5)
            else:
                return

    @staticmethod
    def set_all_kvms_cpu(cpu_start):
        popen = run("pidof kvm")
        if popen.err:
            raise Exception("Coudn't find pid of kvm process")
        pids = map(int, popen.out.strip().split(" "))
        for i, pid in enumerate(pids):
            cpu = int(cpu_start + i)
            popen = run("taskset -p %s %i" % (hex(1 << cpu), pid))
            print "Set KVM process with PID %i to run on CPU %i" % (pid, cpu)
