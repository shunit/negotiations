'''
Created on Jun 12, 2012

@author: eyal
'''
import threading

class Terminatable(object):
    def __init__(self):
        self.__terminated = threading.Event()

    def terminate(self):
        self.__terminated.set()

    @property
    def should_run(self):
        return not self.__terminated.is_set()

class TerminatableThread(Terminatable, threading.Thread):
    def __init__(self, *args, **argv):
        Terminatable.__init__(self)
        threading.Thread.__init__(self, *args, **argv)

#class TerminatableThread(threading.Thread):
#    def __init__(self, *args, **argv):
#        threading.Thread.__init__(self, *args, **argv)
#
#    def terminate(self):
#        threading.Thread._Thread__stop(self) #@UndefinedVariable
#
#    @property
#    def should_run(self):
#        return threading.Thread.is_alive(self)

if __name__ == "__main__":
    import time

    class HiThread(TerminatableThread):
        def __init__(self):
            TerminatableThread.__init__(self, name="hi")

        def run(self):
            while self.should_run:
                print "hi"
                time.sleep(1)
            print "bye"

    a = HiThread()
    a.start()
    time.sleep(3)
    a.terminate()
    time.sleep(1)
