'''
Created on May 27, 2014
@author: fonaro
'''

import os
from os.path import expanduser
import socket
import getpass
import subprocess

class Settings(object):
    '''
    All setting must be read from this class
    '''
    DEFAULTS = {
                "output_dir": "~/moc-output",
                "machines_descriptor": "/var/moc/vms.xml",
                "machines_descriptor_psql": "/var/moc/vms-psql.xml",
                }

    @classmethod
    def readSetting(cls, name):
        return cls.DEFAULTS[name]

    @staticmethod
    def user_home():
        if 'SUDO_USER' in os.environ:
            return '/home/' + os.environ['SUDO_USER']
        return os.path.expanduser("~")

    @staticmethod
    def username():
        return getpass.getuser()

    @staticmethod
    def revision_info():
        return dict(
                id = subprocess.check_output(['hg', 'id', '--id']).strip(),
                branch = subprocess.check_output(['hg', 'id', '--branch']).strip(),
                tags = subprocess.check_output(['hg', 'id', '--tags']).strip()
                )

    @staticmethod
    def hostname():
        return socket.gethostname()

    @classmethod
    def output_dir(cls):
        return expanduser(cls.readSetting("output_dir"))

    @classmethod
    def machine_descriptor(cls):
        return cls.readSetting("machines_descriptor")

    @classmethod
    def machine_descriptor_psql(cls):
        return cls.readSetting("machines_descriptor_psql")
