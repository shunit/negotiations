'''
Created on Aug 23, 2013

@author: nir
'''

'''
adds a flag that determines if the results are faulty
'''
class FaultyFlag(object):
    def __init__(self, faulty=False):
        self.faulty = faulty

    def setFaulty(self, faulty=True):
        self.faulty = faulty

    @property
    def isFaulty(self):
        return self.faulty
