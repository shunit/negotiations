'''
Created on Sep 26, 2014

@autor: shunita

This file contains an implementation of the Negotiator class, that controls the
logic behind the negotiation between two agents (or more, in the future.)

It's main method is negotiate, which takes as an argument the proposal made by
another agent, and returns a counter proposal or the accept the given 
proposal.
'''
import sys
import logging
from mom.Comm.Proposals import DecreaseQProposal
# Importing status values for a proposal
from mom.Comm.Proposals import FIND_DELTA_Q,OFFER,ACCEPT1,REJECT,FINAL1

# Import supported negotiation types constants
from mom.Comm.Proposals import DECREASE_Q

UNDEFINED = -1

NO_PARTNERS = -1

class PartnerStats(object):
    def __init__(self):
        # my_point, rival_point will contain the last proposal made by each side.
        self.my_point = None
        self.rival_point = None

        # If need_to_negotiate is True, it means no agreement was made yet in this round.
        self.need_to_negotiate = True

        # Will hold the id of the guest who started the negotiation.
        self.first_to_talk = UNDEFINED

        # If do_not_trust is True, than we should never negotiate with this partner.
        self.do_not_trust = False

        # outgoing_queue will hold the outgoing messages for this partner. A new
        # message should be appended to the end of the guest's list, and then
        # taken out from the beginning of that list.
        self.outgoing_queue = []

class Negotiator(object):

    def __init__(self, guest_id, number_of_guests):
        '''
        @param guest_id - a string that represents the guest's ID.
        @param number_of_guests - the number of guests in the system, including self.
               The negotiator assumes that the guest names are consecutive numbers.
        '''
        self.guest_id = guest_id
        self.number_of_guests = number_of_guests
        self.logger = logging.getLogger('Negotiator')
        # history will hold all the messages sent and received by this agent in this
        # negotiation round.
        # TODO: not sure history is needed. something more concise should do.
        self.history = {}
        self.p_out_max = UNDEFINED
        self.p_in_min = UNDEFINED
        # Will hold the total amount of memory up for auction.
        self.auction_mem = UNDEFINED
        # Will hold the quantity the agent has bid for in the last round.
        self.recent_q = UNDEFINED

        self.partners = {}
        for i in range(number_of_guests):
            if (i != guest_id):
                self.partners[i] = PartnerStats()

        # current_partner is the index of the next guest to negotiate with.
        self.current_partner = 0


    def next_partner(self):
        guest_count = 0
        stat = self.partners[self.current_partner]
        
        while ((self.current_partner == self.guest_id) or
               (not stat.need_to_negotiate) or
               (stat.do_not_trust)) and (guest_count <= self.number_of_guests):
            self.current_partner += 1
            self.current_partner %= self.number_of_guests
            stat = self.partners[self.current_partner]
            guest_count += 1

        if guest_count <= self.number_of_guests:
            return self.current_partner
        return NO_PARTNERS
        

    def negotiate(self, proposal, data):
         #TODO: re-design the data parameter.
        '''
        Respond to a proposal from another agent.
        @param proposal - an instance of Proposal class or its derived class.
        @param data - a dictionary with at least the following keys: ?????????????????????
        @return -  "accept"/"reject"/"offer"/"delta_q"/"final"
          If the data is invalid, the action will be None.
        If the action is counter offer, a counter proposal is created and will be put in an outgoing 
        messages queue.
        '''
        if not isinstance(proposal, DecreaseQProposal):
            raise NotImplementedError
        if proposal.status == FIND_DELTA_Q:
           # This is the find_delta_q reply from the partner, meaning the guest should make an offer.
           if self.partners[proposal.sender_id].first_to_talk == self.guest_id:
               default_sum = 0.1*self.p_out_max if self.p_out_max != UNDEFINED else 0.1
               self.partners[proposal.sender_id].outgoing_queue.append(
                   DecreaseQProposal(sender_id = self.guest_id,
                                     receiver_id = proposal.sender_id,
                                     status = OFFER,
                                     compensation_sum = default_sum))
               return "offer"
           else: # The guest is the second one to talk, so he should send his preferred delta q.
               self.partners[proposal.sender_id].first_to_talk = proposal.sender_id
               self.partners[proposal.sender_id].outgoing_queue.append(
                   DecreaseQProposal(sender_id = self.guest_id,
                                     receiver_id = proposal.sender_id,
                                     status = FIND_DELTA_Q,
                                     delta_q = min(proposal.delta, 
                                                   self.recent_q if self.recent_q!=UNDEFINED else proposal.delta)))
               return "delta_q"

        elif proposal.status == OFFER:
            # Save the current negotiation point.
            self.partners[proposal.sender_id].rival_point = proposal
            #TODO: implement logic that uses data. currently the negotiator accepts all proposals.
            if proposal.sender_id not in self.history.keys():
                self.history[proposal.sender_id] = {"in": [proposal], "out": []}
            else:
                self.history[proposal.sender_id]["in"].append(proposal)

            # Accept means we no longer need to negotiate.
            self.partners[proposal.sender_id].need_to_negotiate = False

            # Also, "my_point" of the negotiation is the same as the rival's.
            self.partners[proposal.sender_id].my_point = proposal

            # Finally, the action is to accept the offer.
            return "accept"


    def first_proposal(self, negotiation_type, receiver_id):
        #TODO: add more keys to data's structure.
        '''
        Initiate negotiation with another agent.
        @param negotiation_type - currently only DECREASE_Q is supported.
        @param data - a dictionary with at least the following keys: bill, valuation
        @return - an instance of Proposal class.
        '''
        if negotiation_type != DECREASE_Q:
            self.logger.error('Negotiator: first_proposal: '+ \
                              'unimplemented negotiation type: ' + \
                               negotiation_type + '.')
            raise NotImplementedError
        if negotiation_type == DECREASE_Q:
            default_delta_q = ((self.available_mem * 0.1)/self.number_of_guests 
                                if self.available_mem != UNDEFINED else 1)
            my_q = self.recent_q if self.recent_q != UNDEFINED else default_delta_q
            new_proposal = DecreaseQProposal(sender_id = self.guest_id, 
                                             receiver_id = receiver_id,
                                             status = FIND_DELTA_Q,
                                             delta = min(default_delta_q, my_q))
            self.partners[receiver_id].first_to_talk = self.guest_id
            # keep a copy of the message in the history dictionary as an outgoing message.
            if receiver_id not in self.history.keys():
                self.history[receiver_id] = {"out": [new_proposal], "in": []}
            else:
                self.history[receiver_id]["out"].append(new_proposal)
            return new_proposal
        else:
            self.logger.error('Negotiator: first_proposal: '+ \
                              'Invalid negotiation type: ' + \
                               negotiation_type + '.')


    def get_message_to_send(self, negotiation_type):
        receiver_id = self.next_partner()

        if receiver_id == NO_PARTNERS:
            # There is no one to negotiate with in this round.
            return None

        if len(self.partners[receiver_id].outgoing_queue) == 0:
            # Never negotiated with this guest before.
            return self.first_proposal(negotiation_type, receiver_id)
        else:
            # Negotiation with this guest is already in progress.
            return self.partners[receiver_id].outgoing_queue.pop(0)
        
    def update(self, data, adviser_data):
        '''
        Updates the negotiator's p_in_min, p_out_max parameters.
        @param data - a DictDefaultNone received from an auction message.
                      Should contain various statistics about the auction.
        @param adviser_data - a dictionary that contains the adviser's decision,
            specifically bid_p_mem and bid_ranges_mem.
        Important Note: the use of negotiations does not currently support forbidden ranges,
        so bid_ranges_mem should be of the form [(0,q)].
        '''
        if data['p_out_max'] is not None:
            self.p_out_max = data['p_out_max']
        if data['p_in_min'] is not None:
            self.p_in_min = data['p_in_min']
        if data['auction_mem'] is not None:
            self.auction_mem = data['auction_mem']
        if 'bid_ranges_mem' in adviser_data.keys():
            self.recent_q = adviser_data['bid_ranges_mem'][0][1]
        #self.logger.info('Negotiator set p_out_max: %d, p_in_min: %d' % (self.p_out_max, self.p_in_min))

    def round_end(self):
        self.logger.info('End of negotiation round')
        for i in range(self.number_of_guests):
            if i != self.guest_id:
                self.partners[i].need_to_negotiate = True
