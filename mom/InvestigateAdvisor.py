'''
Created on Sep 8, 2011

@author: eyal
'''

from mom.Adviser import Adviser

class InvestigateAdviser(Adviser):
    def __init__(self, base_mem, d_load=100, memory_delta=50):
        Adviser.__init__(self, base_mem, d_load, memory_delta)

    def do_advice(self, mem_dict, base_mem, auction_mem):
        loads_of_money = 999
        # check if covered all the memory range
        if mem_dict is None:
            return 0, 0, 0

        max_mem = max(mem_dict.data.keys())
        min_mem = min(mem_dict.data.keys())
        # if we didn't discover minimum memory performance:
        if mem_dict.get_key(self.base_mem) < min_mem:
            print "== Do advice (#%i): Searching down" % self.rnd
            return 0, 0, 0
        # if we dont have all memory slots:
        q = int(max(max_mem, base_mem) + 2 * mem_dict.delta - base_mem)
        if q <= auction_mem:
            print "== Do advice (#%i): Searching up" % self.rnd
            print "   base: %i, auction: %i" % (base_mem, auction_mem)
            print "   max mem: %i, selected q: %i" % (max_mem, q)
            return q, 0, loads_of_money

        # select memory with least history records
        q = 0
        mem_range = lambda x: x >= self.base_mem and x <= base_mem + auction_mem
        mems = sorted(filter(mem_range, mem_dict.history.keys()),
                      reverse = True)
        min_count = -1
        for mem in mems:
            count = mem_dict.gethistory(mem).__len__()
            if count <= min_count or min_count == -1:
                q = max(mem - base_mem + mem_dict.delta, 0)
                min_count = count

        print "== Do advice (#%i):" % self.rnd
        print "   base: %i, auction: %i" % (base_mem, auction_mem)
        print "   max mem: %i, selected q: %i" % (max_mem, q)

        return loads_of_money, q, 0

def instance(base_mem, d_load = 100, memory_delta = 50):
    return InvestigateAdviser(base_mem, d_load, memory_delta)
