# Memory Overcommitment Manager
# Copyright (C) 2011 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import logging.handlers

class LogUtils(object):
    logging_handlers = {}
    configured = False

    def __init__(self, verbosity, log_to_stdio = True):
        if LogUtils.configured:
            return

        if isinstance(verbosity, str):
            verbosity = LogUtils.get_verbosity_level(verbosity)

        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        if log_to_stdio:
            handler = logging.StreamHandler()
            handler.setLevel(verbosity)
            handler.setFormatter(logging.Formatter(
                        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
            logger.addHandler(handler)
        LogUtils.configured = True

    @staticmethod
    def get_verbosity_level(verbosity):
        if not isinstance(verbosity, str):
            return verbosity
        verbosity = verbosity.lower()
        if verbosity == '5' or verbosity == 'debug':
            return logging.DEBUG
        elif verbosity == '4' or verbosity == 'info':
            return logging.INFO
        elif verbosity == '3' or verbosity == 'warn':
            return logging.WARN
        elif verbosity == '2' or verbosity == 'error':
            return logging.ERROR
        elif verbosity == '1' or verbosity == 'critical':
            return logging.CRITICAL
        return logging.DEBUG

    @classmethod
    def start_file_logging(cls, filename,
                           verbosity = logging.DEBUG,
                           max_bytes = 67108864, backups_count = 1000):
#                           max_bytes = 2097152, backups_count = 5):
        """
        @param verbosity: verbosity level:
            Notice that is limited with root logger verbosity.
            Leaving default value will log at the same verbosity is root looger.
        """

        if LogUtils.logging_handlers.has_key(filename):
            logging.log(logging.WARN, "Logging file already used")
            return

        verbosity = LogUtils.get_verbosity_level(verbosity)

        logger = logging.getLogger()
        handler = logging.handlers.RotatingFileHandler(
                                filename, 'a', max_bytes, backups_count)

        handler.setLevel(verbosity)
        handler.setFormatter(logging.Formatter(
                "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
        logger.addHandler(handler)
        cls.logging_handlers[filename] = handler

    @classmethod
    def stop_file_logging(cls, filename):
        try:
            logging.getLogger().removeHandler(cls.logging_handlers[filename])
            logging.log(logging.DEBUG, "stopping logging for " + filename)
            del cls.logging_handlers[filename]
        except KeyError:
            logging.log(logging.WARN, "LogUtils: No logging to specified file")

if __name__ == "__main__":
    LogUtils("info")
    LogUtils("debug")
    LogUtils.start_file_logging("log-test.log", "debug")

    logging.log(logging.INFO, "You should see me")
    logging.log(logging.WARN, "You should see me")
    logging.log(logging.DEBUG, "You shouldn't see me")

    LogUtils.stop_file_logging("log-test.log")

    logging.log(logging.WARN, "You should see me, but not in file")


