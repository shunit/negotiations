# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import time
import os
import logging

MON = "Monitor"
POL = "Policy"
PERF = "Performance"
HOST = "Host"
INFO = "Info"

class Plotter:

    logger = None
    handler = None

    tps = set((MON, POL, PERF))
    delim = "@"

    @classmethod
    def start_plotting(cls, output_dir):
        cls.logger = logging.getLogger("Plotter")
        cls.handler = logging.handlers.RotatingFileHandler(
                        filename = os.path.join(output_dir, "exp-plotter"),
                        mode = "a")
        cls.handler.setLevel(logging.DEBUG)
        cls.handler.setFormatter(logging.Formatter("%(message)s"))
        cls.logger.addHandler(cls.handler)
        cls.t0 = time.time()

    @classmethod
    def stop_plotting(cls):
        cls.logger.removeHandler(cls.handler)

    def __init__(self, name, tp):
        assert tp in self.tps
        self.name = name
        self.tp = tp


    def plot(self, data):
        t = (data["time"] if data.has_key("time") else time.time()) - self.t0
        self.logger.debug(self.delim.join(map(str,
                    (self.name, self.tp, t, data))))

    @classmethod
    def parse(cls, output_file_lines):
        results = {}
        for line in output_file_lines[1:]:
            if not line:
                continue
            try:
                name, tp, time, data = line.split(cls.delim)
            except ValueError as err:
                print "Couldn't parse line: %s" % line
                continue
            time = float(time)
            ### XXX: temporary:
            p = "p"
            m = "m"
            data = data.replace("Chunk", "").replace("=", ",")  #.replace("p", "0").replace("m","0")
            ## XXX
            data = eval(data)
            data["time"] = time
            results.setdefault(name, {}).setdefault(tp, []).append(data)

        return results
