# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import threading
import logging
from collections import deque
from mom.Collectors import Collector
from mom.Entity import Entity
from mom.Plotter import Plotter, MON
from etc.TerminatableThread import Terminatable
from ConfigParser import NoOptionError

class Monitor(Terminatable):
    """
    The Monitor class represents an entity, about which, data is collected and
    reported.  Each monitor has a dictionary of properties which are relatively
    static such as a name or ID.  Additionally, statistics are collected over
    time and queued so averages and trends can be analyzed.
    """
    def __init__(self, config, name):
        # Guard the data with a semaphore to ensure consistency.
        Terminatable.__init__(self)
        self.data_lock = threading.Lock()
        self.statistics = deque()
        self.variables = {}
        self.fields = None
        self.logger = logging.getLogger(name + "-Monitor")
        try:
            plotting = config.get("main", "plotting-enabled") == "True"
        except NoOptionError:
            plotting = False
        self.plotter = Plotter(name, MON) if plotting else None
        self.ready = threading.Event()
        self.config = config
        self.properties = self._properties
        self.collectors = Collector.get_collectors(
                        self._collectors_list(config), self.properties)

        self.fields = set([f for c in self.collectors for f in c.getFields()])
        self.logger.info("Using fields: %s", list(self.fields))

        self.collect()  # to set ready the monitor

    @property
    def _properties(self):
        """
        Should be overriden
        """
        return {}

    def _collectors_list(self, config):
        """
        Should be overriden
        """
        return []

    # TODO: the documentation of this function says:
    # "if two collectors produce the same statistic only the value produced by the first collector will be saved"
    # while the opposite is true.
    def collect(self):
        """
        Collect a set of statistics by invoking all defined collectors and
        merging the data into one dictionary and pushing it onto the deque of
        historical statistics.  Maintain a history length as specified in the
        config file.

        Note: Priority is given to collectors based on the order that they are
        listed in the config file (ie. if two collectors produce the same
        statistic only the value produced by the first collector will be saved).
        Return: The dictionary of collected statistics
        """

        # The first time we are called, populate the list of expected fields
        data = {}
        for c in self.collectors:
            try:
                data.update(c.collect())
            except Exception as e:
                self.logger.error("Collection %s error: %s", c.__class__, e)
#            except Collector.CollectionError as e:
#                self.logger.error("Collection %s error: %s", c.__class__, e)
#            except Exception as e:
#                self._set_not_ready("Fatal Collector %s, error: %s" % (c.__class__, e))
#                self.terminate()
#                return

        missing = list(self.fields - set(data))
        if len(missing) > 0:
            if self.is_ready:
                self.logger.error("Incomplete data: missing %s", missing)
            return

        with self.data_lock:
            self.statistics.append(data)
            if len(self.statistics) > self.config.getint('main', 'sample-history-length'):
                self.statistics.popleft()
        self._set_ready()

        if self.plotter is not None:
            self.plotter.plot(data)

        return data

    def interrogate(self):
        """
        Take a snapshot of this Monitor object and return an Entity object which
        is useful for rules processing.
        Return: A new Entity object
        """
        if not self.is_ready:
            self.logger.warn("Not ready yet for interrogation")
            return None
        ret = Entity(monitor = self)
        with self.data_lock:
            for prop in self.properties.keys():
                ret._set_property(prop, self.properties[prop])
            for var in self.variables.keys():
                ret._set_variable(var, self.variables[var])
            ret._set_statistics(self.statistics)
        ret._finalize()
        return ret

    def update_variables(self, variables):
        """
        Update the variables array to store any updates from an Entity
        """
        with self.data_lock:
            self.variables.update(variables)

    def _disp_collection_error(self, message = None):
        if message:
            if not self.is_ready:
                self.logger.debug('%s: %s', self.name, message)
            else:
                self.logger.warn('%s: %s', self.name, message)

    def _set_ready(self):
        if not self.is_ready:
            self.logger.info('Ready')
        self.ready.set()

    def _set_not_ready(self, message = None):
        self.ready.clear()
        self.logger.error(message)

    @property
    def is_ready(self):
        return self.ready.is_set()

    @property
    def should_run(self):
        """
        Private helper to determine if the Monitor should continue to run.
        """
        return (self.config.getint('__int__', 'running') == 1
                and super(Monitor, self).should_run)
