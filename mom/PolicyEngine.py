# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import threading
import time
import logging
from Policy.Policy import Policy
from mom.Plotter import Plotter, POL, HOST
from ConfigParser import NoOptionError

class PolicyEngine(threading.Thread):
    """
    At a regular interval, this thread triggers system reconfiguration by
    sampling host and guest data, evaluating the policy and reporting the
    results to all enabled Controller plugins.
    """
    def __init__(self, config, policy_file, libvirt_iface, host_monitor,
                 guest_manager, auction_type):
        threading.Thread.__init__(self, name = "PolicyEngine-%s" % auction_type)
        self.config = config
        self.auction_type = auction_type
        self.logger = logging.getLogger('mom.PolicyEngine-%s' % auction_type)
        self.policy_lock = threading.Lock()
        if self.load_policy(PolicyEngine.read_rules(policy_file)) == False:
            self.logger.error("Couldn't load policy!")
            raise Exception()
        self.properties = {
            'libvirt_iface': libvirt_iface,
            'host_monitor': host_monitor,
            'guest_manager': guest_manager,
            'config': config
            }
        try:
            plotting = config.get("main", "plotting-enabled") == "True"
        except NoOptionError:
            plotting = False
        self.plotters = ({HOST: Plotter(HOST, POL)}
                         if plotting else None)

    @staticmethod
    def read_rules(fname):
        if fname is None or fname == "":
            return ""
        f = open(fname, 'r')
        str = f.read()
        f.close()
        return str

    def load_policy(self, string):
        if string is None or string == "":
            self.logger.warn('%s: No policy specified.', self.getName())
            string = "0"  # XXX: Parser should accept an empty program
        try:
            new_pol = Policy(string)
            new_pol.set_auction_type(self.auction_type)
        except Exception as e:
            self.logger.info("Excpetion: %s" % str(e))
            return False
        with self.policy_lock:
            self.policy = new_pol
        return True

    def rpc_get_policy(self):
        with self.policy_lock:
            str = self.policy.get_string() if self.policy is not None else ""
        return str

    def rpc_set_policy(self, str):
        return self.load_policy(str)

    def get_controllers(self):
        """
        Initialize the Controllers called for in the config file.
        """
        self.controllers = []
        config_str = self.config.get(self.auction_type, 'controllers')
        for name in config_str.split(','):
            name = name.lstrip()
            if name == '':
                continue
            try:
                module = __import__('mom.Controllers.' + name, None, None, name)
                self.logger.debug("Loaded %s controller", name)
            except ImportError:
                self.logger.warn("Unable to import controller: %s", name)
                continue
            self.controllers.append(module.instance(self.properties))

    def do_controls(self):
        """
        Sample host and guest data, process the rule set and feed the results
        into each configured Controller.
        """
        # collect bids
        host = self.properties['host_monitor'].interrogate()
        if host is None:
            self.logger.warn("Host is None")
            return
        guest_list = self.properties['guest_manager'].interrogate().values()

        with self.policy_lock:
            ret = self.policy.evaluate(host, guest_list)
        if ret is False:
            self.logger.warn("False Evaluation")
            return

        for c in self.controllers:
            c.process(host, guest_list)

        if self.plotters is not None:
            # save control and variable of host and guest
            union_dict = lambda who : dict(who.controls.items() +
                                           who.variables.items())
            self.plotters[HOST].plot(union_dict(host))
            for guest in guest_list:
                name = guest.Prop('name')
                if not name in self.plotters:
                    self.plotters[name] = Plotter(name, POL)
                self.plotters[name].plot(union_dict(guest))

    def run(self):
        interval = self.config.getint('main', 'policy-engine-interval')
        self.logger.info("Starting. Interval: %s",interval)
        self.get_controllers()

        start = time.time()
        while self.should_run:
            time.sleep(max(0, interval - (time.time() - start)))
            start = time.time()
            self.do_controls()

        self.logger.info("Ended")

    @property
    def should_run(self):
        return self.config.getint('__int__', 'running') == 1

