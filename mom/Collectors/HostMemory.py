# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

from mom.Collectors.Collector import *

class HostMemory(Collector):
    """
    This Collctor returns memory statistics about the host by examining
    /proc/meminfo and /proc/vmstat.  The fields provided are:
        mem_available - The total amount of available memory (MB)
        mem_unused    - The amount of memory that is not being used for any purpose (MB)
        mem_free      - The amount of free memory including some caches (MB)
        anon_pages    - The amount of memory used for anonymous memory areas (MB)
        swap_in       - The amount of memory swapped in since boot (pages)
        swap_out      - The amount of memory swapped out since boot (pages)
        page_in       - The amount of memory paged in since boot (pages)
        page_out      - The amount of memory paged out since boot (pages)
        major_fault   - The amount of major page faults since boot (pages)
        minor_fault   - The amount of minor page faults since boot (pages)
    """
    def __init__(self, properties):
        self.meminfo = open_datafile("/proc/meminfo")
        self.vmstat = open_datafile("/proc/vmstat")
#         self.swap_in_prev = None
#         self.swap_in_cur = None
#         self.swap_out_prev = None
#         self.swap_out_cur = None
#         self.minflt = None
#         self.majflt = None

    def __del__(self):
        if self.meminfo is not None:
            self.meminfo.close()
        if self.vmstat is not None:
            self.vmstat.close()

    def parse_int_to_mb(self, regex, string):
        val = parse_int(regex, string)
        if val is not None:
            val = val >> 10
        return val

    def parse_int_def_zero(self, regex, string):
        val = parse_int(regex, string)
        return val if val is not None else 0

    def collect(self):
        self.meminfo.seek(0)
        self.vmstat.seek(0)

        contents = self.meminfo.read()
        avail = self.parse_int_to_mb("^MemTotal: (.*) kB", contents)
        anon = self.parse_int_to_mb("^AnonPages: (.*) kB", contents)
        unused = self.parse_int_to_mb("^MemFree: (.*) kB", contents)
        buffers = self.parse_int_to_mb("^Buffers: (.*) kB", contents)
        cached = self.parse_int_to_mb("^Cached: (.*) kB", contents)
        if None not in (unused, buffers, cached):
            free = unused + buffers + cached
            cache_and_buff = cached + buffers
        else:
            free = cache_and_buff = None

        # /proc/vmstat reports cumulative statistics so we must subtract the
        # previous values to get the difference since the last collection.
        contents = self.vmstat.read()
        swap_in = self.parse_int_def_zero("^pswpin (.*)", contents)
        swap_out = self.parse_int_def_zero("^pswpout (.*)", contents)
        minflt = self.parse_int_def_zero("^pgfault (.*)", contents)
        majflt = self.parse_int_def_zero("^pgmajfault (.*)", contents)

        page_in = self.parse_int_def_zero("^pgpgin (.*)", contents)
        page_out = self.parse_int_def_zero("^pgpgout (.*)", contents)

###############################################################################
# Previously reported swap and faults difference.
# Now we will report data since boot and use gradient for plot.
# Liran.
###############################################################################
#         self.swap_in_prev = self.swap_in_cur
#         self.swap_out_prev = self.swap_out_cur
#         self.swap_in_cur = self.parse_int_def_zero("^pswpin (.*)", contents)
#         self.swap_out_cur = self.parse_int_def_zero("^pswpout (.*)", contents)
#
#         if self.swap_in_prev is None:
#             self.swap_in_prev = self.swap_in_cur
#         if self.swap_out_prev is None:
#             self.swap_out_prev = self.swap_out_cur
#
#         swap_in = self.swap_in_cur - self.swap_in_prev
#         swap_out = self.swap_out_cur - self.swap_out_prev
#
#         minflt_cur = self.parse_int_def_zero("^pgfault (.*)", contents)
#         majflt_cur = self.parse_int_def_zero("^pgmajfault (.*)", contents)
#
#         minflt = (minflt_cur - self.minflt) if self.minflt is not None else 0
#         majflt = (majflt_cur - self.majflt) if self.majflt is not None else 0
#         self.minflt = minflt_cur
#         self.majflt = majflt_cur
###############################################################################

        return {
                'mem_available': avail, 'mem_unused': unused, 'mem_free': free,
                'swap_in': swap_in, 'swap_out': swap_out,
                'page_in': page_in, 'page_out': page_out,
                'anon_pages': anon , 'major_fault': majflt,
                'minor_fault': minflt, 'cache_and_buff': cache_and_buff
                }

    @staticmethod
    def getFields():
        return set(['mem_available', 'mem_unused', 'mem_free',
                    'swap_in', 'swap_out', 'page_in', 'page_out',
                   'anon_pages', 'major_fault', 'minor_fault',
                   'cache_and_buff'])

def instance(properties):
    return HostMemory(properties)
