#! /usr/bin/env python

import logging
from mom.Collectors.Collector import Collector
from mom.Comm.Messages import MessageStats

class GuestStatsCollector(Collector):
    """
    Runs on host, and collect stats from guest through GuestCommunicator.
    A guest memory stats Collector implemented over a socket connection.  Any
    data can be passed but the following stats are implemented:
        mem_available - The total amount of available memory (MB)
        mem_unused    - The amount of memory that is not being used for any purpose (MB)
        mem_free      - The amount of free memory including some caches (MB)
        anon_pages    - The amount of memory used for anonymous memory areas (MB)
        swap_in       - The amount of memory swapped in since boot (pages)
        swap_out      - The amount of memory swapped out since boot (pages)
        major_fault   - The amount of major page faults since boot (pages)
        minor_fault   - The amount of minor page faults since boot (pages)
    """

    def __init__(self, properties):
        Collector.__init__(self, properties)
        self.name = properties['name']
        self.logger = logging.getLogger('%s-StatsCollector' % self.name)
        # use any communications channel available
        self.comm = properties['comm_mem' if 'comm_mem' in properties.keys() else 'comm_bw']
        self.msg = MessageStats()

    def collect(self):
        try:
            data = self.comm.communicate(self.msg)
        except:
            return {}

        if data != {}:
            self.logger.debug("memory: total: %s, free: %s MB, cpu: %s",
                              str(data['mem_available']), str(data['mem_free']),
                              str(data['cpu_usage']))
        return data

    @staticmethod
    def getFields():
        return MessageStats.response_keys

def instance(properties):
    return GuestStatsCollector(properties)
