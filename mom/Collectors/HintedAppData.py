from mom.Collectors.Collector import Collector
from mom.Comm.GuestServer import GuestServer

class HintedAppData(Collector):

    def collect(self):
        """
        collect load received by hint from host to GuestServer with message
        MessageExpHintLoad
        """
        load = GuestServer.data['load']
        return {'load': load if load else 0}

    @staticmethod
    def getFields():
        return set(('load',))

def instance(properties):
    return HintedAppData(properties)
