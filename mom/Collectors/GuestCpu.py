# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

from mom.Collectors.Collector import *  #@UnusedWildImport
from subprocess import PIPE, Popen

class GuestCpu(Collector):
    def __init__(self, properties):

        program_name = properties['config'].get('main', 'program')

        if program_name == "Memcached":
            self.prog = "memcached"
        elif program_name == "DayTrader":
            self.prog = "java"
        elif program_name == "test":
            self.prog = "chrome"

        self.cpuinfo = open_datafile("/proc/stat")
        self.prev = self.get_usage_total()

        python_pids = [eval(x.strip()) for x in
                       Popen(["pgrep", "python"], stdout = PIPE).communicate()[0].strip().split("\n")]

        self.python1info = open_datafile("/proc/%i/stat" % python_pids[0])
        self.python2info = open_datafile("/proc/%i/stat" % python_pids[1]) \
            if len(python_pids) > 1 else None
        self.proginfo = []

        self.p1_prev = self.get_pid_usage(self.python1info)
        self.p2_prev = self.get_pid_usage(self.python2info)
        self.prog_prev = self.get_prog_usage()

    def __del__(self):
        for f in [self.cpuinfo, self.python1info, self.python2info] + self.proginfo:
            if f is not None:
                f.close()

    def get_prog_usage(self):
        if len(self.proginfo) == 0:
            try:
                prog_pid = [eval(x.strip()) for x in
                            Popen(["pgrep", self.prog], stdout = PIPE).communicate()[0].strip().split("\n")]
                for pid in prog_pid:
                    self.proginfo.append(open_datafile("/proc/%i/stat" % pid))
            except Exception:
                return 0
        return sum((self.get_pid_usage(x) for x in self.proginfo))

    def get_pid_usage(self, f):
        if f is None:
            return 0
        try:
            f.seek(0)
            contents = f.read()
        except IOError:
            f = None
            return 0
        words = [word for word in contents.split(" ") if len(word) > 0]
        try:
            return eval(words[13]) + eval(words[14])
        except:
            return None

    def get_usage_total(self):
        self.cpuinfo.seek(0)
        contents = self.cpuinfo.read()
        try:
            words = [eval(word)
                     for word in contents.split("\n")[0].split(" ")[1:]
                     if len(word) > 0]
            return (sum(words[0:3]),  # not including idle and IO
                    sum(words[0:4]),  # not including IO
                    sum(words[0:5]))  # total
        except:
            return None

    def collect(self):
        self.curr = self.get_usage_total()
        self.p1 = self.get_pid_usage(self.python1info)
        self.p2 = self.get_pid_usage(self.python2info)
        self.prog = self.get_prog_usage()

        if None in (self.curr, self.p1, self.p2, self.prog):
            return dict((k, None) for k in self.getFields())

        if self.prev is None: self.prev = self.curr
        if self.p1_prev is None: self.p1_prev = self.p1
        if self.p2_prev is None: self.p2_prev = self.p2
        if self.prog_prev is None: self.prog_prev = self.prog

        if self.curr[1] != self.prev[1]:
            usage = float(self.curr[0] - self.prev[0]) / (self.curr[1] - self.prev[1])
        else:
            usage = 0

        if self.curr[2] != self.prev[2]:
            deviator = float(self.curr[2] - self.prev[2])

            io_percent = ((self.curr[2] - self.curr[1]) - (self.prev[2] - self.prev[1])) / deviator
            p1 = min(1, (self.p1 - self.p1_prev) / deviator)
            p2 = min(1, (self.p2 - self.p2_prev) / deviator)
            prog = min(1, (self.prog - self.prog_prev) / deviator)
        else:
            io_percent = p1 = p2 = prog = 0

        self.prev = self.curr
        self.p1_prev = self.p1
        self.p2_prev = self.p2
        self.prog_prev = self.prog

        return {"cpu_usage": round(usage, 2),
                "io_percent": io_percent,
                "python1_usage": round(p1, 2),
                "python2_usage": round(p2, 2),
                "prog": round(prog, 2)}

    @staticmethod
    def getFields():
        return set(["cpu_usage", "io_percent", "python1_usage", "python2_usage", "prog"])

def instance(properties):
    return GuestCpu(properties)

