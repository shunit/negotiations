from mom.Collectors.Collector import *
from mom.Controllers.NetTc import NetTc
import logging
import subprocess
import shlex
import re

class GuestBandwidthTCException(Exception):
    pass

class GuestBandwidthTC(Collector):
    """
    This Collctor returns bandwidth statistics about the Guest by examining output of TC statistics
    """

    # most data here isn't required but it is kept both as a reference and for future use
    # currently we only need: id, sent
    TC_STATS_KEYS =     ["net_" + x for x in 
                        ['id', 'rate', 'ceil', 'sent', 'pkts',\
                         'dropped', 'overlimits', 'requeues',\
                         'cur_rate', 'pps', 'backlog_bytes',\
                         'backlog_pkts', 'cur_requeue', 'lended',\
                         'borrowed', 'giants', 'tokens', 'ctokens']]

    def __init__(self, properties):
        self.logger = logging.getLogger('GuestBandwidthTC')
        self.properties = properties
        # assume name is of the form "vm-%d" where %d is the number
        self.name = properties['name']
        self.id   = int(self.name[ self.name.find('-') + 1 :]) + NetTc.CLASSID_START - 1
        self.net_interface = properties['config'].get('main', 'net-interface')
        self.logger.info("Started for classid %d for %s" % (self.id, self.name))

    def collect(self):
        # define regular expressions for extracting data
        TC_STATS_RE_ROOT1 = 'class htb 1:(\d+) root rate (\d+)(Mbit|Kbit|bit) ceil (\d+)(Mbit|Kbit|bit)'
        TC_STATS_RE1 = 'class htb 1:(\d+) parent \d+:\d+ leaf \d+: prio \d+ rate (\d+)(Mbit|Kbit|bit) ceil (\d+)(Mbit|Kbit|bit)'
        TC_STATS_RE2 = 'Sent (\d+) bytes (\d+) pkt \(dropped (\d+), overlimits (\d+) requeues (\d+)\)'
        TC_STATS_RE3 = 'rate (\d+)(Mbit|Kbit|bit) (\d+)pps backlog (\d+)b (\d+)p requeues (\d+)'
        TC_STATS_RE4 = 'lended: (\d+) borrowed: (\d+) giants: (\d+)'
        TC_STATS_RE5 = 'tokens: (-\d+|\d+) ctokens: (-\d+|\d+)'

        # run command to read the data(uses netlink sockets)
        p = subprocess.Popen(shlex.split('tc -s class show dev %s' % self.net_interface), stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        out, err = p.communicate()
        lines = out.split('\n')
        qdiscs = []
        qdisc = []
        q = None
        for line in lines:
            if line == '':
                if qdisc != []:
                    qdiscs.append(qdisc)
                qdisc = []
            else:
                qdisc.append(line.lstrip())

        for qdisc in qdiscs:
            if ('1:%d' % self.id) in ''.join(qdisc):
                q = qdisc
                break

        if q == None:
            self.logger.warn('No qdisc defined yet for classid %d' % self.id)
            return { x : 0 for x in GuestBandwidthTC.TC_STATS_KEYS }

        
        # extract data
        try:
            tc_stats = { x : 0 for x in GuestBandwidthTC.TC_STATS_KEYS }
            if 'root' in q[0]:
                tc_stats['net_id'], tc_stats['net_rate'], rate_mult,  tc_stats['net_ceil'], ceil_mult =\
                     re.match(TC_STATS_RE_ROOT1, q[0]).groups()
            else:
                tc_stats['net_id'], tc_stats['net_rate'], rate_mult, tc_stats['net_ceil'], ceil_mult  =\
                     re.match(TC_STATS_RE1, q[0]).groups()
            tc_stats['net_sent'], tc_stats['net_pkts'], tc_stats['net_dropped'], tc_stats['net_overlimits'], tc_stats['net_requeues']  =\
                re.match(TC_STATS_RE2, q[1]).groups()
            tc_stats['net_cur_rate'], cur_rate_mult, tc_stats['net_pps'], tc_stats['net_backlog_bytes'], tc_stats['net_backlog_pkts'], tc_stats['net_cur_requeue'] =\
                 re.match(TC_STATS_RE3, q[2]).groups()
            tc_stats['net_lended'], tc_stats['net_borrowed'], tc_stats['net_giants'] =\
                 re.match(TC_STATS_RE4, q[3]).groups()
            tc_stats['net_tokens'], tc_stats['net_ctokens'] =\
                 re.match(TC_STATS_RE5, q[4]).groups()
            if rate_mult == 'Kbit':
                tc_stats['net_rate'] =\
                     str(int(tc_stats['net_rate']) * 1000)
            elif rate_mult == 'Mbit':
                tc_stats['net_rate'] =\
                     str(int(tc_stats['net_rate']) * 1000 * 1000)
            if ceil_mult == 'Kbit':
                tc_stats['net_ceil'] =\
                     str(int(tc_stats['net_ceil']) * 1000)
            elif ceil_mult == 'Mbit':
                tc_stats['net_ceil'] =\
                     str(int(tc_stats['net_ceil']) * 1000 * 1000)
            if cur_rate_mult == 'Kbit':
                tc_stats['net_cur_rate'] =\
                    str(int(tc_stats['net_cur_rate']) * 1000)
            elif cur_rate_mult == 'Mbit':
                tc_stats['net_cur_rate'] =\
                    str(int(tc_stats['net_cur_rate']) * 1000 * 1000)



        except Exception as e:
            self.logger.error("Got exception: %s" % e)
            self.logger.error(out)
        
        return {k : int(v) for k,v in tc_stats.items()}

    @staticmethod
    def getFields():
        return set(GuestBandwidthTC.TC_STATS_KEYS)

def instance(properties):
    return GuestBandwidthTC(properties)

if __name__ == '__main__':
    from time import sleep
    from ConfigParser import SafeConfigParser as scp
    properties = scp()
    properties.add_section('main')
    properties.set('main', 'net-interface', 'eth1')
    properties.set('main', 'name', 'vm-1')

    print properties.get('main', 'net-interface')
    hc = GuestBandwidthTC({'config' : properties,
                           'name'   : 'vm-1'})
    print hc.getFields()
    for i in range(10):
        sleep(3)
        print hc.collect()
        print '---' * 10
        print
