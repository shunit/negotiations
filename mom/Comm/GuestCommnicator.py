#! /usr/bin/env python
# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

from etc.PySock import Client
import logging
from mom.Comm.Messages import MessageError, MessageTimeoutError
import time
import threading


class GuestCommunicator():
    seconds_to_retry = 3

    def __init__(self, ip, port, timeout, name):
        self.client = Client(ip, port, timeout = timeout,
                               name = "%s-communication-client" % name)
        self.logger = logging.getLogger('%s-communicator' % name)

        self.connected = False
        self.last_fail = None

        self.lock = threading.Lock()
        self.logger.info('Guest communicator for ip %s is initialized.', ip)

    def __del__(self):
        self.close()

    def close(self):
        self.client.close()
        self.logger.info("Closed")

    def communicate(self, msg, specific_timeout=None):
        """
        Send a message and collect respond from client
        response must be a string representing a dict
        """
        data = {}

        with self.lock:
            # check time between retries
            if (not self.connected and self.last_fail is not None and
                    time.time() - self.last_fail < self.seconds_to_retry):
                return data
            if specific_timeout is not None:
                self.client.set_timeout(specific_timeout)
            # try to send data
            try:
                data = msg.communicate(self.client)
            except MessageTimeoutError as err:
                self.logger.error('Communication timeout: %s', err)
                raise
            except MessageError as err:
                self.last_fail = time.time()
                # if disconnected, show warning
                if self.connected:
                    self.connected = False
                    self.logger.warn("Communication stopped: %s", err)
                raise
            # if connected after disconnection
            if not self.connected:
                self.connected = True
                self.logger.info("Connected")

        assert isinstance(data, dict), "Data must be dict type"
        return data

