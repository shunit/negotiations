'''
This file contains a base class for proposals made between negotiating agents,
as well as the specific derived class for SmallerBidCompensationProposal.
'''
import struct
import os
import logging

# Constants for DecreaseQProposal status values.
FIND_DELTA_Q = 0
OFFER = 1
ACCEPT1 = 2
ACCEPT2 = 3
REJECT = 4
FINAL1 = 5
FINAL2 = 6
COUNTER_OFFER = 7
NOT_LEADER = 8
UPDATE_MEMBERS = 9
COALITION_SIZE = 10

# Constants for proposal types.
DECREASE_Q = 100
ELIMINATE_Q = 101

def dict_to_proposal(proposal_dict):
    '''
    Creates a Proposal instance out of a dictionary.
    Currently only DecreaseQProposal is supported.
    @param proposal_dict - a dictionary whose keys contain the proposal properties
           (proposal_type, sender_id, receiver_id, compensation_sum, delta).
    @return - the Proposal instance on success, None otherwise.
    '''
    logger = logging.getLogger('dict_to_proposal')
    if 'proposal_type' not in proposal_dict.keys():
        logger.error('Proposal dict does not have a proposal_type.')
        return None
    if proposal_dict['proposal_type'] != DECREASE_Q:
        logger.error('Proposal type is not supported')
    proposal_dict.pop('proposal_type')
    proposal = DecreaseQProposal(**proposal_dict)
    if proposal.status == FIND_DELTA_Q and not hasattr(proposal, 'delta'):
        logger.error('Proposal dict has status FIND_DELTA_Q and no delta')
        return None
    if proposal.status != REJECT and not hasattr(proposal, 'compensation_sum'):
        logger.error('Proposal dict has status that is not REJECT and no compensation_sum')
        return None
    return proposal



class Proposal(object):
    def __init__(self, sender_id, receiver_id):
        '''
        @param sender_id - a string representing the name of the agent who sent 
            the message. Should be a number.
        @param receiver_id - a string representing the name of the agent who will 
            receive the message. Should be a number. 
        '''
        self.sender_id = sender_id
        self.receiver_id = receiver_id

    def as_dictionary(self):
        return {'sender_id': self.sender_id, 'receiver_id': self.receiver_id}

class DecreaseQProposal(Proposal):
    def __init__(self, sender_id, receiver_id, status, compensation_sum = -1, 
                 delta = -1):
        '''
        @param sender_id - a string representing the name of the agent who sent 
            the message.
        @param receiver_id - a string representing the name of the agent who will
            receive the message. Should be a number.
        @param status - a number representing the status of the negotiation.
            See constants above for the different values: FIND_DELTA_Q, OFFER,
            REJECT, ACCEPT1, FINAL1.
        @param compensation_sum - a positive number representing the amount of 
            money per round, that will be paid to the agent who reduced his 
            quantity in the bid.
        @param delta - a positive number representing the amount of memory the 
            sender would like to discuss.
        '''
        super(DecreaseQProposal, self).__init__(sender_id, receiver_id)
        self.status = status
        if compensation_sum > 0:
            self.compensation_sum = compensation_sum
        if delta > 0:
            self.delta = delta
	
    def as_dictionary(self):
        return self.__dict__
