'''
Created on Jul 29, 2011

@author: eyal
'''
from mom.Comm.Messages import get_message
from etc.PySock import TcpThreadedServer
import logging
import collections

class DictDefaultNone(collections.defaultdict):
    def __init__(self):
        collections.defaultdict.__init__(self, lambda: None)

    def __repr__(self):
        return repr(dict(self))

    def __str__(self):
        return str(dict(self))

class GuestServer(TcpThreadedServer):
    """
    A simple TCP server that implements the guest side of the guest network
    Collector.
    """
    port_mem = 2187
    port_guest_comm_mem = 2189
    ports = {"mem": port_mem}
    data = DictDefaultNone()

    def __init__(self, config, monitor, policy, port):
        self.config = config
        self.monitor = monitor
        self.policy = policy
        self.logger = logging.getLogger('GuestServer')
        # If nothing is returned, then it looks only for the port
        ip = config.get('main', 'host')

        TcpThreadedServer.__init__(self, ip, port, timeout = None,
                                   name = 'GuestServer')
        self.logger.info('Guest server for ip %s is up and running.', ip)

    def process(self, msg):
        # parse message to cmd and args:
        try:
            message = get_message(msg)
            return message.process(self.data, self.monitor, self.policy)
        except Exception as err:
            self.logger.error("Error parsing message: %s, %s", msg, err)
            logging.exception("")
            return {"ack": False}

    def interrogate(self):
        return self.data.copy_all()
