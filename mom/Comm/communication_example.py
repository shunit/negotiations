from etc.PySock import TcpThreadedServer, Client

class MyTcpServer(TcpThreadedServer):
    def __init__(self, host, port):
        TcpThreadedServer.__init__(self, host, port)

    def process(self, msg):
        print msg

server = MyTcpServer(host="localhost", port=9731)
server.serve_forever()
try:
    print "initializing client and message"
    client = Client(host="127.0.0.1", port=9731)
    #msg = Message(hello="hello there!")
    print "sending message to client"
    client.send_recv("hello there", 0)
    client.close()
    print "finished"
except:
    pass
server.shutdown()
