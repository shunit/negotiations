'''
Created on Oct 27, 2014

@author: shunita
'''

from mom.Comm.Messages import get_message
from etc.PySock import TcpThreadedServer
import logging
import collections

class DictDefaultNone(collections.defaultdict):
    def __init__(self):
        collections.defaultdict.__init__(self, lambda: None)

    def __repr__(self):
        return repr(dict(self))

    def __str__(self):
        return str(dict(self))

class GuestServerWithPort(TcpThreadedServer):
    """
    A simple TCP server that implements the guest side of the guest network
    Collector.
    """
    def __init__(self, monitor, policy, ip = 'localhost', port = 2187):
        self.monitor = monitor
        self.policy = policy
        self.logger = logging.getLogger('GuestServer')
        self.port = port
        self.data = DictDefaultNone()
        TcpThreadedServer.__init__(self, ip, self.port, timeout = None,
                                   name = 'GuestServerWithPort')

    def process(self, msg):
        # parse message to cmd and args:
        try:
            message = get_message(msg)
            return message.process(self.data, self.monitor, self.policy)
        except Exception as err:
            self.logger.error("Error parsing message: %s, %s", msg, err)
            return {"ack": False}

    def interrogate(self):
        return self.data.copy_all()
