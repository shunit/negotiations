# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import threading
import time
from mom.Monitor import Monitor

class HostMonitor(Monitor, threading.Thread):
    """
    The Host Monitor thread collects and reports statistics about the host.
    """
    def __init__(self, config):
        # thread's name is important to Plotter, must be: Host-Monitor
        threading.Thread.__init__(self, name = "Host-Monitor")
        Monitor.__init__(self, config, "Host")

    @property
    def _properties(self):
        return {'config': self.config}

    def _collectors_list(self, config):
        return config.get('host', 'collectors')

    def run(self):
        self.logger.info("Started")
        interval = self.config.getint('main', 'host-monitor-interval')
        while self.should_run:
            self.collect()
            time.sleep(interval)
        self.logger.info("Ended")

    def guests_current_total_mem(self):
        """
        Gets the total memory of each guest
        :return: A dictionary containing all the guests and the quantity of total memory it has
        """
        return {}
