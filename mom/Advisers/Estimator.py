'''
Created on Mar 6, 2013

@author: eyal
'''

import numpy as np

class Bills():
    def __init__(self):
        self.bills = []

    def add(self, bill, rnd):
        self.bills.append((bill, rnd))

    def val(self, rnd):
        w = np.array(map(lambda b: 1. / (rnd - b[1]), self.bills))
        b = np.array(map(lambda b: b[0], self.bills))
        return np.average(b, weights = w)

    def remove_old(self, min_rnd):
        self.bills = filter(lambda x: x[1] >= min_rnd, self.bills)

    def __len__(self):
        return len(self.bills)

class Estimator(object):
    def __init__(self, qs):
        self.qs = np.array(qs)
        self.bills = np.array([Bills() for i in range(len(qs))])
        self.max_age = 10
        self.logger = logging.getLogger("ESTIMATOR")

    def ind(self, q):
        return np.argmin(np.abs(self.qs - q))

    def get_bill(self, q):
        # the bill range is where qs[i] <= q < qs[i+1]
        return self.bills[self.ind(q)]

    def add_bill(self, q, bill, rnd):
        if q == 0:
            return
        i = self.ind(q)
        self.bills[i].add(float(bill) / q * self.qs[i], rnd)

    def _estimate(self, q, pmin, rnd, qs, bills):
        ub = bills / np.array([min(qi, q) for qi in qs])
        lb = bills / np.array([max(qi, q) for qi in qs])
        ub = np.array([min(pi, pmin) for pi in ub])
        lb = np.array([min(pi, pmin) for pi in lb])
        extra_p = np.array([bills[0] / qs[0]] + list((bills[1:] - bills[:-1]) / (qs[1:] - qs[:-1])))
        extra_p = np.array([min(max(0, pi), pmin) for pi in extra_p])
        ub_by_extra_p = (bills + (q - qs) * extra_p) / q

        # calculate the estimated lower and upper bound with weights
        # inverse to their distance from the calculated q
        w = 1. / (np.abs(q - qs) + 2 * qs[-1])
        ub_est = min(np.average(ub, weights = w),
                     np.average(ub_by_extra_p, weights = w),
                     pmin)
        lb_est = min(np.average(lb, weights = w), pmin)

        return 0.5 * (lb_est + ub_est)

    def estimate(self, q, pmin, rnd):
        # clear old data
        self.logger.error("ESTIMATOR ALERT: estimator is clearing old data.")
        for bills in self.bills:
            bills.remove_old(rnd - self.max_age)

        # calculate qs and bills only once
        inds = (np.array(map(len, self.bills)) > 0)
        qs = self.qs[inds]

        if len(qs) == 0:
            return [0] * len(q)

        bills = np.array(map(lambda b: b.val(rnd), self.bills[inds]))

        # return the estimation
        return [self._estimate(qi, pmin, rnd, qs, bills) for qi in q]

if __name__ == "__main__":
    import pylab as pl

    q = np.arange(100, 1000 + 1, 100)
    est = Estimator(q)
    hist = [
#            [40, 10, -1],
#            [20, 4, -1],
            [130, 10, -1],
            [340, 24, -2],
            [810, 36, -7],
            [380, 30, -80],
            [420, 31, -5],
            [840, 35, -4],
            [870, 36, -8],
            [1000, 36, -10],
            ]
#    q = np.arange(1, 10 + 1)
#    est = Estimator(q)
#    hist = [
#            [1, 10, -1],
#            [3, 24, -2],
#            [8, 36, -7],
#            [4, 30, -80],
#            [4, 31, -5],
#            [8, 35, -4],
#            [9, 36, -8],
#            [10, 36, -10],
#            ]
    for h in hist:
        est.add_bill(*h)

    qs = np.arange(0, 1000, 10)
#    qs = np.arange(0,11)

    p = est.estimate(qs, 0.8, 0)
    pl.plot(qs, [pi[0] for pi in p] , "b", label = "lb")
    pl.plot(qs, [pi[1] for pi in p], "r", label = "ub")
    pl.plot(qs, [0.5 * sum(pi) for pi in p], "k", label = "avg")

    pl.grid(True)
    pl.legend(loc = "best")

    for h in hist:
        pl.plot(h[0], float(h[1]) / h[0], c = pl.cm.jet(float(-h[2]) / 10),  #@UndefinedVariable
#                linestyle = "None", marker = "o", alpha = 0.5, markersize = 8)
                linestyle = "None", marker = "o", alpha = 1.0, markersize = 8)

    pl.show()



