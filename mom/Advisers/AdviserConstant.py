from mom.Adviser import Adviser
from mom.Profiler import Profiler

class AdviserConstant(Adviser):
    def __init__(self, p_mem=0):
        self.p_mem = p_mem
        Adviser.__init__(self, None, 0,
            'hits_rate', 10, lambda x: x)

    def do_advice_mem(self, state, auction_mem):
        return self.p_mem, ([0, auction_mem],)

if __name__ == '__main__':
    a = AdviserConstant(p_mem=0.4)
    print a.do_advice_mem([5, 600], 100)
    print a.do_advice_mem([3, 600], 100)
    print a.do_advice_mem([10, 0], 1000)
