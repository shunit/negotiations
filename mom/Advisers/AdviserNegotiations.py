import csv
import logging
import os
from threading import Timer
from threading import Lock
import sys

from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from mom.Comm.GuestServer import GuestServer
from mom.Comm.GuestCommnicator import GuestCommunicator
from mom.Comm.Messages import MessageAdjustDeltaQ, MessageAdjustSidePayment
from mom.Comm.Proposals import DECREASE_Q, OFFER, COUNTER_OFFER, REJECT, ACCEPT1, FINAL1, ACCEPT2, FINAL2, ELIMINATE_Q


class ForbiddenRangesException(Exception):
    pass


class AdviserNegotiations(AdviserProfitEstimator):
    """
    Adviser with negotiations implementation. In each memory auction round, it executes negotiations with other guests
    and factors them in the returned bid.
    """

    END_OF_NEGOTIATIONS_ROUND = 7
    MATH_EPSILON = 0.0001

    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, vm_number, num_of_guests,
                 comm_timeout, max_total_side_payment, min_compensation, enable_negotiations=True):
        """
        :param max_total_side_payment: the maximal side payment this guest is willing to pay all bill deals together.
            expressed as a fraction of the guest's bill, therefore its value is between [0,1].
        :param min_compensation: The minimal value of compensation the guest is willing to accept in exchange for
            sitting out the auction. Expressed as a fraction of the guest's valuation of the memory he is asking for,
            therefore its value is between [0,1].
        """
        self.vm_number = vm_number
        self.guest_communicators = {}
        self.guest_data = {}  # Don't touch this, use _update_guest_data() and _retrieve_guest_data()
        self.num_of_guests = num_of_guests
        self.auction_mem = 0
        self.p_max_out = 0
        self.current_q = 0
        self.enable_negotiations = enable_negotiations
        self.max_total_side_payment = max_total_side_payment
        self.min_compensation = min_compensation
        self.chosen_partner_id = -1
        self.state = [0, 0]
        self.guest_data_lock = Lock()
        self.extra_memory = 0.0
        self.rejectTimeoutTimer = None  # timer that sends a reject message if other side didn't answer
        for i in range(1, num_of_guests + 1):
            if i == vm_number:
                continue
            self.guest_communicators[i] = GuestCommunicator('192.168.123.%d' % (1 + i),
                                                            GuestServer.port_guest_comm_mem,
                                                            comm_timeout, i)
            self._update_guest_data(i, "valuation_lower_than_mine", False)

        AdviserProfitEstimator.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func)

    def do_advice_mem(self, state, auction_mem):
        self.auction_mem = auction_mem
        # TODO: implement bidding according to negotiation results
        bid = AdviserProfitEstimator.do_advice_mem(self, state, auction_mem)
        if self.is_first_round_mem():
            return bid

        if not self.enable_negotiations:
            return bid

        self.logger.debug("bid before: %s", bid)

        # Checking that there are no forbidden ranges
        if len(bid[1]) != 1 or bid[1][0][0] != 0:
            raise ForbiddenRangesException()

        self.current_q = bid[1][0][1]

        if self.chosen_partner_id >= 0:
            own_delta_q = self._retrieve_guest_data(self.chosen_partner_id, "own_delta_q")
            bid = (self._p_for_q(bid[1][0][1]), [(bid[1][0][0], bid[1][0][1] + own_delta_q)])

        self.logger.debug("bid after: %s", bid)

        return bid

    def do_adjust_delta_q(self, message_content):
        """
        Handles a proposal of Delta Q adjustment. If the message is an offer, so a counter-proposal is sent with the
        desired Delta Q. Otherwise the message must be already the counter-proposal, so this method handles it
        accordingly.
        :param message_content: contains the content of the sent message.
        """
        delta_q = self._determine_delta_q()
        sender_id = message_content["sender_id"]

        # If received an offer, then the adviser needs to return a counter offer or reject the offer
        if message_content["status"] == OFFER:
            # Checking if the message is duplicated, that means: suppose there are two guests, 1 and 2. If 1 sent a
            # message for 2 and 2 sent a message for 1, then the guest with the lower number (in this case - guest 1)
            # will ignore the guest with the higher number's message (in this case the message from 2 to 1).
            if ((self._retrieve_guest_data(sender_id, 'sent_message_adjust_delta_q')) and (
                        self.vm_number < sender_id)):
                self.logger.info(
                    "Message %s ignored by %s because it is duplicated." % (message_content, self.vm_number))
                return

            status = COUNTER_OFFER
            self._send_message(sender_id, MessageAdjustDeltaQ(
                sender_id=self.vm_number, proposal_type=DECREASE_Q, status=status, delta_q=delta_q
            ))
            agreed_delta_q = min(message_content["delta_q"], delta_q)
            self.logger.debug("The agreed delta_q is %s", agreed_delta_q)
            self._update_guest_data(sender_id, "agreed_delta_q", agreed_delta_q)

        # If received a counter offer, the adviser must send a side payment offer
        elif message_content["status"] == COUNTER_OFFER:
            agreed_delta_q = min(message_content["delta_q"], delta_q)
            self.logger.debug("The agreed delta_q is %s", agreed_delta_q)
            self._update_guest_data(sender_id, "agreed_delta_q", agreed_delta_q)
            side_payment = self._get_epsilon(sender_id)
            self._send_message(sender_id, MessageAdjustSidePayment(
                sender_id=self.vm_number, proposal_type=DECREASE_Q, status=OFFER, side_payment=side_payment
            ))

        # If received a rejection, the adviser must wait for the next auction
        elif message_content["status"] == REJECT:
            self._update_guest_data(sender_id, "rejected_in_this_round", True)
            return

        else:
            raise NotImplementedError("do_adjust_delta_q: Method not implemented for status = %s" %
                                      message_content["status"])

    def do_adjust_side_payment(self, message_content):
        if message_content["proposal_type"] == DECREASE_Q:
            self._adjust_side_payment_for_decrease_q(message_content)
        elif message_content["proposal_type"] == ELIMINATE_Q:
            self._adjust_side_payment_for_eliminate_q(message_content)

    def get_estimated_bill_mem_with_negotiations(self, qs, ps, delta_q):
        # TODO: Danielle needs to implement her bill estimation
        return AdviserProfitEstimator.get_estimated_bill_mem(self, qs, ps)[0] - self.p_max_out * delta_q

    def update_params_from_host(self, message_content, allocated_base_mem, load):
        """
        Start point for negotiations round.
        """
        if message_content.get("p_out_max_mem") is not None:
            self.p_max_out = message_content.get("p_out_max_mem")

        if message_content.get("not_mem") is None:
            self.logger.warning("The value of not_mem is None.")

        self.extra_memory = message_content.get("not_mem") - allocated_base_mem

        # The state is the array [load, base_mem]
        self.state = [load, allocated_base_mem]

        if self.is_first_round_mem() or not self.enable_negotiations:
            return

        Timer(self.END_OF_NEGOTIATIONS_ROUND, self._send_final_message_to_best_deal, ()).start()

        self.total_side_payments = 0

        # Initializing communications
        for destination_guest, guest_comm in self.guest_communicators.iteritems():
            # If both guests don't have memory, continue to next iteration
            if not self.guests_with_memory['vm-%s' % destination_guest] and self.extra_memory < self.MATH_EPSILON:
                continue

            if (self._retrieve_guest_data(destination_guest, "valuation_lower_than_mine") and
                    not self.guests_with_memory['vm-%s' % destination_guest] and
                        self.extra_memory > self.MATH_EPSILON):
                last_bill = self.bills[0][2]
                self._send_message(destination_guest, MessageAdjustSidePayment(
                    sender_id=self.vm_number, proposal_type=ELIMINATE_Q, status=OFFER,
                    side_payment=1.0 * last_bill * self.max_total_side_payment /
                                 self._get_number_of_guests_outside_with_lower_valuation()
                ))
            else:
                delta_q = self._determine_delta_q()
                self._send_message(destination_guest, MessageAdjustDeltaQ(
                    sender_id=self.vm_number, proposal_type=DECREASE_Q, delta_q=delta_q, status=OFFER
                ))
            self._update_guest_data(destination_guest, "sent_message_adjust_delta_q", True)
            self._update_guest_data(destination_guest, "own_delta_q", 0)
            self._update_guest_data(destination_guest, "partner_delta_q", 0)
            self.logger.info("This is dawg")
            self._update_guest_data(destination_guest, "side_payment", 0)
            self._update_guest_data(destination_guest, "rejected_in_this_round", False)

    def _adjust_side_payment_for_eliminate_q(self, message_content):
        sender_id = message_content["sender_id"]
        if message_content["status"] == OFFER:
            self._send_message(sender_id, MessageAdjustSidePayment(
                sender_id=self.vm_number, proposal_type=ELIMINATE_Q, status=ACCEPT1,
                side_payment=message_content["side_payment"]
            ))

        elif message_content["status"] == ACCEPT1:
            self._send_message(sender_id, MessageAdjustSidePayment(
                sender_id=self.vm_number, proposal_type=ELIMINATE_Q, status=FINAL1,
                side_payment=message_content["side_payment"]
            ))

        elif message_content["status"] == REJECT:
            return

        elif message_content["status"] == FINAL1:
            return

        else:
            raise NotImplementedError("do_adjust_side_payment: Method not implemented for status = %s" %
                                      message_content["status"])

    def _adjust_side_payment_for_decrease_q(self, message_content):
        sender_id = message_content["sender_id"]

        if self._retrieve_guest_data(sender_id, "agreed_delta_q") is None:
            self._update_guest_data(sender_id, "agreed_delta_q", message_content["agreed_delta_q"])

        if message_content["status"] == OFFER:
            next_message = self._get_next_side_payment_message_from_offer(message_content)
            if next_message.content["status"] == REJECT:
                self._send_reject_to_guest_id(sender_id)
            else:
                self._send_message(sender_id, next_message)

        elif message_content["status"] == ACCEPT1:
            self._send_message(sender_id, MessageAdjustSidePayment(
                sender_id=self.vm_number, proposal_type=DECREASE_Q, status=ACCEPT2,
                side_payment=message_content["side_payment"]
            ))

        elif message_content["status"] == ACCEPT2:
            self._update_guest_data(sender_id, "accepted", True)

        elif message_content["status"] == REJECT:
            self._update_guest_data(sender_id, "accepted", False)
            self._update_guest_data(sender_id, "rejected_in_this_round", True)
            if self.chosen_partner_id == sender_id:
                self.chosen_partner_id = -1
                self._send_final_message_to_best_deal()
            return

        elif message_content["status"] == FINAL1:
            guest_with_max_utility = self._get_max_utility_guest(False)
            self.logger.debug("sender_id = %s, guest_with_max_utility = %s", sender_id, guest_with_max_utility)
            # TODO: If there is a bug, look for it here!
            if guest_with_max_utility == sender_id:
                self._send_message(guest_with_max_utility, MessageAdjustSidePayment(
                    sender_id=self.vm_number, proposal_type=DECREASE_Q, status=FINAL2,
                    side_payment=-self._retrieve_guest_data(guest_with_max_utility, "side_payment")
                ))
                self.chosen_partner_id = sender_id
                self._update_guest_data(sender_id, "valuation_lower_than_mine", True)

                self.total_side_payments += self._retrieve_guest_data(guest_with_max_utility, "side_payment")
        elif message_content["status"] == FINAL2:
            if not self._retrieve_guest_data(sender_id, "rejected_in_this_round"):
                self.rejectTimeoutTimer.cancel()
                self.chosen_partner_id = sender_id
                self._update_guest_data(sender_id, "valuation_lower_than_mine", False)

                self.total_side_payments += message_content["side_payment"]

        else:
            raise NotImplementedError("do_adjust_side_payment: Method not implemented for status = %s" %
                                      message_content["status"])

    def _send_message(self, destination_id, message):
        try:
            self.guest_communicators[destination_id].communicate(message)
        except Exception as err:
            self.logger.info('Error communicating in vm-%d: %s', self.vm_number, err)
            self._print_exception_message()

    def _get_number_of_guests_outside_with_lower_valuation(self):
        return len([guest for guest in range(1, self.num_of_guests + 1)
                    if not self.guests_with_memory["vm-%s" % guest]
                    and self._retrieve_guest_data(guest, "valuation_lower_than_mine")
                    and self.vm_number != guest])

    def _send_final_message_to_best_deal(self):
        guest_with_max_utility = self._get_max_utility_guest(True)
        if guest_with_max_utility is None:
            return
        self.logger.debug("Sending a FINAL1 message to guest %s", guest_with_max_utility)
        self._send_message(guest_with_max_utility, MessageAdjustSidePayment(
            sender_id=self.vm_number, proposal_type=DECREASE_Q, status=FINAL1,
            side_payment=self._retrieve_guest_data(guest_with_max_utility, "side_payment")
        ))
        self.rejectTimeoutTimer = Timer(self._get_final1_timeout(), self._reject_after_timeout,
                                        [guest_with_max_utility])
        self.rejectTimeoutTimer.start()

    def _get_final1_timeout(self):
        return max(0.1, 1.0 / (self.num_of_guests - 1))

    def _reject_after_timeout(self, guest_id):
        self._send_reject_to_guest_id(guest_id)
        self._update_guest_data(guest_id, "accepted", False)
        self._send_final_message_to_best_deal()

    def _send_reject_to_guest_id(self, guest_id):
        self._send_message(guest_id, MessageAdjustSidePayment(
            sender_id=self.vm_number, proposal_type=DECREASE_Q, status=REJECT, side_payment=0
        ))
        self._update_guest_data(guest_id, "rejected_in_this_round", True)

    def _get_max_utility_guest(self, only_accepted):
        max_utility = 0
        guest_with_max_utility = None

        if only_accepted:
            guests_to_iterate = [guest for guest in self.guest_data.iterkeys() if
                                 self._retrieve_guest_data(guest, "accepted")]
        else:
            guests_to_iterate = [guest for guest in self.guest_data.iterkeys() if
                                 not self._retrieve_guest_data(guest, "rejected_in_this_round")]

        for guest in guests_to_iterate:
            if self._retrieve_guest_data(guest, "utility") > max_utility:
                max_utility = self._retrieve_guest_data(guest, "utility")
                guest_with_max_utility = guest

        # TODO: If there is more than one guest with max_utility, choose more wisely

        return guest_with_max_utility

    def _get_next_side_payment_message_from_offer(self, message_content):
        options_to_proceed = []
        side_payment = message_content["side_payment"]
        sender_id = message_content["sender_id"]
        message_accept = MessageAdjustSidePayment(sender_id=self.vm_number, proposal_type=DECREASE_Q, status=ACCEPT1,
                                                  side_payment=side_payment)
        delta_q = self._retrieve_guest_data(sender_id, "agreed_delta_q")

        self.logger.debug('Agreed delta_q for guest %s is %s, and self.current_q is %s', sender_id, delta_q,
                          self.current_q)

        # Accept the offer: reduce the quantity and get compensated
        options_to_proceed.append({"message": message_accept,
                                   "utility": side_payment
                                              + self._get_valuation_at(self.current_q - delta_q)
                                              - self.get_estimated_bill_mem_with_negotiations(
                                       [self.current_q - delta_q],
                                       [self._p_for_q(self.current_q - delta_q)], 0),
                                   "side_payment": side_payment,
                                   "own_delta_q": -delta_q,
                                   "partner_delta_q": 0})

        # Reject the offer
        message_reject = MessageAdjustSidePayment(sender_id=self.vm_number, proposal_type=DECREASE_Q, status=REJECT,
                                                  side_payment=0)
        options_to_proceed.append({"message": message_reject,
                                   "utility": self._get_valuation_at(self.current_q)
                                              - self.get_estimated_bill_mem_with_negotiations([self.current_q],
                                                                                              [self._p_for_q(
                                                                                                  self.current_q)], 0),
                                   "side_payment": 0,
                                   "own_delta_q": 0,
                                   "partner_delta_q": 0})

        message_counter_offer = MessageAdjustSidePayment(sender_id=self.vm_number,
                                                         proposal_type=DECREASE_Q,
                                                         status=OFFER,
                                                         side_payment=(side_payment + self._get_epsilon(sender_id)))
        if self.guests_with_memory["vm-%d" % sender_id]:
            # Counter offer: increase my own quantity
            options_to_proceed.append({"message": message_counter_offer,
                                       "utility": - (side_payment + self._get_epsilon(sender_id))
                                                  + self._get_valuation_at(self.current_q + delta_q)
                                                  - self.get_estimated_bill_mem_with_negotiations(
                                           [self.current_q + delta_q],
                                           [self._p_for_q(self.current_q + delta_q)], 0),
                                       "side_payment": -(side_payment + self._get_epsilon(sender_id)),
                                       "own_delta_q": delta_q,
                                       "partner_delta_q": delta_q})

        # Counter offer: don't increase my own quantity, expect lower bill
        options_to_proceed.append({"message": message_counter_offer,
                                   "utility": - (side_payment + self._get_epsilon(sender_id))
                                              + self._get_valuation_at(self.current_q)
                                              # TODO: this must be changed
                                              - self.get_estimated_bill_mem_with_negotiations([self.current_q],
                                                                                              [self._p_for_q(
                                                                                                  self.current_q)],
                                                                                              delta_q),
                                   "side_payment": -(side_payment + self._get_epsilon(sender_id)),
                                   "own_delta_q": 0,
                                   "partner_delta_q": delta_q})

        options_to_proceed.sort(key=lambda x: x["utility"], reverse=True)
        selected_option = options_to_proceed[0]
        self.logger.info("This is doge")
        self._update_guest_data(sender_id, "side_payment", selected_option["side_payment"])
        self._update_guest_data(sender_id, "own_delta_q", selected_option["own_delta_q"])
        self._update_guest_data(sender_id, "partner_delta_q", selected_option["partner_delta_q"])
        self._update_guest_data(sender_id, "utility", selected_option["utility"])
        self._update_guest_data(sender_id, "accepted", False)
        self.logger.debug("Next message to send: %s", selected_option["message"])
        return selected_option["message"]

    def _p_for_q(self, q):
        V0 = self.V_func(self.perf(self.state))
        V = self.V_func(self.perf([self.state[0], self.state[1] + q]))
        return (V - V0) / q

    def _get_valuation_at(self, memory_quantity):
        # The variable memory_quantity doesn't include the base memory
        state = [self.state[0], self.state[1] + memory_quantity]
        return self.V_func(self.perf(state))

    def _update_guest_data(self, guest_id, key, value):
        """
        A helper function to update the guest_data dictionary. If the key does not exist, this method creates it and
        assigns the value to it. If the key already exists, then it just updates the value.
        """
        self.guest_data_lock.acquire()
        try:
            if guest_id in self.guest_data.keys():
                self.guest_data[guest_id][key] = value
            else:
                self.guest_data[guest_id] = {key: value}
        finally:
            self.guest_data_lock.release()

    def _retrieve_guest_data(self, guest_id, key):
        """
        A helper function to retrieve the value corresponding to the given guest_id and key from the guest_data
        dictionary. If the data does not exist, None is returned.
        """
        ret = None
        self.guest_data_lock.acquire()
        try:
            if guest_id in self.guest_data.keys():
                if key in self.guest_data[guest_id].keys():
                    ret = self.guest_data[guest_id][key]
        finally:
            self.guest_data_lock.release()

        return ret

    def _determine_delta_q(self):
        # process = subprocess.Popen("cat /proc/meminfo | grep MemTotal | awk '{print $2}'", shell=True,
        #                            stdout=subprocess.PIPE)
        # total_memory = process.communicate()[0]
        return 0.1 * self.auction_mem / self.num_of_guests

    def _get_epsilon(self, guest_id):
        """
        :param guest_id: The guest which the negotiation is being done with, must be a double
        """
        epsilon = 0.1 * self.p_max_out * self._retrieve_guest_data(guest_id, "agreed_delta_q")
        return epsilon

    @staticmethod
    def _print_exception_message():
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        logging.exception("Type %s, file %s, line %s" % (exc_type, fname, exc_tb.tb_lineno))


def extract_guests_from_csv(filename):
    """
    Extracts the guests list from 'filename'. The file needs to be written in a csv format, while every line
    represents a guest. Also, the first column defines the guest's name and the second defines its IP.
    :param filename: a path to a file that contains the guests list.
    :return: a dictionary of the guests, in the format name:IP
    """
    guests = {}
    with open(filename, 'rb') as csvfile:
        guests_reader = csv.reader(csvfile)
        for line in guests_reader:
            guests[line[0]] = line[1]

    return guests
