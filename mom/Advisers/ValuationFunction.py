'''
@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from types import FunctionType

class StepValuationFunction:
    def __init__(self, params):
        self.params = params

    @staticmethod
    def __do_step_op(op, p, perf, res=None):
        stop = False
        if res is None:
            res = perf

        if op == "-":
            res -= p[0]
        elif op == "*":
            res = float(res)*float(p[0])
        elif op == "/":
            res = float(res)/float(p[0])
        elif op == "1/":
            res = 1.0/float(res)
        elif op == "norm":
            actual_perf = perf
            min_perf = float(p[0])
            max_perf = float(p[1])
            if len(p) == 4:
                bottom = float(p[2])
                top = float(p[3])
            else:
                bottom = 0.0
                top = 1.0

            if min_perf > max_perf:
                actual_perf = 1.0/float(perf)
                min_perf = 1.0/min_perf
                max_perf = 1.0/max_perf

            res = bottom + ((top-bottom) * ( (actual_perf-min_perf)/(max_perf-min_perf) ))

        elif op == "<":
            if perf < p[0]:
                res, stop = StepValuationFunction.__do_step_op(p[1], p[2:], perf, res)
                stop = True
        elif op == "<=":
            if perf <= p[0]:
                res, stop = StepValuationFunction.__do_step_op(p[1], p[2:], perf, res)
                stop = True
        elif op == ">":
            if perf > p[0]:
                res, stop = StepValuationFunction.__do_step_op(p[1], p[2:], perf, res)
                stop = True
        elif op == ">=":
            if perf >= p[0]:
                res, stop = StepValuationFunction.__do_step_op(p[1], p[2:], perf, res)
                stop = True

        return res, stop

    def __call__(self, perf):
        res = None
        for action in self.params:
            try:
                op = action[0]
                p = action[1:]
            except:
                op = action
                p = []

            res, stop = self.__do_step_op(op, p, perf, res)
            if stop: break

        return res

class SLAValuationFunction(StepValuationFunction):
    def __init__(self, params):
        self.params = params

    def __call__(self, perf):
        return 0

class ValuationFunction(object):

    def __init__(self, function_str, *params):
        if isinstance(function_str, ValuationFunction):
            function_str = str(function_str)
            assert len(params) == 0, "Cannot use any parameters when receiving a valuation function object"
        else:
            assert isinstance(function_str, str), "function_str must be a string"

        if len(params) == 0:
            self.__repr = function_str
        else:
            self.__repr = str([function_str] + list(params))

        self.func = self.get_valuation_func(function_str, *params)
        assert isinstance(self.func, FunctionType) or hasattr(self.func, "__call__"), "Failed to create a function: %s" % self.func

    @classmethod
    def get_valuation_func(cls, function_str, *params):
        if function_str == "step":
            return StepValuationFunction(params)
        elif function_str == "sla":
            return StepValuationFunction(params)

        eval_ret = eval(function_str)

        if isinstance(eval_ret, FunctionType):
            assert len(params) == 0, "Explicit function must not have parameters"
            return eval_ret

        if isinstance(eval_ret, list) or isinstance(eval_ret, tuple):
            assert len(eval_ret) > 0, "Function string is malformed"
            function_str = eval_ret[0]
            params = eval_ret[1:]
            return cls.get_valuation_func(function_str, *params)

        raise Exception("Unknown valuation function: %s" % ([function_str]+list(params)))

    def __repr__(self):
        return self.__repr

    def __call__(self, perf):
        return max(0.0, self.func(perf))

def TestCreateValuationFunction(*params):
    print "Test:", params
    return ValuationFunction(*params)

def TestValuationFunction(v, p, should_be=None):
    print "v(%s) =" % p,
    res = v(p)
    print res,
    if should_be is not None:
        if res == should_be:
            print "OK"
        else:
            print "FAIL, should be:", should_be
    else:
        print

if __name__ == "__main__":
    v = TestCreateValuationFunction("lambda x: 2*x")
    TestValuationFunction(v,2,4)
    TestValuationFunction(v,3,6)
    print

    v = TestCreateValuationFunction("step", ("norm", 10, 20))
    TestValuationFunction(v, 10, 0.0)
    TestValuationFunction(v, 15, 0.5)
    TestValuationFunction(v, 20, 1.0)
    print

    v = TestCreateValuationFunction("step", ("norm", 10, 20, 2, 6))
    TestValuationFunction(v, 10, 2.0)
    TestValuationFunction(v, 15, 4.0)
    TestValuationFunction(v, 20, 6.0)
    print

    v = TestCreateValuationFunction("step", ("norm", 10, 20), ("*", 2))
    TestValuationFunction(v, 10, 0.0)
    TestValuationFunction(v, 14, 0.8)
    TestValuationFunction(v, 15, 1.0)
    TestValuationFunction(v, 20, 2.0)
    print

    v = TestCreateValuationFunction("step", ("norm", 10, 20), (">=", 15, "*", 2), ("*", 0))
    TestValuationFunction(v, 10, 0.0)
    TestValuationFunction(v, 14, 0.0)
    TestValuationFunction(v, 15, 1.0)
    TestValuationFunction(v, 20, 2.0)
    print

    v = TestCreateValuationFunction("step", ("norm", 10, 20), (">", 15, "*", 2), ("*", 0))
    TestValuationFunction(v, 10, 0.0)
    TestValuationFunction(v, 14, 0.0)
    TestValuationFunction(v, 15, 0.0)
    TestValuationFunction(v, 20, 2.0)
    print

    v = TestCreateValuationFunction("step", ("norm", 10, 20, 0, 10), ("<", 15, "-", 100))
    TestValuationFunction(v, 10, 0.0)
    TestValuationFunction(v, 14, 0.0)
    TestValuationFunction(v, 15, 5.0)
    TestValuationFunction(v, 20, 10.0)
    print

    nv = ValuationFunction(str(v))
    print nv
