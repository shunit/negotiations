'''
Created on Nov 29, 2011

@author: eyal
'''
import logging
import numpy as np
import scipy as sp
from mom.Adviser import Adviser, p_digits, p_eps, p_eq, U_eq, U_eps
from etc.HistoryWindow import NumericHistoryWindow
from random import randint

def custom_interp(x, xs, ys):
        denom = xs[1]-xs[0]
        if denom == 0:
            alpha = 0.5
        else:
            alpha = (x-xs[0])*1.0/denom
        y = ys[0]+alpha*(ys[1]-ys[0])
        return y

class AdviserProfit(Adviser):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        Adviser.__init__(self, profiler, base_mem, advice_entry, memory_delta,
                         rev_func, **kwargs)
        weights = lambda i: 1. / 2**i
        self.p_in_min_hist_mem = NumericHistoryWindow(self.history_window_size, weights)
        self.p_out_max_hist_mem = NumericHistoryWindow(self.history_window_size, weights)
        self.won_p_hist_mem = NumericHistoryWindow(self.history_window_size, weights)
        self.bid_p = 0.0
        self.lb = 0.0
        self.ub = 0.0

    def do_advice_mem(self, state, auction_mem):
        """
        :param state: is defined as: state = [load, base_mem, base_bw]
        :returns: p, [(r1, q1), (r2, q2), ...]
        """
        def_bid = 0.0, [(0, auction_mem)]
        if auction_mem == 0:
            return def_bid
        # learn from history
        # calculate target bid value
        self.logger.debug("History: %s", str(self.hist_mem))
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self.p_in_min_hist_mem.add(last.p_in_min)
                self.p_out_max_hist_mem.add(last.p_out_max)
                self.won_p_hist_mem.add(last.won_p)
                assert last.bid_p >= last.won_p - p_eps
            else:
                self.logger.warn("do_advice_mem: No data on last bid results")
        self.logger.debug("round %i p_in_min memory history: %s", self.rnd_mem,
                          str(self.p_in_min_hist_mem.dump()))
        self.logger.debug("round %i p_out_max memory history: %s", self.rnd_mem,
                          str(self.p_out_max_hist_mem.dump()))
        self.logger.debug("round %i won_p memory history: %s", self.rnd_mem,
                          str(self.won_p_hist_mem.dump()))
        ## Generate bids data
        V0 = self.V_func(self.perf(state))
        # all q stations according to the memory for auction
        q = np.asarray(range(self.d_mem, auction_mem, self.d_mem) + [auction_mem])
        # the memory allocation for each q
        self.logger.debug("round %i V0: %s", self.rnd_mem, str(V0))
        # valuation
        V = np.asarray(self.V_func(self.perf([state[0], state[1] + q, 0])))
        self.logger.debug("round %i V: %s", self.rnd_mem, str(V))
        good_V = V > V0
        # if there is no good revenue, return default bid
        try:
            if not any(good_V):
                return def_bid
        except TypeError:
            return def_bid
        # allow only higher valuations
        if not all(good_V):
            V = V[good_V]
            q = q[good_V]
        self.logger.debug("round %i good V: %s", self.rnd_mem, str(V))
        self.logger.debug("round %i good q: %s", self.rnd_mem, str(q))
        # p value is the slope of the revenue-memory graph from the (mem0,V0) point
        # in 3D(bandwidth) this is OK while the other dimension is constant
        with np.errstate(divide = 'ignore', invalid = "ignore"):
            p = (V - V0) / q
        p[np.isinf(p) + np.isnan(p)] = 0
        assert all(p >= 0)
        self.logger.debug("round %i p: %s", self.rnd_mem, str(p))
        _p = np.copy(p) #p[:]  # save the p values for later rq list generation
        # profit according to estimated unit cost
        est = self.get_estimated_bill_mem(q, p)
        est_bill = [t[2] for t in est]
        lbs = np.asarray([t[0] for t in est])
        ubs = np.asarray([t[1] for t in est])
        U = np.maximum(0, V - est_bill)
        self.logger.debug("VISDEC round %i: %s",self.rnd_mem, zip(q, V, est_bill))
        self.last_bill_estimate_mem = dict(
            map(lambda _q, _p: (_q, _p), q, est_bill))
        self.last_profit_estimate_mem = dict(map(lambda _q, _p: (_q, _p), q, U))
        self.last_valuation_estimate_mem = dict(map(lambda _q, _p: (_q, _p),
                                                    q, V))
        self.logger.debug("round %i profit: %s", self.rnd_mem, str(U))
        p_chosen = self.choose_p_mem(p, U, lbs, ubs, q)
        self.bid_p = p_chosen
        self.logger.debug("round %i chosen p: %s", self.rnd_mem, str(p_chosen))
        if p_chosen > p_eps:
            rq = self.rq_list(p_chosen, _p, q)
            return round(p_chosen, p_digits), rq
        else:
            self.bid_p = 0.0
            return def_bid

    def get_estimated_bill_mem(self, q, p):
        self.logger.info("round %s, get_estimated_bill_mem by AdviserProfit called", self.rnd_mem)
        return self.won_p_hist_mem.average() * q

    def do_reset_mem(self):
        self.won_p_hist_mem.clear()
        self.p_out_max_hist_mem.clear()
        self.p_in_min_hist_mem.clear()

    def choose_p_mem(self, p, U, lbs, ubs, q=None, enable_negative_U=False,update_bounds=False):
        # clear places where p < p_in_min from utility
        min_p =  self.p_in_min_hist_mem[0] if self.p_in_min_hist_mem.len() > 0 else 0
        self.logger.debug("VISDEC round %i minimal p: %f", self.rnd_mem, min_p)

        max_p = self.p_out_max_hist_mem[0] if self.p_out_max_hist_mem.len() > 0 else 0
        bid_p = self.bid_p
        self.logger.debug("VISPQ round %i minimal p, max_p, bid_p: (%f,%f,%f)", self.rnd_mem, min_p, max_p, bid_p)
        if (abs(min_p - bid_p) <= p_eps):
            good_ps = p >= (max_p-p_eps) # p is a numpy array, so the result is a numpy array of booleans
        else:
            good_ps = p >= (min_p-p_eps)

        # if the highest p is not enough, choose the highest p possible
        if not any(good_ps):
            return np.max(p)
        # keep the places where p >= target
        bad_ps = np.logical_not(good_ps)
        self.logger.debug("VISPQ round %i bad q,p,U: %s", self.rnd_mem, str(zip(q[bad_ps],p[bad_ps], U[bad_ps])))
        if not all(good_ps) and (enable_negative_U or any(U[good_ps] > 0)):
            p = p[good_ps]
            U = U[good_ps]
            lbs = lbs[good_ps]
            ubs = ubs[good_ps]
        self.logger.debug("VISPQ round %i good q,p,U: %s", self.rnd_mem, str(zip(q[good_ps],p,U)))
        self.logger.debug("round %i maximal profit giving p: %s, max(U): %s, Utilities close to max(U): %s, old p choice: %s",
                          self.rnd_mem, p[np.isclose(U, max(U))], max(U), U[np.isclose(U, max(U))], p[p_eq(U, max(U))])
        # choose the highest utility bid from what left
        # with the highest p value conform the highest utility
        #return max(p[np.isclose(U, max(U))])
        max_p = max(p[np.isclose(U, max(U))])
        # Calculate the UB and LB that match the chosen p.
        match_lb_ub = zip(p, lbs, ubs)
        chosen_lb_ub = [(p1,lb1,ub1) for p1,lb1,ub1 in match_lb_ub if p1 == max_p]
        if update_bounds:
            self.lb = chosen_lb_ub[0][1]
            self.ub = chosen_lb_ub[0][2]
        return max_p

    def perf(self, state):
        return self.profiler.interpolate(state)

    @staticmethod
    def rq_list(p, ps, qs):
        """
        find minimum worth memory for a p, q tuple. which below that value, a
        guest wouldn't like extra memory.
        """
        logger = logging.getLogger("rq_list")
        n = len(qs)
        if n == 0:
            return [(0, 0)]
        # interpolate two adjustance q points according to the p value
        _interpolate = lambda i: int(np.interp(p, [ps[i],ps[i-1]], [qs[i],qs[i-1]])) # This one is still buggy
        #_interpolate = lambda i: int(custom_interp(p, ps[i - 1 : i + 1], qs[i - 1 : i + 1]))
        # get all the regions that the curve is above the line
        rq = []
        # set initial bottom limit
        r = 0 if ps[0] + p_eps >= p else None
        for i in range(1, n):
            # if we are below the line,
            # if it is first time add r value, by interpolating the previous
            # point with the current point
            if r is None:
                if p <= ps[i] + p_eps:
                    r = _interpolate(i)
            # if we are above the line, and we were below before, add a q value
            # by interpolating with previous point
            else:
                if (ps[i] + p_eps < p or (p_eq(ps[i], p) and i < n - 1 and ps[i] > ps[i + 1] + p_eps)):
                    q = _interpolate(i)
                    logger.debug("interpolated for q. i = %i, q = %f, ps[i] = %f, orig q: %f, orig p: %s, "+
                                 "ps[i-1:i+1]: %s, qs[i-1:i+1]: %s, interp: %s, int(interp): %s", 
                                 i, q, ps[i], qs[i], p, ps[i-1:i+1], qs[i-1:i+1], custom_interp(p, ps[i - 1 : i + 1], qs[i - 1 : i + 1]), 
                                 int(custom_interp(p, ps[i - 1 : i + 1], qs[i - 1 : i + 1])))
                    rq.append((r, q))
                    r = None
        # if we have a bottom limit without upper
        if r is not None:
            logger.debug("getting maximal amount for q: %i", qs[-1])
            rq.append((r, qs[-1]))
        return rq if len(rq) != 0 else [(0, 0)]

def main():
    from mom.Adviser import get_adviser
    import exp.core.Loads

    base_mem = 1000
    profiler_file = "doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml"
    profiler_entry = "hits_rate"
    load = exp.core.Loads.LoadConstant(9)
    load_interval = 200
    revenue_func_str = 'lambda x:x'

    adv = get_adviser(name='AdviserProfit',
                      profiler=profiler_file,
                      advice_entry=profiler_entry,
                      rev_func=revenue_func_str,
                      base_mem=base_mem,
                      memory_delta=10)
    auction_mem = 1500

    notification_mem = {'auction_round_mem': 1, 'auction_mem' : auction_mem,\
                    'base_mem': int(base_mem), 'load': 8, 'not_round_mem': 1}
    print "MCD advice mem: ", adv.advice(notification_mem)

if __name__ == '__main__':
    main()
