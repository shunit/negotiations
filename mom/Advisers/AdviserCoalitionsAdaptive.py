import csv
import logging
import os
from threading import Timer
from threading import Lock
import sys
import random
from copy import deepcopy
import numpy as np
import pprint
import traceback

from mom.Adviser import p_digits, p_eps
from mom.Advisers.AdviserProfitEstimatorAge import AdviserProfitEstimatorAge, Bill
from mom.Advisers.AdviserProfit import AdviserProfit
from mom.Advisers.AdviserCoalitions import AdviserCoalitions
from mom.Comm.GuestServer import GuestServer
from mom.Comm.GuestCommnicator import GuestCommunicator
from mom.Comm.Messages import MessageCoalitionOffer
from mom.Comm.Proposals import OFFER, COUNTER_OFFER, REJECT, ACCEPT1, FINAL1, FINAL2, NOT_LEADER, UPDATE_MEMBERS, COALITION_SIZE
from ActiveNegotiations.AgentStrategy import EpsilonGreedyAgentStrategy

SHAPLEY = "shapley"

#   SHOULD_IGNORE_GOOD_OFFERS_FROM_STUPID_AGENTS = True    #True
#   LEAVE_COALITION_CHANCE = 3     # chance of 1 to X

STRATEGY_KEY = 'strategy'
NUMBER_OF_COUNTER_OFFERS = 'number_of_counter_offers'

# choose partner method
BY_PROFIT_PART = 1
BY_EXPECTED_PROFIT = 2

class ForbiddenRangesException(Exception):
    pass


class CoalitionHistoryItem(object):
    
    def __init__(self, rnd, profit_parts, won_memory, bill, bill_wo_coalition):
        self.round = rnd
        self.coalition = profit_parts
        self.won_memory = won_memory
        self.bill = bill
        self.bill_without_coalition = bill_wo_coalition

    def __str__(self):
        return "Round: %s, bill: %s, coalition: %s, bill_without_coalition: %s, won memory: %s" % (
                self.round, self.bill, self.coalition, self.bill_without_coalition, self.won_memory)


class CoalitionHistory(object):
    def __init__(self):
        self.history = []
        self.max_history_size = 100

    def add(self, item):
        self.history.insert(0, item)
        self.history = self.history[:self.max_history_size]

    def size(self):
        return len(self.history)

    def __getitem__(self, key):
        return self.history[key]

    def __str__(self):
        return str([str(i) for i in self.history])


class AdviserCoalitionsAdaptive(AdviserCoalitions):
    """
    Adviser with coalitions implementation. In each memory auction round, it executes negotiations with other guests
    and reports the requested coalition to the host.
    """

    """
    self.vm_number  - my vm#
    self.offer_value, self.accept_threshold
    self.coalition_leader - None / otherwise indicates my leader (can be self.coalition_leader == self.vm_number 
    self._my_coalition_size()
        
    
    self.guest_data:
        'accept_threshold' - last ac I sent to vm#
        'coalition_leader'
        'coalition_size' - the partner's coalition size
        'got_accept': False,
        'got_message_in_queue_this_round': False,
        'interacted_in_this_round': True,
        'last_offered_value' - last ov I sent to vm#,
        'normalized_accept_threshold' - last norm at I sent to vm#,
        'normalized_offer_value' - last norm ov I sent to vm#,
        'max_rejected_value' - the maximum among all 'last_offered_value' to get COUNTER OFFER
        'other_members': {   },
        'ov_or_at_effect': 'at',
        'profit_parts': {   1: 0.2, 2: 0.8},
        'rejected_in_this_round': False,
        'rejection_backoff': 1,
        'removed_by_host_or_left_count': 0,
        'removed_by_host_or_left_in_round': 0,
        'rounds_until_next_interaction': 0,
        'sent_accept': True,
        'sent_message_coalition_offer': True
    """

    END_OF_NEGOTIATIONS_ROUND = 7
    MATH_EPSILON = 0.0001

    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, vm_number, num_of_guests,
                 comm_timeout, enable_negotiations=True, offer_value=0.5, accept_threshold=0.5, 
                 draw_per_partner = False, guard_offers=False,
                 leave_coalition_if_smaller_q=True, fear_coalition_break=True, very_careful=False, shapley=True,
                 should_ignore_good_offers_from_stupid_agents = False,
                 leave_coalition_chance = 3,
                 maximum_counter_offers_per_round = 3):
        """
        @param offer_value - a float in [0,1], represents the profit part the guest will offer to a potential partner.
        @param accept_threshold - a float in [0,1], represents the minimal profit part the guest is willing to consider 
                                  when negotiating a coalition.
        """

        self.should_ignore_good_offers_from_stupid_agents = should_ignore_good_offers_from_stupid_agents
        self.leave_coalition_chance = leave_coalition_chance
        self.maximum_counter_offers_per_round = maximum_counter_offers_per_round

        self.unique_id = random.randint(0, 999999)  # used for debugging only

        self.vm_number = vm_number
        self.guest_communicators = {}
        self.guest_data = {}  # To read/write, use _update_guest_data() and _retrieve_guest_data() which read and write safely.
        self.guest_data_lock = Lock()

        self.num_of_guests = num_of_guests
        self.auction_mem = 0
        self.enable_negotiations = enable_negotiations
        self.alphas = {0:0.5}
        # Coalition parameters
        self.final_lock = Lock()
        self.coalition_leader = None
        self.profit_parts = {}
        self.normalized_offer_value = 0
        self.normalized_accept_threshold = 0
        if draw_per_partner:
            self.chosen_ov = 0
            self.chosen_at = 0
        else:
            self.chosen_ov = offer_value
            self.chosen_at = accept_threshold
        # Last round's coalition
        self.prev_coalition_leader = None
        self.prev_profit_parts = {}
        # Private negotiation parameters
        self.draw_per_partner = draw_per_partner
        self.guard_offers = guard_offers # If True, avoid sending offer values greater than 1.
        self.offer_value = offer_value
        self.accept_threshold = accept_threshold
        self.leave_coalition_if_smaller_q = leave_coalition_if_smaller_q
        self.backoff_multiplier = 2 # (affecting rejection_backoff) TODO: make it 5?
        self.fear_coalition_break = fear_coalition_break
        self.very_careful = very_careful
        self.last_requested = 0
        self.coalition_q = None
        self.ov_or_at_effect = None
        self.shapley = shapley
        
        # Guests who sent us a FINAL1 but did not get a response. Re-initialized every round.
        self.finalists = set()
        # The best guest to cooperate with in this round, who is waiting for us to send FINAL1.
        # May change several times in one round as a result of rejections/timeouts.
        self.chosen_guest_active = None
        # The best guest to cooperate with in this round, from whom we are waiting for a FINAL1.
        # May change several times in one round as a result of rejections/timeouts.
        self.chosen_guest_passive = None
        # The last guest we reached FINAL2 with.
        self.last_finalized_partner = None

        # Are we waiting for another guest's FINAL2 message. If so, will contain that guest's id, otherwise None.
        # Should be accessed only with self.final_lock.
        self.currently_waiting_for_final2 = None
        # After increasing q we wait for one round and do not try to form a coalition.
        self.do_not_negotiate_on_round = -1

        # Did we get a Notify Message already this round. Until we do, we shouldn't answer any 
        # coalition offers (since we don't know the result of the last coalition negotiation.)
        # Should be accessed only under self.notify_lock.
        self.got_notify_message_this_round = False
        self.notify_lock = Lock()
        self.message_queue = []
        # Save data about the actual bill, memory we won and coalition we were in.
        self.coalition_history = CoalitionHistory()

        for i in range(1, num_of_guests + 1):
            if i == vm_number:
                continue
            self.guest_communicators[i] = GuestCommunicator('192.168.123.%d' % (1 + i),
                                                            GuestServer.port_guest_comm_mem,
                                                            comm_timeout, i)
            self._update_guest_data(i, "removed_by_host_or_left_in_round", 0)
            self._update_guest_data(i, "removed_by_host_or_left_count", 0)
            self._update_guest_data(i, "rejection_backoff", 1)
            self._update_guest_data(i, "rounds_until_next_interaction", 0)
            # Do not use logger here, use only after BASE init.

            # init base
            AdviserCoalitions.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, vm_number,
                                       num_of_guests, comm_timeout, enable_negotiations, offer_value, accept_threshold,
                                       draw_per_partner, guard_offers, leave_coalition_if_smaller_q, fear_coalition_break,
                                       very_careful, shapley)
            # init additional values

            self.strategy_lock = Lock()

            self.set_agent_startegy(i)
            strategy = self.get_agent_strategy(i)
            self.logger.info("XXX __init__ VM:%s, %s", i, strategy)

        self.logger.info("CSV round_number,guest_id,load,allocated_base_mem,notify_mem,valuation,"+
                         "bill,bill_wo_coalition,requested_mem,offer_value,accept_threshold,bid_price\n")

        self.logger.info("XXX AdviserCoalitionsAdaptive Inited. should_ignore_good_offers_from_stupid_agents: %s,"
                         "leave_coalition_chance: %d",
                         self.should_ignore_good_offers_from_stupid_agents, self.leave_coalition_chance)

    def do_advice_coalition(self):
        for guest in self.guest_data.iterkeys():
            self._update_guest_data(guest, NUMBER_OF_COUNTER_OFFERS, 0)

        return AdviserCoalitions.do_advice_coalition(self)

    def _should_leave_coalition(self):
        self._logAll("_should_leave_coalition-START")
        self.logger.info("%s should_leave_coalition: do not negotiate on round: %d,this round: %d ",
                         self._prefix(), self.do_not_negotiate_on_round, self.rnd_mem)

        randomly_decide_to_leave = random.randint(1, self.leave_coalition_chance) == 1

        if self.prev_coalition_leader and randomly_decide_to_leave:
            self.logger.info("%s XXX should_leave_coalition - random choice returned TRUE", self._prefix2())
            return True

        if self.do_not_negotiate_on_round == self.rnd_mem + 1:
            return True
        if self.coalition_history.size() < 2 or self.hist_mem.len() < 3:
            return False
        last_rq = self.hist_mem[1].bid_rq
        last_memory = min(self.coalition_history[0].won_memory, last_rq)
        before_last_memory = min(self.coalition_history[1].won_memory, self.hist_mem[2].bid_rq)
        # If we got what we asked for, no need to leave the coalition.
        self.logger.info("%s should_leave_coalition: last memory:%s last requested:%s",
                         self._prefix(), last_memory, last_rq[0][1])
        if len(last_rq) == 1 and last_rq[0][1] <= last_memory:
            self.logger.info("%s should_leave_coalition is False because last requested: %s and last won: %s",
                         self._prefix(), last_rq[0][1], last_memory)
            if last_rq[0][1] < last_memory:
                self.logger.error("got too much memory. requested:%s, got:%s", last_rq[0][1], last_memory)
            return False
        # If we got less memory in the last round compared to the round before
        # that, it's time to leave the coalition.
        ret = self.leave_coalition_if_smaller_q and last_memory < before_last_memory
        self.logger.info("%s should_leave_coalition ? %s because leave_coalition_if_smaller_q: %s and last 2 memory results: %s, %s",
                         self._prefix(), ret, self.leave_coalition_if_smaller_q, before_last_memory, last_memory)
        return ret

    def process_coalition_offer(self, message_content):
        """
        Handles a proposal of a coalition.
        @param message_content: a dictionary with the contents of the sent message.
        """

        self._logAll("process_coalition_offer-START", message_content)
        if self.rnd_mem in [self.do_not_negotiate_on_round, self.do_not_negotiate_on_round-1]:
            self.logger.info("process_coalition_offer - ignoring offer because of self.do_not_negotiate_on_round")
            return

        sender_id = message_content["sender_id"]
        profit_parts = message_content["profit_parts"]
        status = message_content["status"]

        self.logger.info("process_coalition_offer - sender_id:" + str(sender_id) +
                         ", profit_parts: " + str(profit_parts) + ", status: " + str(status))

        rounds_until_next_interaction = self._retrieve_guest_data(sender_id, "rounds_until_next_interaction")
        if rounds_until_next_interaction > 0:
            self.logger.info("%s Ignored message from %s. %s more rounds until next interaction.",
                             self._prefix(), sender_id, rounds_until_next_interaction)
            return

        self._update_guest_data(sender_id, "interacted_in_this_round", True)

        if status == COALITION_SIZE:
            self.logger.info("process_coalition_offer status == COALITION_SIZE")
            # Check if the message is a duplicate communication thread,
            # i.e., there is already a communication thread with this guest.
            # In that case we should ignore the thread started by the guest with the larger id.
            # This can happen only with the first message in the protocol, i.e., COALITION_SIZE.
            if self._retrieve_guest_data(sender_id, 'sent_message_coalition_offer'):
                if self.vm_number < sender_id:
                    # Ignore the other guest's message
                    self.logger.info("%s Message %s ignored by %s because it is duplicate.",
                                     self._prefix(), message_content, self.vm_number)
                    return
            # If I am in a coalition and not the leader, refer the sender to the leader.
            elif self.coalition_leader is not None and self.coalition_leader != self.vm_number:
                self.logger.info("%s Sending MessageCoalitionOffer of NOT_LEADER, leader is %s",
                                 self._prefix() ,self.coalition_leader)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts={},
                    status=NOT_LEADER, leader=self.coalition_leader, other_members={}, coalition_size=self._my_coalition_size()))
                return
            # If we are here then I'm allowed to talk to this partner.
            self.logger.info("process_coalition_offer - status == COALITION_SIZE - allowed to talk to this partner, shapley: %s", self.shapley)
            # Save the partner's coalition size
            self._update_guest_data(sender_id, 'coalition_size', int(message_content['coalition_size']))
            # Reply with an offer containing my own coalition size.
            # Calculate the offer value according to coalition sizes.
            if self.shapley:
                offer_value = SHAPLEY
                my_part = SHAPLEY
                self._update_guest_data(sender_id, 'last_offered_value', SHAPLEY)
                self._update_guest_data(sender_id, 'accept_threshold', SHAPLEY)
                self._update_guest_data(sender_id, 'normalized_offer_value', SHAPLEY)
                self._update_guest_data(sender_id, 'normalized_accept_threshold', SHAPLEY)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number,
                    profit_parts={self.vm_number: my_part, sender_id: offer_value},
                    status=OFFER, leader=None, other_members={}, coalition_size=self._my_coalition_size()))
                # Note: saving non-normalized value
                self._update_guest_data(sender_id, 'last_offered_value',
                                        self.offer_value if not self.shapley else SHAPLEY)
                # Also save normalized offer value and accept threshold for statistics
                self._update_guest_data(sender_id, 'normalized_offer_value', offer_value)
                self._update_guest_data(sender_id, 'normalized_accept_threshold',
                                        self._normalize_accept_threshold(self.accept_threshold, sender_id))
                self.logger.info("process_coalition_offer - status == COALITION_SIZE - shaply values has been updated")
            else:
                self.logger.info("process_coalition_offer - status == COALITION_SIZE - NOT shaply, getting neg params..")
                ov, at = self.get_negparams_from_agent_stategy(sender_id, "COALITION_SIZE - First Offer")
                offer_value = self._normalize_offer_value(ov, sender_id)
                my_part = 1- offer_value
                # We don't offer a value over 1. We'd rather not join the coalition at all.
                self.logger.info("%s XXX - process_coalition_offer status == COALITION_SIZE, sender: %s, " +
                                 "offer_value: %s, guard_offers: %s",
                                 self._prefix2(), sender_id, offer_value, self.guard_offers)
                if offer_value < 1 or not self.guard_offers: # Guard against too high normalized OVs
                        self._send_message(sender_id, MessageCoalitionOffer(
                            sender_id=self.vm_number,
                            profit_parts={self.vm_number: 1-offer_value, sender_id: offer_value},
                            status=OFFER, leader = None, other_members = {}, coalition_size=self._my_coalition_size()))
                        self.logger.info(
                            "%s XXX - process_coalition_offer status == COALITION_SIZE - Message SENT. " +
                            " sender: %s, offer_value: %s, guard_offers: %s",
                            self._prefix2(), sender_id, offer_value, self.guard_offers)
                        # Note: saving non-normalized value
                        self._update_guest_data(sender_id, 'last_offered_value', ov)
                        self._update_guest_data(sender_id, 'accept_threshold', at)
                        # Also save normalized offer value and accept threshold for statistics
                        self._update_guest_data(sender_id, 'normalized_offer_value', offer_value)
                        self._update_guest_data(sender_id, 'normalized_accept_threshold', self._normalize_accept_threshold(at, sender_id))
                else:
                    self.logger.info("%s XXX - process_coalition_offer status == COALITION_SIZE - Message NOT SENT !!!" +
                                     " sender: %s, offer_value: %s, guard_offers: %s",
                                     self._prefix2(), sender_id, offer_value, self.guard_offers)

        elif status in (OFFER, COUNTER_OFFER):

            status_str = "null?"

            if status == OFFER:
                status_str = "OFFER"
                self.logger.info("process_coalition_offer - status == OFFER, getting neg params..")
                self._update_guest_data(sender_id, 'coalition_size', message_content['coalition_size'])
                ov, at = self.get_negparams_from_agent_stategy(sender_id, "OFFER")
                # Also save normalized offer value and accept threshold for statistics
                self._update_guest_data(sender_id, 'last_offered_value', ov)
                self._update_guest_data(sender_id, 'accept_threshold', at)
                self._update_guest_data(sender_id, 'normalized_offer_value', self._normalize_offer_value(ov, sender_id))
                self._update_guest_data(sender_id, 'normalized_accept_threshold', self._normalize_accept_threshold(at, sender_id))
            if status == COUNTER_OFFER:
                status_str = "COUNTER_OFFER"
                self.logger.info("process_coalition_offer - status == COUNTER_OFFER")
                last_offered = self._retrieve_guest_data(sender_id, 'last_offered_value')
                self._update_guest_data(sender_id, 'max_rejected_value',
                                        max(last_offered, self._retrieve_guest_data(sender_id, 'max_rejected_value')))
                # Add Agent Snapshot - ov I sent to partner
                non_normalized_last_offered = self._normalized_to_orig_value(last_offered, sender_id)
                self.add_agent_strategy_snapshot(sender_id, self.rnd_mem, non_normalized_last_offered,
                                                 is_accepted=False, description="CO")

            # This logic is common to OFFER, COUNTER_OFFER: considering the offer.
            at = self._retrieve_guest_data(sender_id, 'accept_threshold')
            ov = self._retrieve_guest_data(sender_id, 'last_offered_value')

            self.logger.info("%s XXX process_coalition_offer - status: %s, at: %s, ov: %s",
                             self._prefix2(), status, at, ov)

            offer = profit_parts[self.vm_number]

            # Add Agent Snapshot - save partner offer
            non_normalized_offer = self._normalized_to_orig_value(offer, sender_id)
            self.add_agent_strategy_snapshot(sender_id, self.rnd_mem, 1-non_normalized_offer,
                                             is_accepted=True, description="PO")

            # Accept a shapley division iff it is a shapley agent
            if (offer == SHAPLEY and not self.shapley) or (offer != SHAPLEY and self.shapley):
                self.logger.info("%s rejecting %s because I am %sa Shapley agent who was offered %s",
                                 self._prefix(), sender_id, "" if self.shapley else "not ", offer)
                self._send_reject_to_guest_id(sender_id)
                return
            if offer == SHAPLEY:
                # If offered a shapley division (this is a shapley agent), immediately accept.
                leader = self.choose_leader(sender_id, sender_id)
                self.logger.info("%s Sending ACCEPT1 to %s, profit parts: %s, leader would be %s",
                                 self._prefix(), sender_id, profit_parts, leader)
                self._update_guest_data(sender_id, "profit_parts", profit_parts)
                self._update_guest_data(sender_id, "coalition_leader", leader)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts=profit_parts ,status=ACCEPT1, leader = None,
                    other_members = {}, coalition_size=self._my_coalition_size()))
                self._update_guest_data(sender_id, "sent_accept", True)
                return

            # Otherwise, this is a profit part offer.
            offered_to_me = self._normalized_to_orig_value(profit_parts[self.vm_number], sender_id)

            self.logger.info("%s XXXS process_coalition_offer part 2 - offered_to_me: %s, status: %s",
                             self._prefix2(), offered_to_me, status_str)

            # ACC / CO / REJ
            response_type, offers_count_so_far = self.should_accept_offer(sender_id, offered_to_me, at)
            if response_type == COUNTER_OFFER:
                strategy = self.get_agent_strategy(sender_id)
                ov, _ = self.get_negparams_from_agent_stategy(sender_id, "CO for CO")
                offer_value = self._normalize_offer_value(ov, sender_id)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number,
                    profit_parts={self.vm_number: 1 - offer_value, sender_id: offer_value},
                    status=COUNTER_OFFER, leader=None, other_members={}, coalition_size=self._my_coalition_size()))
                # Note: saving the non normalized value
                self._update_guest_data(sender_id, 'last_offered_value', ov)
                self._update_guest_data(sender_id, NUMBER_OF_COUNTER_OFFERS, offers_count_so_far + 1)

                self.logger.info("%s XXX process_coalition_offer part - COUNTER_OFFER - offer_value: %s",
                                 self._prefix2(), offer_value)
            elif response_type == ACCEPT1:
                # The offer is acceptable
                # leader is the one with bigger coalition or the proposer in case of a tie.
                leader = self.choose_leader(sender_id, sender_id)
                self.logger.info("%s Sending ACCEPT1 to %s, profit parts: %s, leader would be %s",
                                 self._prefix(), sender_id, profit_parts, leader)
                self._update_guest_data(sender_id, "profit_parts", profit_parts)
                self._update_guest_data(sender_id, "coalition_leader", leader)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts=profit_parts, status=ACCEPT1, leader=None,
                    other_members={}, coalition_size=self._my_coalition_size()))

                self.logger.info(
                    "%s XXX - process_coalition_offer status == %s. 'ACCEPT1' Message SENT. " +
                    " sender: %s, guest-Profit_parts: %s, coalition_leader: %s",
                    self._prefix2(), status_str, sender_id, profit_parts, leader)

                self._update_guest_data(sender_id, "sent_accept", True)
                self._update_guest_data(sender_id, "ov_or_at_effect", "at")
            else:  # response_type == REJECT
            # if offered_to_me < at:
                # The offer is not acceptable, let them know we reject their offer.
                self._send_reject_to_guest_id(sender_id)
                # Document that negotiations did not go well this time.
                self.logger.info("CSV NEGPARAMS %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                         self.rnd_mem, self.vm_number, ov, at,
                         self._normalize_offer_value(ov, sender_id), self._normalize_accept_threshold(at, sender_id),
                         0, # bill
                         0, # bill without coalition
                         None, # was_leader
                         None, #leader
                         None, #ov_or_at_effect
                         )
                self.logger.info(
                    "%s XXX - process_coalition_offer status == %s. " +
                    "(The partner is unlikely to accept our counter offer), sender: %s" +
                    self._prefix2(), status_str, sender_id)

        elif status == ACCEPT1:
            leader = self.choose_leader(sender_id, self.vm_number)
            self.logger.info("%s Got ACCEPT1 from %s, saving profit parts: %s, leader would be %s",
                             self._prefix(), sender_id, profit_parts, leader)
            self._update_guest_data(sender_id, "got_accept", True)
            self._update_guest_data(sender_id, "profit_parts", profit_parts)
            self._update_guest_data(sender_id, "coalition_leader", leader)
            self._update_guest_data(sender_id, "ov_or_at_effect", "ov")

            # Add Agent Snapshot - ov accepted by partner
            normalized_profit_part = float(profit_parts[sender_id])
            non_normalized_profit_part = self._normalized_to_orig_value(normalized_profit_part, sender_id)
            self.add_agent_strategy_snapshot(sender_id, self.rnd_mem, non_normalized_profit_part,
                                             is_accepted=True, description="PA")

            self.logger.info(
                "%s XXX - process_coalition_offer - status == ACCEPT1."
                "sender_id: %s, coalition_leader: %s",
                self._prefix2(), sender_id, leader)

        elif status == FINAL1:
            self.logger.info("%s Got FINAL1 from %s", self._prefix(), sender_id)
            if self.coalition_leader is not None and self.profit_parts:
                self.logger.info("%s rejecting guest %s because I'm already in a coalition- leader: %s  profit_parts: %s ",
                                 self._prefix(), sender_id, self.coalition_leader, self.profit_parts)
                self._send_reject_to_guest_id(sender_id)
                return
            self.logger.info("%s Got FINAL1. Trying to acquire final_lock", self._prefix())
            self.final_lock.acquire()
            self.logger.info("%s Got FINAL1. Acquired final_lock", self._prefix())
            try:
                if self.currently_waiting_for_final2 is not None:
                    self.logger.info("%s Got FINAL1 from %s but I'm waiting for FINAL2 from %s",
                                     self._prefix(), sender_id, self.currently_waiting_for_final2)
                    self.finalists.add(sender_id)
                    self._update_guest_data(sender_id, "other_members", message_content["other_members"])
                else:
                    best_partner = self._get_best_partner(got_accept = False)
                    if sender_id != best_partner:
                        self.logger.info("%s Got FINAL1 from %s but best partner is %s",
                                         self._prefix(), sender_id, best_partner)
                    else:
                        self._send_message(sender_id, MessageCoalitionOffer(
                            sender_id=self.vm_number, profit_parts=profit_parts ,status=FINAL2,
                            other_members=self.prev_profit_parts, leader = None,
                            coalition_size=self._my_coalition_size()))
                        self.last_finalized_partner = sender_id
                        self.coalition_leader = self._retrieve_guest_data(sender_id, "coalition_leader")
                        self.logger.info("%s Got FINAL1 from %s and updated leader to %s",
                                         self._prefix(), sender_id, self.coalition_leader)
                        # Calculate the merged profit parts
                        self.profit_parts = self.calculate_profit_parts_in_joined_coalition(
                            self._retrieve_guest_data(sender_id, "profit_parts"),
                            self.prev_profit_parts,
                            message_content["other_members"])
                        if self.draw_per_partner:
                            self.chosen_ov = self._retrieve_guest_data(sender_id, "last_offered_value")
                            self.chosen_at = self._retrieve_guest_data(sender_id, "accept_threshold")
                        self.normalized_accept_threshold = self._retrieve_guest_data(sender_id, "normalized_accept_threshold")
                        self.normalized_offer_value = self._retrieve_guest_data(sender_id, "normalized_offer_value")
                        self.ov_or_at_effect = self._retrieve_guest_data(sender_id, "ov_or_at_effect")
                        self.update_other_members()
            finally:
                self.final_lock.release()

        elif status == FINAL2:
            # Only finalize the negotiation if the FINAL2 came from the guest we were waiting for.
            if sender_id == self.currently_waiting_for_final2:
                self.rejectTimeoutTimer.cancel()
                self.final_lock.acquire()
                try:
                    self.currently_waiting_for_final2 = None
                    self.coalition_leader = self._retrieve_guest_data(sender_id, "coalition_leader")
                    self.logger.info("%s Got FINAL2 from %s and updated leader to %s",
                                     self._prefix(), sender_id, self.coalition_leader)
                    self.last_finalized_partner = sender_id
                    # Calculate the merged profit parts
                    self.profit_parts = self.calculate_profit_parts_in_joined_coalition(
                        self._retrieve_guest_data(sender_id, "profit_parts"),
                        self.prev_profit_parts,
                        message_content["other_members"])
                    self.logger.info("%s Chosen coalition: %s, leader: %s",
                                     self._prefix(), self.profit_parts, self.coalition_leader)
                    if self.draw_per_partner:
                        self.chosen_ov = self._retrieve_guest_data(sender_id, "last_offered_value")
                        self.chosen_at = self._retrieve_guest_data(sender_id, "accept_threshold")
                    self.normalized_accept_threshold = self._retrieve_guest_data(sender_id, "normalized_accept_threshold")
                    self.normalized_offer_value = self._retrieve_guest_data(sender_id, "normalized_offer_value")
                    self.ov_or_at_effect = self._retrieve_guest_data(sender_id, "ov_or_at_effect")

                    self.logger.info(
                        "%s XXX - process_coalition_offer - Got FINAL2 from %s and updated leader to %s,"
                        "self.chosen_ov: %s, profit_parts: %s, draw_per_partner: %s",
                        self._prefix2(), sender_id, self.coalition_leader, self.chosen_ov, self.profit_parts,
                        self.draw_per_partner)

                finally:
                    self.final_lock.release()
                # If necessary: update the other members of the coalition about the merged coalition and the leader.
                self.update_other_members()

        elif status == REJECT:
            self._update_guest_data(sender_id, "got_accept", False)
            self._update_guest_data(sender_id, "rejected_in_this_round", True)
            # If this guest was the preferred partner, we would need to select a new one.
            if sender_id == self.chosen_guest_active:
                self.chosen_guest_active = None
            if sender_id == self.chosen_guest_passive:
                self.chosen_guest_passive = None
            # If we were waiting for FINAL2 from the sender, we can stop waiting.
            if sender_id == self.currently_waiting_for_final2:
                self.rejectTimeoutTimer.cancel()
                self._stop_waiting_for_final2(sender_id, should_send_reject_to_guest = False)
            # Document that negotiations did not go well this time.
            ov = self._retrieve_guest_data(sender_id, "last_offered_value")
            at = self._retrieve_guest_data(sender_id, "accept_threshold")
            self.logger.info("CSV NEGPARAMS %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                             self.rnd_mem, self.vm_number, ov, at,
                             self._normalize_offer_value(ov, sender_id), self._normalize_accept_threshold(at, sender_id),
                             0, # bill
                             0, # bill without coalition
                             None, # was_leader
                             None, #leader
                             None # ov_or_at_effect
                             )
            self.logger.info("%s XXX - process_coalition_offer - status == 'REJECT' from: %s, ov: %s, at: %s",
                             self._prefix2(), sender_id, ov, at)
            return

        elif status == NOT_LEADER:
            # If we are not already talking to the leader of the other coalition, start talking.
            leader_id = int(message_content["leader"])
            # Save the partner's coalition size from the "not leader" message
            self._update_guest_data(sender_id, 'coalition_size', int(message_content['coalition_size']))
            # Reply with an offer to the leader, containing my own coalition size.
            ov, at = self.get_negparams_from_agent_stategy(sender_id, "NOT_LEADER")
            # Calculate the offer value according to coalition sizes.
            offer_value = self._normalize_offer_value(ov, sender_id)
            if not self._existing_interaction(leader_id) and leader_id != self.vm_number and (offer_value < 1 or not self.guard_offers):
                self._send_message(leader_id, MessageCoalitionOffer(
                    sender_id=self.vm_number,
                    profit_parts={self.vm_number: 1 - offer_value, leader_id: offer_value},
                    status=OFFER, leader = None, other_members = {}, coalition_size=self._my_coalition_size()))
                # Note: saving non-normalized value
                self._update_guest_data(sender_id, 'last_offered_value', ov)
                self._update_guest_data(sender_id, 'accept_threshold', at)
                # Also save normalized offer value and accept threshold for statistics
                self._update_guest_data(sender_id, 'normalized_offer_value', offer_value)
                self._update_guest_data(sender_id, 'normalized_accept_threshold', self._normalize_accept_threshold(at, sender_id))

        elif status == UPDATE_MEMBERS:
            self.logger.info("%s Got UPDATE_MEMBERS from: %s, leader is %s.",
                             self._prefix(), sender_id, self.coalition_leader)
            if sender_id == self.coalition_leader:
                self.logger.info("%s updating to leader: %s, profit_parts: %s",
                                 self._prefix(), message_content["leader"], message_content["profit_parts"])
                self.coalition_leader = message_content["leader"]
                self.profit_parts = message_content["profit_parts"]

        else:
            self.logger.warning("Illegal status in MessageCoalitionOffer, %s", message_content)

    def get_negparams_from_agent_stategy(self, partner_id, description):
        """ Return non-normalized values """
        ov, at = self._draw_negparams_for_partner(partner_id)   # default values

        strategy = self.get_agent_strategy(partner_id)
        if strategy is None:
            self.logger.error("%s XXX ERR (traceback) - get_negparams_from_agent_stategy [desc: %s] - !! Strategy is None! for: %s",
                              self._prefix2(), description, partner_id)
        else:
            self.logger.info("%s XXX - get_negparams_from_agent_stategy [desc: %s] - partner_id: %s, points_str: %s",
                             self._prefix2(), description, partner_id, strategy.dump_points())

            ov, msg = strategy.get_next_best_offer()
            self.logger.info("%s XXX - get_negparams_from_agent_stategy [desc: %s] - partner_id: %s, ov: %s, at: %s, msg: %s",
                             self._prefix2(), description, partner_id, ov, at, msg)
        return ov, at

    def choose_leader(self, partner_id, proposer_id):
        self._logAll("choose_leader-START")
        partner_coalition_size = self._retrieve_guest_data(partner_id, 'coalition_size')
        my_coalition_size = self._my_coalition_size()
        if partner_coalition_size > my_coalition_size:
            self.logger.info("%s choosing %d as leader, partner coalition size: %d my coalition size: %d",
                             self._prefix(),partner_id, partner_coalition_size, my_coalition_size)
            return partner_id
        if partner_coalition_size < my_coalition_size:
            self.logger.info("%s choosing %d as leader, partner coalition size: %d my coalition size: %d",
                             self._prefix(), self.vm_number, partner_coalition_size, my_coalition_size)
            return self.vm_number
        # Tie breaker: proposer of the offer
        self.logger.info("%s choosing %d as leader, partner coalition size: %d my coalition size: %d",
                         self._prefix(),proposer_id, partner_coalition_size, my_coalition_size)
        return proposer_id

    def _get_best_partner(self, got_accept, method=BY_PROFIT_PART):
        """
        @param got_accept - boolean. If true, the best partner will be chosen from the guests that we got ACCEPT from.
        Otherwise, the best partner will be chosen from the guests we did not get ACCEPT from (because we sent them ACCEPT).
        """
        
        if method == BY_PROFIT_PART:
            return AdviserCoalitions._get_best_partner_by_profit_part(self, got_accept)

        if method == BY_EXPECTED_PROFIT:
            return self._get_best_partner_by_expected_profit(got_accept)

        return None  # error!

    def do_advice_mem(self, state, auction_mem):
        """
        Overriding this method to take several future rounds into consideration.
        :param state: is defined as: state = [load, base_mem, base_bw]
        :returns: p, [(r1, q1), (r2, q2), ...]
        """
        self.logger.debug("This is do_advice_mem by AdviserCoalitions")

        advice_from_parent = AdviserProfitEstimatorAge.do_advice_mem(self, state, auction_mem)
        if not self.enable_negotiations:
            return advice_from_parent

        def_bid = 0.0, [(0, auction_mem)]
        if auction_mem == 0:
            return def_bid
        # Fill in details from history
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self.p_in_min_hist_mem.add(last.p_in_min)
                self.p_out_max_hist_mem.add(last.p_out_max)
                self.won_p_hist_mem.add(last.won_p)
                assert last.bid_p >= last.won_p - p_eps
            else:
                self.logger.warn("do_advice_mem: No data on last bid results")
        # Valuation of the current state.
        V0 = self.V_func(self.perf(state))
        # create an array of possible q values.
        q = np.asarray(range(self.d_mem, auction_mem, self.d_mem) + [auction_mem])
        # Valuation for each q value.
        V = np.asarray(self.V_func(self.perf([state[0], state[1] + q, 0])))
        # Boolean array, holds True in index i iff the valuation at the i'th
        # q value is better than getting base mem.
        good_V = V > V0
        # If there is no positive valuation, return default bid.
        # TODO: what is this try .. except? why would there be a TypeError?
        try:
            if not any(good_V):
                return def_bid
        except TypeError:
            return def_bid
        # Allow only valuations higher than V0.
        if not all(good_V):
            V = V[good_V]
            q = q[good_V]
        # p value is the slope of the revenue-memory graph from the (mem0,V0) point
        # in 3D(bandwidth) this is OK while the other dimension is constant
        with np.errstate(divide='ignore', invalid="ignore"):
            p = (V - V0) / q
        p[np.isinf(p) + np.isnan(p)] = 0
        assert all(p >= 0)
        self.logger.debug("round %i p: %s", self.rnd_mem, str(p))
        p_copy = np.copy(p)  # save the p values for later rq list generation

        future_utility = self._run_utility_prediction_on_qs_and_ps(q, p, state)
        U = np.asarray(future_utility)
        if len(self.bills) > 0:
            q_prev = self.bills[0].q
        else:
            q_prev = 0

        # est_bill is used only for debugging purposes.
        # TODO: this may have a bad side effect!
        est = self.get_estimated_bill_mem(q, p)
        est_bill = [t[2] for t in est]
        lbs = np.asarray([t[0] for t in est])
        ubs = np.asarray([t[1] for t in est])
        self.logger.debug("VISDEC round %i: %s", self.rnd_mem, zip(q, V, est_bill))

        # profit according to estimated unit cost
        p_chosen = self.choose_p_mem(p, U, lbs, ubs, q, enable_negative_U=True, update_bounds=True)
        if p_chosen > p_eps:
            rq = self.rq_list(p_chosen, p_copy, q)
            self.last_requested = rq[0][1]
            if self.coalition_q is not None and (rq[0][1] - self.coalition_q >= 10):
                # I've increased q since I joined a coalition by more than 10 MB. I should quit the coalition.
                self.logger.info("%s coalition_q is %s and bid q is %s, I should not negotiate in next round.",
                                 self._prefix(), self.coalition_q, rq[0][1])
                self.do_not_negotiate_on_round = self.rnd_mem + 1
            return round(p_chosen, p_digits), rq
        else:
            return def_bid

    # TODO: update learner (DIC)
    def update_params_from_host(self, message_content, allocated_base_mem, load):
        """
        Starting point for a negotiations round.
        This method is invoked when a MessageNotifyMem is sent by the host.
        """
        self.logger.info("update_params_from_host called for round %s, prev_profit_parts: %s, " +
                         "last_finalized_partner: %s",
                         self.rnd_mem, self.prev_profit_parts, self.last_finalized_partner)
        self._logAll("update_params_from_host-START")

        state = [load, allocated_base_mem + float(message_content["not_mem"])]
        state_at_base = [load, allocated_base_mem]
        if self.hist_mem.len() > 0:
            requested_mem = self.hist_mem[0].bid_rq[0][1]
        else:
            self.logger.error("requested_mem was not extracted from hist: %s" % self.hist_mem[0])
        # See CSV titles in the init method.
        bid_p = self.hist_mem[0].bid_p if self.hist_mem.len()>0 else 0
        self.logger.info("CSV %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",
                          self.rnd_mem, self.vm_number, load, allocated_base_mem, message_content["not_mem"],
                          self.V_func(self.perf(state))-self.V_func(self.perf(state_at_base)), message_content["not_bill_mem"],
                          message_content["bill_without_coalition"], requested_mem, self.chosen_ov, self.chosen_at,
                          bid_p)
        # if self.is_first_round_mem() or not self.enable_negotiations:
        if self.rnd_mem < 1 or not self.enable_negotiations or self.do_not_negotiate_on_round == self.rnd_mem:
            self.coalition_history.add(CoalitionHistoryItem(
                 self.rnd_mem, {}, message_content["not_mem"]-self.base_mem-self.alloc_diff,
                 message_content["not_bill_mem"], message_content["bill_without_coalition"]))
            self.logger.info("Adding coalition history item: %s" % self.coalition_history[0])
            return

        # decrease rounds until next iteration for all potential partners.
        for guest in self.guest_data.iterkeys():
            rounds_until_next_interaction = self._retrieve_guest_data(guest, "rounds_until_next_interaction")
            if rounds_until_next_interaction is None:
                rounds_until_next_interaction = 0
            self._update_guest_data(guest, "rounds_until_next_interaction", max(0, rounds_until_next_interaction - 1))

        # Update the coalition members according to the host's message.
        if message_content.get("coalition_members") is not None:
            # coalition_members keys are in the form "vm-i" and profit_parts keys are indexes.
            coalition_members = message_content.get("coalition_members")

            # TODO: upate leaner
            profit_parts_from_host = {}
            for guest in coalition_members:
                profit_parts_from_host[int(guest.split('-')[1])] = coalition_members[guest]
            self.logger.info("%s Profit parts before update from host: %s",
                             self._prefix(), self.profit_parts)
            # If a guest was removed from the coalition by the host, or left the coalition, we should remember it.
            for guest_id in self.profit_parts:
                if guest_id == self.vm_number:
                    continue
                if guest_id not in profit_parts_from_host:
                    self._update_guest_data(guest_id, "removed_by_host_or_left_in_round", self.rnd_mem)
                    self._update_guest_data(guest_id, "removed_by_host_or_left_count",
                                            self._retrieve_guest_data(guest_id, "removed_by_host_or_left_count") + 1)
                    rejection_backoff = self._retrieve_guest_data(guest_id, "rejection_backoff")
                    self._update_guest_data(guest_id, "rounds_until_next_interaction", rejection_backoff)
                    rejection_backoff += 1
                    self._update_guest_data(guest_id, "rejection_backoff", rejection_backoff)
                    self.logger.info("%s Increased rejection backoff for guest %s. new rejection backoff: %s",
                                     self._prefix(), guest_id, rejection_backoff)
                else:
                    # This was a successful interaction with this guest. Reset the backoff parameters.
                    self._update_guest_data(guest_id, "rounds_until_next_interaction", 0)
                    self._update_guest_data(guest_id, "rejection_backoff", 1)
                    self.logger.info("%s Reset rejection backoff for guest %s to %s",
                                     self._prefix(), guest_id, 1)
            self.profit_parts = profit_parts_from_host
            # Handle the case of a leader leaving the coalition/removed by the host.
            # The coalition is gone and we should start everything from scratch.
            if not self.profit_parts or self.coalition_leader not in self.profit_parts:
                self.coalition_leader = None
                self.profit_parts = {}
                self.coalition_q = None
                self.logger.info("%s setting coalition_q to None in update_params_from_host", self._prefix())
                was_leader = None
                leader = self.vm_number
            else:
                # We are in a coalition: output the normalized negotiation params, and who is the leader.
                was_leader = (self.prev_coalition_leader == self.vm_number or self.prev_coalition_leader is None)
                leader = self.coalition_leader if self.coalition_leader is not None else self.vm_number
                if self.coalition_q is None:
                    self.coalition_q = self.last_requested
                    self.logger.info("%s setting coalition_q to %s in update_params_from_host", self._prefix(), self.last_requested)

                profit_value = message_content["bill_without_coalition"] - message_content["not_bill_mem"]

                if self.last_finalized_partner is None:
                    self.logger.info("%s XXX update_params_from_host - last_finalized_partner is None, ignoring..",
                                     self._prefix2())
                else:
                    strategy = self.get_agent_strategy(self.last_finalized_partner)
                    last_ov = self._retrieve_guest_data(self.last_finalized_partner, "last_offered_value")

                    strategy.add_profit(self.rnd_mem, last_ov, profit_value)

                    self.logger.info("%s XXX update_params_from_host. Saved Profit. "
                                     "round: %s, vm: %d, chosen_ov: %s, profit_value: %s",
                                     self._prefix2(), self.rnd_mem, self.last_finalized_partner,
                                     self.chosen_ov, profit_value)


            # Titles for NEGPARAMS are in SW_graph_generator.
            self.logger.info("CSV NEGPARAMS %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                             self.rnd_mem, self.vm_number, self.chosen_ov, self.chosen_at,
                             self.normalized_offer_value, self.normalized_accept_threshold,
                             message_content["not_bill_mem"], message_content["bill_without_coalition"], was_leader, leader, self.ov_or_at_effect)
            self.logger.info("CSV PBELIEF %s,%s,%s,%s",
                             self.rnd_mem, self.vm_number,
                             (self.prev_profit_parts != {}), (self.profit_parts=={}))
            self.prev_coalition_leader = self.coalition_leader
            self.prev_profit_parts = deepcopy(self.profit_parts)
            self.logger.info("%s Profit parts after update from host: %s",
                             self._prefix(), self.profit_parts)

        self.coalition_history.add(CoalitionHistoryItem(
             self.rnd_mem, self.profit_parts, message_content["not_mem"]-self.base_mem-self.alloc_diff,
             # TODO: (use not_bill_mem & bill_without_coalition)
             message_content["not_bill_mem"], message_content["bill_without_coalition"]))
        self.logger.info("Adding coalition history item: %s" % self.coalition_history[0])
        self._print_backoffs()
        self.last_finalized_partner = None

        # First answer messages from the queue, and then start new interactions
        # only with those that we didn't talk to already in this round.
        while self._get_message_queue_length() > 0:
            message_content = self._get_first_message_in_queue()
            self.process_coalition_offer(message_content)

        # Start new interactions.
        guests_to_negotiate_with = self._get_guests_to_negotiate_with()

        if guests_to_negotiate_with:
            Timer(self.END_OF_NEGOTIATIONS_ROUND, self._send_final_message_for_best_deal, ()).start()
            self.prev_coalition_leader = self.coalition_leader
            self.prev_profit_parts = deepcopy(self.profit_parts)
            self.coalition_leader = None
            self.profit_parts = {}

        guests_to_start_talking_to = {}
        for guest_id, guest_comm in guests_to_negotiate_with.iteritems():
            if self._existing_interaction(guest_id):
                self.logger.info("%s removing %s from guests_to_start_talking_to. Already interacted in this round.",
                                 self._prefix(), guest_id)
                continue
            guests_to_start_talking_to[guest_id] = guest_comm

        self.logger.info("%s guests_to_negotiate_with: %s", self._prefix(), guests_to_negotiate_with.keys())
        self.logger.info("%s guests_to_start_talking_to: %s", self._prefix(), guests_to_start_talking_to.keys())

        for destination_id, guest_comm in guests_to_start_talking_to.iteritems():
            self._send_message(destination_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts={},
                    status=COALITION_SIZE, leader = None, other_members = {},
                    coalition_size=self._my_coalition_size()))
            self._update_guest_data(destination_id, "sent_message_coalition_offer", True)

        # re-check the queue for messages
        while self._get_message_queue_length() > 0:
            message_content = self._get_first_message_in_queue()
            self.process_coalition_offer(message_content)

        # Update the state - processing the data from the host is finished.
        self._access_got_notify(new_value=True)

    def do_advice_mem(self, state, auction_mem):
        """
        Overriding this method to take several future rounds into consideration.
        :param state: is defined as: state = [load, base_mem, base_bw]
        :returns: p, [(r1, q1), (r2, q2), ...]
        """
        self.logger.debug("This is do_advice_mem by AdviserCoalitionsAdaptive")

        advice_from_parent = AdviserProfitEstimatorAge.do_advice_mem(self, state, auction_mem)
        if not self.enable_negotiations:
            return advice_from_parent

        def_bid = 0.0, [(0, auction_mem)]
        if auction_mem == 0:
            return def_bid
        # Fill in details from history
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self.p_in_min_hist_mem.add(last.p_in_min)
                self.p_out_max_hist_mem.add(last.p_out_max)
                self.won_p_hist_mem.add(last.won_p)
                assert last.bid_p >= last.won_p - p_eps
            else:
                self.logger.warn("do_advice_mem: No data on last bid results")
        # Valuation of the current state.
        V0 = self.V_func(self.perf(state))
        # create an array of possible q values.
        q = np.asarray(range(self.d_mem, auction_mem, self.d_mem) + [auction_mem])
        # Valuation for each q value.
        V = np.asarray(self.V_func(self.perf([state[0], state[1] + q, 0])))
        # Boolean array, holds True in index i iff the valuation at the i'th 
        # q value is better than getting base mem.
        good_V = V > V0
        # If there is no positive valuation, return default bid.
        # TODO: what is this try .. except? why would there be a TypeError?
        try:
            if not any(good_V):
                return def_bid
        except TypeError:
            return def_bid
        # Allow only valuations higher than V0.
        if not all(good_V):
            V = V[good_V]
            q = q[good_V]
        # p value is the slope of the revenue-memory graph from the (mem0,V0) point
        # in 3D(bandwidth) this is OK while the other dimension is constant
        with np.errstate(divide = 'ignore', invalid = "ignore"):
            p = (V - V0) / q
        p[np.isinf(p) + np.isnan(p)] = 0
        assert all(p >= 0)
        self.logger.debug("round %i p: %s", self.rnd_mem, str(p))
        p_copy = np.copy(p)  # save the p values for later rq list generation
        
        future_utility = self._run_utility_prediction_on_qs_and_ps(q, p, state)
        U = np.asarray(future_utility)
        if len(self.bills) > 0:
            q_prev = self.bills[0].q
        else:
            q_prev = 0

        # est_bill is used only for debugging purposes.
        # TODO: this may have a bad side effect!
        est = self.get_estimated_bill_mem(q, p)
        est_bill = [t[2] for t in est]
        lbs = np.asarray([t[0] for t in est])
        ubs = np.asarray([t[1] for t in est])
        self.logger.debug("VISDEC round %i: %s",self.rnd_mem, zip(q, V, est_bill))

        # profit according to estimated unit cost
        p_chosen = self.choose_p_mem(p, U, lbs, ubs, q, enable_negative_U=True, update_bounds=True)
        if p_chosen > p_eps:
            rq = self.rq_list(p_chosen, p_copy, q)
            self.last_requested = rq[0][1]
            if self.coalition_q is not None and (rq[0][1] - self.coalition_q >= 10):
                # I've increased q since I joined a coalition by more than 10 MB. I should quit the coalition.
                self.logger.info("%s coalition_q is %s and bid q is %s, I should not negotiate in next round.", 
                                 self._prefix(), self.coalition_q, rq[0][1])
                self.do_not_negotiate_on_round = self.rnd_mem+1
            return round(p_chosen, p_digits), rq
        else:
            return def_bid

    def should_accept_offer(self, guest_id, offered_to_me, at):
        """ Decide how to respond to the offer (COUNTER_OFFER/REJECT/ACCEPT1) """
        count = self._retrieve_guest_data(guest_id, NUMBER_OF_COUNTER_OFFERS)

        if count < self.maximum_counter_offers_per_round:
            return COUNTER_OFFER, count

        if offered_to_me < at:  # not acceptable
            return REJECT, count

        return ACCEPT1, count

    def _get_best_partner_by_expected_profit(self, got_accept):
        if got_accept:
            if self.chosen_guest_active:
                return self.chosen_guest_active
            # Consider guests that we got an ACCEPT message from, and did not reject us or were not rejected by us.
            potential_partners = [guest for guest in self.guest_data.iterkeys() if
                                  self._retrieve_guest_data(guest, "profit_parts") and
                                  self._retrieve_guest_data(guest, "got_accept") and
                                  not self._retrieve_guest_data(guest, "rejected_in_this_round") and
                                  (not self.shapley or self._retrieve_guest_data(guest, "profit_parts")[self.vm_number]==SHAPLEY)]
        else:
            if self.chosen_guest_passive:
                return self.chosen_guest_passive
            # Consider guests that we sent an ACCEPT message to, and did not reject us or were not rejected by us.
            potential_partners = [guest for guest in self.guest_data.iterkeys() if
                                  self._retrieve_guest_data(guest, "profit_parts") and
                                  self._retrieve_guest_data(guest, "sent_accept") and
                                  not self._retrieve_guest_data(guest, "rejected_in_this_round") and
                                  (not self.shapley or self._retrieve_guest_data(guest, "profit_parts")[self.vm_number]==SHAPLEY)]

        if self.shapley:
            weights = [1 for partner in potential_partners]
        else:
            weights = [
                self._retrieve_guest_data(guest, "profit_parts")[self.vm_number] *
                self.get_partner_expected_profit(guest) *
                (1 - self._retrieve_guest_data(guest, "removed_by_host_or_left_count")*1.0/self.rnd_mem) *
                (1 - self._retrieve_guest_data(guest, "removed_by_host_or_left_in_round")*1.0/self.rnd_mem)
                for guest in potential_partners]
        chosen_guest = self._choose_by_weights(potential_partners, weights)
        if got_accept:
            self.chosen_guest_active = chosen_guest
        else:
            self.chosen_guest_passive = chosen_guest
        return chosen_guest

    def get_partner_expected_profit(self, guest_id):
        strategy = self.get_agent_strategy(guest_id)
        return strategy.get_expected_profit()

    def set_agent_startegy(self, guest_id):
        self.strategy_lock.acquire()
        try:
            self.logger.info("%s XXX - set_agent_strategy - creating strategy, guest_id: %s", self._prefix2(), guest_id)
            # _update_guest_data is also thread safe
            self._update_guest_data(guest_id, STRATEGY_KEY, EpsilonGreedyAgentStrategy(guest_id))
        finally:
            self.logger.info("%s XXX - set_agent_strategy - strategy created. guest_id: %s", self._prefix2(), guest_id)
            self.strategy_lock.release()

    def get_agent_strategy(self, guest_id):
        self.strategy_lock.acquire()
        try:
            strategy = self._retrieve_guest_data(guest_id, STRATEGY_KEY)

            if strategy is None:
                tback = traceback.format_stack()
                self.logger.info("%s XXX - get_agent_strategy - Strategy is None, if it happens each round its not ok!"
                                 + ". guest_id: %s, Traceback: " + "".join([str(i) for i in tback]),
                                 self._prefix2(), str(guest_id))
                strategy = EpsilonGreedyAgentStrategy(guest_id)
                self._update_guest_data(guest_id, STRATEGY_KEY, strategy)
                self.logger.info("%s XXX - get_agent_strategy - created strategy again... guest_id: %s",
                                 self._prefix2(), guest_id)

            return strategy
        finally:
            self.logger.info("%s XXX - get_agent_strategy ended. guest_id: %s", self._prefix2(), guest_id)
            self.strategy_lock.release()

    def add_agent_strategy_snapshot(self, sender_id, rnd, ov, is_accepted, description):
        strategy = self.get_agent_strategy(sender_id)
        strategy.add_snapshot(rnd, ov, is_accepted=is_accepted)
        self.logger.info("%s XXXS %s|%s|%s|%s|%s|%s",
                         self._prefix2(),
                         sender_id, rnd, ov, is_accepted,
                         description, strategy.serialize_snapshot())

    def _logAll(self, funcName, message_content=""):
        str = "[myLog] [AdviserCoalitionsAdaptive] " + self._prefix2() + " [" + funcName + "] \n" + \
            "auction_mem: " + pprint.pformat(self.auction_mem, indent=4) + ",\n" + \
            "coalition_leader: " + pprint.pformat(self.coalition_leader, indent=4) + ",\n" + \
            "profit_parts: " + pprint.pformat(self.profit_parts, indent=4) + ",\n" + \
            "normalized_offer_value: " + pprint.pformat(self.normalized_offer_value, indent=4) + ",\n" + \
            "normalized_accept_threshold: " + pprint.pformat(self.normalized_accept_threshold, indent=4) + ",\n" + \
            "chosen_ov: " + pprint.pformat(self.chosen_ov, indent=4) + ",\n" + \
            "chosen_at: " + pprint.pformat(self.chosen_at, indent=4) + ",\n" + \
            "prev_coalition_leader: " + pprint.pformat(self.prev_coalition_leader, indent=4) + ",\n" + \
            "prev_profit_parts: " + pprint.pformat(self.prev_profit_parts, indent=4) + ",\n" + \
            "message_content: " + pprint.pformat(message_content, indent=4) + ",\n" + \
            "guest_data: " + pprint.pformat(self.guest_data, indent=4)

        self.logger.info(str.replace("\r\n", "|||").replace("\n", "|||").replace("\r", "|||"))

    def _prefix2(self):
        """return a prefix to be used when logging for visualizing negotiations"""
        return "[uid: %s, vm: %s, rnd: %s]" % (self.unique_id, self.vm_number, self.rnd_mem)