import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mom.Advisers.ActiveNegotiations.GifBuilder import *
from mom.Advisers.ActiveNegotiations.AgentStrategy import *

THRESHOLD = 0.4

def test_function(x):
    if x < THRESHOLD:
        return 0
    else:
        return max(0, -4 * x + 5)

DESCRIPTION_IDX = 0
AGENT_IDX = 1
AgentsData = [
    ['All',         EpsilonGreedyAgentStrategy(vmId=1, isLI=True, isSP=True, isPL=True, isBZ=True)],
    ['LI PL SP',    EpsilonGreedyAgentStrategy(vmId=1, isLI=True, isSP=True, isPL=True, isBZ=False)],
    ['LI SP',       EpsilonGreedyAgentStrategy(vmId=1, isLI=True, isSP=True, isPL=False, isBZ=False)],
    ['LI PL',       EpsilonGreedyAgentStrategy(vmId=1, isLI=True, isSP=False, isPL=True, isBZ=False)],
    ['PL SP',       EpsilonGreedyAgentStrategy(vmId=1, isLI=False, isSP=True, isPL=True, isBZ=False)],
]

diff_results = []   # [[NAME, ArrOfDiffs], .. ]

print("Starting..")

for agentsData in AgentsData:

    agent = agentsData[AGENT_IDX]

    graph_var_gif = GifBuilder(ylim=1.1, is_save_all_images=True)

    round = 0
    total_profit = 0
    total_diff = 0
    diffs = []

    for i in range(15):
        profit = 0

        ov, exp_msg = agent.get_next_best_offer()

        # profit can be 0 or 1-ov
        profit = test_function(ov)
        if profit > 0:
            profit = 1-ov

        agent.add_snapshot(round, ov, profit)  # add snapshot to manager
        print("exp_msg: " + exp_msg + ", ov: " + str(ov) + ", profit: " + str(profit))

        xs, ys = agent.mgr.getAllPointsUnique()  # get all (x,y)
        graph_mgr = GraphMgr(xs, ys, agent.isLI, agent.isSP, agent.isPL, agent.isBZ)  # build a graph manager accordingly
        xs, ys, functions_x_points, functions_y_points_arr, functions_names = graph_mgr.getPlotPoints()

        #msg = "[Spline, P-Lin] Round %s. TotalDifference: %.2f (%s)" % (round, total_diff, exp_msg)
        msg = "Round %s (Type: %s) NewPoint:%.2f" % (round, exp_msg, ov)


        last_point = []
        if profit == 0:
            last_point = [ov, 0]
        else:
            last_point = [ov, 1-ov]

        #Save as GIF frame only at the last round!
        if i >= 14:
            graph_var_gif.add_graph_manager_snapshot(msg, xs, ys,
                             functions_x_points, functions_y_points_arr, functions_names,
                             threshold=THRESHOLD, last_point=last_point)

        total_profit = total_profit + profit
        dif = abs(ov-THRESHOLD)
        total_diff += dif
        diffs.append(dif)

        print("Round " + str(round) + " done.")
        round += 1

    # After rounds ended:
    diff_results.append([agentsData[DESCRIPTION_IDX], diffs])
    graph_var_gif.save_gif('graph_gif_' + agentsData[DESCRIPTION_IDX] + '.gif')

# save profit graph
# red dashes, blue squares and green triangles

# FUNCTIONS DIFFs
# li-linear interpolation, pl-piecewise linear, sp-spline, bz-bezier
#allFuncsDiff = [0.35, 0.10252525252525255, 0.023737373737373724, 0.056414141414141444, 0.021060606060606085, 0.0008585858585858641, 0.009242424242424246, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841]
#li_pl_sp = [0.35, 0.10252525252525255, 0.01868686868686867, 0.07661616161616164, 0.026111111111111113, 0.010959595959595975, 0.0008585858585858641, 0.009242424242424246, 0.004242424242424242, 0.0058585858585858686, 0.0058585858585858686, 0.0058585858585858686, 0.0058585858585858686, 0.0058585858585858686, 0.0058585858585858686]
#li_sp = [0.35, 0.10252525252525255, 0.008585858585858558, 0.0866666666666667, 0.031111111111111117, 0.021010101010101034, 0.0058585858585858686, 0.000808080808080841, 0.004242424242424242, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841]
#li_pl = [0.35, 0.10252525252525255, 0.023737373737373724, 0.04126262626262628, 0.010959595959595975, 0.004191919191919163, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818]
#pl_sp = [0.35, 0.10252525252525255, 0.008585858585858558, 0.0866666666666667, 0.031111111111111117, 0.021010101010101034, 0.0058585858585858686, 0.000808080808080841, 0.004242424242424242, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841]
#li = [0.35, 0.09747474747474749, 0.023737373737373724, 0.04126262626262628, 0.010959595959595975, 0.004191919191919163, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818]
#sp = [0.35, 0.10252525252525255, 0.008585858585858558, 0.0866666666666667, 0.031111111111111117, 0.021010101010101034, 0.0058585858585858686, 0.000808080808080841, 0.004242424242424242, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841, 0.000808080808080841]
#pl = [0.35, 0.10252525252525255, 0.023737373737373724, 0.04126262626262628, 0.010959595959595975, 0.004191919191919163, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818, 0.005808080808080818]
#PLOT DIFFs
#plt.plot(range(len(allFuncsDiff)), allFuncsDiff, label='All')
#plt.plot(range(len(allFuncsDiff)), li_pl_sp, label='LI PL SP')
#plt.plot(range(len(allFuncsDiff)), li_sp, label='LI SP')
#plt.plot(range(len(allFuncsDiff)), li_pl, label='LI PL')
#plt.plot(range(len(allFuncsDiff)), pl_sp, label='PL SP')
#plt.plot(range(len(allFuncsDiff)), li, label='LI')
#plt.plot(range(len(allFuncsDiff)), sp, label='SP')
#plt.plot(range(len(allFuncsDiff)), pl, label='PL')

for res in diff_results:
    plt.plot(range(len(res[1])), res[1], label=res[0])

plt.legend(loc='upper right')
plt.savefig('diffs')

#PLOT DIFFs SUMs
plt.clf()
names = []
sums = []
for res in diff_results:
    names.append(res[0])
    sums.append(sum(res[1]))

y_pos = np.arange(len(names))
plt.barh(y_pos, sums, align='center', alpha=0.5)
plt.yticks(y_pos, names)
plt.ylabel('Functions')
plt.xlabel('Aggregated Abs Diff')
plt.title('Aggregated Total Absolute Difference from Threshold by Functions combinations')
plt.savefig('diffs_agg')

print("Done.")



