import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

#from mom.Advisers.ActiveNegotiations.FunctionGraphs.BezierGraph import *
from mom.Advisers.ActiveNegotiations.FunctionGraphs.SplineGraph import *
from mom.Advisers.ActiveNegotiations.FunctionGraphs.LinearInterpolationGraph import *
#from mom.Advisers.ActiveNegotiations.FunctionGraphs.PiecewiseLinearFunction import *


def plot_test(name, x, y):

    #pl = PiecewiseLinearFunction(x, y)
    #pl.dump(name + '_FunctionGraphTest_PiecewiseLinear.png')
    #pl.add_plot(plt)

    lin = LinearInterpolationGraph(x, y)
    lin.add_plot(plt)
    tmp_x = np.linspace(0, 1, 100)
    tmp_y = lin.calc_y(tmp_x)
    plt.plot(tmp_x, tmp_y, label='Linear', color='c')

    #bez = BezierGraph(x, y)
    #bez.add_plot(plt)

    spline = SplineGraph(x, y)
    spline.add_plot(plt)
    tmp_x = np.linspace(0, 1, 100)
    tmp_y = spline.calc_y(tmp_x)
    plt.plot(tmp_x, tmp_y, label='Spline', color='m')

    xx = np.linspace(0, 1, 100)

    y1 = np.array(lin.calc_y(xx))
    #y2 = np.array(bez.calc_y(xx))
    y3 = np.array(spline.calc_y(xx))
    #y4 = np.array(pl.calc_y(xx))

    mean_y = (y1 + y3) / 2
    plt.plot(xx, mean_y, linestyle='--', label='Mean', color='r')

    # S.D
    stds = []
    for i in range(len(xx)):
        stds.append(np.std(np.asarray([y1[i], y3[i]])))
    variances = stds + mean_y
    plt.plot(xx, variances, linestyle='--', label='Mean+STD', color='b')

    plt.plot(x, y, 'o') # plot all given points

    # plot maximum y (profit)
    #max_idx = mean_y.argmax()
    #plt.plot(xx[max_idx], mean_y[max_idx], 'o')

    plt.legend(loc='upper right')
    plt.ylabel('Profits')
    plt.xlabel('Offers')
    plt.title('Mean and STD of functions values (UCB example)')
    plt.savefig(name + '_FunctionGraphTest.png')
    plt.close()

xs = [0, 0.1, 0.24, 0.39, 0.5, 1]
ys = [0, 0,   0.76, 0.61, 0.5, 0]
plot_test("test1", xs, ys)

print("Function Graph Test - Done!")