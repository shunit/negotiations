from AgentsHistory import *
from AgentStrategy import *
from GifBuilder import *
#import bisect 
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

print("Starting..")

vmId = 1
#agent = GreedyAgentStrategy(vmId)
agent = EpsilonGreedyAgentStrategy(vmId)


def add_snapshot(round, ov, p):
    agent.add_snapshot(round, ov, p)  # add snapshot to manager
    #graph_var_gif.addProfitAgentPlot(agent.mgr) # update graph var gif

#graph_var_gif = GifBuilder()
#gifSpline = GifBuilder()


def test_function(x):
    if x < 0.4:
        return 0
    else:
        return -4 * x + 5

round = 0

for i in range(10):
    profit = 0
    while profit == 0:
        ov = agent.get_next_best_offer()
        profit = test_function(ov)
        add_snapshot(round, ov, profit)
        print("ov: " + str(ov) + ", profit: " + str(profit))

    print("Round " + str(round) + " done.")
    round += 1

#graph_var_gif.saveGif('out/graph_var_gif.gif')
#gifSpline.saveGif('graphAnimSpline.gif')

print("Done.")









