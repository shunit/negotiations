import numpy as np

"""
AgentOfferValueSnapshot (roundIndex, ov, profit) - the basic element
AgentOfferValueHistory - addOfferSnaphot()
AgentOffersMgr - addOfferSnapshot()
"""

class AgentOfferValueSnapshot:
  """ a snapshot of OV (Round#, OV, Profit) """

  def __init__(self, roundIndex, ov, profit):
    self.roundIndex = roundIndex
    self.ov = round(ov, 2)  # round number to 0.xx precision
    self.profit = profit
  
  def dump(self):
    print("> r{round}, profit:{profit}".format(
      round=self.roundIndex, 
      #wasAccepted=self.wasAccepted, 
      profit=self.profit))


class AgentOfferValueHistory:
  """ Offer Value Snapshots History - List of OVs """

  def __init__(self, ov):
    self.ov = ov
    self.snapshots = [] # array of AgentOfferValueSnapshot

  def addOfferSnaphot(self, snapshot):
    self.snapshots.append(snapshot)

  def dump(self):
    print("[OV {ov}]".format(ov=self.ov))
    for s in self.snapshots:
      s.dump()


class AgentOffersMgr:
  """Agent Mean Profit Graph Mgr """
  
  def __init__(self, vmIdx):
    self.vmIdx = vmIdx
    self.offers = []

  def addOfferSnapshot(self, snapshot):
    """ Add AgentOfferValueSnapshot """
    # find OV bucket
    ovHistory = next(
      (h for h in self.offers if h.ov == snapshot.ov), None)
    if ovHistory == None: # create new if not exist
      ovHistory = AgentOfferValueHistory(snapshot.ov)
      self.offers.append(ovHistory) # add new history elm
      self.offers = sorted(self.offers, key=lambda elm: 
        elm.ov)

    ovHistory.addOfferSnaphot(snapshot) # add snapshot

  def getAllPoints(self):
    """
     returns Xs(offers), Ys(profits) [including 0 & 1]
    """
    x = [0]
    y = [0]

    for ovHist in self.offers:  # iterate over OV history
      ov = ovHist.ov
      profits = []
      for snap in ovHist.snapshots:
        x.append(ov)
        y.append(snap.profit)

    x.append(1)
    y.append(0)

    return x, y

  def getAllPointsUnique(self):
    """
     returns UNIQUE Xs(offers), Ys(profits) [including 0 & 1]
    """
    x = [0]
    y = [0]

    for ovHist in self.offers:  # iterate over OV history (x)
      ov = ovHist.ov

      # for each x there might be more than 1 y, so leats take mean of y
      arr_y = []
      for snap in ovHist.snapshots:
        arr_y.append(snap.profit)

      x.append(ov)
      y.append(np.mean(arr_y))

    x.append(1)
    y.append(0)

    return x, y

  def toGraph(self):
    """
     returns Xs(offers), Ys(mean profits), Errors[variances] 
    """
    x = []
    y = []
    error = []
    for ovHist in self.offers:  # iterate over OV history
      ov = ovHist.ov
      profits = []
      for snap in ovHist.snapshots:
        profits.append(snap.profit)
      
      profits = np.array(profits)

      x.append(ov)
      y.append(profits.mean())
      error.append(profits.var())

    return x, y, error

  def dump(self):
    print("--- Profit Graph for VM" + str(self.vmIdx) + " ---")
    sortedOffers = sorted(self.offers, key=lambda elm: elm.ov)
    for o in sortedOffers:
      o.dump()
