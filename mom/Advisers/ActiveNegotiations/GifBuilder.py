import uuid
import os
import matplotlib.pyplot as plt
import imageio
import datetime


class GifBuilder:
    """ GIF Builder - by Plots """

    def __init__(self, ylim = 2, is_save_all_images = False, name=''):
        self.IS_SAVE_ALL_IMAGES = is_save_all_images
        self.IS_REMOVE_LAST_FRAME_FILE = False
        self.images = []
        self.ylim = ylim
        self.name = name

    def get_file_name(self):
        if self.IS_SAVE_ALL_IMAGES:
            return "gif_frame_%d.png" % (len(self.images))
        else:
            return "gif_frame_" + self.name + ".png"

    def save_gif(self, path):
        imageio.mimsave(path, self.images, duration=2)

    def add_plot(self, plt):
        fname = self.get_file_name()

        plt.xlim((0, 1))
        plt.ylim((0, self.ylim))

        # plt.axis((0,1,0,5))

        # axes = plt.gca()
        # axes.set_xlim([0, 1])
        # axes.set_ylim([0, 5])

        plt.savefig(fname)
        self.images.append(imageio.imread(fname))

        if self.IS_REMOVE_LAST_FRAME_FILE:
            os.remove(fname)  # delete temp file

    def add_profit_agent_plot(self, agentOffersMgr):
        """ [Deprecated] Add new point to AgentMgr Variances graph """

        x, y, error = agentOffersMgr.toGraph()
        fig, ax = plt.subplots()
        fig.set_tight_layout(True)
        ax.errorbar(x, y, yerr=error, fmt='--o', ecolor='r', capsize=5, elinewidth=1, capthick=1)
        ax.set_title('Profit Mean Graph - ' + datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y"))
        self.addPlot(plt)
        plt.close(fig)

    def add_graph_manager_snapshot(self, title, xs, ys, functions_x_points, functions_y_points_arr,
                                   functions_names, threshold=-1, last_point=None):
        """ Add Graph Mgr Plot (functions, etc..) """

        fig, ax = plt.subplots()
        fig.set_tight_layout(True)
        #ax.errorbar(x, y, yerr=error, fmt='--o', ecolor='r', capsize=5, elinewidth=1, capthick=1)

        ax.set_title(title)

        for i in range(len(ys)):
            plt.plot(xs[i], ys[i], 'ro')  # plot points
            plt.text(xs[i], ys[i], "%.2f" % (xs[i]), fontsize=9)

        # plot functions
        for f_idx in range(len(functions_y_points_arr)):
            f_ys = functions_y_points_arr[f_idx]
            plt.plot(functions_x_points, f_ys, label=functions_names[f_idx])

        if threshold is not -1:
            plt.axvline(x=threshold, color='k', linestyle='--', label='Threshold')

        if last_point is not None:
            plt.plot(last_point[0], last_point[1], 'go')
            plt.text(last_point[0], last_point[1], "%.2f" % (last_point[0]), fontsize=9)

        ax.legend()

        self.add_plot(plt)
        plt.close(fig)
