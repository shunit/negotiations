from numpy import ones,vstack
from numpy.linalg import lstsq

class Point:
  """ 2d point representation """
  def __init__(self, x, y):
    self.x = float(x)
    self.y = float(y)

  def __str__(self):
    return "({x}, {y})".format(x=self.x, y=self.y)

class LineEquation:
  """ a Line Equation helper class """

  def __init__(self, p1, p2):
    """ input: 2d points, ex: (1,1) """
    self.p1 = p1
    self.p2 = p2
    points = [(p1.x, p1.y), (p2.x, p2.y)]
    x_coords, y_coords = zip(*points)
    A = vstack([x_coords,ones(len(x_coords))]).T
    self.m, self.c = lstsq(A, y_coords)[0]  #rcond=None
  
  def calc_y(self, x):
    return self.m * x + self.c

  def dump(self):
    print("y = {m}x + {c}".format(m=self.m,c=self.c))