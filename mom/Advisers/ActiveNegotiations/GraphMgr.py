import numpy as np
import cPickle as pickle

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt

from scipy import interpolate

import datetime
from LineEquation import *

from FunctionGraphs.BezierGraph import *
from FunctionGraphs.SplineGraph import *
from FunctionGraphs.LinearInterpolationGraph import *
from FunctionGraphs.PiecewiseLinearFunction import *


class GraphMgr:
    """ Graph Manager """

    X_MIN = 0
    X_MAX = 1

    def __init__(self, xs, ys, isLI=True, isSP=True, isPL=False, isBZ=False):
        """
        :param xs: X's array
        :param ys: Y's array
        :param isLI: is Linear Inrepolation active?
        :param isSP: is Spline active?
        :param isPL: is Piecewise Linear active?
        :param isBZ: is Bezier active?
        """
        self.xs = xs
        self.ys = ys

        # init functions
        self.functions = []

        if isLI:
            lin = LinearInterpolationGraph(xs, ys)
            self.functions.append(lin)
        if isSP:
            spline = SplineGraph(xs, ys)
            self.functions.append(spline)
        if isPL:
            pl = PiecewiseLinearFunction(xs, ys)
            self.functions.append(pl)
        if isBZ:
            bez = BezierGraph(xs, ys)
            self.functions.append(bez)

    def getMaximumPoints(self, round=0, is_plot=False):
        """
            Get maximum Mean & STD points by graph of functions

            max_x       - max x of y point (by all functions Mean)
            max_var_x   - max x of UCB method (variances = stds + mean_y)
        """

        resolution = 100
        x_points = np.linspace(self.X_MIN, self.X_MAX, resolution)
        y_points_sum = np.zeros(resolution) # [0,..,0]

        # all_ys = [ [f1, f2,..], [f1, f2,..], ..]
        all_ys = []
        for i in range(len(x_points)):  # init array of arrays
            all_ys.append([])

        # iterate over function and get y for each x
        for f in self.functions:
            yi = np.array(f.calc_y(x_points))
            y_points_sum += yi

            # add all function y results (for Variance calculation)
            for i in range(len(x_points)):
                all_ys[i].append(yi[i])

        # Mean
        mean_y = y_points_sum / len(self.functions)
        max_idx = mean_y.argmax()
        max_x = x_points[max_idx]
        max_y = mean_y[max_idx]

        # S.D
        stds = []
        for i in range(len(all_ys)):
            stds.append(np.std(all_ys[i]))
        variances = stds + mean_y
        max_idx = variances.argmax()
        max_var_x = x_points[max_idx]
        max_var_y = mean_y[max_idx]
        error = variances[max_idx]

        # if is_plot:
        #     plt.plot(x_points, mean_y, linestyle='--')  # plot mean
        #     plt.plot(self.xs, self.ys, 'o')             # plot points so far
        #     plt.plot(max_x, max_y, 'x')                 # plot max point
        #     plt.errorbar(max_var_x, max_var_y, yerr=error, fmt='--o', ecolor='r', capsize=5, elinewidth=1, capthick=1)
        #     plt.savefig('out/GraphMgr_round_' + str(round) + '.png')
        #     plt.clf()
        #     plt.close()
        #     plt.figure(0)

        return max_x, max_y, max_var_x, max_var_y, error

    def getMaximumXWithZeroY(self):

        x = 0.1 # default in case that no such point

        for i in range(len(self.xs)):
            if self.ys[i] > 0:
                return x    # last point was 0 (y), return its x
            else:
                x = self.xs[i]  # still y=0, save this x

        return x   # will be here only if no points

    def getPlotPoints(self):

        functions_x_points = np.linspace(0, 1, 100)
        functions_y_points_arr = []
        functions_names = []

        # iterate over function and get y for each x
        for f in self.functions:
            yi = np.array(f.calc_y(functions_x_points))
            functions_y_points_arr.append(yi)
            functions_names.append((f.get_name()))

        # xs,ys & x points and ys per function
        return self.xs, self.ys, functions_x_points, functions_y_points_arr, functions_names


    def serializeData(self):
        # removed
        #xs, ys, functions_x_points, functions_y_points_arr, functions_names = self.getPlotPoints()
        #obj = dict(xs=xs, ys=ys, functions_x_points=functions_x_points,
        #           functions_y_points_arr=functions_y_points_arr, functions_names=functions_names)

        obj = dict(xs=self.xs, ys=self.ys)

        return pickle.dumps(obj)

    def deserializeData(self, json_obj):
        obj = pickle.loads(json_obj)
        #return obj['xs'], obj['ys'], obj['functions_x_points'],
        #obj['functions_y_points_arr'], obj['functions_names']
        self.xs = obj['xs']
        self.ys = obj['ys']
