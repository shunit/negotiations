import random
from threading import Lock
import traceback
import pprint
import cPickle as pickle
import sys
import numpy as np
import random

from AgentsHistory import *
from GraphMgr import *


class AgentStrategyAbstract(object):
    """ Interface of AgentStrategy class """

    ROUND_NUMBER_INDEX = 0
    COALITION_PROFIT_INDEX = 1

    def __init__(self, vmId, isLI=True, isSP=True, isPL=False, isBZ=False):
        self.mgr = AgentOffersMgr(vmId)
        self.snapshots = 0
        self.lock = Lock()
        self.profits = []   # arr of tuples<round, coalition_profit>
        self.isLI = isLI
        self.isSP = isSP
        self.isPL = isPL
        self.isBZ = isBZ

    def dump_points(self):
        xs, ys = self.mgr.getAllPointsUnique()  # get all (x,y)
        return "xs: %s, ys: %s" % (xs, ys)

    def add_snapshot(self, round, ov, is_accepted):
        self.lock.acquire()
        try:
            ov = float(ov)
            if not is_accepted:
                self.mgr.addOfferSnapshot(AgentOfferValueSnapshot(roundIndex=round, ov=ov, profit=0))
            else:
                self.mgr.addOfferSnapshot(AgentOfferValueSnapshot(roundIndex=round, ov=ov, profit=1-ov))

            self.snapshots += 1
        finally:
            self.lock.release()

    def add_profit(self, round, ov, profit):
        self.lock.acquire()
        try:
            ov = float(ov)
            coalition_profit = (profit * 1.0) / (1 - ov)
            self.profits.append((round, coalition_profit))
        finally:
            self.lock.release()

    def get_next_best_offer(self):
        raise NotImplementedError('subclasses must override get_next_best_offer()!')

    def serialize_snapshot(self):
        try:
            #x, y = self.mgr.getAllPointsUnique()  # get all (x,y)
            #obj = dict(xs=x, ys=y)
            #return pickle.dumps(obj)

            return repr(pickle.dumps(self.mgr))

            '''
            if len(xs) > 2:
                graph_mgr = GraphMgr(xs, ys)
                return graph_mgr.serializeData()
            else:
                return "WARNING: len is too short: " + str(len(xs)) + ". xs: " + str(xs)
            '''
        except Exception as e:
            # exp_desc = pprint.pformat(e)
            # tback = traceback.format_stack()
            tback = traceback.format_exc()
            return str(e.message) + ", Args:" + str(e.args) + ", Stack:" + str(tback)

    def deserialize_snapshot(self, serialized_obj):
        self.mgr = pickle.loads(serialized_obj.strip('"').replace("\\n", "\n").strip('\n'))

    def get_expected_profit(self):

        if len(self.profits) == 0:
            return 1    # default const value

        return self.profits[-1][self.COALITION_PROFIT_INDEX]    # return last profit

    def is_points_close(self, x, y):
        return np.absolute(x-y) < 0.05

class UniformBootstrapLogic:
    """ Uniform Bootstrap Logic"""

    def __init__(self):
        pass

    MIN_ITERATIONS_BEFORE_EXPLOIT = 1   # ex: 1 iterations means xs: [0, 0.5, 1]

    def __get_next_bootstrap_offer(self, last_x):
        """ return the next offer - uniformly [0-1] """
        step = float(GraphMgr.X_MAX - GraphMgr.X_MIN) / float(self.MIN_ITERATIONS_BEFORE_EXPLOIT + 1)
        return last_x + step

    def is_in_bootstrap_step(self, xs):
        """ check if we reached at least K points """
        return len(xs) < self.MIN_ITERATIONS_BEFORE_EXPLOIT + 2

    def get_next_bootstrap_step(self, xs):
        """ explore uniformly K points """
        cleaned_xs = xs[1:-1]  # ignore first and last (0 & 1)
        if xs is None or len(cleaned_xs) == 0:  # First point
            return self.__get_next_bootstrap_offer(0)
        elif len(cleaned_xs) < self.MIN_ITERATIONS_BEFORE_EXPLOIT:  # rest of bootstrap points
            return self.__get_next_bootstrap_offer(max(cleaned_xs))


class GreedyAgentStrategy(AgentStrategyAbstract, UniformBootstrapLogic):
    """
    a Greedy implementation of Agent Strategy
    """

    def get_next_best_offer(self):
        self.lock.acquire()
        try:
            xs, ys = self.mgr.getAllPointsUnique()  # get all (x,y)

            # 1. Bootstrap - first explore uniformly K points
            if self.is_in_bootstrap_step(xs):
                return self.get_next_bootstrap_step(xs)

            # 2. Greedy
            graph_mgr = GraphMgr(xs, ys, self.isLI, self.isSP, self.isPL, self.isBZ)    # build a graph manager accordingly
            max_x, max_y, max_var_x, max_var_y, error = graph_mgr.getMaximumPoints(len(xs), True)

            return max_x
        finally:
            self.lock.release()


class EpsilonGreedyAgentStrategy(AgentStrategyAbstract, UniformBootstrapLogic):
    """
    a Greedy implementation of Agent Strategy
    """

    EXPLORATION_RATIO = 1  # 1 out of X

    def get_next_best_offer(self):

        self.lock.acquire()
        try:
            xs, ys = self.mgr.getAllPointsUnique()  # get all (x,y)

            # 1. Bootstrap - first explore uniformly K points
            if self.is_in_bootstrap_step(xs):
                return self.get_next_bootstrap_step(xs), "Bootstrap"

            # 2. EpsilonGreedy
            graph_mgr = GraphMgr(xs, ys, self.isLI, self.isSP, self.isPL, self.isBZ)  # build a graph manager accordingly
            max_x, max_y, max_var_x, max_var_y, error = graph_mgr.getMaximumPoints(self.snapshots, True)

            print("max_x: "+str(max_x)+", max_y:"+str(max_y)+", max_var_x:"+str(max_var_x)+", max_var_y:"+str(max_var_y)+", error:"+str(error))

            # before decide to ExploreExploit handle all zeroes
            if max_y == 0:
                return (1 + xs[-2])*0.5, "AllZeroes-RightMidPoint"   # return mid point between the rightmost point before 1

            # 2.1 - Explore - Using UCB Method
            should_explore = random.randint(1, self.EXPLORATION_RATIO) == 1
            if should_explore:
                if self.is_points_close(max_var_x, max_x):   # np.isclose(max_var_x, max_x):
                    if max_x == 0:
                        return 0.99, "NoProfitValue-Try-0.99"    # in case of no profit value, set to 1 in order to fetch at least 1 value
                    else:
                        print("EXPLORE - with mid value!")

                        last_zeroed_x = graph_mgr.getMaximumXWithZeroY()

                        # -- OLD mid-point exploration code --
                        # get mid of last x point of 0 to max x (profit)... (like bool search)
                        #res = last_zeroed_x + (max_var_x - last_zeroed_x)/2

                        # -- NEW smart-random exploration code (from last_zeroed_x to max_var_x) --
                        res = random.randint(int(last_zeroed_x*100), int(max_var_x*100)) / 100.0

                        print("last_zeroed_x:", last_zeroed_x, "max_var_x: ", max_var_x, "NEW POINT: ", res)
                        return res, "EXP-SamePoint"
                else:
                    print("EXPLORE new !", max_var_x, max_x)
                    return max_var_x, "EXP-NewPoint"

            # 2.2 - Exploit
            print("Exploit")
            return max_x, "Exploit"
        finally:
            self.lock.release()

