import numpy as np
#import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.interpolate import splev, splrep


class SplineGraph:
    """ Spline Graph Handler """

    def __init__(self, x, y):
        # x values should be unique!
        self.xs = x
        self.ys = y
        self.degree = 3  # default 3 (ranges: 1 to 5)

        # trim right 0 xs, and leave only 1 (avoid high peaks in ys)
        self.first_zeroed_x_value = 0

        non_zero_idx = 0
        for index, item in enumerate(self.ys):
            if item > 0:
                non_zero_idx = index
                break

        if non_zero_idx > 0:
            first_zeroed_x_idx = non_zero_idx - 1
            self.xs = self.xs[first_zeroed_x_idx:]
            self.ys = self.ys[first_zeroed_x_idx:]
            self.first_zeroed_x_value = self.xs[0]  # save first zeroed y x value

        if len(self.xs) <= 3:
            self.degree = len(self.xs) - 1

        self.spl = splrep(self.xs, self.ys, k=self.degree)

    def get_name(self):
        return "Spline deg " + str(self.degree)

    def calc_y(self, x):
        """ calc x/xs by y/ys """

        xs = np.asarray(x)

        # find the first x idx that provides non zeroed y
        first_non_zeroed_x_idx = 0
        for i in range(len(xs)):
            if xs[i] >= self.first_zeroed_x_value:
                first_non_zeroed_x_idx = i
                break

        # take the first zeroed x value (where y is zero)
        if first_non_zeroed_x_idx == 0:
            return splev(x, self.spl, ext=0)  # calculate values

        first_non_zeroed_x_idx -= 1

        sub_xs = xs[first_non_zeroed_x_idx:]
        sub_ys = splev(sub_xs, self.spl, ext=0)  # calculate values

        return ([0] * first_non_zeroed_x_idx) + sub_ys.tolist()


    def get_maximum(self):
        # find 'hard' maximum
        x = np.linspace(0, 1, 100)  ## TODO: 20
        y = self.calc_y(x)

        # TODO: soft..

        maxIdx = np.where(y == np.amax(y))
        maxX = x[maxIdx]
        maxY = y[maxIdx]

        return maxX, maxY

    def add_plot(self, plt):
        # Build Plotting Graph
        # Build (x,y) points for spline function,
        #  [0-1] in resolution of 100
        pass
        # x = np.linspace(0, 1, 100)
        # y = self.calc_y(x)
        # plt.plot(x, y)