import numpy as np
#import matplotlib.pyplot as plt
from mom.Advisers.ActiveNegotiations.LineEquation import *
import datetime


class PiecewiseLinearFunction:
    """ PiecewiseLinearFunction Graph Handler """

    X_MIN = 0
    X_MAX = 1

    def __init__(self, x, y):
        self.Points = []
        # add all (x,y)'s
        for i in range(len(x)):
            self.Points.append(Point(x[i], y[i]))

    def get_name(self):
        return "Piecewise Linear"

    def __get_y_points(self, inputX):
        """
        Get ys according to given point (by all graphs)
        """
        total_ys = []

        for p in self.Points:  # iterate over all points
            if p.x == inputX:
                total_ys.append(p.y)  # same x - just add point
            else:
                if p.x > inputX:  # x inside on the left line
                    # create line from (0, 0) to p
                    l = LineEquation(Point(self.X_MIN, 0), p)
                elif p.x < inputX:
                    # create line from p to (1, 0)
                    l = LineEquation(p, Point(self.X_MAX, 0))
                # find intersection point (in:x, out:y)
                # newPoint = Point(inputX, l.calc_y(inputX))
                total_ys.append(l.calc_y(inputX))

        return total_ys

    def __get_mean_y(self, inputX):
        return np.mean(self.__get_y_points(inputX))

    def __get_max_y(self, inputX):
        return np.max(self.__get_y_points(inputX))

    def calc_y(self, x):
        """ calc x/xs by y/ys """
        if isinstance(x, list) or isinstance(x, np.ndarray):
            return [self.__get_max_y(x_elm) for x_elm in x]
        else:
            return self.__get_max_y(x)

    def get_maximum(self):
        # find 'hard' maximum
        x = np.linspace(0, 1, 100)  ## TODO: 20
        y = self.calc_y(x)

        # TODO: soft..

        maxIdx = np.where(y == np.amax(y))
        maxX = x[maxIdx]
        maxY = y[maxIdx]

        return maxX, maxY

    def add_plot(self, plt):
        # Build Plotting Graph
        # Build (x,y) points for spline function,
        #  [0-1] in resolution of 100
        pass
        # x = np.linspace(0, 1, 100)
        # y = self.calc_y(x)
        # plt.plot(x, y)

    def dump(self, fname):
        """ Dump plot """
        pass
        # plt.figure(111)
        # fig, ax = plt.subplots()
        # fig.set_tight_layout(True)
        # ax.set_title('Var Graph - ' + datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y"))
        # plt.xlim((0, 1))
        # plt.ylim((0, 10))
        #
        # for p in self.Points:  # iterate over all points
        #     # draw function per point
        #     xs = [0, p.x, 1]
        #     ys = [0, p.y, 0]
        #     plt.plot(xs, ys, linewidth=1, linestyle='--')
        #
        # self.add_plot(plt)
        #
        # plt.savefig(fname)
        # plt.figure(0)