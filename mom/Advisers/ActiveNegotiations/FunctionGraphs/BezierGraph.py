import numpy as np
import bezier


class BezierGraph:
    """ Bezier Graph Handler """

    def __init__(self, x, y):
        # x values should be unique!
        self.xs = x
        self.ys = y
        nodes = np.asfortranarray([x, y])
        self.degree = 2  # no major diff from the rest

        # trim right 0 xs, and leave only 1 (avoid high peaks in ys)
        self.first_zeroed_x_value = 0

        non_zero_idx = 0
        for index, item in enumerate(self.ys):
            if item > 0:
                non_zero_idx = index
                break

        if non_zero_idx > 0:
            first_zeroed_x_idx = non_zero_idx - 1
            self.xs = self.xs[first_zeroed_x_idx:]
            self.ys = self.ys[first_zeroed_x_idx:]
            self.first_zeroed_x_value = self.xs[0]  # save first zeroed y x value



        self.curve = bezier.Curve(nodes, degree=self.degree)
        # nodes1 = np.asfortranarray([x, y])
        # curve1 = bezier.Curve(nodes1, degree=2)

    def get_name(self):
        return "Bezier deg " + str(self.degree)

    def calc_y(self, x):
        """ calc x/xs by y/ys """
        if isinstance(x, list) or isinstance(x, np.ndarray):

            xs = np.asarray(x)

            # find the first x idx that provides non zeroed y
            first_non_zeroed_x_idx = 0
            for i in range(len(xs)):
                if xs[i] >= self.first_zeroed_x_value:
                    first_non_zeroed_x_idx = i
                    break

            # take the first zeroed x value (where y is zero)
            if first_non_zeroed_x_idx == 0:
                return self.curve.evaluate_multi(x)[1]  # calculate values

            first_non_zeroed_x_idx -= 1

            sub_xs = xs[first_non_zeroed_x_idx:]
            sub_ys = self.curve.evaluate_multi(np.asarray(sub_xs))[1]  # calculate values

            return ([0] * first_non_zeroed_x_idx) + sub_ys.tolist()


            #return self.curve.evaluate_multi(np.asarray(x))[1]
        else:
            return self.curve.evaluate(x)[1][0]

    def get_maximum(self):
        # find 'hard' maximum
        x = np.linspace(0, 1, 100)  ## TODO: 20
        y = self.calc_y(x)

        # TODO: soft..

        maxIdx = np.where(y == np.amax(y))
        maxX = x[maxIdx]
        maxY = y[maxIdx]

        return maxX, maxY

    def add_plot(self, plt):
        # Build Plotting Graph
        # Build (x,y) points for spline function,
        #  [0-1] in resolution of 100
        pass
        # x = np.linspace(0, 1, 100)
        # y = self.calc_y(x)
        # plt.plot(x, y)