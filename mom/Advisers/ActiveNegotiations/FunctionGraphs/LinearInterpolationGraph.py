import numpy as np
#import matplotlib.pyplot as plt
from scipy import interpolate


class LinearInterpolationGraph:
    """ Linear interpolation Graph Handler """

    def __init__(self, x, y):
        self.xs = x
        self.ys = y
        self.f = interpolate.interp1d(x, y, kind='linear')  # fill_value="extrapolate"

    def get_name(self):
        return "Linear interpolation"

    def calc_y(self, x):
        """ calc x/xs by y/ys """
        return self.f(x)

    def get_maximum(self):
        # find 'hard' maximum
        x = np.linspace(0, 1, 100)  ## TODO: 20
        y = self.calc_y(x)

        # TODO: soft..

        maxIdx = np.where(y == np.amax(y))
        maxX = x[maxIdx]
        maxY = y[maxIdx]

        return maxX, maxY

    def add_plot(self, plt):
        # Build Plotting Graph
        # Build (x,y) points for spline function,
        #  [0-1] in resolution of 100
        pass
        # x = np.linspace(0, 1, 100)
        # y = self.calc_y(x)
        # plt.plot(x, y)