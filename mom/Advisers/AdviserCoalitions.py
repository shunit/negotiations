import csv
import logging
import os
from threading import Timer
from threading import Lock
import sys
import random
from copy import deepcopy
import numpy as np

from mom.Adviser import p_digits, p_eps
from mom.Advisers.AdviserProfitEstimatorAge import AdviserProfitEstimatorAge, Bill
from mom.Advisers.AdviserProfit import AdviserProfit
from mom.Comm.GuestServer import GuestServer
from mom.Comm.GuestCommnicator import GuestCommunicator
from mom.Comm.Messages import MessageCoalitionOffer
from mom.Comm.Proposals import OFFER, COUNTER_OFFER, REJECT, ACCEPT1, FINAL1, FINAL2, NOT_LEADER, UPDATE_MEMBERS, COALITION_SIZE

SHAPLEY = "shapley"

class ForbiddenRangesException(Exception):
    pass

class CoalitionHistoryItem(object):
    
    def __init__(self, rnd, profit_parts, won_memory, bill, bill_wo_coalition):
        self.round = rnd
        self.coalition = profit_parts
        self.won_memory = won_memory
        self.bill = bill
        self.bill_without_coalition = bill_wo_coalition

    def __str__(self):
        return "Round: %s, bill: %s, coalition: %s, bill_without_coalition: %s, won memory: %s" % (
                self.round, self.bill, self.coalition, self.bill_without_coalition, self.won_memory)
        
class CoalitionHistory(object):
    def __init__(self):
        self.history = []
        self.max_history_size = 100

    def add(self, item):
        self.history.insert(0, item)
        self.history = self.history[:self.max_history_size]

    def size(self):
        return len(self.history)

    def __getitem__(self, key):
        return self.history[key]

    def __str__(self):
        return str([str(i) for i in self.history])

class AdviserCoalitions(AdviserProfitEstimatorAge):
    """
    Adviser with coalitions implementation. In each memory auction round, it executes negotiations with other guests
    and reports the requested coalition to the host.
    """

    END_OF_NEGOTIATIONS_ROUND = 7
    MATH_EPSILON = 0.0001

    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, vm_number, num_of_guests,
                 comm_timeout, enable_negotiations=True, offer_value=0.5, accept_threshold=0.5, 
                 draw_per_partner = False, guard_offers=False,
                 leave_coalition_if_smaller_q=True, fear_coalition_break=True, very_careful=False, shapley=True):
        """
        @param offer_value - a float in [0,1], represents the profit part the guest will offer to a potential partner.
        @param accept_threshold - a float in [0,1], represents the minimal profit part the guest is willing to consider 
                                  when negotiating a coalition.
        """
        self.vm_number = vm_number
        self.guest_communicators = {}
        self.guest_data = {}  # To read/write, use _update_guest_data() and _retrieve_guest_data() which read and write safely.
        self.guest_data_lock = Lock()

        self.num_of_guests = num_of_guests
        self.auction_mem = 0
        self.enable_negotiations = enable_negotiations
        self.alphas = {0:0.5}
        # Coalition parameters
        self.final_lock = Lock()
        self.coalition_leader = None
        self.profit_parts = {}
        self.normalized_offer_value = 0
        self.normalized_accept_threshold = 0
        if draw_per_partner:
            self.chosen_ov = 0
            self.chosen_at = 0
        else:
            self.chosen_ov = offer_value
            self.chosen_at = accept_threshold
        # Last round's coalition
        self.prev_coalition_leader = None
        self.prev_profit_parts = {}
        # Private negotiation parameters
        self.draw_per_partner = draw_per_partner
        self.guard_offers = guard_offers # If True, avoid sending offer values greater than 1.
        self.offer_value = offer_value
        self.accept_threshold = accept_threshold
        self.leave_coalition_if_smaller_q = leave_coalition_if_smaller_q
        self.backoff_multiplier = 2 # TODO: make it 5?
        self.fear_coalition_break = fear_coalition_break
        self.very_careful = very_careful
        self.last_requested = 0
        self.coalition_q = None
        self.ov_or_at_effect = None
        self.shapley = shapley
        
        # Guests who sent us a FINAL1 but did not get a response. Re-initialized every round.
        self.finalists = set()
        # The best guest to cooperate with in this round, who is waiting for us to send FINAL1.
        # May change several times in one round as a result of rejections/timeouts.
        self.chosen_guest_active = None
        # The best guest to cooperate with in this round, from whom we are waiting for a FINAL1.
        # May change several times in one round as a result of rejections/timeouts.
        self.chosen_guest_passive = None
        # Are we waiting for another guest's FINAL2 message. If so, will contain that guest's id, otherwise None.
        # Should be accessed only with self.final_lock.
        self.currently_waiting_for_final2 = None
        # After increasing q we wait for one round and do not try to form a coalition.
        self.do_not_negotiate_on_round = -1

        # Did we get a Notify Message already this round. Until we do, we shouldn't answer any 
        # coalition offers (since we don't know the result of the last coalition negotiation.)
        # Should be accessed only under self.notify_lock.
        self.got_notify_message_this_round = False
        self.notify_lock = Lock()
        self.message_queue = []
        # Save data about the actual bill, memory we won and coalition we were in.
        self.coalition_history = CoalitionHistory()
        
        for i in range(1, num_of_guests + 1):
            if i == vm_number:
                continue
            self.guest_communicators[i] = GuestCommunicator('192.168.123.%d' % (1 + i),
                                                            GuestServer.port_guest_comm_mem,
                                                            comm_timeout, i)
            self._update_guest_data(i, "removed_by_host_or_left_in_round", 0)
            self._update_guest_data(i, "removed_by_host_or_left_count", 0)
            self._update_guest_data(i, "rejection_backoff", 1)
            self._update_guest_data(i, "rounds_until_next_interaction", 0)

        AdviserProfitEstimatorAge.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, vm_number=vm_number, update_alpha_guard=False)
        self.logger.info("CSV round_number,guest_id,load,allocated_base_mem,notify_mem,valuation,"+
                         "bill,bill_wo_coalition,requested_mem,offer_value,accept_threshold,bid_price\n")

    def do_advice_coalition(self):
        self.logger.info("do_advice_coalition called. round %s, prev_profit_parts: %s", self.rnd_mem, self.prev_profit_parts)
        self._access_got_notify(new_value=False)
        # If no new coalitions were created this round, use the one from previous round.
        if self.coalition_leader is None or not self.profit_parts:
            self.coalition_leader = self.prev_coalition_leader
            self.profit_parts = deepcopy(self.prev_profit_parts)

        # Reset members for the next round.
        self.finalists = set()
        self.chosen_guest_active = None
        self.chosen_guest_passive = None
        # Reset items in guest data for the next round.
        for guest in self.guest_data.iterkeys():
            self._update_guest_data(guest, "got_accept", False)
            self._update_guest_data(guest, "sent_accept", False)
            self._update_guest_data(guest, "sent_message_coalition_offer", False)
            self._update_guest_data(guest, "profit_parts", {})
            self._update_guest_data(guest, "coalition_leader", None)
            self._update_guest_data(guest, "rejected_in_this_round", False)
            self._update_guest_data(guest, "interacted_in_this_round", False)
            self._update_guest_data(guest, "got_message_in_queue_this_round", False)
            self._update_guest_data(guest, "coalition_size", 0)
            self._update_guest_data(guest, "ov_or_at_effect", None)

        # Calculate the response for the host.
        coalition_members = {}
        if self._should_leave_coalition():
            # TODO: maybe also add a backoff on the guests?
            self.coalition_leader = None
            self.profit_parts = {}
            self.coalition_q = None
            self.logger.info("%s setting coalition_q to None in do_advice_coalition", self._prefix())
        else:
            #If a shapley deal was reached, all values of the profit_parts map would be "shapley".
            for (member, part) in self.profit_parts.iteritems():
                coalition_members['vm-%d' % member] = part
        self.logger.info("%s Sending to host %s", self._prefix(), coalition_members)
        self._empty_message_queue()
        return coalition_members

    def _should_leave_coalition(self):
        self.logger.info("%s should_leave_coalition: do not negotiate on round: %d,this round: %d ", 
                         self._prefix(), self.do_not_negotiate_on_round, self.rnd_mem)
        if self.do_not_negotiate_on_round == self.rnd_mem + 1:
            return True
        if self.coalition_history.size() < 2 or self.hist_mem.len() < 3:
            return False
        last_rq = self.hist_mem[1].bid_rq
        last_memory = min(self.coalition_history[0].won_memory, last_rq)
        before_last_memory = min(self.coalition_history[1].won_memory, self.hist_mem[2].bid_rq)
        # If we got what we asked for, no need to leave the coalition.
        self.logger.info("%s should_leave_coalition: last memory:%s last requested:%s", 
                         self._prefix(), last_memory, last_rq[0][1])
        if len(last_rq) == 1 and last_rq[0][1] <= last_memory:
            self.logger.info("%s should_leave_coalition is False because last requested: %s and last won: %s",
                         self._prefix(), last_rq[0][1], last_memory)
            if last_rq[0][1] < last_memory:
                self.logger.error("got too much memory. requested:%s, got:%s", last_rq[0][1], last_memory)
            return False
        # If we got less memory in the last round compared to the round before 
        # that, it's time to leave the coalition.
        ret = self.leave_coalition_if_smaller_q and last_memory < before_last_memory
        self.logger.info("%s should_leave_coalition ? %s because leave_coalition_if_smaller_q: %s and last 2 memory results: %s, %s",
                         self._prefix(), ret, self.leave_coalition_if_smaller_q, before_last_memory, last_memory)
        return ret

    def _prob_of_coalition_break(self, q_current, q_new):
        if q_current >= q_new:
            return 0
        else:
            # TODO: reconsider probability / use better logic.
            return 0.5

    def _estimated_num_rounds_until_coalition(self, coalition_size):
        # TODO: this can be better. Like, log(coalition_size), or based on history of creating the existing coalition.
        return 5

    def _num_future_rounds(self):
        """The number of rounds left to participate in the auction.
        TODO: could be changed to return the number of rounds in which the valuation and load are not expected to change.
        """
        return 10

    def process_coalition_message(self, message_content):
        """Wraps process_coalition_offer. Only calls it if we already got the Notify message, 
        otherwise saves the message in a queue."""
        if not self._access_got_notify():
            self._add_message_to_queue(message_content)
            self._update_guest_data(message_content["sender_id"], "got_message_in_queue_this_round", True)
        else:
            self.process_coalition_offer(message_content)

    def process_coalition_offer(self, message_content):
        """
        Handles a proposal of a coalition.
        @param message_content: a dictionary with the contents of the sent message.
        """
        if self.rnd_mem in [self.do_not_negotiate_on_round, self.do_not_negotiate_on_round-1]:
            return

        sender_id = message_content["sender_id"]
        profit_parts = message_content["profit_parts"]
        status = message_content["status"]

        rounds_until_next_interaction = self._retrieve_guest_data(sender_id, "rounds_until_next_interaction")
        if rounds_until_next_interaction > 0:
            self.logger.info("%s Ignored message from %s. %s more rounds until next interaction.", 
                             self._prefix(), sender_id, rounds_until_next_interaction)
            return

        self._update_guest_data(sender_id, "interacted_in_this_round", True)

        if status == COALITION_SIZE:
            # Check if the message is a duplicate communication thread, i.e., there is already a communication thread with this guest.
            # In that case we should ignore the thread started by the guest with the larger id.
            # This can happen only with the first message in the protocol, i.e., COALITION_SIZE.
            if self._retrieve_guest_data(sender_id, 'sent_message_coalition_offer'):
                if self.vm_number < sender_id:
                    # Ignore the other guest's message
                    self.logger.info("%s Message %s ignored by %s because it is duplicate.", 
                                     self._prefix(), message_content, self.vm_number)
                    return
            # If I am in a coalition and not the leader, refer the sender to the leader.
            elif self.coalition_leader is not None and self.coalition_leader != self.vm_number:
                self.logger.info("%s Sending MessageCoalitionOffer of NOT_LEADER, leader is %s",
                                 self._prefix() ,self.coalition_leader)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts={},
                    status=NOT_LEADER, leader=self.coalition_leader, other_members={}, coalition_size=self._my_coalition_size()))
                return
            # If we are here then I'm allowed to talk to this partner.
            # Save the partner's coalition size
            self._update_guest_data(sender_id, 'coalition_size', int(message_content['coalition_size']))
            # Reply with an offer containing my own coalition size.
            # Calculate the offer value according to coalition sizes.
            if self.shapley:
                offer_value = SHAPLEY
                my_part = SHAPLEY
                self._update_guest_data(sender_id, 'last_offered_value', SHAPLEY)
                self._update_guest_data(sender_id, 'accept_threshold', SHAPLEY)
                self._update_guest_data(sender_id, 'normalized_offer_value', SHAPLEY)
                self._update_guest_data(sender_id, 'normalized_accept_threshold', SHAPLEY)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number,
                    profit_parts={self.vm_number: my_part, sender_id: offer_value},
                    status=OFFER, leader = None, other_members = {}, coalition_size=self._my_coalition_size()))
            else:
                ov, at = self._draw_negparams_for_partner(sender_id)
                offer_value = self._normalize_offer_value(ov, sender_id)
                my_part = 1- offer_value
                #We don't offer a value over 1. We'd rather not join the coalition at all. Sometimes the partner will offer us something better than 0.
                if offer_value < 1 or not self.guard_offers: # Guard against too high normalized OVs
                        self._send_message(sender_id, MessageCoalitionOffer(
                            sender_id=self.vm_number,
                            profit_parts={self.vm_number: 1-offer_value, sender_id: offer_value},
                            status=OFFER, leader = None, other_members = {}, coalition_size=self._my_coalition_size()))
                        # Note: saving non-normalized value
                        self._update_guest_data(sender_id, 'last_offered_value', ov)
                        self._update_guest_data(sender_id, 'accept_threshold', at)
                        # Also save normalized offer value and accept threshold for statistics
                        self._update_guest_data(sender_id, 'normalized_offer_value', offer_value)
                        self._update_guest_data(sender_id, 'normalized_accept_threshold', self._normalize_accept_threshold(at, sender_id))

            
        elif status in (OFFER, COUNTER_OFFER):
            if status == OFFER:
                self._update_guest_data(sender_id, 'coalition_size', message_content['coalition_size'])
                ov, at = self._draw_negparams_for_partner(sender_id)
                # Also save normalized offer value and accept threshold for statistics
                self._update_guest_data(sender_id, 'last_offered_value', ov)
                self._update_guest_data(sender_id, 'accept_threshold', at)
                self._update_guest_data(sender_id, 'normalized_offer_value', self._normalize_offer_value(ov, sender_id))
                self._update_guest_data(sender_id, 'normalized_accept_threshold', self._normalize_accept_threshold(at, sender_id))
            if status == COUNTER_OFFER:
                self._update_guest_data(sender_id, 'max_rejected_value', 
                                        max(self._retrieve_guest_data(sender_id, 'last_offered_value'),
                                            self._retrieve_guest_data(sender_id, 'max_rejected_value')))

            # This logic is common to OFFER, COUNTER_OFFER: considering the offer.
            at = self._retrieve_guest_data(sender_id, 'accept_threshold')
            ov = self._retrieve_guest_data(sender_id, 'last_offered_value')

            offer = profit_parts[self.vm_number]
            # Accept a shapley division iff it is a shapley agent
            if (offer == SHAPLEY and not self.shapley) or (offer != SHAPLEY and self.shapley):
                self.logger.info("%s rejecting %s because I am %sa Shapley agent who was offered %s", 
                                 self._prefix(), sender_id, "" if self.shapley else "not ", offer)
                self._send_reject_to_guest_id(sender_id)
                return
            if offer == SHAPLEY: 
                # If offered a shapley division (this is a shapley agent), immediately accept.
                leader = self.choose_leader(sender_id, sender_id)
                self.logger.info("%s Sending ACCEPT1 to %s, profit parts: %s, leader would be %s",
                                 self._prefix(), sender_id, profit_parts, leader)
                self._update_guest_data(sender_id, "profit_parts", profit_parts)
                self._update_guest_data(sender_id, "coalition_leader", leader)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts=profit_parts ,status=ACCEPT1, leader = None,
                    other_members = {}, coalition_size=self._my_coalition_size()))
                self._update_guest_data(sender_id, "sent_accept", True)
                return

            # Otherwise, this is a profit part offer.
            offered_to_me = self._normalized_to_orig_value(profit_parts[self.vm_number], sender_id)
            if offered_to_me < at: #and not np.isclose(offered_to_me, self.accept_threshold):
                # The offer is not acceptable. Propose a counter offer, unless the partner is likely to 
                # decline the new offered value, based on past experience.
                offer_value = self._normalize_offer_value(ov, sender_id)
                max_rejected_value = self._retrieve_guest_data(sender_id, 'max_rejected_value')
                if (max_rejected_value is None or ov > max_rejected_value) and (offer_value < 1 or not self.guard_offers):
                    # Note: comparing non-normalized values
                    self._send_message(sender_id, MessageCoalitionOffer(
                        sender_id=self.vm_number,
                        profit_parts={self.vm_number: 1-offer_value, sender_id: offer_value},
                        status=COUNTER_OFFER, leader = None, other_members = {}, coalition_size=self._my_coalition_size()))
                    # Note: saving the non normalized value
                    self._update_guest_data(sender_id, 'last_offered_value', ov)
                else: # The partner is unlikely to accept our counter offer, or our offer is too high for these coalition sizes. Let them know we reject their offer.
                    self._send_reject_to_guest_id(sender_id)
                    # Document that negotiations did not go well this time.
                    self.logger.info("CSV NEGPARAMS %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                             self.rnd_mem, self.vm_number, ov, at,
                             self._normalize_offer_value(ov, sender_id), self._normalize_accept_threshold(at, sender_id),
                             0, # bill
                             0, # bill without coalition
                             None, # was_leader
                             None, #leader
                             None, #ov_or_at_effect
                             )

            else: # The offer is acceptable
                # leader is the one with bigger coalition or the proposer in case of a tie.
                leader = self.choose_leader(sender_id, sender_id)
                self.logger.info("%s Sending ACCEPT1 to %s, profit parts: %s, leader would be %s", 
                                 self._prefix(), sender_id, profit_parts, leader)
                self._update_guest_data(sender_id, "profit_parts", profit_parts)
                self._update_guest_data(sender_id, "coalition_leader", leader)
                self._send_message(sender_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts=profit_parts ,status=ACCEPT1, leader = None, 
                    other_members = {}, coalition_size=self._my_coalition_size()))
                self._update_guest_data(sender_id, "sent_accept", True)
                self._update_guest_data(sender_id, "ov_or_at_effect", "at")
            
        elif status == ACCEPT1:
            leader = self.choose_leader(sender_id, self.vm_number)
            self.logger.info("%s Got ACCEPT1 from %s, saving profit parts: %s, leader would be %s", 
                             self._prefix(), sender_id, profit_parts, leader)
            self._update_guest_data(sender_id, "got_accept", True)
            self._update_guest_data(sender_id, "profit_parts", profit_parts)
            self._update_guest_data(sender_id, "coalition_leader", leader)
            self._update_guest_data(sender_id, "ov_or_at_effect", "ov")

        elif status == FINAL1:
            self.logger.info("%s Got FINAL1 from %s", self._prefix(), sender_id)
            if self.coalition_leader is not None and self.profit_parts:
                self.logger.info("%s rejecting guest %s because I'm already in a coalition- leader: %s  profit_parts: %s ",
                                 self._prefix(), sender_id, self.coalition_leader, self.profit_parts)
                self._send_reject_to_guest_id(sender_id)
                return
            self.logger.info("%s Got FINAL1. Trying to acquire final_lock", self._prefix())
            self.final_lock.acquire()
            self.logger.info("%s Got FINAL1. Acquired final_lock", self._prefix())
            try:
                if self.currently_waiting_for_final2 is not None:
                    self.logger.info("%s Got FINAL1 from %s but I'm waiting for FINAL2 from %s",
                                     self._prefix(), sender_id, self.currently_waiting_for_final2)
                    self.finalists.add(sender_id)
                    self._update_guest_data(sender_id, "other_members", message_content["other_members"])
                else:
                    best_partner = self._get_best_partner_by_profit_part(got_accept = False)
                    if sender_id != best_partner:
                        self.logger.info("%s Got FINAL1 from %s but best partner is %s", 
                                         self._prefix(), sender_id, best_partner)
                    else:
                        self._send_message(sender_id, MessageCoalitionOffer(
                            sender_id=self.vm_number, profit_parts=profit_parts ,status=FINAL2, 
                            other_members=self.prev_profit_parts, leader = None, 
                            coalition_size=self._my_coalition_size()))
                        self.coalition_leader = self._retrieve_guest_data(sender_id, "coalition_leader")
                        self.logger.info("%s Got FINAL1 from %s and updated leader to %s", 
                                         self._prefix(), sender_id, self.coalition_leader)
                        # Calculate the merged profit parts
                        self.profit_parts = self.calculate_profit_parts_in_joined_coalition(
                            self._retrieve_guest_data(sender_id, "profit_parts"), 
                            self.prev_profit_parts, 
                            message_content["other_members"])
                        if self.draw_per_partner:
                            self.chosen_ov = self._retrieve_guest_data(sender_id, "last_offered_value")
                            self.chosen_at = self._retrieve_guest_data(sender_id, "accept_threshold")
                        self.normalized_accept_threshold = self._retrieve_guest_data(sender_id, "normalized_accept_threshold")
                        self.normalized_offer_value = self._retrieve_guest_data(sender_id, "normalized_offer_value")
                        self.ov_or_at_effect = self._retrieve_guest_data(sender_id, "ov_or_at_effect")
                        self.update_other_members()
            finally:
                self.final_lock.release()

        elif status == FINAL2:
            # Only finalize the negotiation if the FINAL2 came from the guest we were waiting for.
            if sender_id == self.currently_waiting_for_final2:
                self.rejectTimeoutTimer.cancel()
                self.final_lock.acquire()
                try:
                    self.currently_waiting_for_final2 = None
                    self.coalition_leader = self._retrieve_guest_data(sender_id, "coalition_leader")
                    self.logger.info("%s Got FINAL2 from %s and updated leader to %s", 
                                     self._prefix(), sender_id, self.coalition_leader)
                    # Calculate the merged profit parts
                    self.profit_parts = self.calculate_profit_parts_in_joined_coalition(
                        self._retrieve_guest_data(sender_id, "profit_parts"), 
                        self.prev_profit_parts, 
                        message_content["other_members"])
                    self.logger.info("%s Chosen coalition: %s, leader: %s", 
                                     self._prefix(), self.profit_parts, self.coalition_leader)
                    if self.draw_per_partner:
                        self.chosen_ov = self._retrieve_guest_data(sender_id, "last_offered_value")
                        self.chosen_at = self._retrieve_guest_data(sender_id, "accept_threshold")
                    self.normalized_accept_threshold = self._retrieve_guest_data(sender_id, "normalized_accept_threshold")
                    self.normalized_offer_value = self._retrieve_guest_data(sender_id, "normalized_offer_value")
                    self.ov_or_at_effect = self._retrieve_guest_data(sender_id, "ov_or_at_effect")
                finally:
                    self.final_lock.release()
                # If necessary: update the other members of the coalition about the merged coalition and the leader.
                self.update_other_members()

        elif status == REJECT:
            self._update_guest_data(sender_id, "got_accept", False)
            self._update_guest_data(sender_id, "rejected_in_this_round", True)
            # If this guest was the preferred partner, we would need to select a new one.
            if sender_id == self.chosen_guest_active:
                self.chosen_guest_active = None
            if sender_id == self.chosen_guest_passive:
                self.chosen_guest_passive = None
            # If we were waiting for FINAL2 from the sender, we can stop waiting.
            if sender_id == self.currently_waiting_for_final2:
                self.rejectTimeoutTimer.cancel()
                self._stop_waiting_for_final2(sender_id, should_send_reject_to_guest = False)
            # Document that negotiations did not go well this time.
            ov = self._retrieve_guest_data(sender_id, "last_offered_value")
            at = self._retrieve_guest_data(sender_id, "accept_threshold")
            self.logger.info("CSV NEGPARAMS %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                             self.rnd_mem, self.vm_number, ov, at,
                             self._normalize_offer_value(ov, sender_id), self._normalize_accept_threshold(at, sender_id),
                             0, # bill
                             0, # bill without coalition
                             None, # was_leader
                             None, #leader
                             None # ov_or_at_effect
                             )
            return

        elif status == NOT_LEADER:
            # If we are not already talking to the leader of the other coalition, start talking.
            leader_id = int(message_content["leader"])
            # Save the partner's coalition size from the "not leader" message
            self._update_guest_data(sender_id, 'coalition_size', int(message_content['coalition_size']))
            # Reply with an offer to the leader, containing my own coalition size.
            ov, at = self._draw_negparams_for_partner(sender_id)
            # Calculate the offer value according to coalition sizes.
            offer_value = self._normalize_offer_value(ov, sender_id)
            if not self._existing_interaction(leader_id) and leader_id != self.vm_number and (offer_value < 1 or not self.guard_offers):
                self._send_message(leader_id, MessageCoalitionOffer(
                    sender_id=self.vm_number,
                    profit_parts={self.vm_number: 1 - offer_value, leader_id: offer_value},
                    status=OFFER, leader = None, other_members = {}, coalition_size=self._my_coalition_size()))
                # Note: saving non-normalized value
                self._update_guest_data(sender_id, 'last_offered_value', ov)
                self._update_guest_data(sender_id, 'accept_threshold', at)
                # Also save normalized offer value and accept threshold for statistics
                self._update_guest_data(sender_id, 'normalized_offer_value', offer_value)
                self._update_guest_data(sender_id, 'normalized_accept_threshold', self._normalize_accept_threshold(at, sender_id))



        elif status == UPDATE_MEMBERS:
            self.logger.info("%s Got UPDATE_MEMBERS from: %s, leader is %s.", 
                             self._prefix(), sender_id, self.coalition_leader)
            if sender_id == self.coalition_leader:
                self.logger.info("%s updating to leader: %s, profit_parts: %s",
                                 self._prefix(), message_content["leader"], message_content["profit_parts"])
                self.coalition_leader = message_content["leader"]
                self.profit_parts = message_content["profit_parts"]

        else:
            self.logger.warning("Illegal status in MessageCoalitionOffer, %s", message_content)

    def choose_leader(self, partner_id, proposer_id):
        partner_coalition_size = self._retrieve_guest_data(partner_id, 'coalition_size')
        my_coalition_size = self._my_coalition_size()
        if partner_coalition_size > my_coalition_size:
            self.logger.info("%s choosing %d as leader, partner coalition size: %d my coalition size: %d", 
                             self._prefix(),partner_id, partner_coalition_size, my_coalition_size)
            return partner_id
        if partner_coalition_size < my_coalition_size:
            self.logger.info("%s choosing %d as leader, partner coalition size: %d my coalition size: %d",
                             self._prefix(), self.vm_number, partner_coalition_size, my_coalition_size)
            return self.vm_number
        # Tie breaker: proposer of the offer
        self.logger.info("%s choosing %d as leader, partner coalition size: %d my coalition size: %d",
                         self._prefix(),proposer_id, partner_coalition_size, my_coalition_size)
        return proposer_id

    def update_other_members(self):
        """
        Update the members of a previous coalition about the merge into a new coalition.
        """
        self.logger.info("%s Updating other members that new leader is %s", 
                         self._prefix(), self.coalition_leader)
        for member_id in self.prev_profit_parts:
            if member_id != self.vm_number:
                self._send_message(member_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts=self.profit_parts,
                    status=UPDATE_MEMBERS, leader=self.coalition_leader, other_members={}, coalition_size=0))

    def calculate_profit_parts_in_joined_coalition(self, new_profit_parts, prev_profit_parts, other_members):
        """
        @param new_profit_parts - the result of the negotiation between two coalitions.
        @param prev_profit_parts - the previous coalition.
        @param other_members - the other coalition.
        """
        merged_profit_parts = {}
        if not prev_profit_parts: # I was not part of a coalition
            prev_profit_parts = {self.vm_number: 1}
        if not other_members: # My partner was not part of a coalition
            partner_id = [guest_id for guest_id in new_profit_parts.keys() if guest_id != self.vm_number][0]
            other_members = {partner_id: 1}
        my_coalition_part = new_profit_parts[self.vm_number]

        if my_coalition_part == SHAPLEY:
            for member in prev_profit_parts.keys():
                merged_profit_parts[member] = SHAPLEY
            for member in other_members.keys():
                merged_profit_parts[member] = SHAPLEY
            self.logger.info("%s Merged coalition: %s, new_profit_parts: %s, prev_profit_parts: %s, other_members: %s",
                         self._prefix(), merged_profit_parts, new_profit_parts, prev_profit_parts, other_members)
            return merged_profit_parts

        other_coalition_part = 1-my_coalition_part
        for member,member_part in prev_profit_parts.iteritems():
            merged_profit_parts[member] = my_coalition_part * member_part
        for member, member_part in other_members.iteritems():
            merged_profit_parts[member] = other_coalition_part * member_part
        self.logger.info("%s Merged coalition: %s, new_profit_parts: %s, prev_profit_parts: %s, other_members: %s",
                         self._prefix(), merged_profit_parts, new_profit_parts, prev_profit_parts, other_members)
        return merged_profit_parts

    def update_params_from_host(self, message_content, allocated_base_mem, load):
        """
        Starting point for a negotiations round. 
        This method is invoked when a MessageNotifyMem is sent by the host.
        """
        self.logger.info("update_params_from_host called for round %s, prev_profit_parts: %s", self.rnd_mem, self.prev_profit_parts)
        state = [load, allocated_base_mem + float(message_content["not_mem"])]
        state_at_base = [load, allocated_base_mem]
        if self.hist_mem.len() > 0:
            requested_mem = self.hist_mem[0].bid_rq[0][1]
        else:
            self.logger.error("requested_mem was not extracted from hist: %s" % self.hist_mem)
        # See CSV titles in the init method.
        bid_p = self.hist_mem[0].bid_p if self.hist_mem.len()>0 else 0
        self.logger.info("CSV %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",
                          self.rnd_mem, self.vm_number, load, allocated_base_mem, message_content["not_mem"], 
                          self.V_func(self.perf(state))-self.V_func(self.perf(state_at_base)), message_content["not_bill_mem"], 
                          message_content["bill_without_coalition"], requested_mem, self.chosen_ov, self.chosen_at,
                          bid_p)
        # if self.is_first_round_mem() or not self.enable_negotiations:
        if self.rnd_mem < 1 or not self.enable_negotiations or self.do_not_negotiate_on_round == self.rnd_mem:
            self.coalition_history.add(CoalitionHistoryItem(
                 self.rnd_mem, {}, message_content["not_mem"]-self.base_mem-self.alloc_diff,
                 message_content["not_bill_mem"], message_content["bill_without_coalition"]))
            self.logger.info("Adding coalition history item: %s" % self.coalition_history[0])
            return

        # decrease rounds until next iteration for all potential partners.
        for guest in self.guest_data.iterkeys():
            rounds_until_next_interaction = self._retrieve_guest_data(guest, "rounds_until_next_interaction")
            self._update_guest_data(guest, "rounds_until_next_interaction", max(0, rounds_until_next_interaction - 1))

        # Update the coalition members according to the host's message.
        if message_content.get("coalition_members") is not None:
            # coalition_members keys are in the form "vm-i" and profit_parts keys are indexes.
            coalition_members = message_content.get("coalition_members")
            profit_parts_from_host = {}
            for guest in coalition_members:
                profit_parts_from_host[int(guest.split('-')[1])] = coalition_members[guest]
            self.logger.info("%s Profit parts before update from host: %s",
                             self._prefix(), self.profit_parts)
            # If a guest was removed from the coalition by the host, or left the coalition, we should remember it. 
            for guest_id in self.profit_parts:
                if guest_id == self.vm_number:
                    continue
                if guest_id not in profit_parts_from_host:
                    self._update_guest_data(guest_id, "removed_by_host_or_left_in_round", self.rnd_mem)
                    self._update_guest_data(guest_id, "removed_by_host_or_left_count", 
                                            self._retrieve_guest_data(guest_id, "removed_by_host_or_left_count") + 1)
                    rejection_backoff = self._retrieve_guest_data(guest_id, "rejection_backoff")
                    self._update_guest_data(guest_id, "rounds_until_next_interaction", rejection_backoff)
                    self._update_guest_data(guest_id, "rejection_backoff", rejection_backoff*self.backoff_multiplier)
                    self.logger.info("%s Increased rejection backoff for guest %s. new rejection backoff: %s",
                                     self._prefix(), guest_id, rejection_backoff*self.backoff_multiplier)
                else:
                    # This was a successful interaction with this guest. Reset the backoff parameters.
                    self._update_guest_data(guest_id, "rounds_until_next_interaction", 0)
                    self._update_guest_data(guest_id, "rejection_backoff", 1)
                    self.logger.info("%s Reset rejection backoff for guest %s to %s",
                                     self._prefix(), guest_id, 1)
            self.profit_parts = profit_parts_from_host
            # Handle the case of a leader leaving the coalition/removed by the host.
            # The coalition is gone and we should start everything from scratch.
            if not self.profit_parts or self.coalition_leader not in self.profit_parts:
                self.coalition_leader = None
                self.profit_parts = {}
                self.coalition_q = None
                self.logger.info("%s setting coalition_q to None in update_params_from_host", self._prefix())
                was_leader = None
                leader = self.vm_number
            else:
                # We are in a coalition: output the normalized negotiation params, and who is the leader.
                was_leader = (self.prev_coalition_leader == self.vm_number or self.prev_coalition_leader is None)
                leader = self.coalition_leader if self.coalition_leader is not None else self.vm_number
                if self.coalition_q is None:
                    self.coalition_q = self.last_requested
                    self.logger.info("%s setting coalition_q to %s in update_params_from_host", self._prefix(), self.last_requested)

            # Titles for NEGPARAMS are in SW_graph_generator.
            self.logger.info("CSV NEGPARAMS %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                             self.rnd_mem, self.vm_number, self.chosen_ov, self.chosen_at,
                             self.normalized_offer_value, self.normalized_accept_threshold,
                             message_content["not_bill_mem"], message_content["bill_without_coalition"], was_leader, leader, self.ov_or_at_effect)
            self.logger.info("CSV PBELIEF %s,%s,%s,%s",
                             self.rnd_mem, self.vm_number, 
                             (self.prev_profit_parts != {}), (self.profit_parts=={}))
            self.prev_coalition_leader = self.coalition_leader
            self.prev_profit_parts = deepcopy(self.profit_parts)
            self.logger.info("%s Profit parts after update from host: %s", 
                             self._prefix(), self.profit_parts)

        self.coalition_history.add(CoalitionHistoryItem(
             self.rnd_mem, self.profit_parts, message_content["not_mem"]-self.base_mem-self.alloc_diff, 
             message_content["not_bill_mem"], message_content["bill_without_coalition"]))
        self.logger.info("Adding coalition history item: %s" % self.coalition_history[0])
        self._print_backoffs()

        # First answer messages from the queue, and then start new interactions 
        # only with those that we didn't talk to already in this round.
        while self._get_message_queue_length() > 0:
            message_content = self._get_first_message_in_queue()
            self.process_coalition_offer(message_content)

        # Start new interactions.
        guests_to_negotiate_with = self._get_guests_to_negotiate_with()

        if guests_to_negotiate_with:
            Timer(self.END_OF_NEGOTIATIONS_ROUND, self._send_final_message_for_best_deal, ()).start()
            self.prev_coalition_leader = self.coalition_leader
            self.prev_profit_parts = deepcopy(self.profit_parts)
            self.coalition_leader = None
            self.profit_parts = {}

        guests_to_start_talking_to = {}
        for guest_id, guest_comm in guests_to_negotiate_with.iteritems():
            if self._existing_interaction(guest_id):
                self.logger.info("%s removing %s from guests_to_start_talking_to. Already interacted in this round.",
                                 self._prefix(), guest_id)
                continue
            guests_to_start_talking_to[guest_id] = guest_comm

        self.logger.info("%s guests_to_negotiate_with: %s", self._prefix(), guests_to_negotiate_with.keys())
        self.logger.info("%s guests_to_start_talking_to: %s", self._prefix(), guests_to_start_talking_to.keys())

        for destination_id, guest_comm in guests_to_start_talking_to.iteritems():
            self._send_message(destination_id, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts={},
                    status=COALITION_SIZE, leader = None, other_members = {}, 
                    coalition_size=self._my_coalition_size()))
            self._update_guest_data(destination_id, "sent_message_coalition_offer", True)

        # re-check the queue for messages
        while self._get_message_queue_length() > 0:
            message_content = self._get_first_message_in_queue()
            self.process_coalition_offer(message_content)

        # Update the state - processing the data from the host is finished.
        self._access_got_notify(new_value=True)

    def _print_backoffs(self):
        b = {}
        r = {}
        for guest in range(1, self.num_of_guests + 1):
            b[guest] = self._retrieve_guest_data(guest, 'rejection_backoff')
            r[guest] = self._retrieve_guest_data(guest, 'rounds_until_next_interaction')
        self.logger.info("round: %s, backoffs: %s, rounds until next interaction: %s", self.rnd_mem, b, r)

    def _my_coalition_size(self):
        return max(1, len(self.prev_profit_parts.keys()))

    def _normalize_offer_value(self, orig_value, partner_id):
        if orig_value == SHAPLEY:
            return SHAPLEY
        their_size = self._retrieve_guest_data(partner_id, 'coalition_size')
        my_size = self._my_coalition_size()
        normalized_value = orig_value * 2 * their_size / (their_size + my_size)
        self.logger.info("%s %s's coalitions size: %s, my coalition size: %s, normalized_offer_value: %s",
                         self._prefix(), partner_id, their_size, my_size, normalized_value)
        return normalized_value

    def _normalize_accept_threshold(self, orig_value, partner_id):
        if orig_value == SHAPLEY:
            return SHAPLEY
        their_size = self._retrieve_guest_data(partner_id, 'coalition_size')
        my_size = self._my_coalition_size()
        normalized_value = orig_value * 2 * my_size / (their_size + my_size)
        self.logger.info("%s %s's coalitions size: %s, my coalition size: %s, normalized_accept_threshold: %s",
                         self._prefix(), partner_id, their_size, my_size, normalized_value)
        return normalized_value

    def _draw_negparams_for_partner(self, partner_id):
        if not self.draw_per_partner:
            return self.offer_value, self.accept_threshold
        their_size = self._retrieve_guest_data(partner_id, 'coalition_size')
        my_size = self._my_coalition_size()
        upper_bound_ov = 0.5*(their_size+my_size)/their_size
        ov = random.uniform(0, upper_bound_ov)
        return ov, 1-ov

    def _normalized_to_orig_value(self, normalized_value, partner_id):
        their_size = self._retrieve_guest_data(partner_id, 'coalition_size')
        my_size = self._my_coalition_size()
        orig_value = normalized_value * 0.5 * (their_size + my_size) / my_size
        self.logger.info("%s %s's coalitions size: %s, my coalition size: %s, de-normalized_offer_value: %s",
                         self._prefix(), partner_id, their_size, my_size, orig_value)
        return orig_value

    def _get_guests_to_negotiate_with(self):
        guests_to_negotiate_with = {}
        if self.coalition_leader is None and not self.profit_parts:
            # We are not in any coalition. We should talk to everyone.
            guests_to_negotiate_with = self.guest_communicators
        elif self.coalition_leader != self.vm_number:
            # We are in a coalition but we are not the leader.
            guests_to_negotiate_with = {}
        else:
            # We are the leader of a coalition. Find the guests who are not in our coalition.
            guests_to_negotiate_with = {}
            for guest_id, guest_comm in self.guest_communicators.iteritems():
                if guest_id not in self.profit_parts:
                    guests_to_negotiate_with[guest_id] = guest_comm
        # Filter out guests with backoff.
        partners = {}
        for guest_id, guest_comm in guests_to_negotiate_with.iteritems():
            rounds = self._retrieve_guest_data(guest_id, "rounds_until_next_interaction")
            if rounds > 0:
                self.logger.info("%s removing %s from guests_to_negotiate_with. %s rounds to go until interaction with this guest.",
                                 self._prefix(), guest_id, rounds)
                continue
            partners[guest_id] = guest_comm
        return partners

    def _get_best_partner_by_profit_part(self, got_accept):
        """
        @param got_accept - boolean. If true, the best partner will be chosen from the guests that we got ACCEPT from.
        Otherwise, the best partner will be chosen from the guests we did not get ACCEPT from (because we sent them ACCEPT).
        """
        if got_accept:
            if self.chosen_guest_active:
                return self.chosen_guest_active
            # Consider guests that we got an ACCEPT message from, and did not reject us or were not rejected by us.
            potential_partners = [guest for guest in self.guest_data.iterkeys() if
                                  self._retrieve_guest_data(guest, "profit_parts") and
                                  self._retrieve_guest_data(guest, "got_accept") and
                                  not self._retrieve_guest_data(guest, "rejected_in_this_round") and 
                                  (not self.shapley or self._retrieve_guest_data(guest, "profit_parts")[self.vm_number]==SHAPLEY)]
        else:
            if self.chosen_guest_passive:
                return self.chosen_guest_passive
            # Consider guests that we sent an ACCEPT message to, and did not reject us or were not rejected by us.
            potential_partners = [guest for guest in self.guest_data.iterkeys() if
                                  self._retrieve_guest_data(guest, "profit_parts") and
                                  self._retrieve_guest_data(guest, "sent_accept") and
                                  not self._retrieve_guest_data(guest, "rejected_in_this_round") and
                                  (not self.shapley or self._retrieve_guest_data(guest, "profit_parts")[self.vm_number]==SHAPLEY)]

        if self.shapley:
            weights = [1 for partner in potential_partners]
        else:
            weights = [self._retrieve_guest_data(guest, "profit_parts")[self.vm_number]*
                (1 - self._retrieve_guest_data(guest, "removed_by_host_or_left_count")*1.0/self.rnd_mem)*
                (1 - self._retrieve_guest_data(guest, "removed_by_host_or_left_in_round")*1.0/self.rnd_mem)
                for guest in potential_partners]
        chosen_guest = self._choose_by_weights(potential_partners, weights)
        if got_accept:
            self.chosen_guest_active = chosen_guest
        else:
            self.chosen_guest_passive = chosen_guest
        return chosen_guest

    def _send_final_message_for_best_deal(self):
        chosen_guest = self._get_best_partner_by_profit_part(got_accept = True)
        if chosen_guest is None:
            return
        chosen_profit_parts = self._retrieve_guest_data(chosen_guest, "profit_parts")
        self.logger.info("%s Sending final message for chosen guest: %s, chosen profit_parts: %s",
                         self._prefix(), chosen_guest, chosen_profit_parts)
        self.final_lock.acquire()
        try:
            if self.coalition_leader is None and not self.profit_parts:
                self._send_message(chosen_guest, MessageCoalitionOffer(
                    sender_id=self.vm_number, profit_parts=chosen_profit_parts, status=FINAL1, 
                    other_members=self.prev_profit_parts, leader = None, coalition_size=self._my_coalition_size()))
                self.currently_waiting_for_final2 = chosen_guest
                self.rejectTimeoutTimer = Timer(self._get_final1_timeout(), self._reject_after_timeout,
                                                [chosen_guest])
                self.rejectTimeoutTimer.start()
        finally:
            self.final_lock.release()

    def _get_final1_timeout(self):
        return max(0.1, 1.0 / (self.num_of_guests - 1))

    def _reject_after_timeout(self, guest_id):
        self._stop_waiting_for_final2(guest_id, should_send_reject_to_guest = True)

    def _stop_waiting_for_final2(self, guest_id, should_send_reject_to_guest):
        """This will be called if the there was a timeout while waiting for FINAL2
           or we got a reject from the guest we were waiting for.
           If this is the timeout case, should_send_reject_to_guest will be True.  
        """
        sent_final2_to_someone = False
        self.final_lock.acquire()
        try:
            self.currently_waiting_for_final2 = None
            if should_send_reject_to_guest:
                self._send_reject_to_guest_id(guest_id)
            # If the best partner has sent us FINAL1 and has not timed out yet, we should answer him.
            best_partner_to_reply_to = self._get_best_partner_by_profit_part(got_accept = False)
            self.logger.info("%s Finished waiting for final2, best_partner_to_reply_to: %s, finalists: %s",
                             self._prefix(), best_partner_to_reply_to, self.finalists)
            if best_partner_to_reply_to in self.finalists and not self._retrieve_guest_data(best_partner_to_reply_to, "rejected_in_this_round"):
                profit_parts = self._retrieve_guest_data(best_partner_to_reply_to, "profit_parts")
                self._send_message(best_partner_to_reply_to, MessageCoalitionOffer(
                                   sender_id=self.vm_number, profit_parts=profit_parts, status=FINAL2,
                                   other_members=self.prev_profit_parts, leader = None, 
                                   coalition_size=self._my_coalition_size()))
                self.coalition_leader = self._retrieve_guest_data(best_partner_to_reply_to, "coalition_leader")
                self.profit_parts = self.calculate_profit_parts_in_joined_coalition(
                    self._retrieve_guest_data(best_partner_to_reply_to, "profit_parts"),
                    self.prev_profit_parts,
                    self._retrieve_guest_data(best_partner_to_reply_to, "other_members"))
                self.normalized_accept_threshold = self._retrieve_guest_data(best_partner_to_reply_to, "normalized_accept_threshold")
                self.normalized_offer_value = self._retrieve_guest_data(best_partner_to_reply_to, "normalized_offer_value")
                self.update_other_members()
                sent_final2_to_someone = True
        finally:
            self.final_lock.release()
        # None of the finalists were good enough for a deal - send a FINAL1 to the next best partner.
        if not sent_final2_to_someone:
            self._send_final_message_for_best_deal()

    def _send_reject_to_guest_id(self, guest_id):
        if self.chosen_guest_active == guest_id:
            self.chosen_guest_active = None
        if self.chosen_guest_passive == guest_id:
            self.chosen_guest_passive = None
        self._send_message(guest_id, MessageCoalitionOffer(
            sender_id=self.vm_number, profit_parts={}, status=REJECT, leader=None, other_members={}, coalition_size=0))
        self._update_guest_data(guest_id, "rejected_in_this_round", True)

    def _send_message(self, destination_id, message):
        """
        @param destination_id - the number of the guest to send the message to.
        @param message - the message to send.
        """
        try:
            self.guest_communicators[destination_id].communicate(message)
            self.logger.info("%s From %s To %s Message %s", self._prefix(), self.vm_number, destination_id, message)
        except Exception as err:
            self.logger.info('Error communicating in vm-%d: %s', self.vm_number, err)
            self._print_exception_message()

    def _prefix(self):
        """return a prefix to be used when logging for visualizing negotiations"""
        return "VISNEG %s %s" % (self.vm_number, self.rnd_mem)

    def _choose_by_weights(self, items, weights):
        """
        @param items - a list of items to choose from
        @param weights - a list of weights, the same size as "items"
        """
        if sum(weights) == 0:
            weights = [1 for item in items]
        normalized_weights = [w*1.0/sum(weights) for w in weights]
        # Get a random number between [0,1).
        r = random.random()
        weight_sum = 0
        for i in range(len(normalized_weights)):
            weight_sum += normalized_weights[i]
            if r <= weight_sum:
                return items[i]

    def _update_alpha(self, value):
        """Only update once in every round."""
        # A negative value or a value larger than 1 means the learning needs to restart.
        if value < 0 or value > 1:
            self.logger.debug("Updating alpha from %s to 0.5" % value)
            value = 0.5
        if self.rnd_mem not in self.alphas:
            self.alphas[self.rnd_mem] = value
            self.alpha = value

    def _existing_interaction(self, guest_id):
       return (self._retrieve_guest_data(guest_id, "interacted_in_this_round") 
               or self._retrieve_guest_data(guest_id, "got_message_in_queue_this_round")
               or self._retrieve_guest_data(guest_id, "sent_message_coalition_offer"))

    def _update_guest_data(self, guest_id, key, value):
        """
        A helper function to update the guest_data dictionary. If the key does not exist, this method creates it and
        assigns the value to it. If the key already exists, then it just updates the value.
        """
        self.guest_data_lock.acquire()
        try:
            if guest_id in self.guest_data.keys():
                self.guest_data[guest_id][key] = value
            else:
                self.guest_data[guest_id] = {key: value}
        finally:
            self.guest_data_lock.release()

    def _retrieve_guest_data(self, guest_id, key):
        """
        A helper function to retrieve the value corresponding to the given guest_id and key from the guest_data
        dictionary. If the data does not exist, None is returned.
        """
        ret = None
        self.guest_data_lock.acquire()
        try:
            if guest_id in self.guest_data.keys():
                if key in self.guest_data[guest_id].keys():
                    ret = self.guest_data[guest_id][key]
        finally:
            self.guest_data_lock.release()

        return ret

    def _access_got_notify(self, new_value = None):
        """read/write got_notify_message_this_round safely.
           write is done iff new_value is specified. In that case returns None.
           in case of read, return the current value of the variable.
        """
        ret = None
        self.notify_lock.acquire()
        try:
            if new_value is not None:
                self.got_notify_message_this_round = new_value
                self.logger.info("%s updated got_notify_message_this_round to: %s", self._prefix(), new_value)
            else:
                ret = self.got_notify_message_this_round
                self.logger.info("%s returning got_notify_message_this_round: %s", self._prefix(), ret)
        finally:
            self.notify_lock.release()
        return ret

    def _add_message_to_queue(self, message_content):
        self.notify_lock.acquire()
        try:
            self.message_queue.append(message_content)
            self.logger.info("%s Adding message to queue. Sender: %s Status: %s", 
                             self._prefix(), message_content["sender_id"], message_content["status"])
        finally:
            self.notify_lock.release()

    def _get_first_message_in_queue(self):
        message_content = None
        self.notify_lock.acquire()
        try:
            message_content = self.message_queue.pop(0)
            self.logger.info("%s First message in queue. Sender: %s Status: %s",
                             self._prefix(), message_content["sender_id"], message_content["status"])
        finally:
            self.notify_lock.release()
        return message_content

    def _get_message_queue_length(self):
        ret = None
        self.notify_lock.acquire()
        try:
            ret = len(self.message_queue)
            self.logger.info("%s Message queue length is %s.", self._prefix(), ret)
        finally:
            self.notify_lock.release()
        return ret

    def _empty_message_queue(self):
        self.notify_lock.acquire()
        try:
            self.messge_queue = []
            self.logger.info("%s Emptied message queue.", self._prefix())
        finally:
            self.notify_lock.release()


    def _estimate_bill(self, q0, with_coalitions=False):
        weight_rnd = lambda r: 1. / 2 ** (self.rnd_mem - r)
        weight_q = lambda q: float(q0) / q if q != 0 else 0.
        weight = lambda r, q: weight_rnd(r) * weight_q(q)
        if with_coalitions:
            bills = [h.bill for h in self.coalition_history.history]
            weights = [weight(h.round, h.won_memory) for h in self.coalition_history.history]
        else:
            bills = [b.bill for b in self.bills]
            weights = [weight(b.round, b.q) for b in self.bills]
        return self._average(bills, weights=weights)

    def _estimate_point(self, q_prev, bill, p_in_min, p_out_max, q, p, with_coalitions=True): # TODO: change back to False
        """
        returns: lower_bound, upper_bound, estimated_bill.
        Return average bill_per_unit multiplied by the given q. 
        The average is weighted by the age of the data.
        """
        # Get the estimations for a bill without any coalitions.
        lb, ub, est = AdviserProfitEstimatorAge._estimate_point(self, q_prev, bill, p_in_min, p_out_max, q, p)
        # TODO: disable the fallback in case of no coalitions.
        if not with_coalitions:
            return (lb, ub, est)
        # Calculate a weighted average over actual bills.
        unit_bills = [(h.bill/h.won_memory)*1.0/(2**i) for (i,h) in enumerate(self.coalition_history.history) if h.won_memory > 0]
        if not unit_bills:
            # The guest has never received any memory.
            return (lb, ub, est)
        weights_sum = sum([1.0/(2**i) for i in range(self.coalition_history.size())])
        if weights_sum == 0:
            weights_sum = 1.0
        avg_unit_bill = sum(unit_bills)/weights_sum
        estimated_bill = avg_unit_bill*q
        if estimated_bill < 0 and self.very_careful:
            return (lb, ub, est)
        return (estimated_bill, ub, estimated_bill)

    ###################### Fear of Coalition Break Logic #########################
    def _prob_of_coalition_break(self, q_current, q_new):
        if q_current >= q_new:
            return 0
        else:
            return 0.25

    def _estimated_num_rounds_until_coalition(self):
        # TODO: this can be better. Like, log(coalition_size), or based on history of creating the existing coalition.
        return 5

    def _num_future_rounds(self):
        """The number of rounds left to participate in the auction."""
        #TODO: could be changed to return the number of rounds in which the valuation and load are not expected to change.
        return 10

    def _profit_delta_for_next_rounds(self, q, q_prev, p_in_min, p_out_max, p, state):
        delta_V = (self.V_func(self.perf((state[0], state[1]+q, 0)))
                   - self.V_func(self.perf((state[0], state[1]+q_prev, 0))))
        bill_wo_coalition_for_q_prev = self._estimate_bill(q_prev, with_coalitions=False)
        if self.coalition_history.size() > 0:
            bill_for_q_prev = self._estimate_bill(q_prev, with_coalitions=True)
            #self.coalition_history[0].bill
            #bill_wo_coalition_for_q_prev = self.estimate_bill(q_prev, with_coalitions=True)
            #self.coalition_history[0].bill_without_coalition
        else: 
            bill_for_q_prev = 0
            #bill_wo_coalition_for_q_prev = 0
        # If I'm not in a coalition, estimate the bill based on old bills (without coalition) and bounds.
        if not self.prev_profit_parts: 
            # Using bill_wo_coalition_for_q_prev for a more accurate estimate.
            delta_bill = (self._estimate_point(q_prev, bill_wo_coalition_for_q_prev, p_in_min, p_out_max, q, p, with_coalitions=False)[2]
                          - self._estimate_point(q_prev, bill_wo_coalition_for_q_prev, p_in_min, p_out_max, q_prev, p, with_coalitions=False)[2])
            profit_delta = self._num_future_rounds() * (delta_V - delta_bill)
            self.logger.info("VISPROFIT round %i: %s",self.rnd_mem, (q, delta_V*self._num_future_rounds(),
                             delta_bill*self._num_future_rounds(), profit_delta, q_prev))
            return profit_delta
        # If I am in a coalition but not afraid that it will break, assume my bill will be based on actual recent bills.
        if q <= q_prev or not self.fear_coalition_break:
            delta_bill = (self._estimate_point(q_prev, bill_for_q_prev, p_in_min, p_out_max, q, p, with_coalitions=True)[2]
                          - self._estimate_point(q_prev, bill_for_q_prev, p_in_min, p_out_max, q_prev, p, with_coalitions=True)[2])
            profit_delta = self._num_future_rounds() * (delta_V - delta_bill)
            self.logger.info("VISPROFIT round %i: %s",self.rnd_mem, (q, delta_V*self._num_future_rounds(), 
                             delta_bill*self._num_future_rounds(), profit_delta, q_prev))
            return profit_delta
        # Otherwise, assume the coalition will break with some probability and we will pay the full bill.
        #pr = self._prob_of_coalition_break(q_prev, q)

        # Otherwise, I have to leave the coalition (or the host will make me leave)
        pr = 1

        bill_estimate_q_with_coalition = \
            self._estimate_point(q_prev, bill_for_q_prev, p_in_min, p_out_max, q, p, with_coalitions=True)[2]
        bill_estimate_q_without_coalition = \
            self._estimate_point(q_prev, bill_wo_coalition_for_q_prev, p_in_min, p_out_max, q, p, with_coalitions=False)[2]
        bill_estimate_q = (pr * bill_estimate_q_without_coalition 
                           + (1-pr) * bill_estimate_q_with_coalition)
        bill_estimate_q_prev = self._estimate_point(q_prev, bill_for_q_prev, p_in_min, p_out_max, q_prev, p, with_coalitions=True)[2]

        profit_delta = 0
        rounds_without_coalition = min(self._num_future_rounds(), self._estimated_num_rounds_until_coalition())
        profit_delta += (rounds_without_coalition * (delta_V- (bill_estimate_q - bill_estimate_q_prev)))

        rounds_after_coalition = max(0, self._num_future_rounds() - self._estimated_num_rounds_until_coalition())
        profit_delta += (rounds_after_coalition
                         *(delta_V-(bill_estimate_q_with_coalition - bill_estimate_q_prev)))
        delta_bill = (rounds_without_coalition * (bill_estimate_q - bill_estimate_q_prev)
                      + rounds_after_coalition * (bill_estimate_q_with_coalition - bill_estimate_q_prev))
        self.logger.info("VISPROFIT round %i: %s",self.rnd_mem, (q, delta_V*self._num_future_rounds(), delta_bill, profit_delta, q_prev))
        return profit_delta

    def _profit_delta_for_next_rounds_v2(self, q, q_prev, p_in_min, p_out_max, p, state):
        delta_V = (self.V_func(self.perf((state[0], state[1]+q, 0)))
                   - self.V_func(self.perf((state[0], state[1]+q_prev, 0))))
        bill_avg_for_q_prev = self._estimate_bill(q_prev, with_coalitions=False)
        bill_estimate_for_q_prev = self._estimate_point(q_prev, bill_avg_for_q_prev, p_in_min, p_out_max, q_prev, p, with_coalitions=False)[2]
        bill_estimate_for_q = self._estimate_point(q_prev, bill_avg_for_q_prev, p_in_min, p_out_max, q, p, with_coalitions=False)[2]
        delta_bill = bill_estimate_for_q - bill_estimate_for_q_prev
        discount_q_prev = 0
        discount_q = 0

        my_profit_part = 0
        if self.prev_profit_parts:
            my_profit_part = self.prev_profit_parts[self.vm_number]

        #for alt_profit_part in [0.2, 0.5, 0.8, my_profit_part]:
        for alt_profit_part in [my_profit_part]:

            if self.prev_profit_parts: # I'm in a coalition
                ##### testing a simple function #####
                exp_discount = self._expected_discount_simple(self.prev_profit_parts)
                discount_q_prev = exp_discount * self._num_future_rounds()
                if q <= q_prev or not self.fear_coalition_break:
                    discount_q = exp_discount * self._num_future_rounds()
                else: # assume the host will take me out of the coalition for sure for increasing q
                    rounds_after_coalition = max(0, self._num_future_rounds() - self._estimated_num_rounds_until_coalition())
                    discount_q = exp_discount * rounds_after_coalition

                ######## end of test, old logic: ######
                #expected_discount_per_unit = self._expected_discount(self.prev_profit_parts, alt_profit_part)
                #discount_q_prev = expected_discount_per_unit * q_prev * self._num_future_rounds()
                #if q <= q_prev or not self.fear_coalition_break:
                #    discount_q = expected_discount_per_unit * q * self._num_future_rounds()
                #else: # assume the host will take me out of the coalition for sure for increasing q
                #    rounds_after_coalition = max(0, self._num_future_rounds() - self._estimated_num_rounds_until_coalition())
                #    discount_q = expected_discount_per_unit * q * rounds_after_coalition

            discount_delta = discount_q - discount_q_prev
            delta_profit = (delta_V - delta_bill)*self._num_future_rounds() + discount_delta
            
            self.logger.info("VISSIM round %i: %s",
                             self.rnd_mem, (q, delta_V*self._num_future_rounds(), 
                                            delta_bill*self._num_future_rounds()-discount_delta, 
                                            delta_profit, q_prev, alt_profit_part))
        return delta_profit


    def _expected_discount(self, coalition_members, alt_profit_part = None):
        # 1. get coalition history with close members
        discounts = []
        weights = []
        if self.coalition_history.size() == 0:
            return 0
        for (i,h) in enumerate(self.coalition_history.history):
            if not h.coalition:
                continue
            discount = h.bill_without_coalition - h.bill
            coalition_discount = discount / h.coalition[self.vm_number]
            if h.won_memory > 0:
                per_unit = coalition_discount / h.won_memory
            else:
                per_unit = coalition_discount

            # Weights are based on coalition similarity. Time is not a factor in it, currently.
            #if h.coalition.keys() == coalition_members.keys():
            #    weight = 1
            #else:
            #    intersection, union = AdviserCoalitions._intersect_union(h.coalition.keys(), coalition_members.keys())
                # Even a coalition with none of the current members can help predict the discount.
            #    weight = max(len(intersection)*1.0/len(union), 0.1)
            # TODO: remove. (This is done to be as close as possible to previous bill estimation.
            # Weighting according to time.
            weight = 1.0/(2**i)

            discounts.append(per_unit)
            #discounts.append(coalition_discount)
            weights.append(weight)
        # 2. calculate discount per memory unit for the entire coalition
        if not discounts: # I've never been in a coalition
            return 0
        avg_discount = np.average(discounts, weights = weights)
        # 3. calculate the expected discount for me by my profit part
        if alt_profit_part is not None:
            my_profit_part = alt_profit_part
        else:
            my_profit_part = coalition_members[self.vm_number]
        return avg_discount * my_profit_part
       
    def _expected_discount_simple(self, coalition_members):
        discounts = []
        weights = []
        if self.coalition_history.size() == 0:
            return 0
        for (i,h) in enumerate(self.coalition_history.history):
            if not h.coalition:
                continue
            discount = h.bill_without_coalition - h.bill
            discounts.append(discount)
            weights.append(1.0/2**i)
        if not discounts: # I've never been in a coalition
            return 0
        avg_discount = np.average(discounts, weights = weights)
        return avg_discount

    def _run_utility_prediction_on_qs_and_ps(self, qs, ps, state):
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self._insert_bill(Bill(last.round, last.won_mem - last.base_mem, last.bill))

        if len(self.bills) > 0:
            q_prev = self.bills[0].q
            bill_prev = self.bills[0].bill
        else:
            q_prev = 0
            bill_prev = 0

        if self.ub == self.lb:
            self._update_alpha(0.5)
        else:
            self._update_alpha((bill_prev-self.lb)/(self.ub - self.lb))
        self.logger.info("AdviserCoalitions: DamnBug round %i alpha: %s, ub: %s, lb: %s, prev_bill: %s, got_notify: %s",
                         self.rnd_mem, self.alpha, self.ub, self.lb, bill_prev, self._access_got_notify())

        p_in_min = self.p_in_min_hist_mem.average()
        p_out_max = self.p_out_max_hist_mem.average()
        self._clear_old()
        debug = list(map(lambda q, p: self._profit_delta_for_next_rounds_v2(q, q_prev, p_in_min,
            p_out_max, p, state), qs, ps))
        ret = list(map(lambda q, p: self._profit_delta_for_next_rounds(q, q_prev, p_in_min,
            p_out_max, p, state), qs, ps))
        return debug


    def do_advice_mem(self, state, auction_mem):
        """
        Overriding this method to take several future rounds into consideration.
        :param state: is defined as: state = [load, base_mem, base_bw]
        :returns: p, [(r1, q1), (r2, q2), ...]
        """
        self.logger.debug("This is do_advice_mem by AdviserCoalitions")

        advice_from_parent = AdviserProfitEstimatorAge.do_advice_mem(self, state, auction_mem)
        if not self.enable_negotiations:
            return advice_from_parent

        def_bid = 0.0, [(0, auction_mem)]
        if auction_mem == 0:
            return def_bid
        # Fill in details from history
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self.p_in_min_hist_mem.add(last.p_in_min)
                self.p_out_max_hist_mem.add(last.p_out_max)
                self.won_p_hist_mem.add(last.won_p)
                assert last.bid_p >= last.won_p - p_eps
            else:
                self.logger.warn("do_advice_mem: No data on last bid results")
        # Valuation of the current state.
        V0 = self.V_func(self.perf(state))
        # create an array of possible q values.
        q = np.asarray(range(self.d_mem, auction_mem, self.d_mem) + [auction_mem])
        # Valuation for each q value.
        V = np.asarray(self.V_func(self.perf([state[0], state[1] + q, 0])))
        # Boolean array, holds True in index i iff the valuation at the i'th 
        # q value is better than getting base mem.
        good_V = V > V0
        # If there is no positive valuation, return default bid.
        # TODO: what is this try .. except? why would there be a TypeError?
        try:
            if not any(good_V):
                return def_bid
        except TypeError:
            return def_bid
        # Allow only valuations higher than V0.
        if not all(good_V):
            V = V[good_V]
            q = q[good_V]
        # p value is the slope of the revenue-memory graph from the (mem0,V0) point
        # in 3D(bandwidth) this is OK while the other dimension is constant
        with np.errstate(divide = 'ignore', invalid = "ignore"):
            p = (V - V0) / q
        p[np.isinf(p) + np.isnan(p)] = 0
        assert all(p >= 0)
        self.logger.debug("round %i p: %s", self.rnd_mem, str(p))
        p_copy = np.copy(p)  # save the p values for later rq list generation

        
        future_utility = self._run_utility_prediction_on_qs_and_ps(q, p, state)
        U = np.asarray(future_utility)
        if len(self.bills) > 0:
            q_prev = self.bills[0].q
        else:
            q_prev = 0

        # est_bill is used only for debugging purposes.
        # TODO: this may have a bad side effect!
        est = self.get_estimated_bill_mem(q, p)
        est_bill = [t[2] for t in est]
        lbs = np.asarray([t[0] for t in est])
        ubs = np.asarray([t[1] for t in est])
        self.logger.debug("VISDEC round %i: %s",self.rnd_mem, zip(q, V, est_bill))

        # profit according to estimated unit cost
        p_chosen = self.choose_p_mem(p, U, lbs, ubs, q, enable_negative_U=True, update_bounds=True)
        if p_chosen > p_eps:
            rq = self.rq_list(p_chosen, p_copy, q)
            self.last_requested = rq[0][1]
            if self.coalition_q is not None and (rq[0][1] - self.coalition_q >= 10):
                # I've increased q since I joined a coalition by more than 10 MB. I should quit the coalition.
                self.logger.info("%s coalition_q is %s and bid q is %s, I should not negotiate in next round.", 
                                 self._prefix(), self.coalition_q, rq[0][1])
                self.do_not_negotiate_on_round = self.rnd_mem+1
            return round(p_chosen, p_digits), rq
        else:
            return def_bid

    @staticmethod
    def _intersect_union(group1, group2):
        intersection = []
        for i in group1:
            if i in group2:
                intersection.append(i)
        union = list(set(group1+group2))
        return intersection, union
    

    @staticmethod
    def _print_exception_message():
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        logging.exception("Type %s, file %s, line %s" % (exc_type, fname, exc_tb.tb_lineno))


