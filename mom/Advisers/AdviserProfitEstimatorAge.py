'''
Created on Nov 29, 2011

@author: eyal

Validity of a measurement is only by its age and not according to the state.
'''
import numpy as np
from mom.Advisers.AdviserProfit import AdviserProfit
from collections import namedtuple, defaultdict
from mom.Adviser import p_eps

class Bill(namedtuple("Bill", "round q bill")):
    def is_valid(self, current, max_age):
        return current - self.round <= max_age

class AdviserProfitEstimatorAge(AdviserProfit):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        AdviserProfit.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs)
        self.bills = []
        self.max_age = 100
        self.current_enable_flag = 0
        self.alpha = 0.5
        self.update_alpha_guard = True
        if kwargs.has_key('update_alpha_guard'):
            self.update_alpha_guard = kwargs['update_alpha_guard']
        use_estimator = None
        if kwargs.has_key('use_estimator'):
            use_estimator = kwargs['use_estimator']
        if use_estimator is None:
            self.enable_flags = [True]
        elif not isinstance(use_estimator, list):
            raise ValueError("Use estimator flag must be a list of booleans")
        else:
            self.enable_flags = list(use_estimator)

    def _clear_old(self):
        new_bills = list(filter(lambda b: b.is_valid(self.rnd_mem, self.max_age), self.bills))
        if len(new_bills) != len(self.bills):
            self.logger.debug("cleared some history");
        self.bills = new_bills

    def _insert_bill(self, new_bill):
        if len(self.bills) == 0 or new_bill.round > self.bills[0].round:
            self.bills.insert(0, new_bill)

    def _average(self, vals, weights=None):
        if weights is None:
            return np.average(vals)
        elif abs(sum(weights)) <= 1e-5:
            return 0.0
        else:
            return np.average(vals, weights=weights)

    def _estimate_bill(self, q0):
        weight_rnd = lambda r: 1. / 2 ** (self.rnd_mem - r)
        weight_q = lambda q: float(q0) / q if q != 0 else 0.
        weight = lambda r, q: weight_rnd(r) * weight_q(q)
        bills = []
        weights = []
        for b in self.bills:
            bills.append(b.bill)
            weights.append(weight(b.round, b.q))
        return self._average(bills, weights=weights)

    def _estimate_point_old(self, q_prev, bill, p_in_min, p_out_max, q, p):
        if q < q_prev:
            ub = q * p_out_max
            ub = min(ub, bill / q_prev * q)
            ub = min(ub, bill)
            lb = max(0, bill - (q_prev - q) * p_in_min)
        elif q > q_prev:
            ub = bill + (q - q_prev) * p
            if abs(bill) <= p_eps:
                lb = 0
            else:
                lb = bill + (q - q_prev) * p_in_min
        else:
            return bill, bill, bill
        ret = (min(ub, p_out_max * q) + min(lb, p_out_max * q)) * .5
        return lb, ub, ret

    def _estimate_point(self, q_prev, bill, p_in_min, p_out_max, q, p):
        """
        returns: lower_bound, upper_bound, estimated_bill
        Estimate the lower bound and upper bound and the actual bill, for a new point q, 
        according to the q_prev, the bill in q_prev, p_in_min and p_out_max, and the unit price p.
        This is only good for bills without coalition.
        If there are coalitions, the bill_without_coalition is the one 
        we save in hist_mem and therefore the one that will be used here.
        """
        if q < q_prev:
            # Equation 3
            lb = max(bill/q_prev * q, bill + (q - q_prev)*p_out_max)
            # Last equation on page 6
            ub = max(0, bill*(1 - q/q_prev))
        elif q > q_prev:
            # equation 2
            ub = bill + (q - q_prev) * p
            # equation 1
            lb = bill + (q - q_prev) * p_in_min
        else:
            return bill, bill, bill
        if self.update_alpha_guard and self.alpha < 0:
            self.alpha = 0.5
        return lb, ub, (self.alpha * ub + (1 - self.alpha) * lb)


    def _is_estimator_enabled(self):
        return self.enable_flags[self.current_enable_flag]

    def do_reset_mem(self):
        self.logger.debug("do_reset_mem called")
        self.bills = []
        AdviserProfit.do_reset_mem(self)

    def next_valuation(self):
        self.current_enable_flag = (self.current_enable_flag + 1) % len(self.enable_flags)
        return AdviserProfit.next_valuation(self)

    def get_estimated_bill_mem(self, qs, ps):
        self.logger.info("round %s, get_estimated_bill_mem by AdviserProfitEstimatorAge called", self.rnd_mem)
        if not self._is_estimator_enabled():
            self.logger.debug("round %i estimator is not enabled", self.rnd_mem)
            return [0] * len(qs)
        self.logger.debug("round %i estimating bill", self.rnd_mem)
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self._insert_bill(Bill(last.round, last.won_mem - last.base_mem, last.bill))

        if len(self.bills) > 0:
            q_prev = self.bills[0].q
            bill_prev = self.bills[0].bill
        else:
            q_prev = 0
            bill_prev = 0
        if self.update_alpha_guard:
            if self.ub == self.lb:
                self.alpha = 0.5
            else:
                self.alpha = (bill_prev-self.lb)/(self.ub - self.lb)
            self.logger.info("AdviserProfitEstimatorAge round %i alpha: %s, ub: %s, lb: %s", self.rnd_mem, self.alpha,self.ub, self.lb)

        p_in_min = self.p_in_min_hist_mem.average()
        p_out_max = self.p_out_max_hist_mem.average()
        self._clear_old()
        bill = self._estimate_bill(q_prev, with_coalitions=False)
        self.logger.debug("round %i bills history: %s", self.rnd_mem, self.bills)
        self.logger.debug("round %i q_prev=%i, p_in_min = %.03f, p_out_max = %.03f, bill = %.03f",
            self.rnd_mem, q_prev, p_in_min, p_out_max, bill)
        ret = list(map(lambda q, p: self._estimate_point(q_prev, bill, p_in_min,
            p_out_max, q, p, with_coalitions=False), qs, ps))
        self.logger.debug("round %i estimated bills: %s", self.rnd_mem, map(lambda _q, _p: (_q, round(_p[0], 3), round(_p[1], 3), round(_p[2], 3)), qs, ret))
        #return map(lambda b: b[2], ret)
        return ret

if __name__ == '__main__':
    from mom.Profiler3d import fromxml
    profiler_file = "doc/profiler-memcached-inside-spare-50-win-500k-tapuz25-3d.xml"
    profiler_entry = "hits_rate"
    revenue_func_str = 'lambda x:x'
    base_mem = 600
    #base_bw = 1000
    profiler = fromxml(profiler_file, profiler_entry)
    adv = AdviserProfitEstimator(profiler = profiler,
                      base_mem = base_mem,
                      #base_bw = base_bw,
                      advice_entry = profiler_entry,
                      d_mem=10,
                      #d_bw=1000,
                      rev_func=eval(revenue_func_str))

    qs = range(10,1001,10)
    ind = np.argmin(map(lambda x: abs(x - 439), qs))
    print ind, qs[ind]
    np.argmin([])
