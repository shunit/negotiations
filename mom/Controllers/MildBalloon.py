'''
Created on Dec 17, 2011

@author: eyal
'''
import logging
from threading import Thread, Semaphore
import time
from mom.Controllers.Balloon import Balloon

class BackBallooner(Thread):
    def __init__(self, guest, balloon_func):
        self.guest = guest
        self.balloon = balloon_func
        self.__sem = Semaphore()
        self.__should_run = True
        self.__diff = 0
        self.__target = None
        self.__curr = None
        Thread.__init__(self, name = "BackBalloon")

    def run(self):
        while self.should_run():
            self.__sem.acquire()
            target = self.__target
            diff = self.__diff
            self.__curr += diff
            nxt = self.__curr
            self.__sem.release()

            if diff > 0:
                nxt = min(nxt, target)
            elif diff < 0:
                nxt = max(nxt, target)

            self.balloon(self.guest, nxt)
            time.sleep(1)

    def set_target(self, target, time):
        start = False
        self.__sem.acquire()
        self.__curr = self.__target
        self.__target = target
        if self.__curr == None:
            start = True
            self.__curr = self.__target
        self.__diff = int((self.__target - self.__curr) / ((0.5 * time) if time != 0 else 1))
        self.__sem.release()
        if start: self.start()

    def finish(self):
        self.__sem.acquire()
        self.__should_run = False
        self.__sem.release()

    def should_run(self):
        self.__sem.acquire()
        var = self.__should_run
        self.__sem.release()
        return var

class MildBalloon(Balloon):
    def __init__(self, properties):
        self.dt = properties['guest_manager'].config.getint('main', 'policy-engine-interval')
        self.workers = {}
        Balloon.__init__(self, properties)

    def process_guest(self, guest):
        target = guest.GetControl('control_mem')
        if target is None:
            return

        name = guest.Prop('name')
        if not self.workers.has_key(name):
            self.workers[name] = BackBallooner(guest, self.balloon)
        self.workers[name].set_target(target, self.dt)

    def process(self, host, guests):
        remove = set(self.workers.keys()).difference(set(g.Prop('name') for g in guests))
        if remove is not None:
            for guest in remove:
                self.workers[guest].finish()
                self.workers.pop(guest)

        for guest in guests:
            self.process_guest(guest)

def instance(properties):
    return MildBalloon(properties)



