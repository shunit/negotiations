import logging
import subprocess
import shlex
import threading
from etc.decorators import Singleton
from exp.core.VMDesc import get_vm_ip

class NetTcException(Exception):
    pass

class NetTc:
    __metaclass__ = Singleton # You're welcome Boris (Liran)
    """
        Linux QOS using TC for more information on TC see
        http://luxik.cdi.cz/~devik/qos/htb/
        http://lartc.org/howto/

        Creates an hierarchy over @interface.
        1:9  - default(not guest traffic)
        1:10 - guest 1
        1:n  - guest n-1
         *
         * +-----+     +---------+     +-----------+      +-----------+     +-----+
         * |     |     |  qdisc  |     | class 1:1 |      | class 1:9 |     |     |
         * | NIC |     | def 1:0 |     |   rate    |      |   rate    |     | sfq |
         * |     | --> |         | --> |   peak    | -+-> |   peak    | --> | 9:0 |
         * +-----+     +---------+     +-----------+  |   +-----------+     +-----+
         *                                            |
         *                                            |   +-----------+     +-----+
         *                                            |   | class 1:10|     |     |
         *                                            |   |   rate    |     | sfq |
         *                                            +-> |   peak    | --> |10:0 |
         *                                            |   +-----------+     +-----+
         *                                           ...
         *                                            |   +-----------+     +-----+
         *                                              |   | class 1:n |     |     |
         *                                            |   |   rate    |     | sfq |
         *                                            +-> |   peak    | --> | n:0 |
         *                                                +-----------+     +-----+
         *

    """
    @classmethod
    def from_parameters(cls, interface, total_rate, is_sharing):
        " Initialize from parameters "
        host_bw = 4000
        return cls(interface, total_rate, host_bw, is_sharing)

    @classmethod
    def from_properties(cls, properties):
        " Initialize from properties "
        interface = properties['config'].get('main', 'net-interface')
        total_rate = int(properties['config'].get('main', 'total-net-rate'))
        host_bw = int(properties['config'].get('host', 'host-bw'))
        return cls(interface, total_rate, host_bw, False)


    CLASSID_START = 10
    def __init__ (self, interface, total_rate, host_bw, is_sharing = False):
        """
            initialize the NetTc class setting the root of the hierarchy and the default qdisc.

            total_rate - bw of @interface NIC in kilobits MUST be an integer and cannot be changed
        """
        self.logger = logging.getLogger('mom.Controllers.NetTc')
        self.interface = interface
        self.total_rate = total_rate
        # beware of inconsistency when the value in your auction.conf mismatches the value in your main
        self.host_bw = host_bw
        self.is_sharing = is_sharing
        self.current_total = self.host_bw
        self.total_changed = False
        
        self.logger.info("Running on interface %s with total bandwidth %d host bandwidth is %d and the interface sharing is set to %s" % 
                         (self.interface, self.total_rate, self.host_bw, str(self.is_sharing)))

        # ip to (classid, rate) dictionary of all guests
        self.classids = {}
        self.lock = threading.Lock()

        # This parameters sets kernel htb processing to record actual rate of outgoing data for each htb class
        # see http://luxik.cdi.cz/~devik/qos/htb/manual/userg.htm#actual at the end of the page
        p = subprocess.Popen(shlex.split('sudo sh -c "echo 1 > /sys/module/sch_htb/parameters/htb_rate_est"'),
                              stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        out, err = p.communicate()

        self._change_qdisc('sudo tc qdisc add dev %s root handle 1: htb default 9' % (self.interface))

        # add 1 for the default non guest traffic 1:9
        self._change_qdisc('sudo tc class add dev %s parent 1: classid 1:1 htb rate %dkbit' % (self.interface, self.current_total))

        self._change_qdisc('sudo tc class add dev %s parent 1:1 classid 1:9 htb rate %dkbit' % (self.interface, self.host_bw))

        self._change_qdisc('sudo tc qdisc add dev %s parent 1:9 handle 9: sfq perturb 10' % (self.interface))

        # just in case the qdisc class already exists
        self._change_qdisc('sudo tc class change dev %s parent root classid 1:1 htb rate %dkbit ceil %dkbit' %\
                                      (self.interface, self.current_total, self.current_total))

        self.logger.info("NetTC ready")

    def _translate_to_ip(self, ip):
        ret = None
        try:
            ret = get_vm_ip(ip)
            self.logger.info("translate %s to %s" % (ip, ret))
        except:
            ret = ip
        return ret

    def _change_qdisc(self, cmd):
        #self.logger.info(cmd)
        p = subprocess.Popen(shlex.split(cmd.replace('add', 'delete')), stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        out, err = p.communicate()
        #self.log(out,err)

        p = subprocess.Popen(shlex.split(cmd), stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        out, err = p.communicate()
        self.log(out,err)

    def log(self, out, err):
        if len(out) > 0:
            self.logger.warn(out)
        if len(err) > 0:
            self.logger.error(err)
            raise NetTcException, 'NetTc err: %s' % err

    def add_guest(self, ip, amount):
        """
           add a guest to the qdisc hierarchy

           assume initialy all guests' bandwidth sum up to @total_rate

           ip - the guest vm ip address
           amount - the bw to be set for the guest in kilobytes, MUST be an integer, MUST sum up to @total_rate together with all leafs.
        """
        ip = self._translate_to_ip(ip)
        if ip in self.classids.keys():
            self.logger.warn("No need to add - just change")
            self.change_guest_bw(ip, amount)
            return

        self.lock.acquire()
        if not (self.current_total + amount <= self.total_rate):
            self.logger.error("Attempting to add %dkbit which is more than the total %d" % (amount, self.total_rate))
        else:
            self.logger.info("Bandwidth add is legal")
        self.current_total += amount
        if self.is_sharing:
            ceil = self.current_total
        else:
            ceil = amount

        self.total_changed = True

        # take the last part of an ip address "192.168.124.X" and the minus 3 is because the ip address of vm-1 is 192.168.124.3
        new_classid = int(ip[ip.rfind('.') + 1:]) - 3 + NetTc.CLASSID_START

        # change root
        self._change_qdisc('sudo tc class change dev %s parent root classid 1:1 htb rate %dkbit ceil %dkbit' %\
                                              (self.interface, self.current_total, self.current_total))
        self._change_qdisc('sudo tc class add dev %s parent 1:1 classid 1:%d htb rate %dkbit ceil %dkbit' % (self.interface,  new_classid, amount, ceil))

        self._change_qdisc('sudo tc qdisc add dev %s parent 1:%d handle %d: sfq perturb 10' % (self.interface,  new_classid,  new_classid))

        self._change_qdisc('sudo tc filter add dev %s protocol ip prio 0 u32 match ip src %s flowid 1:%d' % (self.interface, ip,  new_classid))

        self.logger.info("Added classid %d for %s with %dKbit" % ( new_classid, ip, amount))
        self.classids[ip] = (new_classid, amount)
        self.lock.release()

    def change_guest_bw(self, ip, amount):
        """
           change an existing guests bandwidth - changes are deffered until hierarchy is valid(\sum(leafs) == @total_rate)

           ip -     the guest vm ip address
           amount - the bw to be set for the guest in kilobytes, MUST be an integer,
                    MUST sum up to @total_rate together with all leafs.
        """
        ip = self._translate_to_ip(ip)
        self.lock.acquire()
        if ip not in self.classids.keys():
            self.logger.warn("no classid for %s trying to add.." % (ip))
            self.lock.release()
            self.add_guest(ip, amount)
            return

        classid = self.classids[ip][0]
        if amount != self.classids[ip][1]:
            self.total_changed = True
        self.current_total -= self.classids[ip][1]
        self.current_total += amount
        if self.is_sharing:
            ceil = self.current_total
        else:
            ceil = amount

        # change root
        self._change_qdisc('sudo tc class change dev %s parent root classid 1:1 htb rate %dkbit ceil %dkbit' %\
                                              (self.interface, self.current_total, self.current_total))
        # change leaf
        self._change_qdisc('sudo tc class change dev %s parent 1:1 classid 1:%d htb rate %dkbit ceil %dkbit' %\
                                              (self.interface, classid, amount, ceil))
        self.classids[ip] = (classid, amount)
        
        self.logger.info("Changed classid %d for %s to rate %dkbit ceil %dkbit" % (classid, ip, amount, ceil))

        self.lock.release()

    def fix_total_if_changed(self):
        if not self.total_changed:
            return

        self.logger.info("Fixing total after change, new total is: %d" % self.current_total)
        for ip in self.classids.keys():
            classid = self.classids[ip][0]
            amount = self.classids[ip][1]
            ceil = self.current_total
            self._change_qdisc('sudo tc class change dev %s parent 1:1 classid 1:%d htb rate %dkbit ceil %dkbit' %\
                                              (self.interface, classid, amount, ceil))

        self.total_changed = False

    def process(self, host, guests):
        for guest in guests:
            self.change_guest_bw(guest.Prop("name"), guest.GetControl("control_bw"))

        if self.is_sharing:
            self.fix_total_if_changed()

        if (self.current_total > self.total_rate):
            self.logger.error("Bad Bandwidth assignment!!! (%s > %s)" % (self.current_total, self.total_rate))


def instance(properties):
    return NetTc.from_properties(properties)


if __name__ == "__main__":
    #FORMAT = "%(asctime)-15s %(clientip)s %(user)-8s %(message)s"
    logging.basicConfig()
    nc = NetTc('eth1', 1 << 20, 4000)
    nc.add_guest('192.168.124.3', 1<<10)
    nc.add_guest('192.168.124.3', 1<<11)
