import logging
from math import floor
from mom.Comm.Messages import MessageAnnounceMem, MessageTimeoutError, MessageError
from etc.ParallelInvoker import ParallelInvoker
import time
from mom.LogUtils import LogUtils


class Announcer(object):
    def __init__(self, properties, verbosity="info"):
        LogUtils(verbosity)
        self.logger = logging.getLogger('Announcer')
        self.round = -1
        self.min_bid_interval = 1
        self.auction_mem = 0
        self.current_guests_allocation = {}
        
    # internal thread for collecting bids
    def _bid_collect_thread(self, guest, start_time, warmup_ended=False):
        msg = {}
        last_bid = {}
        msg_class = MessageAnnounceMem
        msg = {'auction_round_mem' : self.round, 'auction_mem': self.auction_mem,
               'base_mem': guest.GetVar('base_mem'),
               'closing_time': start_time + self.auction_bid_time,
               'warmup_ended': warmup_ended,
               'guests_with_memory': self.current_guests_allocation}
        # default empty bid
        last_bid = {"bid_round": 0, "bid_p": 0, "bid_ranges": []}

        # verify we didn't forget anything of the message keys
        assert set(msg.keys()) == msg_class.message_keys

        got_bid = False
        while True:
            try:
                bid_collection_start = time.time()
                # check if enough time is left for bid collection
                if self.auction_bid_time != 0 and msg['closing_time'] <= time.time():
                    break
                # send announce message of the resource type 
                # TODO: create 2 communications ports
                bid = guest.Prop('comm_mem').communicate(msg_class(**msg))
                if bid:
                    last_bid = bid
                    got_bid = True
                    if self.auction_bid_time is None:
                        break
            except MessageTimeoutError:
                break
            except MessageError as ex:
                if not got_bid:
                    # in error case, make default 0 response.
                    self.logger.warn("didn't response with bid: %s", str(ex))

            # make sure the interval between two announces is at least
            # min_bid_interval.
            if self.auction_bid_time != 0:
                guest.UpdateVars(last_bid)
                time.sleep(max(0, self.min_bid_interval - (time.time() - bid_collection_start)))
            else: 
                guest.UpdateVars(last_bid)
                break  # simulations

        guest.UpdateVars(last_bid)

    def calc_auction_mem(self, host, guests):
        auction_mem = 0
        if host.Prop('config').has_option('host', 'auction-mem'):
            auction_mem = host.Prop('config').getint('host', 'auction-mem')
        if auction_mem == 0:
            self.logger.debug("calculating auction memory")
            mem_available = host.Stat('mem_available')
            host_mem = host.Prop('config').getint('host', 'host-mem')
            guests_base_mem = 0
            for i, g in enumerate(guests):
                self.logger.debug("vm-%i base_mem = %i", i, g.GetVar('base_mem'))
                guests_base_mem += g.GetVar('base_mem')
            self.logger.debug("{'mem_available': %i, 'host_mem': %i, " +
                              "'guests_base_mem': %i}", mem_available, host_mem,
                              guests_base_mem)
            auction_mem = mem_available - host_mem - guests_base_mem

        return auction_mem


    def pre_process(self, host, guests):
    # set host and guest properties before announcing the auction
        for guest in guests:
            base_mem = guest.Prop('base_mem')
            # set initial base memory for control
            curr_mem = guest.GetVar("alloc_prev_mem") 
            curr_mem = curr_mem if curr_mem else 0
            # calculate base memory for next round
            self.logger.info("curr_mem %s" % curr_mem)
            self.logger.info("base_mem %s" % base_mem)
            # save memory values in Entity variables
            guest.SetVar('curr_mem', curr_mem)
            guest.SetVar('base_mem', base_mem)

            self.current_guests_allocation[guest.Prop("name")] = curr_mem - base_mem == 0

        # calculate auction memory
        self.auction_mem = max(0, self.calc_auction_mem(host, guests))
        self.logger.debug("memory available for auction: %i", self.auction_mem)
        if self.auction_mem == 0:
            self.logger.warn("No memory for auction!")
        # update host for auction policy and notifier
        host.SetVar('auction_round_mem', self.round)
        host.SetVar('auction_mem', self.auction_mem)

    def process(self, host, guests):
        self.round += 1

        self.pre_process(host, guests)
        self.auction_bid_time = host.GetVar('mem-bid-collection-interval')
        warmup_ended = host.GetVar('warmup_ended')
        if self.auction_bid_time != 0:
            invoker = ParallelInvoker(guests,
                        name = lambda g: "%s-bid-collector-mem" % (g.Prop("name")))
            start_time = time.time()
            invoker.start_and_join(lambda g: self._bid_collect_thread,
                               args = lambda g: [g, start_time, warmup_ended],
                               timeout = self.auction_bid_time)
        else:  # for simulations
            for g in guests:
                self._bid_collect_thread(g, 0)

class AnnouncerDoubleOvercommit(Announcer):
    def __init__(self, factor, *args, **kwrds):
        self.factor = factor
        Announcer.__init__(self, *args, **kwrds)

    def calc_auction_mem(self, host, guests):
        auction_mem = max(0, host.Stat("mem_free")) * 0.5  # - host.Prop('config').getint('host', 'host-mem')
        for g in guests:
            auction_mem += max(0, g.GetVar('curr_mem') - g.GetVar('base_mem'))

        return int(auction_mem * self.factor)

class FixedAnnoucer(Announcer):
    def __init__(self, mb, *args, **kwrds):
        self.mb = mb
        Announcer.__init__(self, *args, **kwrds)

    def calc_auction_mem(self, host, guests):
        return self.mb
