# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import logging

class Balloon:
    """
    Simple Balloon Controller that uses the libvirt setMemory() API to resize
    a guest's memory balloon.  Output triggers are:
        - balloon_target - Set guest balloon to this size (kB)
    """
    def __init__(self, properties):
        self.libvirt = properties['libvirt_iface']
        self.logger = logging.getLogger('mom.Controllers.Balloon')

    def balloon(self, guest, target):
        dom = self.libvirt.getDomainFromID(guest.Prop('id'))
        if dom is None or target is None:
            return

        gname = guest.Prop('name')
        target = int(target)

        # check maximum allowed memory (defined by libvirt's domain maxMemory)
        max_mem = dom.maxMemory() >> 10  # in MB
        if target > max_mem:
            self.logger.warn("%s reached it's memory limit", gname)
            target = max_mem
            guest.Control('control_mem', target)

        try:
            self.logger.debug("Ballooning %s: from %i to %i MB",
                              gname, guest.Stat('libvirt_curmem'), target)
        except KeyError: pass

        # actuate balloon:
        target_kb = target << 10
        if dom.setMemory(target_kb):
            self.logger.warn("Error while ballooning: %s", gname)

    def process_guest(self, guest):
        self.balloon(guest, guest.GetControl('control_mem'))

    def process(self, host, guests):
        def sort_key(guest): # return target - current. if this is negative, we'll prefer this guest
            ret = 0
            try:
                target = guest.GetControl('control_mem')
                current = guest.Stat('libvirt_curmem')
                ret = target - current
            except KeyError:
                pass
            finally:
                self.logger.debug("%s sort key: %i", guest.Prop('name'), ret)
                return ret

        sorted_guests = sorted(guests, key=sort_key)
        for guest in sorted_guests:
#        for guest in guests:
            self.process_guest(guest)

def instance(properties):
    return Balloon(properties)
