#!/usr/bin/python
'''
Created on Aug 30, 2014

The allocator for the network resource implemnting:
Maile & Tuffin[04] Multi-Bid Auctions for Bandwidth Allocation in Communication Networks

@author: borisp
'''

#TODO: think of how to evealuate the leftovers:
# This is a problem since the paper doesn't reference it..(they either distribute leftovers freely or they cause bidders to overpay sometimes) 
# The problem is that if a player already has all he wanted to get, then if he gets more he shouldn't pay for it..
# RE: In the paper if there is more than is needed for everyone the results are undefined...(zero division)

from math import log, ceil, floor
from collections import OrderedDict
import numpy as np

# theta is the pseudo valuation function
theta = lambda i, q_i,: q_i

class MTBid:
    '''
        concave means the for q_i, q_j -> p_i < p_j
        p_i = gradiant at point q_i 
        pqs = [(p_1, q_1), ... (p_i, q_i) ...]
    '''
    MAX_P = 0xFFFFFFFF

    def __init__(self, name, pqs, sorted_keys = []):
        '''
        possible configurations:

        '''
        self.name = name
        if type(pqs) == list:
            pqs.sort()
            self.pqs = dict(pqs)
        elif type(pqs) == dict:
            self.pqs = pqs

        if not sorted_keys:
            keys = self.pqs.keys()
            self.keys = sorted(keys)
        else:
             self.keys = sorted_keys

        # the reverse dictionary - mapping quantity with price
        #self.qps = {}
        #for k,v in self.pqs.items():
        #    self.qps[v] = k

        #compare_keys = zip(self.keys[:-1], self.keys[1:])
        #for i in xrange(len(compare_keys)):
        #    # check value is monotonic and rising
        #    k1 = compare_keys[i][0]
        #    k2 = compare_keys[i][1]
        #    demand1 = k1 * self.pqs[k1]
        #    demand2 = k2 * self.pqs[k2]
        #    print demand1, demand2
        #    assert demand1 <= demand2, "function isn't monotonic and rising"
        #
        #    # check value is concave - "diminishing returns"
        #    incline1 = k1
        #    incline2 = k2
        #    assert incline2 <= incline1, "function isn't concave"
    def remove(self, k):
        self.keys.remove(k)
        v = self.pqs.pop(k)    
        #self.qps.pop(v)

    def add_pq(self, p, q):
        self.pqs[p] = q
        #self.qps[q] = p
        if p not in self.keys:
             self.keys.append(p)
             self.keys.sort()

    def __repr__(self):
        output = 'MTBid name: %s\nKeys: %s\npqs: {' % (self.name, str(self.keys))
        for k in self.keys:
            output += '%f : %f, ' % (k, self.pqs[k])
        output += '}\n'
        return output

    def to_graph(self):
        import pylab as pl
        from etc.plots import draw_n_save, pl_apply_defaults
        from out.sim_output_trace import exp_plot
        import os

        # the value for every q - unlike p(the gradient at every q)
        self.value = OrderedDict({0 : 0})
        cur_val = 0
        cur_q = 0
        for p in self.keys:
            q = self.pqs[p]
            self.value[q] = cur_val + p * (q - cur_q)
            cur_q = q
            cur_val = self.value[q]

        print self.value        
        pl.clf()
        func_val = lambda dic: [val for q, val in dic.items() ] 
        func_quantity = lambda dic: [q for q, val in dic.items() ]
        exp_plot({i: self.value for i in xrange(1)}, (func_quantity, func_val), 'value', "None", "-", 'bandwidth')
        # only windows on my computer.. make it more generic
        out_dir = os.environ['HOMEPATH'] + r"\Documents\moc-output\sim-output"
        size = (10, 5) 
        draw_n_save(os.path.join(out_dir, "bw-bids-test-%s.png" % self.name), size)

def get_aggregated_demand(bids, qmax):
    '''
        @return the aggreagated demand as a dictionary representing the step function
    '''
    ind = [0 for b in bids]

    # sort all keys pairs(key, bidder_index, key_index)
    all_keys = []
    all_keys_ind = 0
    for i in xrange(len(bids)):
        keys = [(bids[i].keys[k_i], i, k_i) for k_i in xrange(len(bids[i].keys))]
        all_keys.extend(keys)
    all_keys.sort()
    #print all_keys

    res = {}

    get_cur_p_by_index = lambda i : keys[i][ind[i]]
    get_cur_q_by_index = lambda i : bids[i].pqs[keys[i][ind[i]]]
    get_next_q_by_index = lambda i : bids[i].pqs[keys[i][ind[i] + 1]]
    get_prev_q_by_index = lambda i : bids[i].pqs[keys[i][ind[i] - 1]]

    cur_sum = sum([bids[i].pqs[bids[i].keys[0]] for i in xrange(len(bids))])
    cur_key = all_keys[0][0]
    res[cur_key] = cur_sum
    for all_keys_i in xrange(1, len(all_keys)):
        k, i, k_i = all_keys[all_keys_i]    
        k_p, i_p, k_i_p = all_keys[all_keys_i - 1]
        #print k,i,k_i
        #print k_p, i_p, k_i_p

        diff_cur_sum = (bids[i_p].pqs[bids[i_p].keys[k_i_p + 1]] - bids[i_p].pqs[k_p]) if (k_i_p + 1 < len(bids[i_p].keys)) else -bids[i_p].pqs[k_p]
        #print (k_i_p + 1), len(bids[i_p].keys)

        cur_sum += diff_cur_sum
        #print '[%d] = %d' % (k, cur_sum)
        #print res
        if k not in res:
            res[k] = cur_sum

    #return MTBid('AggregatedBids', res, list(set([k for k,i,k_i in all_keys])))
    return MTBid('AggregatedBids', res)

def market_clearing_price(bids, aggr_demand, qmax):
    '''
        returns the clearing price - both the above and the below
    '''
    # all the return parameters
    clear_quantity_plus = clear_price_plus = clear_quantity = clear_price = None

    keys = aggr_demand.keys

    for k in keys:
        if aggr_demand.pqs[k] > qmax:
            clear_price = k
            clear_quantity = aggr_demand.pqs[clear_price]
        else:
            clear_price_plus = k
            clear_quantity_plus = aggr_demand.pqs[clear_price_plus]
            break

    #print aggr_demand
    #print 'clearing prices:', clear_price, clear_quantity, clear_price_plus, clear_quantity_plus 

    # all is well and we have both clearing prices
    if clear_price is not None and clear_price_plus is not None:
        return (clear_price, clear_quantity, clear_price_plus, clear_quantity_plus)

    # the total aggr_demand is smaller than qmax
    elif clear_price is None and clear_price_plus is not None:
        return (0, aggr_demand.pqs[keys[0]], clear_price_plus, clear_quantity_plus)

    # the clear_price is exactly at the maximal price possible
    elif clear_price is not None and clear_price_plus is None :
        return (clear_price, clear_quantity, None, None)

    # impossible
    else:
        assert False, "impossible state."

# TODO: add caching of results to prevent repeated searches
# TODO: use binary search
def get_quantity_at_clearing_price(bid, clear_price):
    buying_at = None
    for i in reversed(xrange(len(bid.keys))):
        if bid.keys[i] >= clear_price:
            buying_at = bid.keys[i]
        else:
            break
    if buying_at is not None:
        return bid.pqs[buying_at]
    else:
        # no price the bidder is ready to pay is high enough
        return 0

#def get_price_for_quantity(bid, start, price):
#    start_index = bid.keys.index(start)
#    for i in xrange(start_index, len(bid.keys)):

def allocate(bids, qmax):
    """
    allocations for @bids with qmnx BW according to MT04
    """
    #print 'allocating %d' % qmax
    if len(bids) == 0:
        return []
    
    res = []

    aggr_demand = get_aggregated_demand(bids, qmax)

    #print 'aggr demand:', aggr_demand
    # see the paper by MT04 to understand the "u" notion
    u, du, u_plus, du_plus = market_clearing_price(bids, aggr_demand, qmax)

    #print 'u: %s du: %s u_plus: %s du_plus: %s' % (u, du, u_plus, du_plus)
    
    # case when all the resource is bought at maximum price
    if u_plus is None and du_plus is None and du >= qmax:
        u_plus = np.Infinity
        du_plus = 0
    elif u_plus is None or du is None:
        raise Exception("Bad state")

    for bid in bids:
        # the float makes the whole calculation a floating point one
        if du == du_plus:
            ai_leftovers = 0
        else:
            q_at_u = float(get_quantity_at_clearing_price(bid, u))
            q_at_u_plus = get_quantity_at_clearing_price(bid, u_plus)
            #print 'q_at_u: %f q_at_u_plus: %f' % (q_at_u, q_at_u_plus)
            ai_leftovers = ((q_at_u - q_at_u_plus) / (du - du_plus)) * (qmax - du_plus)
        ai = get_quantity_at_clearing_price(bid, u_plus)
        #print '%d %d' % (bids.index(bid), get_quantity_at_clearing_price(bid, u_plus))
        res.append((ai, bid, ai_leftovers))

    return res

def get_payment(bid, ai, ai_leftovers):
    q_to_be_paid = ai + ai_leftovers
    q_paid_for = 0
    payment = 0
    total_amount = 0
    for k in reversed(bid.keys):
        if q_paid_for == q_to_be_paid:
            return round(payment,5)
        amount = min(bid.pqs[k], q_to_be_paid) - q_paid_for
        # account for resource that was already paid for since (p, <up to this q>)
        q_paid_for += amount
        #print 'bidder pays for amount %d at price %f' % (amount, k)
        payment += (amount * k)
    print 'bidder is getting %d for free' % (q_to_be_paid - q_paid_for)
    return round(payment,5)


def allocate_with_payments(bids, qmax):
    # buy the whole quantity in case it is for free    
    for b in bids:
        while b.pqs[b.keys[-1]] >= qmax:
            if len(b.pqs) == 1:
                key = b.keys[-1]
                b.add_pq(key, qmax)
            b.remove(b.keys[-1])
    if b.pqs[b.keys[-1]] < qmax:
        b.add_pq(0, qmax)

    # allocations with no payments
    total_alloc = allocate(bids, qmax)
    res = [[ai + ai_leftovers, get_payment(bid, ai, ai_leftovers)] for ai,bid,ai_leftovers in total_alloc]
    social_wellfare = sum(payment for _, payment in res)

    # calculate the payments
    #print 'total alloc',
    #print [(a[0], a[2]) for a in total_alloc]
    #print 'social wellfare %d' % social_wellfare

    for i in xrange(len(bids)):
        cur_bids = bids[:i] + [MTBid('shillBid', [(0,0)])] + bids[i+1:]
        #print '===' * 5 
        #print cur_bids        
        cur_alloc = allocate(cur_bids, qmax)
        #print 'alloc:\n', [(ai, ai_leftovers) for ai,_,ai_leftovers in cur_alloc]
        #print '===' * 5 
        social_wellfare_without_i = sum([get_payment(cur_bids[j], cur_alloc[j][0], cur_alloc[j][2]) for j in xrange(len(bids)) if j != i])
        #print 'social wellfare without %d is %d' % (i, social_wellfare_without_i)
        pi = social_wellfare - social_wellfare_without_i
        #qi = sum([get_ci(j) for j in xrange(len(bids)) if j != i])
        res[i][1] = pi

    # the output is in the [(q_i, p_i)...] format
    return res, social_wellfare

def test():
    bid = {-0.000000 : 131000.000000, 0.000320 : 130000.000000, 0.002680 : 127000.000000, 0.002920 : 126000.000000, 0.012570 : 125000.000000, 0.018430 : 97000.000000, 0.024690 : 96000.000000, 0.042620 : 95000.000000, 0.060950 : 94000.000000, 0.078570 : 93000.000000}
    b1 = MTBid('b1', bid)
    b2 = MTBid('b2', bid)
    b3 = MTBid('b3', bid)
    
    res = allocate_with_payments([b1,b2,b3], 2**18 - 16000)
    #print res
    assert res[0][0] == 82048.0
    assert res[1][0] == 82048.0
    assert res[2][0] == 82048.0
    assert res[0][1] == 3776.684000000001
    assert res[1][1] == 3776.684000000001
    assert res[2][1] == 3776.684000000001

def test2():
    bid1 = dict([(0.01467, 154000.0), (0.0198, 151000.0), (0.02335, 150000.0), (0.02584, 149000.0), (0.02593, 128000.0), (0.06032, 101000.0), (0.06911, 88000.0), (0.0731, 78000.0)])
    bid2 = dict([(0.0062, 173000.0), (0.01447, 155000.0), (0.02934, 154000.0), (0.05186, 128000.0), (0.05313, 127000.0), (0.07858, 126000.0), (0.12479, 100000.0), (0.13146, 89000.0)])
    bid3 = dict([(0.00381, 174000.0), (0.00929, 173000.0), (0.02171, 155000.0), (0.05939, 151000.0), (0.07779, 128000.0), (0.169, 102000.0), (0.18718, 100000.0), (0.20732, 88000.0), (0.22126, 77000.0)])
    b1 = MTBid('b1', bid1)
    b2 = MTBid('b2', bid2)
    b3 = MTBid('b3', bid3)

    res, sw = allocate_with_payments([b1,b2,b3], 2**18)
    assert res[0][0] == 8144
    assert res[1][0] == 126000
    assert res[2][0] == 128000
    assert res[0][1] == 111.65424000000348
    assert res[1][1] == 6904.972480000004
    assert res[2][1] == 16289.432480000003
    assert sw == 39635.2764
