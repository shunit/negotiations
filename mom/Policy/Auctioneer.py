from mom.Adviser import p_eps, p_digits
from mom.Controllers.Announcer import Announcer
from mom.Controllers.Notifier import Notifier
import logging
from random import shuffle
from mom.Policy.MPSPAllocator import allocate, MPSPBid
import time
from collections import defaultdict
from mom.LogUtils import LogUtils
from copy import deepcopy
import numpy as np
import itertools
import math

list_wo = lambda lst, i: lst[:i] + lst[i + 1:]


def list_wo2(lst, i, j):
    ret = lst[:i] + lst[i + 1: j] + lst[j + 1:]
    if len(ret) == 0:
        ret = [MPSPBid('vm-1', 0.0, [(0, 0)], None)]
    return ret


GName = lambda b: b.Prop("name") if b.GetVar("parent") is None else b.GetVar(
    "parent").Prop("name") + "+"

def unique_list(list_to_dedup):
        deduped = []
        for d in list_to_dedup:
            if d not in deduped:
                deduped.append(d)
        return deduped

class CoalitionCounter(object):

    def __init__(self):
        self.coalitions = []
        self.supporters = []

    def add_support(self, coalition, guest):
        added = False
        for i,c in enumerate(self.coalitions):
            if self.similar(coalition, c):
                self.supporters[i].append(guest)
                added = True
        if not added:
            self.coalitions.append(coalition)
            self.supporters.append([guest])

    def similar(self, coalition1, coalition2):
        if coalition1.keys() != coalition2.keys():
            return False
        for g in coalition1.keys():
            if not (coalition1[g]=="shapley" and coalition2[g]=="shapley") and not np.isclose(coalition1[g], coalition2[g]):
                return False
        return True

    def get_majority_version_and_supporters(self):
        max_support_count = max([len(s) for s in self.supporters])
        majority_version = [(self.coalitions[i], s) for i,s in enumerate(self.supporters)
                            if len(s) == max_support_count]
        if len(majority_version) > 1:
            return None, None
        return majority_version[0]


class Auctioneer(object):
    total_bills = defaultdict(int)

    def __init__(self, verbosity="debug", simulate=False):
        # TODO: _auction_type defaults to memory, because the auctioneer is
        # started in auction.rules (a LISP script in doc/). this goes through
        # mom/Policy/Parser.py, which constructs a new auctioneer object
        LogUtils(verbosity)
        self.logger = logging.getLogger("Auctioneer")
        self.simulate = simulate
        self.announcer = Announcer(None, verbosity=verbosity)
        self.notifier = Notifier(None)
        self.bid_collection_interval = None
        self.calculation_interval = None
        self.notification_interval = None
        self.total_bills = defaultdict(int)
        self.extra_prev = defaultdict(int)
        self.p_in_min = 0
        self.p_out_max = 0
        self.warmup_rounds = None
        self.enable_coalition_with_outside_guests = True

    def wait_for_time(self, seconds_to_wait, start):
        if self.simulate: return
        current_wait = time.time() - start

        if current_wait < seconds_to_wait:
            time.sleep(seconds_to_wait - current_wait)

    def set_auction_parameters(self, host, name):
        param = host.Prop('config').getfloat('host', name)
        host.SetVar(name, param)
        return param

    # called in auction.rules
    def auction(self, host, bidders):
        ret = None
        t0 = time.time()
        t = 0

        self.bid_collection_interval = self.set_auction_parameters(host,
                                                                   'mem-bid-collection-interval')
        self.calculation_interval = self.set_auction_parameters(host,
                                                                'mem-calculation-interval')
        self.notification_interval = self.set_auction_parameters(host,
                                                                 'mem-notification-interval')

        # check if we need to reset
        self.logger.debug("Checking reset timer")
        host.SetVar('warmup_ended', False)
        if self.warmup_rounds is None:
            self.logger.debug("Getting initial warmup reset counter")
            self.warmup_rounds = self.set_auction_parameters(host,
                                                             'warmup_rounds')
        else:
            self.logger.debug("Updating warmup reset counter")
            self.warmup_rounds -= 1
            self.warmup_rounds = max(self.warmup_rounds, -1)
        if self.warmup_rounds >= 0:
            self.logger.debug("Rounds till reset: %i", self.warmup_rounds)
            if self.warmup_rounds == 0:
                self.logger.debug("Warmup ended, resetting auction")
                self.warmup_rounds = -1
                self.reset_mem()
                host.SetVar('warmup_ended', True)
        else:
            # do not reset more than once
            host.SetVar('warmup_ended', False)

        # set extra_prev variable
        for b in bidders:
            extra_prev = self.extra_prev[b.Prop('name')]
            b.SetVar("alloc_prev_mem", extra_prev + b.Prop("base_mem"))

        # announce an auction
        if not self.simulate:
            try:
                self.announcer.process(host, bidders)
            except Exception as ex:
                self.logger.error("exception in announcer: %s", ex)
                raise

        t += self.bid_collection_interval
        self.wait_for_time(t, t0)

        # calculate results
        try:
            start_time = time.time()
            ret = self.do_auction_mem(host, bidders)
            end_time = time.time()
            host.SetVar("auction_time_mem", end_time - start_time)
            self.logger.debug("auction_time_mem: %f" % (end_time - start_time))
        except Exception as ex:
            self.logger.error("exception in do_auction: %s", ex)
            raise

        t += self.calculation_interval
        self.wait_for_time(t, t0)

        # send notification back to bidders on results
        if not self.simulate:
            try:
                self.notifier.process(host, bidders)
            except Exception as ex:
                self.logger.error("exception in notifier: %s", ex)
                raise

        t += self.notification_interval
        self.wait_for_time(t, t0)
        self.logger.debug("Overall auction time is %f", time.time() - t0)

        return ret

    def _sort_bids_mem(self, bids):
        # compute bidders extra memory allocations
        # sort bidders for allocation preference
        # random shuffle
        shuffle(bids)
        # sort by last allocation, with less preference to children
        if len(self.extra_prev) != 0: # first time: no previous allocation
            bids.sort(key = lambda b: self.extra_prev[b.name], reverse = True)
        # sort by p value
        bids.sort(key = lambda b: b.p, reverse = True)

    def _prepare_bids(self, auction_round, bidders):
        '''
        Extracts and splits bids from bidders
        @param auction_round the current round
        @param bidders guest descriptors
        @param reduction the reduction factor. If zero,
        splitting will not be done
        @return a list of sorted bids
        '''
        bids = []
        for b in bidders:
            bid = self.get_bid_mem(b, auction_round)
            bids.append(bid)
        self._sort_bids_mem(bids)
        map(lambda b: self.logger.debug("Bids after sort: %s", str(b)), bids)
        return bids

    def _guest_SW(self, bids, alloc, guest):
        ret = 0
        for b, a in zip(bids, alloc):
            if b.name != guest: continue
            self.logger.debug("%s p=%.02f, a=%i, parent: %i", guest, b.p, a, b.is_parent)
            ret += b.p * a
        return ret

    def _SW_from_list(self, bids, alloc, exclude=None):
        ret = 0
        for b, a in zip(bids, alloc):
            if b.name != exclude:
                ret += b.p * a
        return ret

    def _calculate_alloc_mem(self, auction_round, auction_mem, bids,
                             exclude=None):
        """Calculate an allocation of the memory and potentially exclude some guests from the allocation.
        @param exclude - a list of guest names to exclude from the allocation.
        In most cases the list will contain only one guest. It will be longer in cases of coalitions.
        """
        if exclude is not None:
            bids = filter(lambda b: b.name not in exclude, bids)
        extra, sw = allocate(bids, auction_mem, self.extra_prev)
        return extra, sw

    def extract_coalitions_info_from_bidders(self, bidders):
        bidder_to_coalition = {}
        for b in bidders:
            bidder_to_coalition[b.Prop("name")] = b.GetVar("coalition_members")
        coalitions = unique_list(bidder_to_coalition.values())

        # The coalition members dictionary for each guest.
        guest_to_final_coalition = {}
        while coalitions:
            c = coalitions.pop(0)
            if not c: # Empty coalition
                continue
            guests_in_c = set(c.keys())
            prev_guests_in_c = None
            while guests_in_c != prev_guests_in_c:
                prev_guests_in_c = deepcopy(guests_in_c)
                new_guests_in_c = deepcopy(guests_in_c)
                for g in guests_in_c:
                    guest_coalition = bidder_to_coalition[g]
                    new_guests_in_c.update(guest_coalition.keys())
                    if guest_coalition in coalitions:
                        coalitions.remove(guest_coalition)
                guests_in_c = deepcopy(new_guests_in_c)

            # Find majority version among the guests in c.
            counter = CoalitionCounter()
            for g in guests_in_c:
                counter.add_support(bidder_to_coalition[g], g)
            majority_version, supporting_guests = counter.get_majority_version_and_supporters()
            if majority_version is None:
                # There is no majority version. The host will not respect this coalition.
                for g in guests_in_c:
                    guest_to_final_coalition[g] = {}
                continue
            # Otherwise, there is exactly one majority version.
            self.logger.info("majority_version: %s, supporting_guests: %s, bidder_to_coalition: %s", 
                             majority_version, supporting_guests, bidder_to_coalition)
            # Handle the case where the majority_version is no coalition.
            if majority_version == {}:
                for g in supporting_guests:
                    guest_to_final_coalition[g] = {}
            else:
                # Normalize the profit parts for the supporting guests
                if majority_version.values()[0] == "shapley":
                    final_profit_parts = majority_version
                else:
                    sum_of_parts = sum([majority_version[g] for g in supporting_guests])
                    final_profit_parts = {}
                    for g in supporting_guests:
                        final_profit_parts[g] = majority_version[g]*1.0/sum_of_parts
                # Update the final coalition for each supporting guest.
                for g in supporting_guests:
                    guest_to_final_coalition[g] = final_profit_parts
            # The non-supporting guests are out of the coalition.
            for g in majority_version.keys():
                if g not in supporting_guests:
                    guest_to_final_coalition[g] = {}

        # Handle guests whose coalition dictionary was empty
        for (bidder_name, coalition) in bidder_to_coalition.iteritems():
            if bidder_name not in guest_to_final_coalition:
                guest_to_final_coalition[bidder_name] = {}

        return guest_to_final_coalition

    def maybe_remove_outside_guests_from_coalitions(self, guest_to_coalition, guests_with_mem):
        if self.enable_coalition_with_outside_guests:
            return guest_to_coalition
        coalitions = unique_list(guest_to_coalition.values())
        for c in coalitions:
            new_c = {}
            # The new coalition will contain only the guests with memory.
            for guest in c:
                if guest in guests_with_mem:
                    new_c[guest] = c[guest]
            # A coalition with only one guest is not a coalition at all.
            if len(new_c.keys()) < 2:
                new_c = {}
            # Normalize the profit parts of the new coalition.
            total_profit_parts = sum(new_c.values())
            for g in new_c.keys():
                new_c[g] = new_c[g]/total_profit_parts
            # Update the coalitions map
            for guest in c:
                guest_to_coalition[guest] = new_c if guest in new_c else {}
        return guest_to_coalition
 

    def get_guests_with_mem(self, bids, extra):
        guests_with_mem = []
        # We only allow guests who have memory and got all the memory they asked for. 
        # (Allowing the guest on the border to be in a coalition is also not stable).
        for (b,e) in zip(bids, extra):
            if e > 0 and b.max_q <= e:
                guests_with_mem.append(b.name)
        self.logger.info('guests_allowed: %s' % guests_with_mem)
        return guests_with_mem
          
    def do_auction_mem(self, host, bidders, show=True):
        """
        @param host: a host of type mom.Entity
                needs the following variables: auction_round_mem, auction_mem
        @param bidders: a list of type mom.Entity representing bidders
                each bidder needs the following:
                bid_p - price of required memory in cents per mb
                bid_ranges -
        @return: set values for 'control_mem' and 'control_bill_mem' for each bidder
                 in the received list.
        """
        host_round = host.GetVar('auction_round_mem')
        if host_round is None:  # No auction before one is announced
            return
        auction_mem = host.GetVar('auction_mem')
        payments = defaultdict(int)
        self.logger.debug("Memory Auction %i started", host_round)
        bids = self._prepare_bids(host_round, bidders)
        extra, sw = self._calculate_alloc_mem(host_round, auction_mem, bids)
        # calculate the payments
        for name in map(lambda g: g.Prop('name'), bidders):
            self.logger.debug("Round %i payment for bidder %s", host_round, name)
            extra_wo, sw_wo = self._calculate_alloc_mem(host_round,
                                                        auction_mem, bids, [name])
            self.logger.debug("Round %i excluded alloc: %s", host_round, extra_wo)
            self.logger.debug("Round %i excluded SW: %.02f", host_round, sw_wo)
            sw_of_guest = self._guest_SW(bids, extra, name)
            payments[name] = sw_wo - (sw - sw_of_guest)
            self.logger.debug("Round %i guest %s sw=%.02f, sw_wo=%.02f, sw_of_guest=%.02f",
                              host_round, name, sw, sw_wo, sw_of_guest)
            self.logger.debug("Round %i guest %s payment: %.02f", host_round,
                              name, payments[name])
            assert -p_eps <= payments[name] <= (1 + p_eps) * sw_of_guest, \
                "%s got an illegal bill: %0.2f, sw_of_guest=%0.2f, sw_wo=%.02f, sw=%.02f" % (name,
                                                                                      payments[name], sw_of_guest, sw_wo, sw)
        # Handle coalitions
        self.logger.info("Handle Coalitions")
        guest_to_coalition_members = self.extract_coalitions_info_from_bidders(bidders)
        self.logger.info("guest_to_coalition_members before: %s" % guest_to_coalition_members)
        guest_to_coalition_members = self.maybe_remove_outside_guests_from_coalitions(guest_to_coalition_members, 
                                                                                      self.get_guests_with_mem(bids, extra))
        self.logger.info("guest_to_coalition_members after removing outside guests: %s" % guest_to_coalition_members)
        coalitions = unique_list(guest_to_coalition_members.values())
        self.logger.info("CSV round,active_coalitions,largest_coalition_size\n")
        self.logger.info("CSV %s\t%s\t%s\n" % (host_round, [c for c in coalitions if c], max([len(c) for c in coalitions])))
        # Calculate new bills with coalitions.
        payments_after_coalitions = payments.copy()
        for coalition in coalitions:
            if len(coalition) == 0:
                continue
            if coalition.values()[0] == "shapley":
                # Calculate shapley value
                #1. calculate the bill for each subgroup
                bills_per_subgroup = self._get_bill_for_all_subgroups(coalition, host_round, auction_mem, bids, extra, sw)
                #2. calculate the new bill for each coalition member: enumerate over all possible orders.
                number_of_orders = math.factorial(len(coalition.keys()))
                for member in coalition.keys():
                    member_bill = bills_per_subgroup[frozenset([member])]
                    sum_of_contributions = 0
                    for order in self._get_all_orders(coalition.keys()):
                        member_index = order.index(member)
                        sum_of_contributions += (bills_per_subgroup[frozenset(order[:member_index])] + # all the players before i 
                                                 member_bill - # i's bill
                                                 bills_per_subgroup[frozenset(order[:member_index+1])]) # all players up to i, including i
                    member_discount = sum_of_contributions*1.0/number_of_orders
                    payments_after_coalitions[member] = payments[member]-member_discount
            else:
                previous_total_bill = sum([payments[member] for member in coalition.keys()])
                extra_wo, sw_wo = self._calculate_alloc_mem(host_round, auction_mem, bids, coalition.keys())
                sw_of_coalition_members = sum([self._guest_SW(bids, extra, member) for member in coalition.keys()])
                new_total_bill = sw_wo - (sw- sw_of_coalition_members)
                for member in coalition.keys():
                    profit_part = coalition[member]
                    payments_after_coalitions[member] = payments[member] - profit_part * (previous_total_bill-new_total_bill)
        self.logger.info("Old bills: %s, New bills: %s" % (payments, payments_after_coalitions))
            

        self._update_history_and_controls_mem(host_round, host, bidders, bids,
                                              extra, sw, payments, payments_after_coalitions, 
                                              guest_to_coalition_members)
        if show:
            data = dict(((b.Prop('name'), (
                ("bid_p_mem", str(round(b.GetVar("bid_p_mem"), 2))),
                ("bid_ranges_mem", b.GetVar("bid_ranges_mem")),
                ("extra", b.GetVar('mem_extra')),
                ("mem", b.GetControl('control_mem')),
                ("payment", str(round(b.GetControl('control_bill_mem'), 2))),))
                         for b in bidders))
            self.logger.info(
                "=== auction: %i, memory: %i ===\n" % (
                    host_round, auction_mem) +
                "\n".join(["    %s: {%s}" %
                           (name, ", ".join(["%s: %s" % v for v in data[name]]))
                           for name in sorted(data.keys())]))

    def _get_all_orders(self, group):
        return itertools.permutations(group)

    def _get_bill_for_group(self, group, host_round, auction_mem, bids, extra, sw):
        extra_wo, sw_wo = self._calculate_alloc_mem(host_round, auction_mem, bids, group)
        sw_of_coalition_members = sum([self._guest_SW(bids, extra, member) for member in group])
        new_total_bill = sw_wo - (sw- sw_of_coalition_members)
        return new_total_bill

    def _get_bill_for_all_subgroups(self, coalition, host_round, auction_mem, bids, extra, sw):
        d = {}
        d[frozenset([])] = 0 
        for size in range(1, len(coalition.keys())+1):
            for subset_as_tuple in itertools.combinations(set(coalition.keys()), size):
               d[frozenset(subset_as_tuple)] = self._get_bill_for_group(subset_as_tuple, host_round, auction_mem, bids, extra, sw)
        return d
    
    def _update_history_and_controls_mem(self, auction_round, host, bidders,
                                         bids, extra, sw, payments, payments_after_coalitions, 
                                         guest_to_coalition):
        # p values, unit SW, total bill, previous alloc and total bill
        self.p_in_min = float('inf')
        self.p_out_max = 0
        total_allocated = 0
        for b, e in zip(bids, extra):
            name = b.name
            bidder = filter(lambda g: g.Prop('name') == b.name, bidders)[0]
            payment = payments_after_coalitions[name]
            alloc = e + bidder.Prop('base_mem')
            coalition_members = {}
            if name in guest_to_coalition:
                coalition_members = guest_to_coalition[name]
            # controls
            bidder.Control('control_mem', alloc)
            bidder.Control('control_bill_mem', payment)
            bidder.Control('control_bill_mem_wo_coalitions', payments[name])
            bidder.Control('control_coalition_members', coalition_members)
            bidder.SetVar('mem_extra', e)
            # databases
            self.total_bills[name] += payment
            self.extra_prev[name] = e
            # history
            bid_max_q = b.rqs[-1][1]
            self.total_bills[name] += payment
            self.extra_prev[name] = e
            total_allocated += e
            if e > 0:
                self.p_in_min = min(self.p_in_min, b.p)
            if e < bid_max_q:
                self.p_out_max = max(self.p_out_max, b.p)
        if self.p_in_min == float('inf'):
            self.p_in_min = 0
        _flag_tie_winners_mem(bidders)
        # parameters used by notifier later
        host.SetVar('p_in_min_mem', self.p_in_min)
        host.SetVar('p_out_max_mem', self.p_out_max)
        host.SetVar('results_round_mem', auction_round)
        self.logger.debug("Round %i p_in_min=%.05f, p_out_max=%.05f",
                          auction_round,
                          self.p_in_min, self.p_out_max)

    def get_bid_mem(self, bidder, curr_round):
        try:
            rnd = int(bidder.GetVar('bid_round_mem'))
            p = float(round(bidder.GetVar('bid_p_mem'), p_digits))
            rq = bidder.GetVar('bid_ranges_mem')
            n = len(rq)
            # check bid values
            if any(map(lambda x: x is None, (rnd, p, rq))):
                raise ValueError("Didn't get bid")
            if rnd != curr_round:
                raise ValueError("Wrong round")
            if p < 0:
                raise ValueError("p is negative")
            if n == 0:
                raise ValueError("No rq list")
            if any((rq[i - 1][1] >= rq[i][0] for i in range(1, n)) or  # r_i-1 >= q_i
                           any((rq[i][0] < 0 or rq[i][1] < 0 or rq[i][0] > rq[i][1]
                                for i in range(n)))):  #r<0, q<0, r>q
                raise ValueError("rq list not ordered")
        except Exception as ex:
            self.logger.warn(
                "Bad bid value of guest %s: p: %s, qr: %s, Error: %s",
                bidder.Prop("name"), str(bidder.GetVar('bid_p_mem')),
                str(bidder.GetVar('bid_ranges_mem')), ex)
            p = 0
            rq = [(0, 0)]

        # set values in bidder entity
        bidder.SetVar('bid_p_mem', p)
        bidder.SetVar('bid_ranges_mem', rq)

        return MPSPBid(bidder.Prop("name"), p, rq, None)


def _flag_tie_winners_mem(bidders):
    # find p value for matches the limit between loosing and winning
    p_tie = None
    for b in bidders:
        if b.GetVar('mem_extra') > 0 and \
                        b.GetVar('mem_extra') not in \
                        [rq[1] for rq in b.GetVar('bid_ranges_mem')]:
            p_tie = b.GetVar('bid_p_mem')
            break
    if p_tie is None:
        return
    # find all bidders with the limit bid value
    matches = filter(lambda b: abs(b.GetVar('bid_p_mem') - p_tie) < p_eps,
                     bidders)
    # tie is only when number of bidders with p_tie is higher then 1
    if len(matches) > 1:
        for b in matches:
            b.SetVar('tie_winner_mem', True)


if __name__ == '__main__':
    # unit tests: one simple test with two guests and forbidden ranges,
    # second is a real assertion fail that occurred
    from mom.Entity import Entity

    # first test: a small 2 guests with forbidden ranges fail
    print "test 1"
    auctioneer = Auctioneer(verbosity='debug')
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    vm1 = Entity()
    vm2 = Entity()
    vm1._set_property('name', 'vm-1')
    vm2._set_property('name', 'vm-2')
    vm1.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 100.0)
    vm1.SetVar('bid_ranges_mem', [(3, 99)])  # 4)])
    vm1._set_property('base_mem', 0)
    vm2.SetVar('bid_p_mem', 99.5)
    vm2.SetVar('bid_ranges_mem', [(98, 100)])
    vm2._set_property('base_mem', 0)
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders, True)

    # second test: a reproduction of a real fail
    print "test 2"
    auctioneer = Auctioneer(verbosity='debug')
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 10700)
    gnames = map(lambda n: 'vm-%i' % n, range(1, 8 + 1))
    bidders = []
    for gname in gnames:
        guest = Entity()
        guest._set_property('name', gname)
        guest._set_property('base_mem', 0)
        guest.SetVar('bid_round_mem', 86)
        bidders.append(guest)

    bidders[0].SetVar('bid_p_mem', 1641.94)
    bidders[1].SetVar('bid_p_mem', 760.28)
    bidders[2].SetVar('bid_p_mem', 503.82)
    bidders[3].SetVar('bid_p_mem', 225.92)
    bidders[4].SetVar('bid_p_mem', 234.74)
    bidders[5].SetVar('bid_p_mem', 168.62)
    bidders[6].SetVar('bid_p_mem', 0)
    bidders[7].SetVar('bid_p_mem', 202.18)

    bidders[0].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[1].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[2].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[3].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[4].SetVar('bid_ranges_mem', [(1536, 1550)])
    bidders[5].SetVar('bid_ranges_mem', [(1963, 1980)])
    bidders[6].SetVar('bid_ranges_mem', [(0, 0)])
    bidders[7].SetVar('bid_ranges_mem', [(1550, 1560)])

    auctioneer.do_auction_mem(host, bidders)

    # third test: r=q
    print "test 3"
    auctioneer = Auctioneer(verbosity='debug')
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm2 = Entity()
    vm1._set_property('name', 'vm-1')
    vm2._set_property('name', 'vm-2')
    vm1.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 100.0)
    vm1.SetVar('bid_ranges_mem', [(99, 99)])  # 4)])
    vm1._set_property('base_mem', 0)
    vm2.SetVar('bid_p_mem', 99.5)
    vm2.SetVar('bid_ranges_mem', [(98, 100)])
    vm2._set_property('base_mem', 0)
    bidders = [vm1, vm2]
    auctioneer.extra_prev['vm-1'] = 3
    auctioneer.extra_prev['vm-2'] = 97
    auctioneer.p0 = 0.02
    auctioneer.do_auction_mem(host, bidders)

    print "test 4"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.p0 = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(99, 99)])#4)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 1)
    vm2.SetVar('bid_ranges_mem', [(2, 100)])
    vm2._set_property('base_mem', 0)
    auctioneer.extra_prev['vm-1'] = 3
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r1, q1], [r2, q2], q1<m<r2
    print "test 5"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.p0 = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(30, 50), (60, 80)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 4)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 55
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r, q], r = m
    print "test 6"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.p0 = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(50, 80)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r1, q1], [r2, q2], q1=m
    print "test 7"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.p0 = 4
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(30, 50), (70, 80)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r1, q1], [r2, q1], m = r2
    print "test 8"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.p0 = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(30, 40), (50, 70)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r, q], r<m<q
    print "test 9"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.p0 = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(40, 60)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5.0)
    vm2.SetVar('bid_ranges_mem', [(0, 40), (65, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)
