\subsection{MOC-Future work}
In a virtualization system, resources and in particular memory are
best allocated dynamically to those who need them most. It is
relatively easy to optimize any target function over the performance
of the guest virtual machines, when the entity optimizing it has all
the data it needs, such as performance measurements and the relative
importance of the different guests. However, in a commercial cloud,
where the guests are owned by clients and not by the cloud
provider, clients do not have an incentive to freely and accurately
share this information, which is viable to the optimization.

We have designed and implemented an intra-physical machine auction
mechanism for the purpose of on-line memory reallocation (using memory
borrowing mechanisms). The mechanism is based on the progressive
second price (PSP) auction, which we enhanced to fit the special case of
memory auctions. The protocol requires the guests to bid for
additional memory, stating how much memory they need and how much they
are wiling to pay for it. If everything else is static, including load, guests can learn their
performance as a function of the amount of physical memory they have
by repeatedly changing memory amounts and measuring performance. Then,
for a given quantity of memory, 
it is a dominant strategy for the guest to bid a price which equals
its valuation of the performance resulting from this memory quantity.

An important question is what quantity of memory to bid for. The guest
would like to maximize its profit, but the price it pays and whether
or not it gets allocated depends on bids other guests make. The guest
must learn the global nature of its neighbors' bids, so that it can
evaluate its profit as a function of the quantity its bids for.

Another decision the guest needs to make is when to exploit the local
optimum it reached, and when to explore (by trying other quantities of
memory), to detect changes in the system's memory pressure through
neighbors' bids. Bid changes might also result from the other guests' strategy
convergence process, even if loads remain constant.

In a real environment the load changes dynamically. In addition, other
resources (such as I\\O or CPU) might become bottlenecks, limiting the
performance. The guest should be able to collect performance
statistics on-line, identify its situation, and extract the impact of
memory on its performance for the current situation. 

Fast ownership changes of memory may make the memory useless to all
the temporary owners, in particular if the memory is used for
caching. One of the adaptations we made to the PSP is long term memory rental:
only $\alpha$ of the memory is lost in each auction round. The guest
keeps renting the rest for the old price in which it won
it. This slows down memory ownership changes. The host needs to learn
the current situation and change the value of $\alpha$
accordingly.

In a slowly changing system, the memory allocations and the overall
bills to pay reach stability after several auction rounds. 
However, in a fast dynamic system with $\alpha<1$, the memory allocation does
not reach stability. Instead, the transient auction results (prices and allocated
memory quantities) become important. In such situation, it is no longer
true that the guest's dominant strategy is to bid a price which equals
its valuation of the memory. Instead, the bid price needs to reflect
the guest's beliefs on the future load it will experience and the
memory pressure the system will be in. For example, the guest might
bid for more memory than it needs because it is currently cheap, if it
anticipates its own load to increase or the prices to rise.

