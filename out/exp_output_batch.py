'''
Created on May 16, 2013

@author: eyal
'''
from out.remote_exp_output import plot
from threading import Thread
import time

if __name__ == '__main__':
    update_files = True
    create_data_files = True
    run_simulation = False

    plots = [
#             ("mix-linear-ones", 4),
#             ("mix-linear-pareto", 4),
#             ("mix-linear-1rich", 6),
#             ("mix-linear-3rich", 6),
#             ("mix-secondorder-ones", 7),
#             ("mix-secondorder-pareto", 7),
#             ("mix-secondorder-1rich", 9),
#             ("mix-secondorder-3rich", 9),

#             ("mc-linear-ones", 4),
#             ("mc-linear-pareto", 4),
#             ("mc-linear-1rich", 6),
#             ("mc-linear-3rich", 6),
#             ("mc-secondorder-ones", 7),
#             ("mc-secondorder-pareto", 7),
#             ("mc-secondorder-1rich", 9),
#             ("mc-secondorder-3rich", 9),
#             ("mc-piecewise60-ones", 5),
#             ("mc-piecewise60-pareto", 5),
#             ("mc-piecewise60-1rich", 5),
#             ("mc-piecewise60-3rich", 5),

#             ("mcd-linear-ones-1", 4),
#             ("mcd-linear-pareto-1", 4),
#             ("mcd-linear-1rich-1", 6),
#             ("mcd-linear-3rich-1", 6),
#             ("mcd-secondorder-ones-1", 7),
#             ("mcd-secondorder-pareto", 7),
#             ("mcd-secondorder-1rich", 9),
#             ("mcd-secondorder-3rich", 9),
#             ("mcd-piecewise1400-ones-1", 5),
#             ("mcd-piecewise1400-pareto", 5),
#             ("mcd-piecewise1400-1rich", 5),
#             ("mcd-piecewise1400-3rich", 5),
#             ("mcd-1-piecewise1400-pareto", 5),
#             ("mcd-1-secondorder-pareto", 5),
#             ("mcd-4-piecewise1400-pareto", 5),
#             ("mcd-4-secondorder-pareto", 5),
#             ("mcd-5-piecewise1400-pareto", 5),
#             ("mcd-5-secondorder-pareto", 5)
#             ("batch-mix-linear-pareto-17", 4),
#             ("batch-mix-linear-pareto-18", 4),
#             ("batch-mix-linear-pareto-19", 4)
#              ("random-seed-mcd", 3)
#              ("batch-Memcached-6", 3)
              ("batch-mcd-others", 3)
             ]

    main_exps = {"mcd-1-piecewise1400-pareto": "ginseng-0.0", "mcd-1-secondorder-pareto": "ginseng-0.0",
                 "mcd-4-piecewise1400-pareto": "ginseng-0.3", "mcd-4-secondorder-pareto": "ginseng-0.3",
                 "mcd-5-piecewise1400-pareto": "ginseng-0.3", "mcd-5-secondorder-pareto": "ginseng-0.3",
                 "random-seed-mcd": "divided", "batch-Memcached-6": "ginseng-0.0", "batch-mcd-others": "divided"
                }

    print plots
    for n, rev_reduction in plots:
#        main_exp = main_exps.get(n, "ginseng")
        main_exp = main_exps.get(n, "ginseng-0.3")
        print main_exp
        filenames = [n]
        maindir = n
        perf_reduction = 1
        plot(filenames = filenames,
            maindir = maindir,
            rev_reduction = rev_reduction,
            perf_reduction = perf_reduction,
            update_files = update_files,
            create_data_files = create_data_files,
            run_simulation = run_simulation,
            main_exp = main_exp)
#        Thread(target = plot, name = n,
#               kwargs = {"filenames": filenames,
#                         "maindir": maindir,
#                         "rev_reduction": rev_reduction,
#                         "perf_reduction": perf_reduction,
#                         "update_files" : True,
#                         "create_data_files": True,
#                         "run_simulation" : True
#                         }).start()
#        time.sleep(10)
