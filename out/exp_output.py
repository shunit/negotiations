'''
Created on Mar 5, 2012
@author: eyal
'''

import matplotlib
matplotlib.use('cairo')
import pylab as pl
import numpy as np

import os
from etc.plots import draw_n_save, pl_apply_defaults
from mom.Plotter import Plotter, HOST, PERF
from exp.core.Loads import LoadTrace
from out.common import calc_best_valuation
from out.common.common import process_description_file, process_traces, get_exp_data, \
    perf_time, mon_time, get_vals, sync_files

# for eval use:
from numpy import array, nan  #@UnusedImport
from exp.core import Loads
from out.exp_output_boxplot import boxplot_create_file, boxplot_plot_file

# select options:
update_files = True
create_data_files = True
run_simulation = True


##mcd-secondorder-ones
#filenames = ["mcd-secondorder-ones-1"]
#maindir = "mcd-secondorder-ones"
#rev_reduction = 5

## mcd-linear-3rich
#filenames = ["mcd-linear-3rich-1"]
#maindir = "mcd-linear-3rich"
#rev_reduction = 5

## mcd-linear-1rich
#filenames = ["mcd-linear-1rich-1"]
#maindir = "mcd-linear-1rich"
#rev_reduction = 5

## mcd-linear-pareto
#filenames = ["mcd-linear-pareto-0", "mcd-linear-pareto-1"]
#maindir = "mcd-linear-pareto"
#rev_reduction = 5

## mcd-linear-ones
#filenames = ["mcd-linear-ones-0", "mcd-linear-ones-1"]
#maindir = "mcd-linear-ones"
#rev_reduction = 1

filenames = ["mcd-test-piecewise1400-pareto"]
maindir = "mcd-test-piecewise1400-pareto"
rev_reduction = 5

#####################################################################

## mc mc-pl-pareto
#filenames = ["batch-MemoryConsumer-%i" % i for i in [20]]
#maindir = "mc-pl-pareto"
#rev_reduction = 9

## mc v e p
#filenames = ["mc-linear-ones", "mc-linear-ones-0"]
#maindir = "mc-v-e-p"
#rev_reduction = 5

## mc square pareto
#filenames = ["batch-MemoryConsumer-%i" % i for i in [22]]
#maindir = "mc-square-pareto"
#rev_reduction = 9

## mcd v = p
#filenames = ["batch-Memcached-%i" % i for i in [257]]
#maindir = "mcd-v-e-p"
#rev_reduction = 4

## mcd square pareto
#filenames = ["batch-Memcached-%i" % i for i in [256]]
#maindir = "mcd-square-pareto"
#rev_reduction = 7

## mcd pl 3rich
#filenames = ["batch-Memcached-%i" % i for i in [252, 255]]
#maindir = "mcd-pl-3rich"
#rev_reduction = 6

if filenames[0].find("Memcached") != -1 or filenames[0].find("mcd") != -1:
    start_min = 20 - 200. / 60
    end_min = 50
    exp_time = 60
    perf_reduction = 4
#    profiler_file = "%s/workspace/moc/doc/profiler-memcached-inside-spare-50-win-500k.xml" % os.path.expanduser("~")
    profiler_file = "%s/workspace/moc/doc/profiler-memcached-inside-spare-50-win-500k-tapuz21.xml" % os.path.expanduser("~")
    sat_dict = {1:0.6, 2:0.7, 3:1.0, 4:1.0, 5:1.2, 6:1.2, 7:1.4, 8:1.6, 9:1.8, 10:1.8}
#    filenames.extend(["mcd-alternatives-%i" % i for i in [255, 256, 257]])
    load_interval = 200
else:
    start_min = 10
    end_min = start_min + int(200. * 4 / 60)
    exp_time = 30
    perf_reduction = 2
    profiler_file = "%s/workspace/moc/doc/profiler-mc-spare-50-satur-2000-tapuz21.xml" % os.path.expanduser("~")
    sat_dict = dict((i, 2.0) for i in range(11))
#    filenames.extend(["mc-alternatives-%i" % i for i in range(2)])
    load_interval = 10

remotedir = "%s/moc-output/exp" % os.path.expanduser("~")
localmaindir = "%s/moc-output/exp-tapuz21/%s" % (os.path.expanduser("~"), maindir)
localdirs = [os.path.join(localmaindir, f) for f in filenames]

rev_position = (0.12, 0.12, 0.85, 0.7)
perf_position = (0.12, 0.12, 0.85, 0.75)
filetype = ".eps"
size = (3.5, 2)

total_memory = float(10012)

ginseng_sim_file = "ginseng_results.txt"
opt_file = "opt_results.txt"

pl_apply_defaults({"legend.fontsize": 6,
                   "axes.labelsize": 6,
                   "xtick.labelsize": 5})

perf_field = "hits_rate"
profiler_entry = "hits_rate"

all_tests = ["divided", "ginseng", "hinted-host-swapping", "hinted-mom"]

test_color = {"divided": (0.5, 0.5, 1.0),
             "ginseng": (1.0, 0.5, 0.5),
             "hinted-host-swapping": (99. / 255, 176. / 255, 99. / 255),
             "hinted-mom": "c"}

test_marker = {"divided": "s",
             "ginseng": "o",
             "hinted-host-swapping": "v",
             "hinted-mom": "^"}

is_auction = lambda name: name.find("ginseng") != -1

iter_vms = lambda data: ["vm-%i" % n for n in sorted([int(k.split("-")[1]) for k in data.iterkeys() if k != HOST])]

def create_data_file(localdir, rev_funcs):
    data, info = get_exp_data(localdir, perf_field, rev_funcs)

    # collect the performance data
    all_perf = {}
    all_rev = {}

    for n in data.iterkeys():
        all_perf[n] = {}
        all_rev[n] = {}
        for test in all_tests:
            all_perf[n][test] = {}
            all_rev[n][test] = {}
            for vm in iter_vms(data[n].values()[0]):
                try:
                    perf_times = np.array(perf_time(data[n][test][vm]))
                    mon_times = np.array(mon_time(data[n][test][vm]))
                    mon_times = mon_times[(mon_times >= start_min) & (mon_times < end_min)]

                    vals = np.array(get_vals(data[n][test][vm][PERF], 'perf'))
                    all_perf[n][test][vm] = [vals[np.argmin(np.abs(perf_times - t))] for t in mon_times]

                    vals = np.array(get_vals(data[n][test][vm][PERF], 'rev'))
                    all_rev[n][test][vm] = [vals[np.argmin(np.abs(perf_times - t))] for t in mon_times]

                except KeyError as ex:
                    pass

    nums = sorted(all_perf.keys())

    def calc_sw(test, vals, n):
        ret = []
        if len(vals[n][test].keys()) == 0:
            return np.nan
        for vm in vals[n][test].keys():
            try:
                r = vals[n][test][vm]
            except KeyError as ex:
                print "num: %i, test: %s: couldn't evaluate performance: %s" % (
                        n, test, ex)
            if len(r) > 0:
                ret.append(np.average(r))
        return sum(np.array(ret))

    res = {}
    for tp, values in [("rev", all_rev), ("perf", all_perf)]:
        res[tp] = {}
        for i, test in enumerate(all_tests):
            res[tp][test] = []
            for n in nums:
                res[tp][test].append(calc_sw(test, values, n))

    file(os.path.join(localdir, "results.txt"), "w").write(str((nums, res)))

def draw_data_file(dirs):
    nums = set()
    results = []
    for d in dirs:
        n, res = eval("".join(file(os.path.join(d, "results.txt")).readlines()))
        res["nums"] = n
        res["test_num"] = d.split("-")[-1]
        results.append(res)
        nums.update(n)

    nums = sorted(nums)

    opt_results = eval("".join(file(os.path.join(localmaindir, opt_file)).readlines()))
    genseng_sim = eval("".join(file(os.path.join(localmaindir, ginseng_sim_file)).readlines()))
    opt = {"rev": [], "perf": []}
    sim = {"rev": [], "perf": []}
    no_limit = {"rev": [], "perf": []}
    static = {"rev": [], "perf": []}
    i_start = int(round(start_min * 60 / 12))
    i_end = int(round(end_min * 60 / 12))
    for n in nums:
        try:
            # average over the rounds, than sum over the guests
            s = sum(map(lambda x: np.average(x['opt-val'][i_start: i_end]), opt_results[n].values()))
            opt["rev"].append(float(s) / (10 ** rev_reduction))
        except:
            opt["rev"].append(np.nan)

        try:
            s = sum(map(lambda x: np.average(x['opt-perf'][i_start: i_end]), opt_results[n].values()))
            opt["perf"].append(float(s) / (10 ** perf_reduction))
        except:
            opt["perf"].append(np.nan)

        # collect ginseng simulation results
        rev = 0
        perf = 0
        rev_static = 0
        perf_static = 0
        for vm in genseng_sim[n].keys():
            if vm == HOST: continue
            rev += np.average(genseng_sim[n][vm]["rev"][i_start: i_end]) / (10 ** rev_reduction)
            perf += np.average(genseng_sim[n][vm]["perf"][i_start: i_end]) / (10 ** perf_reduction)

        sim["rev"].append(rev)
        sim["perf"].append(perf)

        static["rev"].append(rev_static)
        static["perf"].append(perf_static)

    opt["rev"] = np.array(opt["rev"])
    opt["perf"] = np.array(opt["perf"])
    sim["rev"] = np.array(sim["rev"])
    sim["perf"] = np.array(sim["perf"])
    no_limit["rev"] = np.array(no_limit["rev"])
    no_limit["perf"] = np.array(no_limit["perf"])
    static["rev"] = np.array(static["rev"])
    static["perf"] = np.array(static["perf"])

    w = 0.1

    for field, reduction in [("rev", rev_reduction),
                             ("perf", perf_reduction)]:
        pl.clf()
        ax0 = pl.gca()
        # plot values
        res = {}
        avg = {}
        std = {}
        for test in all_tests:

#            if test != "ginseng":
#                continue

            res[test] = [[] for i in range(len(nums))]
            for r in results:
                for n, ri in zip(r["nums"], r[field][test]):
                    if np.isnan(ri): continue
                    ri /= (10 ** reduction)

                    i = nums.index(n)
                    res[test][i].append(ri)

#                    # scatter the points
#                    ax0.plot(n, ri, color = test_color[test],
#                               marker = test_marker[test], markersize = 2,
#                               linestyle = "None")
#                    # text test number
#                    ax0.text(n, ri, r["test_num"], size = 4)

            avg[test] = map(np.average, res[test])
            std[test] = map(np.std, res[test])

            for n, a, s in zip(nums, avg[test], std[test]):
                if np.isnan(a) or np.isnan(s):
                    continue
                ax0.plot([n, n], [max(0, a - s), a + s],
                         color = test_color[test], linewidth = 1)
                ax0.plot([n - w, n + w], [max(0, a - s), max(0, a - s)],
                         color = test_color[test], linewidth = 1)
                ax0.plot([n - w, n + w], [a + s, a + s],
                         color = test_color[test], linewidth = 1)

            ax0.plot(nums, avg[test], label = test if test != "divided" else "static",
                     color = test_color[test],
                     marker = test_marker[test], markersize = 4
                     )

#        # add ginseng improvement text
#        for i in range(len(nums)):
#            if avg["divided"][i] == 0:
#                continue
#            h = avg["ginseng"][i] + 0.06 * (pl.ylim()[1] - pl.ylim()[0])
#            ax0.text(nums[i], h,
#                     "%.1f" % (avg["ginseng"][i] / avg["divided"][i]),
#                     ha = "center", va = "bottom",
#                     backgroundcolor = "w", size = 6
#                     )

        print "OPT:", field, ":", np.array(avg["ginseng"]) / np.array(opt[field])
        print "DIV", field, ":", np.array(avg["ginseng"]) / np.array(avg['divided'])
        print "MOM", field, ":", np.array(avg["ginseng"]) / np.array(avg['hinted-mom'])

        ax0.set_position(rev_position if field == "rev" else perf_position)
        ax0.set_xticks(nums)
        sat = eval("".join(file(os.path.join(localmaindir, "sat.txt"))))
        ax0.set_xticklabels(["%i/%.1f" % (n,
                                  float(sat[n] * 1024 + n * 56 + 500) / total_memory)
                     for n in nums])
        ax0.set_xlim(nums[0] - 0.5, nums[-1] + 0.5)

        ax0.plot(nums, opt[field], "--kx", markersize = 5, label = "Upper Bound")
        ax0.plot(nums, sim[field], "--ro", markersize = 4, label = "Ginseng Simulation")

#        ax0.plot(nums, static[field], "--",
#                 color = test_color["divided"],
#                 marker = test_marker["divided"],
#                 markersize = 4, label = "Static Simulation")
#        ax0.plot(nums, no_limit[field], "-k", label = "No Memory Limits")
#
#        ax0.set_xticklabels([r"%i/%.1f/%.1f" % (n, oci, rs) for n, oci, rs in zip(nums, oc, rss)])
#        if field == "rev":
#            ax0.plot(ax0.get_xlim(), 2 * [theoretical_max / (10 ** reduction)],
#                     "--k", label = "Upper Bound")


        pl.legend(loc = 'upper center', bbox_to_anchor = (0.5, 1.03),
                  bbox_transform = pl.gcf().transFigure,
                  ncol = 3,
                  frameon = False)

        ax0.set_xlabel("Number of VMs / Overcommitment", labelpad = 2)
        if field == "rev":
            ax0.set_ylabel("Social Welfare [$/s]")
        else:
            ax0.set_ylabel(r"Performance [$10^%i$hits]" % reduction, labelpad = 0)

        draw_n_save(os.path.join(localmaindir, "exp-" + maindir + "-" + field + ".eps"),
                    size = size)
        draw_n_save(os.path.join(localmaindir, "exp-" + maindir + "-" + field + ".png"),
                    size = size)

def get_loads_trace(path):
    loads = {}
    folders = os.listdir(path)
    nums = sorted([int(f.split("-")[-1])
               for f in folders if f.startswith("num-") and os.path.isdir(os.path.join(path, f))])

    for n in nums:
        read_dir = os.path.join(path, "num-%i" % n)
        # read the experiment output
        try:
            data = file(os.path.join(read_dir, "ginseng", "exp-plotter")).readlines()
            data = Plotter.parse(data)
        except:
            continue
        loads[n] = {}
        for vm in data.keys():
            if vm == HOST: continue
            try:
                loads[n][vm] = LoadTrace([x["time"] for x in data[vm][PERF]], [x["load"] for x in data[vm][PERF]])
            except KeyError:
                print "no reults in experiment ", n, vm

    return loads

if __name__ == '__main__':
    if update_files:
        if not os.path.exists(localmaindir): os.mkdir(localmaindir)
        for f in filenames:
            sync_files(os.path.join(remotedir, f),
                       os.path.join(localmaindir, f),
                       ["bidder-*", "exp-out*", "prog-*"])

    if create_data_files:
        process_description_file(localdirs, localmaindir, sat_dict)

    rev_funcs = eval("".join(file(os.path.join(localmaindir, "valuations.txt"))))

    if create_data_files:
        process_traces(localdirs[0], localmaindir, perf_field, rev_funcs)
        for f in localdirs:
            create_data_file(f, rev_funcs)
            boxplot_create_file(f, profiler_file, perf_field, start_min, end_min)

    loads = eval("".join(file(os.path.join(localmaindir, "loads.txt"))))
    loads_trace = eval("".join(file(os.path.join(localmaindir, "trace_load.txt"))))

    if run_simulation:
        opt_results, ginseng_results, no_mem_limit, static = calc_best_valuation(
                                         loads_trace, rev_funcs,
                                         optimization_res = 20, base_mem = 600,
                                         profiler_file = profiler_file,
                                         profiler_entry = profiler_entry,
                                         load_interval = load_interval,
                                         rounds = exp_time * 60 / 12
                                         )
        file(os.path.join(localmaindir, "opt_results.txt"), "w").write(str(opt_results))
        file(os.path.join(localmaindir, "ginseng_results.txt"), "w").write(str(ginseng_results))
    draw_data_file(localdirs)
    pl.clf()
    boxplot_plot_file(localdirs, localmaindir)
