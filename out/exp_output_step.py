'''
Created on Mar 5, 2012

@author: eyal
'''

import matplotlib
from out.common.common import perf_time, mon_time, get_vals
from sim.simulate import HOST
matplotlib.use('cairo')
import pylab as pl
import numpy as np
import sys

import os
from mom.Plotter import Plotter, PERF, MON
from etc.plots import draw_n_save, multifunction, pl_apply_defaults
from matplotlib.transforms import Bbox
from etc.Settings import Settings
from out.common.common import sync_files

def init_globals(args):
    global paper_mode
    global length
    global expname
    global remotedir
    global localdir
    global gylabels
    global num2Kper_sec
    global gposition
    global bar_width
    global xlims
    global params
    global exp
    global perf_fields
    global file_type
    global size
    global t0
    global start
    global end
    global def_markers

    paper_mode = "paper" in args

    length = 10
    expname = args[1] #"pgbench-1"
    remotedir = "%s/moc-output/step/%s" % (Settings.user_home(), expname)
    localdir = "%s/moc-output/step-%s/%s" % (Settings.user_home(), Settings.hostname(),
                                             expname)

    try: os.makedirs(localdir)
    except: pass

    pl_apply_defaults()
    gylabels = ["Performance [1/s]", "Memory [MB]"]
    num2Kper_sec = lambda x: float(x) / length
    gposition = (0.1, 0.22, 0.78, 0.6)  #(0.08, 0.22, 0.8, 0.6)
    bar_width = 5
    xlims = (30, 250)  #(300, 1500)

    params = {'axes.labelsize': 6,
              'text.fontsize': 6,
              'legend.fontsize': 5,
              'font.size' : 8,
              'xtick.labelsize': 6,
              'ytick.labelsize': 6}
    pl.rcParams.update(params)


    if expname.find("MC") != -1:
        exp = "mc"
        perf_fields = ["hits", "throughput"]
    elif expname.find("memcached") != -1:
        exp = "memcached"
        perf_fields = ["get_hits_total", "get_total"]
    elif expname.find("pgbench") != -1:
        exp = "pgbench"
        perf_fields = ["tps_with_connections_time", "tps_without_connections_time"]

    if paper_mode:
        file_type = ".eps"
    else:
        file_type = ".png"

    size = (3.5, 1.2)
    t0 = 3700
    start = 8
    end = 12

    def_markers = lambda n: [pl.Line2D.filled_markers[i] for i in range(n)]

def exp_plot(data, funcs, axes, plot_kwargs):
    vm = sorted(set(data.keys()).difference(set((HOST,))))[0]
    all_xy = [[]]
    xmin = 100000000
    xmax = -1
    try:
        for i in range(len(funcs)):
            xi = funcs[i][0](data[vm])
            yi = funcs[i][1](data[vm])
            xi = xi[-len(yi):]

            xj = []
            yj = []
            for xx, yy in zip (xi, yi):
                if 0 <= xx <= 100000:
                    xj.append(xx)
                    yj.append(yy)
            xmin = min(xmin, min(xj))
            xmax = max(xmax, max(xj))
            all_xy[-1].append((xj, yj))


#            xmin = min(xmin, min(xi))
#            xmax = max(xmax, max(xi))
#            all_xy[-1].append((xi, yi))
    except Exception as ex:
        print "error: %s" % ex
    step_plot(all_xy, axes, plot_kwargs, xlims)  #(xmin, xmax))

def draw_exp(folder):
    read_dir = os.path.join(localdir, folder)
    prefix = folder

    # memcached view
    if exp == "memcached":
        data = {}
        for line in file(os.path.join(read_dir, "prog-vm-1")):
            if line.find("target") == -1 or line.find("usage") == -1:
                continue
            for tup in line.split(" - ")[-1].split(", "):
                k, v = tup.split(": ")
                data.setdefault(k, []).append(v)

        pl.figure(figsize = (20, 20))
        for k, v in data.iteritems():
            pl.plot(v, label = k)
        pl.legend()
        draw_n_save(os.path.join(localdir, "%s-memcached" % (prefix) + file_type), size)

    # read the experiment output

    data = Plotter.parse(file(os.path.join(read_dir, "exp-plotter")).readlines())

    funcs = []
    plt_kw = []
    axes = []

    # add performance graphs
    for prop in reversed(perf_fields):

        def perf_func(prop):
            return lambda dct: (np.array(get_vals(get_vals(dct[PERF], 'perf'), prop)) /
                                np.array(get_vals(dct[PERF], 'duration')))

        funcs.append((perf_time, perf_func(prop)))
#         plt_kw.append({"label":prop, "alpha": 0.5, "linewidth": 0})
        plt_kw.append({"label":prop, "alpha": 1.0, "linewidth": 0})
        axes.append(0)

    funcs.append((mon_time, lambda dct: get_vals(dct[MON], 'mem_available')))
    plt_kw.append({"label":"Memory", "color":"r", "marker": "None", "linestyle": "-"})
    axes.append(1)

    funcs.append((mon_time, lambda dct: get_vals(dct[MON], 'mem_unused')))
    plt_kw.append({"label":"Unused Memory", "color": "k", "marker": "None", "linestyle": ":"})
    axes.append(1)

#    funcs.append((mon_time, lambda dct: get_vals(dct[MON], 'cache_and_buff')))
#    plt_kw.append({"label":"Buffers and Cache", "marker": "None", "linestyle": "--"})
#    axes.append(0)

#    funcs.append((perf_time, lambda dct: get_vals(dct[PERF], 'load')))
#    plt_kw.append({"label":"Load", "marker": "None", "linestyle": "--"})
#    axes.append(4)

#    funcs.append((mon_time, lambda lst: get_vals(lst[MON], 'cpu_usage')))
#    plt_kw.append({"label":"CPU", "marker": "None", "linestyle": "--"})
#    axes.append(2)

#    funcs.append((mon_time, lambda lst: get_vals(lst[MON], 'major_fault')))
#    plt_kw.append({"label":"Major Faults", "marker": "o", "linestyle": "None", "alpha": 0.5})
#    axes.append(3)

    pl.clf()
    pl.gca().set_position(gposition)
    exp_plot(data, funcs, axes, plt_kw)
    pl.xlabel("Time [s]")
    draw_n_save(os.path.join(localdir, "%s" % (prefix) + file_type), size)
#    draw_n_save(os.path.join(localdir, "step-memcached-%s" % length + file_type), size)

def step_plot(data, axes, plot_kwargs, xlim = None, ylim = None, ylabels = None):

    all_axes = []
    lines = []
    max_y = [0] * (max(axes) + 1)
    if not ylabels:
        ylabels = [kw["label"] for kw in plot_kwargs]

    num = len(data)

    x0 = 0.05
    x1 = 0.85
    y0 = 0.1
    y1 = 0.95
    height = (y1 - y0) / num if num else 0

    for plot_i, subplot_funcs in enumerate(data):
        lines = []
        pl.subplot(num, 1, plot_i + 1)

        sp_axes = [pl.gca()]
        for i in range(max(axes)):
            sp_axes.append(pl.twinx(sp_axes[0]))

        all_axes.append(sp_axes)

        position = 0
        labels = []
        for i, xy in enumerate(subplot_funcs):
            # update max y
            max_y[axes[i]] = max(max_y[axes[i]], max(xy[1]))

            # draw the line
            ax = sp_axes[axes[i]]
            if "color" not in plot_kwargs[i]:
                plot_kwargs[i]["color"] = pl.cm.jet(float(i + 1) / len(subplot_funcs))  # @UndefinedVariable

            if axes[i] != 0:
                lines.append(ax.plot(xy[0], xy[1], **plot_kwargs[i])[0])
            else:
                lines.append(ax.bar(np.array(xy[0]) + position, xy[1], bar_width, 0, **plot_kwargs[i])[0])
                position += bar_width / 2

            labels.append(plot_kwargs[i]["label"])

        # set axes y labels
        for ax_ind, ax in enumerate(sp_axes):
            ax.set_ylabel(gylabels[ax_ind])

        # show only last plot x ticks
        if plot_i < num - 1:
            for ax in sp_axes:
                ax.get_xaxis().set_ticklabels([])

    # make all axes in all subplots with the same limits
    for sp_axes in all_axes:
        for i, ax in enumerate(sp_axes):
            ax.set_ylim((0 , max_y[i]))
            if xlim:
                ax.set_xlim(*xlim)
            if ylim:
                ax.set_ylim(*ylim)


    legend_cols = len(axes) / len(ylabels)
    if legend_cols == 1: legend_cols = len(ylabels)

    pl.legend(lines, [l.replace("_", " ").capitalize() for l in labels],
              loc = 'upper center', bbox_to_anchor = (0.5, 1),
              bbox_transform = pl.gcf().transFigure,
              ncol = legend_cols)
    if all_axes:
        all_axes[-1][0].set_xlabel("Time [s]")

if __name__ == '__main__':
    init_globals(sys.argv)
    sync_files(remotedir, localdir, ["bidder-*", "exp-out"])

    for folder in [f for f in os.listdir(localdir)
                   if os.path.isdir(os.path.join(localdir, f))]:

        print "Drawing: ", folder
        draw_exp(folder)
