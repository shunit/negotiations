import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse
import os
import re
import json

#experiment_id = 202
#num_guests = 5

from mom.Advisers.ActiveNegotiations.AgentStrategy import *

GUEST_ID_INDEX = 1
ROUND_NUMBER_INDEX = 0
HOST_CSV_FIELDS = 3
USER = "guypeled"
EXPERIMENTS_FOLDER_FORMAT = "/home/" + USER + "/moc-output/exp/coalitions-%d"
#EXPERIMENTS_FOLDER_FORMAT = "E:\\University\\CloudProject2019\\coalitions-%d"
COLORS = ['r', # red
          '#8BC34A', # light green
          '#FFEB3B', # yellow
          'b', # blue
          '#F06292', # pink
          '#FF5722', # orange
          '#9C27B0', # purple
          '#6D4C41', # brown
          '#546E7A', # gray
          '#2E7D32', #dark green
         ]
BASE_MEM = 653

class GraphItem:
    def __init__(self, sid, rnd, ov, is_accepted, desc, obj):
        self.sender_id = sid
        self.round = rnd
        self.ov = ov
        self.is_accepted = is_accepted
        self.desc = desc
        self.GraphObj = obj

class VmInfo:
    def __init__(self, id, name, ov, at):
        self.id = id
        self.name = name
        self.ov = ov
        self.at = at

def get_exp_path(exp_id):
    return EXPERIMENTS_FOLDER_FORMAT % exp_id

def get_exp_out_path(exp_id):
    return os.path.join(get_exp_path(exp_id), "p0-0.00-freq-60")

def read_serialized_vms_graphs_from_logs(log_files):
  # Open the file and read only CSV lines
  vms_graphs = []
  for fname in log_files:
    f = open(fname,"r")
    file_lines = f.readlines()
    graph_items = [parse_graph_from_line(l) for l in file_lines if ("XXXS" in l and "|" in l)]
    print("graph_items in %s: %d" % (fname, len(graph_items)))
    vms_graphs.append(graph_items)
    f.close()
  return vms_graphs

def parse_graph_from_line(line):
    FIRST_ANCHOR = "XXXS"
    # sender_id, rnd, ov, is_accepted, description, strategy.serialize_snapshot()
    idx = line.index(FIRST_ANCHOR)
    startIdx = idx + len(FIRST_ANCHOR)
    sub = line[startIdx : len(line)]
    tokens = sub.split('|')
    senderId = int(tokens[0])
    round = int(tokens[1])
    ov = float(tokens[2])
    is_accepted = bool(tokens[3])
    desc = str(tokens[4])
    obj = tokens[5]
    return GraphItem(senderId, round, ov, is_accepted, desc, obj)

def eval_info_file_content(info):
    '''
    Serialize 'info' file with the dictionary data
    Remove objects from info text. Something like "< Bla Bla >"
    '''
    DICT_OBJ_PATTERN = re.compile("<[^<>]*>")
    count = -1
    while count != 0:
        info, count = DICT_OBJ_PATTERN.subn("None", info)
    return eval(info)

def get_vms_info(info_file):
    vms_info = {}
    # read vms info
    f = open(info_file, "r")
    info_dictionary = eval_info_file_content(f.readlines()[0])
    for key in info_dictionary['vms'].keys():
        vm = info_dictionary['vms'][key]
        vm_id = int(vm['adviser_args']['vm_number'])
        vm_name = str(vm['adviser_args']['name'])
        vm_at = float(vm['adviser_args']['accept_threshold'])
        vm_ov = float(vm['adviser_args']['offer_value'])
        vms_info[vm_id] = VmInfo(vm_id, vm_name, vm_ov, vm_at)
    f.close()
    return vms_info

def save_graph(fname, title, offer_values_dic, xs, ys, functions_x_points, functions_y_points_arr,
                               functions_names, threshold=-1, last_point=None):

    fig, ax = plt.subplots()
    fig.set_tight_layout(True)

    plt.xlim((0, 1))
    plt.ylim((0, 2))

    #ax.errorbar(x, y, yerr=error, fmt='--o', ecolor='r', capsize=5, elinewidth=1, capthick=1)

    ax.set_title(title)

    for i in range(len(ys)):
        cur_ov = xs[i]  #round(xs[i], 2)
        plt.plot(cur_ov, ys[i], 'ro')  # plot points
        #plot point info
        if cur_ov == 0 or cur_ov == 1:
            plt.text(cur_ov, ys[i], str(cur_ov), fontsize=9)
        else:
            offers = offer_values_dic[cur_ov]
            p_info = ""
            if len(offers) == 1:
                p_info = "%.2f (#%i-%s)" % (cur_ov, offers[0].round, offers[0].desc)
            else:
                list_of_rounds = ["#%i:%s-%s" % (o.round, 'T' if o.is_accepted else 'F', o.desc) for o in offers]
                p_info_list = "%s\n" % ("\n".join(list_of_rounds))
                plt.text(xs[i], ys[i]+0.025, p_info_list, fontsize=6, color='silver')
                p_info = "%.2f" % (cur_ov)
            last_offer = offers[0]

            plt.text(xs[i], ys[i], p_info, fontsize=8)

    # plot functions
    for f_idx in range(len(functions_y_points_arr)):
        f_ys = functions_y_points_arr[f_idx]
        plt.plot(functions_x_points, f_ys, label=functions_names[f_idx])

    if threshold is not -1:
        plt.axvline(x=threshold, color='k', linestyle='--', label='Threshold')

    if last_point is not None:
        plt.plot(last_point[0], last_point[1], 'go')
        plt.text(last_point[0], last_point[1], "%.2f" % (last_point[0]), fontsize=9)

    ax.legend()
    plt.savefig(fname, format="png")
    plt.close(fig)

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Create graphs from experiments.')
  parser.add_argument("--num_guests", type=int, default=10, help='The number of guests in the experiment, e.g., 5.')
  parser.add_argument("--min_exp_id", type=int, default=0, help="define a range of exp ids to work on. Use with max_exp_id.")
  parser.add_argument("--max_exp_id", type=int, default=0, help="define a range of exp ids to work on. Use with min_exp_id.")
  parser.add_argument("--smart_agents_count", type=int, default=1,
                      help="define a range of smart agents ids from 1 to X.")

  args = parser.parse_args()
  num_guests = args.num_guests
  smart_agents_count = args.smart_agents_count

  guest_ids = [i for i in range(1, num_guests+1)]
  smart_ids = [i for i in range(1, smart_agents_count+1)]

  if args.min_exp_id == -1 or args.max_exp_id == -1:
      print("ERROR MIN/MAX EXP IDs")
      sys.exit()

  for experiment_id in range(args.min_exp_id, args.max_exp_id + 1):

      info_file = os.path.join(get_exp_out_path(experiment_id), "info")
      vms_info = get_vms_info(info_file)
      with open( "%s_2" % (info_file) , 'w') as outfile:   # save json as "info_2"
          for k in vms_info.keys():
              vm = vms_info[k]
              line = 'vm: %s, name: %s, at: %s, ov: %s\n' % (str(k), str(vm.name), str(vm.at), str(vm.ov))
              outfile.writelines(line)

      print("Reading Graphs..")
      # bidder_files = ["/home/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem" %
      #                (experiment_id, i) for i in smart_ids]
      bidder_files = [os.path.join(get_exp_out_path(experiment_id), "bidder-vm-%d-mem" % i) for i in smart_ids]
      smart_vms_graphs = read_serialized_vms_graphs_from_logs(bidder_files)

      print("Creating Graphs..")
      new_smart_dir_path = os.path.join(get_exp_path(experiment_id), "smart-agents")
      if not os.path.exists(new_smart_dir_path):
        os.mkdir(new_smart_dir_path)

      for i in smart_ids:   # Iterate over smart agents
          vm_path = os.path.join(new_smart_dir_path, "vm_%d" % i)
          if not os.path.exists(vm_path):
            os.mkdir(vm_path)

          graphs = smart_vms_graphs[i-1]

          for sender_id in range(1, num_guests+1):  # Iterate over all guests that talked with smart agent i

              last_graph = None
              offer_values_dic = {} # {0.1: [snapshot, ..], x2: [snapshot], ..}
              for gr in graphs:
                  if gr.sender_id is not sender_id:
                      continue
                  # sid, rnd, ov, is_accepted, desc, obj
                  cur_ov = round(gr.ov, 2)
                  if cur_ov not in offer_values_dic:
                      offer_values_dic[cur_ov] = [gr]
                  else:
                      offer_values_dic[cur_ov].append(gr)

                  last_graph = gr

              if last_graph == None:    # skip vm == sender_id (self)
                  continue

              agent = EpsilonGreedyAgentStrategy(i)
              agent.deserialize_snapshot(last_graph.GraphObj)
              xs, ys = agent.mgr.getAllPointsUnique()  # get all (x,y)

              if len(xs) < 3:
                  print(" %d/%d SKIPPED, less than 3 points.", i, len(smart_ids))
                  continue

              graph_mgr = GraphMgr(xs, ys)  # build a graph manager accordingly
              xs, ys, functions_x_points, functions_y_points_arr, functions_names = graph_mgr.getPlotPoints()
              plot_fname = os.path.join(vm_path, "grpah_vm_%d.png" % sender_id)
              title = "vm: %s (%s), Round %s" % (sender_id, vms_info[sender_id].name, last_graph.round)
              save_graph(plot_fname, title, offer_values_dic, xs, ys, functions_x_points, functions_y_points_arr,
                     functions_names, threshold=vms_info[sender_id].at, last_point=None)
          print("%d/%d done." % (i, len(smart_ids)))

      print("Done.")