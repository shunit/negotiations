'''
Created on Feb 11, 2013

@author: eyal
'''
import matplotlib
import sys
from matplotlib.ticker import FormatStrFormatter
matplotlib.use('Agg')
import pylab as pl
import os
import collections
from sim.SimResultsProcess import get_tools
from sim.simulate import HOST
import numpy as np
from etc.plots import nice_surf, draw_n_save, pl_apply_defaults, nice_surf2
from out.common.common import sync_files, copy_files
from numpy import array  #@UnusedImport


def parse_fname(fname):
    p0 = None
    oc = None
    parts = fname.split("-")
    for i in range(len(parts)):
        if parts[i] == "oc":
            oc = float(parts[i + 1])
        elif parts[i] == "p0_mem":
            p0 = float(parts[i + 1])
    return p0, oc

if __name__ == '__main__':
    sim_name = "sim-p0-mcd-1"

    if len(sys.argv) > 1:
        sim_name = sys.argv[1]

    prefix = "sim-" + ("mcd" if sim_name.find("mcd") != -1 else "mc") + "-static-"

    localdir = "%s/sim-output/%s" % (os.path.expanduser("~"), sim_name)
    remotedir = "%s/moc-output/sim/%s" % (os.path.expanduser("~"), sim_name)
    fig_size = (3.6, 3.6 * 2.0 / 2.6)
    fig_pos = (0.17, 0.15, 0.6, 0.8)  #[left, bottom, width, height]
    cbar_pos = (0.8, 0.15, 0.1, 0.8)  #[left, bottom, width, height]
    pl_apply_defaults()

    outdir = os.path.join(localdir, "plots-eps")
    pngoutdir = os.path.join(localdir, "plots-png")
    datadir = os.path.join(localdir, "data")

    if not os.path.exists(localdir): os.mkdir(localdir)
    if not os.path.exists(outdir): os.mkdir(outdir)
    if not os.path.exists(pngoutdir): os.mkdir(pngoutdir)
    if not os.path.exists(datadir): os.mkdir(datadir)
    copy_files(remotedir, datadir, ["bidder-*", "exp-out*", "prog-*", "plots"])
    datadir = os.path.join(datadir, sim_name)

    ## read data from files
    opt = {}
    res = collections.defaultdict(dict)
    mem = set()
    p0s = set()

    for f in os.listdir(datadir):
        if f.startswith("sim"):
            p0, oc = parse_fname(f)
            res[oc][p0] = eval(file(os.path.join(datadir, f)).readline())
            mem.add(oc)
            p0s.add(p0)
        elif f.startswith("opt"):
            p0, oc = parse_fname(f)
            opt[oc] = eval(file(os.path.join(datadir, f)).readline())

    mem = sorted(list(mem))
    p0s = sorted(list(p0s))

    ## prepare main results dictionary, contains matrices of values:
    results = {}
    sz = (len(mem), len(p0s))
    tools = get_tools()
    for name, t in tools.items():
        results[name] = np.ndarray(sz)

    # iterate all evaluation points
    for i, m in enumerate(mem):
        for j, a in enumerate(p0s):

            if set(p0s) != set(res[m].keys()):
                continue

            tools = get_tools()
            for name, data in res[m][a].iteritems():
                # process the host and guests
                if name == HOST:
                    for t in tools.itervalues(): t.process_host(data)
                else:
                    # add the optimized results to the simulation results
                    data.update(opt[m][name])
                    for t in tools.itervalues(): t.process_guest(data)

            # add the simulation value to the result:
            for name, t in tools.items():
                results[name][i, j] = t.finish()

    sw_max = np.max(results["social-welfare"])
    file(os.path.join(localdir, "sw_max.txt"), "w").write(str(sw_max))

    for name, vals in results.iteritems():
        if name == "perf_waste":
            continue
        colorbar_suffix = ""
        zlim = None
        cbar_ticks = None
        cbar_formatter = "%i"

        if sim_name.find("mcd") != -1:
            if name == "social-welfare":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "utility":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "host-revenue":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "poa":
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 0.06, 0.05)
            elif name == "waste":
                cbar_ticks = range(0, 1501, 300)
            elif name == "ties":
                cbar_formatter = "%.3f"
                cbar_ticks = np.arange(0.0, 1.01, 0.2)
        else:
            if name == "social-welfare":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "utility":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "host-revenue":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "poa":
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 0.06, 0.05)
            elif name == "waste":
                cbar_ticks = range(0, 1501, 300)
            elif name == "ties":
                cbar_formatter = "%.2f"
                cbar_ticks = [0, 1e-5]

        pl.clf()
#        nice_surf(p0s, mem, vals, zlim = zlim,
#                  cbar_pos = cbar_pos,
#                  cbar_formatter = cbar_formatter,
#                  cbar_ticks = cbar_ticks)
        nice_surf2(p0s, mem, vals,
                  cbar_pos = cbar_pos,
                  cbar_formatter = cbar_formatter)
        pl.gca().set_position(fig_pos)
        pl.xlim([min(p0s), max(p0s)])
        pl.gca().set_xticks(np.arange(min(p0s), max(p0s) + 0.1, 0.2))
        pl.gca().xaxis.set_major_formatter(FormatStrFormatter("%.1f"))
        pl.ylim([min(mem), max(mem)])

        pl.xlabel(r"$p_0$ out of minimal accepted $p$")
        pl.ylabel("Memory Overcommitment")
        pl.grid(True)
        draw_n_save(os.path.join(outdir, prefix + name + ".eps"), fig_size)
        draw_n_save(os.path.join(pngoutdir, prefix + name + ".png"), fig_size)


    for p0 in p0s:
        pl.clf()
        for name, vals in results.iteritems():
            if name not in ("social-welfare", "utility", "host-revenue"):
                continue
            vals = vals / sw_max
            pl.plot(mem, vals[:, p0s.index(p0)], label = name)

        pl.legend(loc = "best")
        pl.xlabel("Overcommitment")
        pl.ylabel(r"% of $\max{SW}$")
        pl.gca().set_position(fig_pos)
        draw_n_save(os.path.join(outdir, prefix + ("p0=%.1f" % p0) + ".eps"), fig_size)
        draw_n_save(os.path.join(pngoutdir, prefix + ("p0=%.1f" % p0) + ".png"), fig_size)

