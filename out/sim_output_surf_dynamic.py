'''
Created on Feb 26, 2013

@author: eyal
'''
import os
from numpy import *  #@UnusedWildImport
import pylab as pl
import numpy as np
from etc.plots import draw_n_save, nice_surf
from out.common.common import sync_files

sim_name = "dynamic_mcd-oc-1.50-2"
remotedir = "%s/moc-output/sim/%s" % (os.path.expanduser("~"), sim_name)
localdir = "/home/eyal/moc-output/sim-dshad4/" + sim_name
#localdir = "/home/eyal/moc-output/sim/" + sim_name
outdir = os.path.join(localdir, "plots")
pngoutdir = os.path.join(localdir, "png-plots")
datadir = os.path.join(localdir, "data")

fig_pos = (0.2, 0.17, 0.6, 0.75)  #[left, bottom, width, height]
cbar_pos = (0.82, 0.17, 0.1, 0.75)  #[left, bottom, width, height]

if __name__ == '__main__':
    if not os.path.exists(localdir): os.mkdir(localdir)
    if not os.path.exists(outdir): os.mkdir(outdir)
    if not os.path.exists(pngoutdir): os.mkdir(pngoutdir)
    if not os.path.exists(datadir): os.mkdir(datadir)

    sync_files(remotedir, datadir, ["sim-*", "info-*"])

    results = eval("".join(file(os.path.join(datadir, "all-results")).readlines()))
    a = results['alphas']
    T = 1. / array(results['Ts'])

    normalize_value = np.max(np.max(results['social-welfare']))
    print normalize_value

    for name in (
                 'ties',
                 'utility',
                 'host-revenue',
                 'social-welfare',
                 ):

        vals = array(results[name])
        cbar_ticks = None  #np.arange(0.0, 1.01, 0.1)
        cbar_formatter = "%.2f"
        if sim_name.find("mcd") != -1:
            if name == "social-welfare":
                vals /= normalize_value
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "utility":
                vals /= normalize_value
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "host-revenue":
                vals /= normalize_value
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "ties":
                cbar_ticks = np.arange(0, 1.01, 0.2)
                cbar_formatter = "%.1f"
        else:
            if name == "social-welfare":
                vals /= normalize_value
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "utility":
                vals /= normalize_value
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "host-revenue":
                vals /= normalize_value
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "ties":
                cbar_ticks = np.arange(0, .02, 0.01)
                cbar_formatter = "%.2f"

        pl.clf()
        nice_surf(a, T, vals, cbar_pos = cbar_pos, cbar_formatter = cbar_formatter,
                  cbar_ticks = cbar_ticks)
        pl.gca().set_position(fig_pos)
        pl.xlabel("Reclaim Factor")
        pl.ylabel("Dynamicity (" + r"$T_{auction} / T_{load}$" + ")")
        pl.gca().set_yscale("log")
        pl.xticks([0.1, 0.4, 0.7, 1.0])
        pl.gca().yaxis.set_major_formatter(pl.FormatStrFormatter("%.2f"))
        draw_n_save(os.path.join(outdir, name + ".eps"), (3.5, 2.5))
        draw_n_save(os.path.join(pngoutdir, name + ".png"), (3.5, 2.5))


#        pl.clf()
#        pl.plot(T, best_a)
#        pl.xlabel(r"$T_{load} / T_{auction}$")
#        pl.ylabel("Best Reclaim Factor")
#        pl.gca().set_xscale("log")
#        pl.gca().xaxis.set_major_formatter(pl.FormatStrFormatter("%i"))
#        draw_n_save(os.path.join(outdir, "assaf-" + name + ".png"), (10, 10))




