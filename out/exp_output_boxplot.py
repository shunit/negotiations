'''
Created on Mar 4, 2013

@author: eyal
'''

from numpy import array  #@UnusedImport
import pylab as pl
import numpy as np
from etc.plots import pl_apply_defaults, draw_n_save
import os
from mom import Profiler
from out.common.common import get_exp_data, iter_vms, sync_files, get_vals, perf_time, get_time_from_type
from mom.Plotter import PERF, MON


def boxplot_create_file(path, profiler_file, field, start_min, end_min, main_exp = "ginseng"):
    data, info = get_exp_data(path, field, None)
    profiler = Profiler.fromxml(profiler_file, field)

    # collect the performance data
    all_perf = {}
    for n in data.iterkeys():
        all_perf[n] = {}
        # print data[n][main_exp]
        for vm in iter_vms(data[n].values()[0]):
            try:
                # print data[n][main_exp]
                vals = get_vals(data[n][main_exp][vm][PERF], 'perf')
                # print vals
                # times = perf_time(data[n][main_exp][vm])
                times = get_time_from_type(data[n][main_exp][vm], plot_type=PERF)
                # print times
                vals = [float(v) for t, v in zip(times, vals) if start_min <= t <= end_min]
                all_perf[n][vm] = vals[1:]

                print len(vals), " - ", n, vm

                ls = get_vals(data[n][main_exp][vm][PERF], 'load')
            except KeyError as ex:
                print "AAAAAAAAAAAAAA"
                pass

    # print all_perf

    nums = sorted(all_perf.keys())

    P = []
    L = []
    M = []
    for i in nums:
        if not data.has_key(i) or not data[i].has_key(main_exp):
            continue
        for j, vm in enumerate(iter_vms(data[i][main_exp])):

            perf_data = data[i][main_exp][vm][PERF]
            try:
                mon_data = data[i][main_exp][vm][MON]
            except KeyError:
                print "ERROR! No monitor data for num: %i, guest: %s" % (i, vm)
                mon_data = []

            p = np.array(get_vals(perf_data, 'perf'))
            l = np.array(get_vals(perf_data, 'load'))
            t = np.array(get_vals(perf_data, 'time'))
            dt = np.array(get_vals(perf_data, 'duration'))
            mt = np.array(get_vals(mon_data, 'time'))
            p = p  # / dt

            allm = np.array(get_vals(mon_data, 'mem_available'), float)

            def avg(arg):
                try:
                    return np.average(arg) if len(arg) > 0 else 0
                except AttributeError as er:
                    return 0

            limit = [avg(allm[list(
                 set.intersection(set(np.where(ti - .5 * dti <= mt)[0]),
                                 set(np.where(mt < ti + .5 * dti)[0])))])
                 for ti, dti in zip(t, dt)]
            limit = np.array(limit)

            t = (t - t[0]) / 60

            trim = (start_min <= t) & (t <= end_min) & (np.isnan(p) == 0) & \
                        (np.isnan(l) == 0) & (np.isnan(limit) == 0)

            p = p[trim]
            l = l[trim]
            limit = limit[trim]

            P.extend(p)
            L.extend(l)
            M.extend(limit)

    P = np.array(P)
    L = np.array(L)
    M = np.array(M)

    # perf(load,mem)
    loads = set(L)
    for k, li in enumerate(sorted(loads)):
        ind = np.where(L == li)
        pl.plot(M[ind], P[ind],
                markersize = 6,
                marker = "o",
                linestyle = "None",
                label = "load: " + str(li),
#                 alpha = 0.5,
                 alpha = 1.0,
                color = pl.cm.jet(float(li - 1) / max(L)))  # @UndefinedVariable

    # predicted performance from testbed vs. actual performance
    pl.clf()
    p_eval = np.array(map(lambda t: float(profiler.interpolate(t)), zip(L, M)))

    p_eval_sorted = np.array(sorted(list(set(p_eval))))
    xvals = []
    yvals = []
    for pi in p_eval_sorted:
        lst = P[p_eval == pi]
        xvals.append(pi)
        yvals.append(list(lst))
    file(os.path.join(path, "boxplot_data"), "w").write(str({"x": list(xvals), "y": yvals}))

def boxplot_plot_file(paths, outdir, axis_max, axis_step, func, k, name):
    dcts = []
    for f in paths:
        dcts.append(eval("".join(file(os.path.join(f, "boxplot_data"), "r").readlines())))

    ox = []
    oys = []
    # print "!111111"
    for d in dcts:
        # print "22222222222"
        # print d
        # print d["x"], " ", d["y"]
        ox.extend(d["x"])
        oys.extend(d["y"])
    ox = np.array(ox)
    oys = np.array(oys)

    x = []
    ys = []
    # print "EEEE"
    for xi, y in zip(ox, oys):
        # print "DDDDDD"
        if len(y) > 3:
            # print "CCCC"
            ys.append(func(np.array(y)))
            x.append(func(xi))

            #    all_bad = 0
            #    all = 0
            #    point = 1.4
            #
            #    for xi, ysi in zip(x, ys):
            #        if xi < point:
            #            pl.plot([xi] * len(ysi), ysi, "ob")
            #            continue
            #        ysi = np.array(ysi)
            #        ybad = ysi[ysi < point]
            #        ygood = ysi[ysi >= point]
            #        all += len(ysi)
            #        if len(ybad) > 0:
            #            pl.plot([xi] * len(ybad), ybad, "or")
            #            all_bad += len(ybad)
            #        if len(ygood) > 0:
            #            pl.plot([xi] * len(ygood), ygood, "og")
            #
            #    print float(all_bad) / all
            #    pl.show()

    ax = pl.gca()
    pl.plot((0, axis_max), (0, axis_max), "k-", label = "Theoretical")
    # print "AAAAAAAAAAAAAAAAA\n"
    # print ys
    # print "\n"
    # print "BBBBBBBBBBBBBBBBB\n"
    bp = ax.boxplot(ys, positions = x, sym = 'k+',
                    widths = 0.2, bootstrap = 5000)
    pl.xticks(np.arange(0, axis_max + axis_step / 2, axis_step))
    pl.yticks(np.arange(0, axis_max + axis_step / 2, axis_step))
    pl.xlim((0, axis_max))
    pl.ylim((0, axis_max))
    pl.legend(loc = "upper left")
    pl.setp(bp['boxes'], color = 'black', linewidth = 1)
    pl.setp(bp['whiskers'], color = 'black', linewidth = 1, linestyle = "-")
    pl.setp(bp['fliers'], markersize = 3.0)

    #    cnt = 0
    #    tot = 0
    #    for xi, ysi in zip(x, ys):
    #        if xi < 3.4: continue
    #        tot += len(ysi)
    #        yi = ysi[ysi < 3.4]
    #        cnt += len(yi)
    #        pl.plot([xi] * len(yi), yi, "x", linestyle = "None")
    #    print float(cnt) / tot

    pl.xlabel("Predicted [%shits/s]" % k, labelpad = 2)
    pl.ylabel("Actual [%shits/s]" % k, labelpad = 2)
    pl.gca().set_position((0.13, 0.13, 0.8, 0.8))
    #    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.eps" % name), (3.2, 3.2))
    #    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.png" % name), (3.2, 3.2))
    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.png" % name), (8, 8))

if __name__ == "__main__":
    pl_apply_defaults()
    user_dir = os.path.expanduser("~")

    filenames = ["batch-Memcached-%i" % i for i in [252, 255, 256, 257]]
    localdir = "%s/moc-output/exp-tapuz21/mcd-boxplot" % user_dir
    remotedir = "%s/moc-output/exp" % user_dir
    paths = [os.path.join(localdir, f) for f in filenames]

#    profiler_file = "%s/workspace/moc/doc/profiler-memcached-inside-spare-50-win-500k.xml" % user_dir
    profiler_file = "%s/workspace/moc/doc/profiler-memcached-inside-spare-50-win-500k-tapuz21.xml" % user_dir
    field = "hits_rate"

    start_min = 20
    end_min = 50

    limit = 3
    step = 0.5
    func = lambda x: x * 1e-3
    k = "k"

    create_files = True


    if not os.path.exists(localdir): os.mkdir(localdir)
    for f in filenames:
        sync_files(os.path.join(remotedir, f), os.path.join(localdir, f),
                   ["bidder-*", "exp-out*", "prog-*"])
    if create_files:
        for f in paths:
            boxplot_create_file(f, profiler_file, field, start_min, end_min)
    boxplot_plot_file(paths, localdir)
