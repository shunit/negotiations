#from time import strptime, strftime
from datetime import datetime
import os
import argparse

number_to_status_name = {1: "OFFER", 2: "ACCEPT", 3: "UNUSED", 
                         4: "REJECT", 5: "FINAL1", 6: "FINAL2", 
                         7: "COUNTER_OFFER", 8: "NOT_LEADER", 
                         9: "UPDATE_MEMBERS", 10: "COALITION_SIZE"}
    

def process_file(log_file):
    f = open(log_file, "r")
    lines = f.readlines()
    f.close()
    time_to_msg = []
    for line in lines:
        parts = line.split("VISNEG")
        if len(parts) < 2: # VISNEG is not in there
            continue
    
        line_time = (datetime.strptime(line.split(" - ")[0], "%Y-%m-%d %H:%M:%S,%f") - datetime(1970, 1, 1)).total_seconds()
        #line_time = line.split(" - ")[0].replace(",",".")
        s = parts[1]
        rnd = int(s.strip(" ").split()[1])
        agent = s.strip(" ").split()[0]
        if "MessageCoalitionOffer" in s and "To" in s:
            time_to_msg.append({"time": line_time, 
                                "importance": 1,
                                "round": rnd,
                                "sender": agent, 
                                "receiver": s.split("To ")[1].split(" ")[0], 
                                "status": number_to_status_name[int(s.split("status=")[1].split(",")[0])], 
                                "profit_parts": s.split("profit_parts={")[1].split("}")[0].replace(",", " "),
                                "raw_message": s.split("Message ")[1].replace(","," ")})
        time_to_msg.append({"time": line_time,
                            "importance": 2,
                            "round": rnd,
                            "agent": agent,
                            "raw_message": s.split("%s %s " % (agent, rnd))[1].replace(",", " ")})
    return time_to_msg

def write_to_csv(dest_folder, time_to_msg):
    f1 = open(os.path.join(dest_folder, "Messages.csv") , "w")
    f1.write("time,round,sender,reciever,status,profit_parts,raw_message\n")
    f2 = open(os.path.join(dest_folder, "MessagesPlus.csv") , "w")
    f2.write("time,round,agent,raw_message\n")
    for msg in sorted(time_to_msg, key=lambda x: x["time"]):
        #time_as_string = strftime("%Y-%m-%d %H:%M:%S %f", msg["time"])
        #time_as_string = msg["time"]
        if msg["importance"] == 1:
            f1.write("%s,%s,%s,%s,%s,%s,%s" % 
                    (msg["time"], msg["round"], msg["sender"], msg["receiver"], 
                     msg["status"], msg["profit_parts"], msg["raw_message"]))
        else:
            f2.write("%s,%s,%s,%s" %
                    (msg["time"], msg["round"], msg["agent"], msg["raw_message"]))
    f1.close()
    f2.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract negotiation-related log messages into CSV file.')
    parser.add_argument("exp_id", type=int, help='Experiment id, e.g., 202.')
    parser.add_argument("num_guests", type=int, help='The number of guests in the experiment, e.g., 5.')
    args = parser.parse_args()

    experiment_id = args.exp_id
    num_guests = args.num_guests
    guest_ids = [i for i in range(1, num_guests+1)]
    bidder_files = ["/home/shunita/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"%(experiment_id, i) for i in guest_ids]
    time_to_msg = []
    for log_file in bidder_files:
        time_to_msg += process_file(log_file)
    write_to_csv("/home/shunita/moc-output/exp/coalitions-%d" % experiment_id, time_to_msg)

