'''
Allows plotting of multi functions

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
import matplotlib
matplotlib.use("pdf")

import pylab as pl
import numpy as np

class MultiFunctionsPlotter(object):
    '''
    Allows plotting of multi functions
    '''

    plot_available_properties = [
        "agg_filter",                       # unknown
        "alpha",                            # float (0.0 transparent through 1.0 opaque)
        "animated",                         # [True | False]
        "antialiased", "aa",                # [True | False]
        "axes",                             # an Axes instance
        "clip_box",                         # a matplotlib.transforms.Bbox instance
        "clip_on",                          # [True | False]
        "clip_path",                        # [ (Path, Transform) | Patch | None ]
        "color", "c",                       # any matplotlib color
        "contains",                         # a callable function
        "dash_capstyle",                    # ['butt' | 'round' | 'projecting']
        "dash_joinstyle",                   # ['miter' | 'round' | 'bevel']
        "dashes",                           # sequence of on/off ink in points
        "drawstyle",                        # ['default' | 'steps' | 'steps-pre' | 'steps-mid' | 'steps-post']
        "figure",                           # a matplotlib.figure.Figure instance
        "fillstyle",                        # ['full' | 'left' | 'right' | 'bottom' | 'top' | 'none']
        "gid",                              # an id string
        "label",                            # string or anything printable with '%s' conversion.
        "linestyle", "ls",                  # ['-' | '--' | '-.' | ':' | 'None' | ' ' | ''] and any drawstyle in combination with a linestyle, e.g., 'steps--'.
        "linewidth", "lw",                  # float value in points
        "lod",                              # [True | False]
        "marker",                           # unknown
        "markeredgecolor", "mec",           # any matplotlib color
        "markeredgewidth", "mew",           # float value in points
        "markerfacecolor", "mfc",           # any matplotlib color
        "markerfacecoloralt", "mfcalt",     # any matplotlib color
        "markersize", "ms",                 # float
        "markevery",                        # unknown
        "path_effects",                     # unknown
        "picker",                           # float distance in points or callable pick function fn(artist, event)
        "pickradius",                       # float distance in points
        "rasterized",                       # [True | False | None]
        "sketch_params",                    # unknown
        "snap",                             # unknown
        "solid_capstyle",                   # ['butt' | 'round' | 'projecting']
        "solid_joinstyle",                  # ['miter' | 'round' | 'bevel']
        "transform",                        # a matplotlib.transforms.Transform instance
        "url",                              # a url string
        "visible",                          # [True | False]
        "xdata",                            # 1D array
        "ydata",                            # 1D array
        "zorder",                           # any number
    ]

    def __init__(self,
                 all_xy,
                 iter_order = None,
                 xlim = None,
                 ylim = None,
                 x_label = "Time [m]",
                 remove_outlayers = False,
                 **kwargs):
        '''
        Constructor
        '''

        self.all_xy = all_xy
        self.iter_order = iter_order if iter_order else all_xy.keys()
        self.subplots_count = len(self.iter_order)
        self.xlim = xlim
        self.ylim = ylim
        self.x_label = x_label
        self.remove_outlayers = remove_outlayers

        self.kwargs = kwargs

    def plot(self):
        all_axes = []

        for plot_i, subplot_title in enumerate(self.iter_order):
            '''
            Plot each plot array in different row
            '''
            subplot_xy = self.all_xy[subplot_title]
            axes_count = max([ xy[-1]["axe"] for xy in subplot_xy])

            pl.subplot(self.subplots_count, 1, plot_i + 1)
            main_axe = pl.gca()
            main_axe.set_title(subplot_title)
            main_axe.set_xlabel(self.x_label, labelpad = 2)

            sp_axes = self.get_subplot_axes(main_axe, axes_count)
            all_axes.append(sp_axes)

            self.plot_sublot_and_get_x_lables(subplot_xy, sp_axes)
            self.set_subplots_y_labels(subplot_xy, sp_axes)

            # show only last plot x ticks
            if plot_i < self.subplots_count - 1:
                for ax in sp_axes:
                    ax.get_xaxis().set_ticklabels([])

        x0 = 0.06
        x1 = 0.86
        y0 = 0.11
        y1 = 0.93
        space = 0.01
        shrink_factor = 0.01

        h = (y1 - y0 - (self.subplots_count - 1) * shrink_factor) / self.subplots_count

        # make all axes in all subplots with the same limits
        for ii, sp_axes in enumerate(all_axes):
            for ax in sp_axes:
                ax.set_position([x0, y0 + (self.subplots_count - 1 - ii) * (h + space), x1 - x0, h])

                if self.ylim:
                    ax.set_ylim(*self.ylim)

                if self.xlim:
                    ax.set_xlim(*self.xlim)

    def get_subplot_axes(self, main_axe, count):
        main_axe.grid(True)
        subplot_axes = [pl.twinx(main_axe) for _ in range(count)]

        for i, axe in enumerate(subplot_axes):
            axe.spines['right'].set_position(('outward', 40 * i))
            axe.grid(False)

        return [main_axe] + subplot_axes

    def plot_sublot_and_get_x_lables(self, subplot_xy, sp_axes):
        labels = []
        lines = []

        axes_y_limits = {}

        for plot_i, xy in enumerate(subplot_xy):
            x = xy[0]
            y = xy[1]
            if len(xy) > 3:
                err = xy[2]
            else:
                err = None

            plot_kwargs = xy[-1]

            if len(x) == 0 or len(y) == 0:
                continue

            # draw the line
            cur_ax_key = plot_kwargs["axe"]
            axe = sp_axes[cur_ax_key]

            if self.remove_outlayers:
                lims = axes_y_limits.setdefault(cur_ax_key, [float("inf"), float("-inf")])

                m = np.median(y)
                d = np.abs(y - m)
                mdev = np.median(d)
                relavent = np.argmax(d<mdev)

                lims[0] = min(lims[0], np.min(y[relavent:]) - mdev*0.1)
                lims[1] = max(lims[1], np.max(y[relavent:]) + mdev*0.1)

            function_args = {k:plot_kwargs[k] for k in plot_kwargs if k in self.plot_available_properties}
            if err is not None:
                res_line = axe.errorbar(x, y, yerr=err, **function_args)
            else:
                res_line = axe.plot(x, y, **function_args)[0]

            lines.append(res_line)

            default_label = "Plot-%s" % plot_i
            label = plot_kwargs.get("legend_label", default_label)
            if label is None:
                label = default_label

            if not isinstance(label, str):
                label = str(label)

            label = label.replace("_", " ")
            labels.append(label)

        if len(subplot_xy) > 1:
            leg = sp_axes[0].legend(lines, labels,
                      loc = 'best',
                      fancybox=True,
                      #bbox_to_anchor = (0.5, 0.05),
                      #bbox_transform = pl.gcf().transFigure,
                      #ncol = legend_cols,
                      #frameon = True
                      )

            # set the alpha value of the legend: it will be translucent
            leg.get_frame().set_alpha(0.5)

        if self.remove_outlayers:
            for ax_key, lims in axes_y_limits.iteritems():
                ax = sp_axes[ax_key]
                ax.set_ylim(lims)

    def get_joined_args(self, relavent_args, key):
        matching_args = [kw[key] if kw.has_key(key) else None for kw in relavent_args]
        if not matching_args: return None

        # Assert that all of the units match (don't mix and match)
        assert matching_args.count(matching_args[0]) == len(matching_args)

        return matching_args[0]

    def set_subplots_y_labels(self, subplot_xy, sp_axes):
        # set axes y labels
        for ax_ind, ax in enumerate(sp_axes):
            relavent_args = [xy[-1] for xy in subplot_xy if xy[-1]["axe"] == ax_ind]

            unit = self.get_joined_args(relavent_args, "units")
            yscale = self.get_joined_args(relavent_args, "yscale")

            if unit == None:
                unit = "Not Specified"

            labels = set([kw["y_label"].replace("_", " ") if kw.has_key("y_label") else None for kw in relavent_args])
            labels_str = ' / '.join(labels)

            final_label = "%s [%s]" % (labels_str, unit)

            ax.set_ylabel(final_label, labelpad = 2)

            if yscale: ax.set_yscale(yscale)

