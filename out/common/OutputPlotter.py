'''
@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from matplotlib import use
use('pdf')

from out.common.visuals import get_visual
from out.common.OutputView import VMsView
import argparse
from out.common.views import get_views
from copy import copy, deepcopy
from os.path import expanduser
from tempfile import mkdtemp
from mom.Plotter import Plotter, PERF, HOST #, POL, MON
import re
import os
import traceback
from etc.publish import remote_sync

class OutputPlotter:

    PLOTTER_FILE = "exp-plotter"
    INFO_FILE = "info"

    CHECKIN_DATA_FILES = [PLOTTER_FILE, INFO_FILE]

    DICT_OBJ_PATTERN = re.compile("<[^<>]*>")

    def __init__(self, views, source_dir, target_dir = None, draw_args = {}, debug = False, subfolders_list = [], output_to_sub_folders = True):
        self.source_dir = expanduser(source_dir)
        self.work_dir = mkdtemp()
        if target_dir:
            self.target_dir = expanduser(target_dir)
        else:
            self.target_dir = self.source_dir

        self.draw_args = draw_args
        self.debug = debug
        self.subfolders_list = subfolders_list
        self.output_to_sub_folders = output_to_sub_folders

        self.setViews(views)

        if not os.path.exists(self.source_dir):
            raise IOError("Source path not found: %s" % self.source_dir)

        if not os.path.exists(self.target_dir):
            os.makedirs(self.target_dir)

    def setViews(self, views):
        self.views = views

    def plotAll(self, plotEachDataSet = True, plotJoinedDataSet = True):
        self.checkinData()
        self.retrieveEvaluationDataSet()

        output_formats = set()
        if plotEachDataSet:
            for work_dir, evalData in self.evalDataSet.iteritems():
                print "Plotting %s" % work_dir
                output_formats.update( self.drawEvaluation(evalData) )

        if plotJoinedDataSet:
            print "Plotting joined experiments VMs data..."
            output_formats.update( self.drawEvaluation(self.joinedVMsDataSet) )

        self.checkoutPlots(output_formats)

    def checkinData(self):
        remote_sync(
                    src_path = self.source_dir,
                    dst_path = self.work_dir,
                    include = self.CHECKIN_DATA_FILES + self.subfolders_list,

                    include_subfolders = len(self.subfolders_list) == 0,
                    delete = True,
                    delete_excluded = True,
                    show_progress = False,
                    verbose = False)

    def checkoutPlots(self, output_formats):
        remote_sync(
                    src_path = self.work_dir,
                    dst_path = self.target_dir,
                    include = output_formats,

                    include_subfolders = True,
                    delete = False,
                    delete_excluded = False,
                    show_progress = False,
                    verbose = False)

    def drawEvaluation(self, evalData):
        output_formats = set()

        for view in self.views:
            try:
                local_draw_args = copy(self.draw_args)
                if self.output_to_sub_folders:
                    output_path = os.path.join(self.work_dir, evalData.work_dir)
                else:
                    output_path = self.work_dir
                    local_draw_args["prefix"] = evalData.work_dir

                if not os.path.exists(output_path):
                    os.makedirs(output_path)

                out_ext = view.drawAndSave(
                    output_path = output_path,
                    data = evalData,
                    draw_args = local_draw_args)

                if out_ext:
                    output_formats.add("*.%s" % out_ext)
            except Exception as e:
                if self.debug: print traceback.format_exc()
                print "Fail: %s with \"%s\"" % (view.name, e)

        return output_formats

    def retrieveEvaluationDataSet(self):
        self.evalDataSet = {}

        for work_path, work_dir in [ (os.path.join(self.work_dir,f), f) for f in os.listdir(self.work_dir) ]:
            if not os.path.isdir(work_path): continue
            if work_dir.startswith("."): continue

            plotter_file_path = os.path.join(work_path, self.PLOTTER_FILE)
            info_file_path = os.path.join(work_path, self.INFO_FILE)

            if not os.path.isfile(plotter_file_path):
                print "Plotter file does not exist:", plotter_file_path
                continue

            if not os.path.isfile(info_file_path):
                print "Info file does not exist:", info_file_path
                continue

            # read the evaluation output
            plotter_content = file(plotter_file_path).readlines()
            info_content = file(info_file_path).read()

            try:
                data = Plotter.parse(plotter_content)
                info = self.__evalInfo(info_content)
            except:
                print "Failed loading",work_dir
                raise

            self.evalDataSet[work_dir] = EvaluationData(data, info, work_dir)
            self.evalDataSet[work_dir].correct_time()

            ll = [dic['time'] for dic in self.evalDataSet['p0-0.00-freq-60']['vm-1']['Performance']]
            print len(ll)
            ll = [dic['time'] for dic in self.evalDataSet['p0-0.00-freq-60']['vm-2']['Performance']]
            print len(ll)

            if "long_titles" in info:
                print "Load:", work_dir, " - Title: ", info["long_titles"]
            else:
                print "Load:", work_dir

        joinedVMsDataSet = {}
        joinedVMsInfoSet = {}
        joined_itervms_list = []
        joined_iterhosts_list = []

        for work_dir, evalData in self.evalDataSet.iteritems():
            vms = evalData.itervms()
            if len(vms) == 0:
                continue
            elif len(vms) == 1:
                joinedVMsDataSet[work_dir] = evalData[vms[0]]
                joinedVMsInfoSet[work_dir] = evalData.info
                joined_itervms_list.append(work_dir)
            else:
                for vm in vms:
                    cur_key = work_dir, vm
                    joinedVMsDataSet[cur_key] = evalData[vm]
                    joinedVMsInfoSet[cur_key] = evalData.info
                    joined_itervms_list.append(cur_key)

            host_key = work_dir, HOST
            joinedVMsDataSet[host_key] = evalData[HOST]
            joinedVMsInfoSet[host_key] = evalData.info
            joined_iterhosts_list.append(host_key)

        self.joinedVMsDataSet = EvaluationData(joinedVMsDataSet, joinedVMsInfoSet, "Joined")
        self.joinedVMsDataSet.set_itervms_list(joined_itervms_list)
        self.joinedVMsDataSet.set_iterhosts_list(joined_iterhosts_list)

    @classmethod
    def __evalInfo(cls, info):
        '''
        Remove objects from info text. Something like "< Bla Bla >"
        '''
        count = -1
        while count != 0:
            info, count = cls.DICT_OBJ_PATTERN.subn("None",info)

        return eval(info)

class EvaluationData(dict):
    def __init__(self, data, info, work_dir = None):
        dict.__init__(self, data)

        self.info = info
        self.work_dir = work_dir

        self.itervms_list = None
        self.iterhosts_list = None

    def set_itervms_list(self, itervms_list):
        self.itervms_list = itervms_list

    def set_iterhosts_list(self, iterhosts_list):
        self.iterhosts_list = iterhosts_list

    def getRevenueFunctions(self):
        return [(k, self.info['vms_desc'][k]['adviser_args']['rev_func']) for k in self.info['vms_desc'].keys()]

    def correct_time(self):
        t0 = float("inf")

        for _plotter_name, plotter_data in self.iteritems():
            for tp in plotter_data.keys():
                try: t0 = min(t0, plotter_data[tp][0]['time'])
                except KeyError: pass

        for _plotter_name, plotter_data in self.iteritems():
            for tp in plotter_data.keys():
                for dct in plotter_data[tp]:
                    dct['time'] -= t0

    def itervms(self):
        if not self.itervms_list:
            self.itervms_list = ["vm-%i" % n for n in sorted([int(plotter_name.split("-")[1]) for plotter_name in self.iterkeys() if plotter_name not in [HOST]])]

        return copy(self.itervms_list)

    def iterhosts(self):
        if not self.iterhosts_list:
            return [HOST]

        return copy(self.iterhosts_list)

if __name__ == '__main__':
        from out.common.OutputView import VMsOutputJSON

        parser = argparse.ArgumentParser(description='Plot experiments.')
        parser.add_argument("-s", "--source", required=True)
        parser.add_argument("-t", "--target")
        parser.add_argument("-r", "--raw-data", action="store_true")
        parser.add_argument("-e", "--each", action="store_true")
        parser.add_argument("-j", "--joined", action="store_true")
        parser.add_argument("-f", "--flatten", action="store_true")
        parser.add_argument("views", nargs="+")

        args = parser.parse_args()

        views, unknown = get_views(args.views)
        visuals, unknown = get_visual(unknown)
        if unknown:
            print "Unknown views:",unknown

        if visuals:
            v = VMsView("output-view", visuals=visuals)
            views.append(v)

        if args.raw_data:
            views = [ VMsOutputJSON.fromView(v) for v in views ]

        if not args.each and not args.joined:
            args.each = True

        OutputPlotter(
                      views,
                      args.source,
                      args.target,
                      output_to_sub_folders = not args.flatten,
                      debug = True).plotAll(
                                  plotEachDataSet = args.each,
                                  plotJoinedDataSet = args.joined)

