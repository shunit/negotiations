'''
Created on May 29, 2014
@author: Liran Funaro
'''

from matplotlib import use
use('pdf')

# import pylab as pl
# import numpy as np
# import collections
# from mom import Profiler
# from etc.plots import draw_n_save, pl_apply_defaults
# from out.common.common import sync_files, get_vals, perf_time, iter_vms, \
#     pol_time, exp_plot

from mom.Plotter import Plotter, PERF, HOST #, POL, MON

import re
import sys, os
import traceback

from etc.NamedParameters import NamedParameters, ListMultiParameterGroup,\
    BooleanParameterGroup
from etc.publish import remote_sync
from etc.Settings import Settings

class EvaluationOutputPlotter:

    PLOTTER_FILE = "exp-plotter"
    INFO_FILE = "info"

    CHECKIN_DATA_FILES = [PLOTTER_FILE, INFO_FILE]

    DICT_OBJ_PATTERN = re.compile("<[^<>]*>")

    def __init__(self, evaluation_type, name, views, draw_args = {}, debug = False):
        self.evaluation_type = evaluation_type
        self.name = name
        self.views = views
        self.draw_args = draw_args
        self.debug = debug

        has_workspace_in_path = True
        self.source_dir = os.path.join(Settings.user_home(),
                                       "workspace",
                                       Settings.output_dir(),
                                       evaluation_type,
                                       name)
        self.target_dir = os.path.join(Settings.user_home(), "workspace",
                                       "-".join( [ Settings.output_dir(),
                                                   Settings.hostname()     ]),
                                       evaluation_type,
                                       name)

        if not os.path.exists(self.source_dir):
            self.source_dir = os.path.join(Settings.user_home(),
                                           Settings.output_dir(),
                                           evaluation_type,
                                           name)
            has_workspace_in_path = False
            if not os.path.exists(self.source_dir):
                raise IOError("Source path not found: %s" % self.source_dir)

        if has_workspace_in_path:
            if not os.path.exists(self.target_dir):
                os.makedirs(self.target_dir)
        else:
            self.target_dir = os.path.join(Settings.user_home(),
                                           "-".join([Settings.output_dir(),
                                                     Settings.hostname()]),
                                           evaluation_type,
                                           name)
            os.makedirs(self.target_dir)

    def plotAll(self, plotEachDataSet = True, plotJoinedDataSet = True):
        self.checkinData()
        self.retrieveEvaluationDataSet()

#       draw_bars()
#       draw_composed_exp(data, info)

        output_formats = set()
        if plotEachDataSet:
            for work_dir, evalData in self.evalDataSet.iteritems():
                print "Plotting %s" % work_dir
                output_formats.update( self.drawEvaluation(evalData) )

    #           boxplot_create_file(0)
    #           boxplot_plot_file(a / 100.)
    #           draw_memcached(remotedir, localdir, a / 100)

        if plotJoinedDataSet:
            print "Plotting joined experiments VMs data..."
            output_formats.update( self.drawEvaluation(self.joinedVMsDataSet) )

        self.checkoutPlots(output_formats)

    def checkinData(self):
        remote_sync(
                    src_path = self.source_dir,
                    dst_path = self.target_dir,
                    include = self.CHECKIN_DATA_FILES,

                    include_subfolders = True,
                    delete = True,
                    delete_excluded = False,
                    show_progress = False,
                    verbose = False)

    def checkoutPlots(self, output_formats):
        remote_sync(
                    src_path = self.target_dir,
                    dst_path = self.source_dir,
                    include = output_formats,

                    include_subfolders = True,
                    delete = False,
                    delete_excluded = False,
                    show_progress = False,
                    verbose = False)

    def drawEvaluation(self, evalData):
        output_formats = set()

        for view in self.views:
            try:
                output_path = os.path.join(self.target_dir, evalData.work_dir)
                if not os.path.exists(output_path):
                    os.makedirs(output_path)

                out_ext = view.drawAndSave(
                    output_path = output_path,
                    data = evalData,
                    draw_args = self.draw_args)

                if out_ext:
                    output_formats.add("*.%s" % out_ext)
            except Exception as e:
                print "Fail: %s with \"%s\"" % (view.name, e)
                if self.debug: print traceback.format_exc()

        return output_formats

    def retrieveEvaluationDataSet(self):
        self.evalDataSet = {}

        for work_path, work_dir in [ (os.path.join(self.target_dir,f), f) for f in os.listdir(self.target_dir) ]:
            if not os.path.isdir(work_path): continue

            plotter_file_path = os.path.join(work_path, self.PLOTTER_FILE)
            info_file_path = os.path.join(work_path, self.INFO_FILE)

            if not os.path.isfile(plotter_file_path):
                print "Plotter file does not exist:", plotter_file_path
                continue

            if not os.path.isfile(info_file_path):
                print "Info file does not exist:", info_file_path
                continue

            # read the evaluation output
            plotter_content = file(plotter_file_path).readlines()
            info_content = file(info_file_path).readline()

            data = Plotter.parse(plotter_content)
            info = self.__evalInfo(info_content)

            self.evalDataSet[work_dir] = EvaluationData(data, info, work_dir)
            self.evalDataSet[work_dir].correct_time()

            if "long_titles" in info:
                print "Load:", work_dir, " - Title: ", info["long_titles"]
            else:
                print "Load:", work_dir

        joinedVMsDataSet = {}
        joined_itervms_list = []

        for work_dir, evalData in self.evalDataSet.iteritems():
            vms = evalData.itervms()
            if len(vms) == 0:
                continue
            elif len(vms) == 1:
                joinedVMsDataSet[work_dir] = evalData[vms[0]]
                joined_itervms_list.append(work_dir)
            else:
                for vm in vms:
                    cur_key = "%s(%s)" % (work_dir, vm)
                    joinedVMsDataSet[cur_key] = evalData[vm]
                    joined_itervms_list.append(cur_key)

        self.joinedVMsDataSet = EvaluationData(joinedVMsDataSet, None, "Joined")
        self.joinedVMsDataSet.set_itervms_list(joined_itervms_list)

    @classmethod
    def __evalInfo(cls, info):
        '''
        Remove objects from info text. Something like "< Bla Bla >"
        '''
        count = -1
        while count != 0:
            info, count = cls.DICT_OBJ_PATTERN.subn("None",info)

        return eval(info)

#     def __collectPerformanceRevenueData(self):
#         # collect the performance data
#         all_perf = {}
#         all_rev = {}
#         all_load = {}
#
#         for work_dir, evalData in self.evalDataSet.iteritems():
#             all_perf[work_dir] = {}
#             all_rev[work_dir] = {}
#             for vm in iter_vms(evalData.data):
#                 try:
#                     vals = get_vals(get_vals(evalData.data[vm][PERF], 'perf'), "hits_rate")
#                     times = perf_time(evalData.data[vm])
#                     vals = [float(v) for t, v in zip(times, vals) if self.discard_min <= t <= self.end_min]
#                     all_perf[work_dir][vm] = vals[1:]
#
#                     vals = get_vals(self.data[work_dir][vm][PERF], 'rev')
#                     vals = [float(v) for t, v in zip(times, vals) if self.discard_min <= t <= self.end_min]
#                     all_rev[work_dir][vm] = vals[1:]
#
#                     vals = get_vals(self.data[work_dir][vm][PERF], 'load')
#                     vals = [float(v) for t, v in zip(times, vals) if self.discard_min <= t <= self.end_min]
#                     all_load[work_dir][vm] = vals[1:]
#                 except KeyError: pass
#
#         return (all_perf, all_rev, all_load)
#
#     def draw_bars(self):
#         all_perf, all_rev, _ = self.__collectPerformanceRevenueData()
#         p0s = np.array(sorted(all_perf.keys()))
#
#         def calc_social_welfare(key):
#             revenue_sum = []
#             for vm in all_perf[key].keys():
#                 revenue_list = all_rev[key][vm]
#                 revenue_sum.append(sum(revenue_list))
#             return sum(np.array(revenue_sum))
#
#         revs = map(calc_social_welfare, p0s)
#
#         w = 0.1
#         h = revs / min(revs) if min(revs) != 0 else revs
#         x = p0s / 100.
#         pl.bar(left = x - w / 2,
#                height = h,
#                width = w,
#                bottom = 0,
#                color = "w")  #(0.5, 0.5, 1.0))
#         for xi, hi in zip(x, h):
#             pl.text(xi, hi / 2, "%.2f" % hi,
#                     rotation = -90,
#                     ha = "center", va = "center")
#         pl.xticks(x)
#         pl.xlim((-0.05, 1.1))
#         pl.ylim((0, 1.6))
#         pl.gca().set_position(self.position)
#         pl.xlabel("p0", labelpad = 2)
#         pl.ylabel("Relative SW", labelpad = 2)
#         pl.gca().set_position(self.position)
#
#         draw_n_save(**self.bars_draw_args)

class EvaluationData(dict):
    def __init__(self, data, info, work_dir = None):
        dict.__init__(self, data)

        self.info = info
        self.work_dir = work_dir

        self.itervms_list = None

    def set_itervms_list(self, itervms_list):
        self.itervms_list = itervms_list

    def getRevenueFunctions(self):
        return [(k, self.info['vms_desc'][k]['adviser_args']['rev_func']) for k in self.info['vms_desc'].keys()]

    def correct_time(self):
        t0 = 99999999999
        vms_key = "vms" if "vms" in self.info else "vms_desc"

        for plotter_name, plotter_data in self.iteritems():
            for tp in plotter_data.keys():
                try: t0 = min(t0, plotter_data[tp][0]['time'])
                except KeyError: pass

            if plotter_name == HOST or PERF not in plotter_data:
                continue

            rev_funcs = [lambda x: x]
            val_switch_func = None
            if vms_key in self.info.keys() and plotter_name in self.info[vms_key]:
                if type(self.info[vms_key][plotter_name]['adviser_args']['rev_func']) is str:
                    rev_funcs = [eval(self.info[vms_key][plotter_name]['adviser_args']['rev_func'])]
                else:
                    rev_funcs = map(lambda f: eval(f) if isinstance(f, str) else f,
                                    self.info[vms_key][plotter_name]['adviser_args']['rev_func'])
                    # rev_func = [eval(t) for t in self.info[vms_key][plotter_name]['adviser_args']['rev_func']]
                    # module = __import__("exp.core.Loads",
                    #                     fromlist=self.info[vms_key][plotter_name]['val_switch_func']['type'])
                    # load = getattr(module, self.info[vms_key][plotter_name]['val_switch_func']['type'])
                    # load_function_args = self.info[vms_key][plotter_name]['val_switch_func'].copy()
                    # del(load_function_args["type"])
                    # val_switch_func = load(**load_function_args)

            print plotter_data[PERF][0]['perf']
            for i in range(len(plotter_data[PERF])):
                if "get_hits_total" in plotter_data[PERF][i]['perf'] and 'hits_rate' not in plotter_data[PERF][i]['perf']:
                    try:
                        plotter_data[PERF][i]['perf']["hits_rate"] = plotter_data[PERF][i]['perf']["get_hits_total"] / plotter_data[PERF][i]['perf']["test_time"]
                    except KeyError:
                        pass
                if "hits_rate" not in plotter_data[PERF][i]['perf']:
                    plotter_data[PERF][i]['perf']["hits_rate"] = 0

                # if type(rev_func) is not list:
                #     plotter_data[PERF][i]['rev'] = rev_func(plotter_data[PERF][i]['perf']["hits_rate"])
                # else:
                #     t = plotter_data[PERF][i]['perf']["test_time"]
                #     rev_func_index = val_switch_func(plotter_data[PERF][i]['perf']["test_time"])
                #     plotter_data[PERF][i]['rev'] = rev_func[rev_func_index](plotter_data[PERF][i]['perf']["hits_rate"])

                current_valuation = 0
                try:
                    current_valuation = [x for x in plotter_data[MON] if x['time'] <= plotter_data[PERF][i]['time']][-1]['current_valuation']
                except KeyError:
                    pass
                except IndexError:
                    pass

                rev_func = rev_funcs[current_valuation]
                print "rev_func = %s" % rev_func
                print "current_valuation = %s" % current_valuation
                plotter_data[PERF][i]['rev'] = rev_func(plotter_data[PERF][i]['perf']["hits_rate"])

        for plotter_name, plotter_data in self.iteritems():
            if plotter_name != HOST:
                continue
            plotter_data[PERF] = []
            for i in range(len(plotter_data[MON])):
                t = plotter_data[MON][i]
                sw = 0
                for name, g in self.iteritems():
                    if name == HOST:
                        continue
                    val = 0
                    try:
                        val = [x for x in g[PERF] if x['time'] <= t][-1]['rev']
                    except IndexError:
                        pass
                    sw += val
                plotter_data[PERF].append({'sw': sw})

        for plotter_name, plotter_data in self.iteritems():
            for tp in plotter_data.keys():
                for dct in plotter_data[tp]:
                    dct['time'] -= t0

    def itervms(self):
        if not self.itervms_list:
            self.itervms_list = ["vm-%i" % n for n in sorted([int(plotter_name.split("-")[1]) for plotter_name in self.iterkeys() if plotter_name not in [HOST]])]

        return self.itervms_list

#class EvaluationOutputSettings:
#     FLAT_FILENAME_FORMAT = "%(name)s.%(ext)s"
#     SIMPLE_TYPED_FILENAME_FORMAT = "%(type)s.%(ext)s"
#     TYPED_FILENAME_FORMAT = "%(name)s-%(type)s.%(ext)s"
#
#     def __init__(self, evaluation_type, name, draw_args = {}):
#         self.evaluation_type = evaluation_type
#         self.name = name
#         self.paper = draw_args
#
#         self.source_dir = os.path.join(Settings.user_home(),
#                                        Settings.output_dir(),
#                                        evaluation_type,
#                                        name)
#         self.target_dir = os.path.join(Settings.user_home(),
#                                        "-".join( [ Settings.output_dir(),
#                                                    Settings.hostname()     ]),
#                                        evaluation_type,
#                                        name)
#
#         if not os.path.exists(self.source_dir):
#             raise IOError("Source path not found: %s" % self.source_dir)
#
#         if not os.path.exists(self.target_dir):
#             os.makedirs(self.target_dir)
#
#         self.bars_draw_args = dict(name = self.getOutputFilePath(), size = (40, 8))
#         self.host_p_vals_draw_args = dict(name = self.getOutputFilePath(type="host-p-vals"), size = (20, 8))
#         self.composed_evaluation_view_draw_args = dict(name = self.getOutputFilePath(type="composed-evaluation-view"), size = (7, 3))
#         self.boxplot_draw_args = dict(name = self.getOutputFilePath(type="boxplot"), size = (8, 8))
#
#         self.memcached_draw_args_for_vm = lambda i: dict(name = self.getOutputFilePath(type="vm-%i-memcached" % i), size = (48, 8))
#
#         '''
#         Hard coded. Why?
#         '''
#         self.discard_min = 0
#         self.end_min = 10
#         self.position = (0.1, 0.22, 0.88, 0.72)
#         self.size = (7.0, 2.4) #size = (3.5, 1.2)
#
#         pl_apply_defaults({'legend.fontsize': 10,
#                            'axes.labelsize': 10,
#                            'font.size' : 10,
#                            'xtick.labelsize': 10,
#                            'ytick.labelsize': 10, })
#
#     def getOutputFilePath(self, filename_format = None, work_dir = None, **extra):
#         '''
#         Return the output file path relative to root; based on a generic format
#         '''
#
#         if filename_format is None:
#             '''
#             Select format if not specified
#             '''
#             if "type" in extra:
#                     filename_format = self.SIMPLE_TYPED_FILENAME_FORMAT if work_dir else self.TYPED_FILENAME_FORMAT
#             else:
#                 filename_format = self.FLAT_FILENAME_FORMAT
#
#         '''
#         Use empty work directory if not specified
#         '''
#         if work_dir is None: work_dir = ""
#
#         format_dict = dict(self.filename_formater_dict, **extra)
#         return os.path.join(self.target_dir, work_dir, filename_format % format_dict)

# class EvaluationDataPlotter(EvaluationData):
#     def __init__(self, evalData, evalSettings, views):
#         EvaluationData.__init__(self, evalData.data, evalData.info, evalData.work_dir)
#         self.evalSettings = evalSettings
#         self.views = views
#         self.output_path = os.path.join(self.evalSettings.target_dir, self.work_dir)
#
#     def drawEvaluation(self, compare_data = None, debug = False):
#         for view in self.views:
#             try: self.drawAndSaveView(view, compare_data)
#             except Exception as e:
#                 print "Fail: %s with \"%s\"" % (view.name,e)
#                 if debug: print traceback.format_exc()
#
# #         self.drawHostPValues()
#
#     def drawAndSaveView(self, view, compare_data = None):
#         view.drawAndSave(main_data = self.data,
#                          compare_data = compare_data,
#                          info = self.info,
#                          output_path = self.output_path)
#
#     def drawHostPValues(self):
#         # p values
#         pl.clf()
#         for p, marker in zip(["p-in-min", "p-out-max"], ["^", "v"]):
#             pl.plot(pol_time(self.data[HOST]), get_vals(self.data[HOST][POL], p),
#                     marker=marker, alpha=1, label = "%s" % p)
#             pl.legend(loc="best")
#             pl.grid(True)
#             pl.xlabel("Time [min]")
#         draw_n_save(**self.evalSettings.host_p_vals_draw_args)
#
#     def draw_composed_exp(self, data, info):
#         p0s = [0, 20]
#
#         k_per_sec = lambda x: x / 1000.
#         to_GB = lambda x: x / 1024.
#         d = collections.defaultdict(dict)
#
#         for a in p0s:
#             for vm in data[a].keys():
#                 for k in data[a][vm].keys():
#                     d[vm][k + "-" + str(a)] = [x.copy() for x in data[a][vm][k]]
#                     d[vm][k] = data[0][vm][k]
#         funcs = []
#         props = []
#
#         linestyles = {0: "-",
#                       20: ":"}
#         markers = {0: "None", 20: "None"}
#         colors = {"mem": "b", "sw": "r"}
#
#         funcs.append(
#             (lambda dct: map(lambda t: t['time'] / 60., dct[PERF + "-" + '0']),
#              lambda dct: map(k_per_sec, get_vals(dct[PERF + "-" + '0'], 'rev')))
#                      )
#         props.append(
#             {"label": r"SW p0=0.0",
#              "linestyle": linestyles[0],
#              "marker": markers[0],
#              "linewidth": 1,
#              "color": colors["sw"]}
#                      )
#         funcs.append(
#             (lambda dct: map(lambda t: t['time'] / 60., dct[MON + "-" + '0']),
#              lambda dct: map(to_GB, get_vals(dct[MON + "-" + '0'], 'mem_available')))
#                      )
#         props.append(
#             {"label": r"Memory p0=0.0",
#              "linestyle": linestyles[0],
#              "marker": "None",
#              "linewidth": 1,
#              "color": colors["mem"]}
#                      )
#
#         funcs.append(
#             (lambda dct: map(lambda t: t['time'] / 60., dct[PERF + "-" + '20']),
#              lambda dct: map(k_per_sec, get_vals(dct[PERF + "-" + '20'], 'rev')))
#                      )
#         props.append(
#             {"label": r"SW p0=0.2",
#              "linestyle": linestyles[20],
#              "marker": markers[20],
#              "linewidth": 1,
#              "color": colors["sw"]}
#                      )
#         funcs.append(
#             (lambda dct: map(lambda t: t['time'] / 60., dct[MON + "-" + '20']),
#              lambda dct: map(to_GB, get_vals(dct[MON + "-" + '20'], 'mem_available')))
#                      )
#         props.append(
#             {"label": r"Memory p0=0.2",
#              "linestyle": linestyles[20],
#              "marker": "None",
#              "linewidth": 1,
#              "color": colors["mem"]}
#                      )
#
#         pl.clf()
#         exp_plot(d, funcs, props, axes = [0, 1, 0, 1], xlim = (0, 16))
#         draw_n_save(**self.evalSettings.composed_evaluation_view_draw_args)
#
#     #def boxplot_create_file(path, profiler_file, field, start_min, end_min, main_exp = "ginseng"):
#     def boxplot_create_file(self, p0):
#         profiler_file = "%s/moc/doc/profiler-memcached-inside-spare-50-win-500k-tapuz21.xml" % (os.environ['HOME'])
#         field = 'hits_rate'
#         profiler = Profiler.fromxml(profiler_file, field)
#
#         # collect the performance data
#         all_perf = {}
#         for vm in iter_vms(self.data):
#             try:
#                 vals = get_vals(get_vals(self.data[vm][PERF], 'perf'), field)
#                 times = perf_time(self.data[vm])
#                 vals = [float(v) for t, v in zip(times, vals)]
#                 all_perf[vm] = vals[1:]
#
#                 print len(vals), " - ", vm
#
# #                 ls = get_vals(self.data[vm][PERF], 'load')
#             except KeyError: pass
#
# #         nums = sorted(all_perf.keys())
#
#         P = []
#         L = []
#         M = []
#
#         for i, vm in enumerate(iter_vms(self.data)):
#             if vm != 'vm-1': continue
#             perf_data = self.data[vm][PERF]
#             try:
#                 mon_data = self.data[vm][MON]
#             except KeyError:
#                 print "ERROR! No monitor data for num: %i, guest: %s" % (i, vm)
#                 mon_data = []
#
#             p = np.array(get_vals(get_vals(perf_data, 'perf'), field))
#             l = np.array(get_vals(perf_data, 'load'))
#             t = np.array(get_vals(perf_data, 'time'))
#     #        dt = np.array(get_vals(perf_data, 'duration'))
#     #        dt = np.array(get_vals(get_vals(perf_data, 'perf'), 'test_time'))
#             dt = np.array(get_vals(get_vals(perf_data, 'perf'), 'measure_duration'))
#             mt = np.array(get_vals(mon_data, 'time'))
#             p = p  # / dt
#
#             allm = np.array(get_vals(mon_data, 'mem_available'), float)
#
#             def avg(arg):
#                 try:
#                     return np.average(arg) if len(arg) > 0 else 0
#                 except AttributeError:
#                     return 0
#
#             limit = [avg(allm[list(
#                  set.intersection(set(np.where(ti - .5 * dti <= mt)[0]),
#                                  set(np.where(mt < ti + .5 * dti)[0])))])
#                  for ti, dti in zip(t, dt)]
#             limit = np.array(limit)
#
#             t = (t - t[0]) / 60
#
#             '''
#             trim = (start_min <= t) & (t <= end_min) & (np.isnan(p) == 0) & \
#                         (np.isnan(l) == 0) & (np.isnan(limit) == 0)
#             print "DEBUG: ", d.keys()
#
#             p = p[trim]
#             l = l[trim]
#             limit = limit[trim]
#             '''
#
#             P.extend(p)
#             L.extend(l)
#             M.extend(limit)
#
#         P = np.array(P)
#         L = np.array(L)
#         M = np.array(M)
#
#         # perf(load,mem)
#         loads = set(L)
#         for li in sorted(loads):
#             ind = np.where(L == li)
#             pl.plot(M[ind], P[ind],
#                     markersize = 6,
#                     marker = "o",
#                     linestyle = "None",
#                     label = "load: " + str(li),
#                      alpha = 0.5,
#     #                 alpha = 1.0,
#                     color = pl.cm.jet(float(li - 1) / max(L)))  # @UndefinedVariable
#
#         # predicted performance from testbed vs. actual performance
#         pl.clf()
#         p_eval = np.array(map(lambda t: float(profiler.interpolate(t)), zip(L, M)))
#
#         p_eval_sorted = np.array(sorted(list(set(p_eval))))
#         xvals = []
#         yvals = []
#         for pi in p_eval_sorted:
#             lst = P[p_eval == pi]
#             xvals.append(pi)
#             yvals.append(list(lst))
#         file(os.path.join(self.evalSettings.target_dir, "boxplot_data-%.2f" % p0), "w").write(str({"x": list(xvals), "y": yvals}))
#
#     def boxplot_plot_file(self, p0):
#         path = os.path.join(self.evalSettings.target_dir, "boxplot_data-%.2f" % p0)
#         dcts = []
#         dcts.append(eval("".join(file(path, "r").readlines())))
#         ox = []
#         oys = []
#         for d in dcts:
#             ox.extend(d["x"])
#             oys.extend(d["y"])
#         ox = np.array(ox)
#         oys = np.array(oys)
#
#         func = lambda x: x * 1e-3
#         axis_max = 3
#         axis_step = 0.5
#         x = []
#         ys = []
#         for xi, y in zip(ox, oys):
#             if len(y) > 3:
#                 ys.append(func(np.array(y)))
#                 x.append(func(xi))
#
#     #    all_bad = 0
#     #    all = 0
#     #    point = 1.4
#     #
#     #    for xi, ysi in zip(x, ys):
#     #        if xi < point:
#     #            pl.plot([xi] * len(ysi), ysi, "ob")
#     #            continue
#     #        ysi = np.array(ysi)
#     #        ybad = ysi[ysi < point]
#     #        ygood = ysi[ysi >= point]
#     #        all += len(ysi)
#     #        if len(ybad) > 0:
#     #            pl.plot([xi] * len(ybad), ybad, "or")
#     #            all_bad += len(ybad)
#     #        if len(ygood) > 0:
#     #            pl.plot([xi] * len(ygood), ygood, "og")
#     #
#     #    print float(all_bad) / all
#     #    pl.show()
#
#         ax = pl.gca()
#         pl.plot((0, axis_max), (0, axis_max), "k-", label = "Theoretical")
#         bp = ax.boxplot(ys, positions = x, sym = 'k+',
#                         widths = 0.2, bootstrap = 5000)
#         pl.xticks(np.arange(0, axis_max + axis_step / 2, axis_step))
#         pl.yticks(np.arange(0, axis_max + axis_step / 2, axis_step))
#         pl.xlim((0, axis_max))
#         pl.ylim((0, axis_max))
#         pl.legend(loc = "upper left")
#         pl.setp(bp['boxes'], color = 'black', linewidth = 1)
#         pl.setp(bp['whiskers'], color = 'black', linewidth = 1, linestyle = "-")
#         pl.setp(bp['fliers'], markersize = 3.0)
#
#     #    cnt = 0
#     #    tot = 0
#     #    for xi, ysi in zip(x, ys):
#     #        if xi < 3.4: continue
#     #        tot += len(ysi)
#     #        yi = ysi[ysi < 3.4]
#     #        cnt += len(yi)
#     #        pl.plot([xi] * len(yi), yi, "x", linestyle = "None")
#     #    print float(cnt) / tot
#
#         k = "k"
#         pl.xlabel("Predicted [%shits/s]" % k, labelpad = 2)
#         pl.ylabel("Actual [%shits/s]" % k, labelpad = 2)
#         pl.gca().set_position((0.13, 0.13, 0.8, 0.8))
#     #    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.eps" % name), (3.2, 3.2))
#     #    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.png" % name), (3.2, 3.2))
#         draw_n_save(**self.evalSettings.boxplot_draw_args)
#
#     def draw_memcached(self, remotedir, localdir, p0):
#         for i in range(1, 1 + 1):
#             data = {}
#             for line in file(os.path.join(remotedir, "p0-%.02f" % (p0), "prog-vm-%i" % i)):
#                 if line.find("target") == -1 or line.find("usage") == -1:
#                     continue
#                 for tup in line.split(" - ")[-1].split(", "):
#                     if not tup.strip():
#                         continue
#                     try:
#                         k, v = tup.split(": ")
#                         data.setdefault(k, []).append(v)
#                     except:
#                         pass
#
#             interval = 1 # plot every interval-th point
#             pl.figure(figsize = (60, 20))
#     #        data['fix'] = [int(x) - int(y) for x, y in zip(data['total_malloced'], data['statm'])]
#             for k, v in data.iteritems():
#     #            if k not in ["fix"]: continue
#                 if k not in ["target", "total", "usage", "diff", "statm"]:
#                     continue
#                 pl.plot([float(j) / 60 for j in range(len(v[::interval]))], v[::interval], label = k)
#             pl.legend(loc = 0)
#             pl.grid()
#
#     #        pl.ylim(-50, 0)
#     #        pl.xlim(0, 18800)
#             pl.show()
#
#             pl.ylabel("MB")
#             pl.xlabel("Approx. time [min]")
#             draw_n_save(**self.evalSettings.memcached_draw_args(i))

if __name__ == '__main__':
        if len(sys.argv) < 3:
            sys.exit("Must supply at least 2 parameters: exp_type and exp_name")

        exp_type = sys.argv[1]
        exp_name = sys.argv[2]

        from out.common.views import *
        # from out.common.OutputView import VMsOutputCSV

        params = NamedParameters({
            "views" : ListMultiParameterGroup(dict(
                # Static load
                mem = [VIEW_MEMORY_SUMMERY, VIEW_MEMORY_SWAP, VIEW_MEMORY_PGFAULTS],
                mem_old = [VIEW_MEMORY_SUMMERY, VIEW_MEMORY_SWAP_OLD],
                machine_perf = [VIEW_CPU, VIEW_PYTHON],

                cpu_usage = [VIEW_CPU],

                # sql_perf = [VIEW_SQL_PERFORMANCE, VIEW_SQL_QUERIES],
                # sql_perf_compare = [VIEW_SQL_COMPARE_PERFORMANCE],
                #
                # sql_perf_csv = [VMsOutputCSV.fromView(VIEW_SQL_PERFORMANCE)],
                #
                # sql_perf_for_load = [VIEW_SQL_PERF_FOR_LOAD, VIEW_SQL_PERF_FOR_LOAD_COMPARE],
                # sql_perf_for_mem = [VIEW_SQL_PERF_FOR_MEM, VIEW_SQL_PERF_FOR_MEM_COMPARE],
                #
                # sql_perf_to_mem = [VIEW_SQL_PERF_TO_MEM_FOREACH_LOAD, VIEW_SQL_PERF_TO_MEM_FOREACH_LOAD_COMPARE],

                memcached_perf = [VIEW_MCD_PREFORMANCE, VIEW_MCD_CACHE_VIEW],
                memcached_hits_rate = [VIEW_MCD_HIT_RATE],
                memcached_throughput = [VIEW_MCD_THROUGHPUT],
                # memcached_perf_to_bw = [VIEW_MCD_PERF_FOR_BW, VIEW_MCD_PERF_FOR_BW_COMPARE],
                #memcached_get_per_bw = [VIEW_MCD_PERF_TO_BW_FOREACH_LOAD, VIEW_MCD_PERF_TO_BW_FOREACH_LOAD_COMPARE],

                # bw = [VIEW_BANDWIDTH_RATE]
            ), ["mem", "machine_perf", "memcached_hits_rate", "memcached_perf"]),

            "plot_params" : BooleanParameterGroup([
                "each-data",
                "joined-data"
            ], ["each-data", "joined-data"])
        })

        params.inquire_parameters(sys.argv[3:])

        plot_params = params["plot_params"]

        EvaluationOutputPlotter(
                exp_type,
                exp_name,
                params["views"],
                debug = True).plotAll(
                                  plotEachDataSet = plot_params["each-data"],
                                  plotJoinedDataSet = plot_params["joined-data"])

