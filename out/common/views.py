'''
Common Views

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''

from OutputView import VMsView, CPUView
from out.common.visuals import *
from out.common.OutputView import VMsOutputJSON

def get_views(views_names):
    views = []
    unknown = []
    all_views = globals()

    for v in views_names:
        cur_view = v.upper().replace("-","_")

        if cur_view in all_views:
            views.append(all_views[cur_view])
            continue

        cur_view = "VIEW_" + cur_view
        if cur_view in all_views:
            views.append(all_views[cur_view])
            continue

        unknown.append(v)

    return views, unknown

VIEW_CPU = CPUView(name = "cpu-view")
VIEW_LLC_HIT_RATIO = CPUView(name = "llc-view", title="CPUs LLC Hit Ratio", key_pattern="cpu_%s_llc_hit_ratio")

VIEW_PRQ = VMsView(
               name = "prq-view",
               visuals = [VISUAL_P_VALUE, VISUAL_EXTRA_MEMORY, VISUAL_R_VALUE,
                          VISUAL_Q_VALUE, VISUAL_BILL],
               size = (20,8))

VIEW_BANDWIDTH_RATE = VMsView(
               name = "net-rate-view",
               visuals = [VISUAL_BANDWIDTH_RATE])

VIEW_LLC_ALLOC = VMsView(
               name = "llc-alloc-view",
               visuals = [VISUAL_LLC_ALLOC_WITHOUT_PIT, VISUAL_LLC_ALLOC_WITH_PIT],
               ylim = (0,20))

VIEW_MEMORY_PGFAULTS = VMsView(
               name = "mem-pgfaults-view",
               visuals = [VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN,
                          VISUAL_PG_FAULT_MINOR_PER_MIN])

VIEW_MEMORY_SWAP = VMsView(
               name = "mem-swap-view",
               visuals = [VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN,
                          VISUAL_PG_FAULT_MINOR_PER_MIN,
                          VISUAL_MEMORY_SWAP_OUT_PER_MIN,
                          VISUAL_MEMORY_SWAP_IN_PER_MIN,
                          VISUAL_MEMORY_PAGE_OUT_PER_MIN,
                          VISUAL_MEMORY_PAGE_IN_PER_MIN])

VIEW_MEMORY_SWAP_OLD = VMsView(
               name = "mem-swap-old-view",
               visuals = [VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN_OLD,
                          VISUAL_PG_FAULT_MINOR_PER_MIN_OLD,
                          VISUAL_MEMORY_SWAP_OUT_PER_MIN_OLD,
                          VISUAL_MEMORY_SWAP_IN_PER_MIN_OLD])

VIEW_MEMORY_SUMMERY = VMsView(
               name = "mem-summery-view",
               visuals=[VISUAL_MEMORY, VISUAL_MEMORY_UNUSED, VISUAL_MEMORY_FREE,
                        VISUAL_MEMORY_CACHE_BUFF])

VIEW_PYTHON = VMsView(
               name = "python-view",
               visuals = [VISUAL_MEMORY, VISUAL_PYTHON1,
                          VISUAL_PYTHON2])

VIEW_TRACE = VMsView(
                name = "exp-view",
                visuals = [VISUAL_LOAD, VISUAL_SW, VISUAL_PERFORMANCE_K,
                           VISUAL_MEMORY, VISUAL_PG_FAULT_MAJOR_PER_MIN])

VIEW_PERFORMANCE = VMsView(
               name = "perf-view",
               visuals = [VISUAL_MCD_TOTAL_GETS, VISUAL_MCD_TOTAL_HITS,
               VISUAL_MCD_TOTAL_MISSES, VISUAL_MCD_HIT_PERCENT,
               VISUAL_PG_FAULT_MAJOR_PER_MIN])

VIEW_MCD_VALUATION_NEGOTIATIONS = VMsView(
    name="mcd-valuation-neg",
    visuals=[VISUAL_REVENUE_NEGOTIATIONS, VISUAL_MCD_HIT_RATE, VISUAL_LOAD_U("Clients")]
)

VIEW_SIDE_PAYMENTS = VMsView(name="mcd-side-payments", visuals=[VISUAL_SP])

VIEW_BILL = VMsView(
               name = "bill-view",
               visuals = [VISUAL_BILL],
               size = (20,8))

VIEW_REVENUE = VMsView(
                name = "revenue-view",
                visuals = [VISUAL_REVENUE_NEGOTIATIONS])


VIEW_MCD_PREFORMANCE = VMsView(
               name = "mcd-perf",
               visuals = [VISUAL_MCD_TOTAL_HITS, VISUAL_BANDWIDTH_RATE,
                          VISUAL_LOAD_U("Clients")])

VIEW_MCD_HIT_RATE = VMsView(
               name = "mcd-hit-rate",
               visuals = [VISUAL_MCD_HIT_RATE, VISUAL_LOAD_U("Clients")])

VIEW_MCD_THROUGHPUT = VMsView(
               name = "mcd-throughput",
               visuals = [VISUAL_MCD_THROUGHPUT, VISUAL_LOAD_U("Clients")])

VIEW_MCD_CACHE_VIEW = VMsView(
               name = "mcd-cache",
               visuals = [VISUAL_MCD_HIT_RATE,
                          VISUAL_MCD_THROUGHPUT,
                          VISUAL_LLC_ALLOC_WITHOUT_PIT,
                          VISUAL_LLC_OCCUPENCY,
                          VISUAL_LOAD_U("Clients")])

VIEW_MCD_THROUGHPUT_WITH_CACHE = VMsView(
               name = "mcd-throughput",
               visuals = [VISUAL_MCD_THROUGHPUT, VISUAL_LOAD_U("Clients")])

VIEW_MCD_PERF_FOR_BW = VMsView(
               name = "mcd-perf-bw",
               visuals = [VISUAL_MCD_TOTAL_GETS_FOR_BANDWIDTH])

VIEW_MCD_PERF_FOR_BW_COMPARE = VMsView(
               name = "mcd-perf-bw-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_MCD_TOTAL_GETS_FOR_BANDWIDTH])

VIEW_MCD_PERF_TO_BW_FOREACH_LOAD = VMsView(
               name = "mcd-performance-to-bw-foreach-load-view",
               visuals = [VISUAL_MCD_PERF_TO_BW_FOREACH_LOAD],
               compare_visuals = [])

VIEW_MCD_PERF_TO_BW_FOREACH_LOAD_COMPARE = VMsView(
               name = "mcd-performance-to-bw-foreach-load-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_MCD_PERF_TO_BW_FOREACH_LOAD])

###############################################################################
# MCD memory vs. cache
###############################################################################
VIEW_MCD_PERFORMANCE_MEM_CACHE = VMsView(
               name = "mcd-perf",
               visuals = [VISUAL_MCD_HIT_RATE,
                          VISUAL_MCD_THROUGHPUT,
                          VISUAL_MCD_HIT_PERCENT,
                          VISUAL_MEMORY,
                          VISUAL_LOAD_U("Clients")
                          ])

VIEW_MCD_THROUGHPUT_MEM = VMsView(
               name = "mcd-throughput",
               visuals = [VISUAL_MCD_THROUGHPUT,
                          VISUAL_MEMORY,
                          VISUAL_LOAD_U("Clients")],
               remove_outlayers = True)

VIEW_MCD_HITRATE_MEM = VMsView(
               name = "mcd-hitrate",
               visuals = [VISUAL_MCD_HIT_RATE,
                          VISUAL_MEMORY,
                          VISUAL_LOAD_U("Clients")],
               remove_outlayers = True)

###############################################################################
# Cache
###############################################################################
VIEW_CACHE = VMsView(
               name = "cache-stats-view",
               visuals = [VISUAL_LLC_ALLOC_WITHOUT_PIT,
                          VISUAL_LLC_OCCUPENCY,
                          VISUAL_LLC_LOCAL_EXTERNAL_BANDWIDTH,
                          VISUAL_LLC_TOTAL_EXTERNAL_BANDWIDTH,
                          VISUAL_LLC_COS])

VIEW_CACHE_MONEY = VMsView(
               name = "economy-view",
               visuals = [VISUAL_CACHE_REAL_SW,
                          VISUAL_CACHE_REV,
                          VISUAL_CACHE_BILL,
                          VISUAL_CACHE_PROFIT,
                          VISUAL_CACHE_ALLOC],
               compare_visuals=[])

VIEW_CACHE_EXPECTED_MONEY = VMsView(
               name = "expected-economy-view",
               visuals = [VISUAL_CACHE_EXPECTED_SW,
                          VISUAL_CACHE_EXPECTED_REV,
                          VISUAL_CACHE_BILL,
                          VISUAL_CACHE_EXPECTED_PROFIT,
                          VISUAL_CACHE_ALLOC],
               compare_visuals=[])

VIEW_CACHE_EXPECTED_SW_COMPARE = VMsView(
               name = "expected-sw-compare-view",
               visuals = [],
               compare_visuals=[VISUAL_CACHE_EXPECTED_SW])

VIEW_CACHE_SW_COMPARE = VMsView(
               name = "sw-compare-view",
               visuals = [],
               compare_visuals=[VISUAL_CACHE_REAL_SW])

VIEW_CACHE_MONEY_RAW = VMsOutputJSON(
               name = "economy-raw",
               visuals = [VISUAL_CACHE_EXPECTED_SW,
                          VISUAL_CACHE_EXPECTED_REV,
                          VISUAL_CACHE_BILL,
                          VISUAL_CACHE_EXPECTED_PROFIT,
                          VISUAL_CACHE_ALLOC,
                          VISUAL_PHORONIX_RESULTS,
                          VISUAL_VALUATION,
                          VISUAL_LLC_OCCUPENCY,
                          VISUAL_LLC_ALLOC,
                          VISUAL_CPU_COUNT,
                          ],
               compare_visuals=[])

###############################################################################
# Phoronix Performance
###############################################################################
VIEW_PHORONIX_EVALUATION = VMsView(
               name = "phoronix-evaluation-view",
               visuals = [VISUAL_PHORONIX_RESULTS, VISUAL_LLC_ALLOC_WITHOUT_PIT, VISUAL_CPU_COUNT])

VIEW_PHORONIX_PERFORMANCE_EVALUATION = VMsView(
               name = "phoronix-performance-evaluation-view",
               visuals = [VISUAL_PHORONIX_PERFORMANCE, VISUAL_LLC_ALLOC_WITHOUT_PIT, VISUAL_CPU_COUNT],
               compare_visuals=[VISUAL_PHORONIX_PERFORMANCE])


VIEW_PHORONIX_EVALUATION_RAW = VMsOutputJSON(
               name = "phoronix-evaluation-view",
               visuals = [VISUAL_PHORONIX_RESULTS,
                          VISUAL_LLC_ALLOC_WITH_OR_WITHOUT_PIT,
                          VISUAL_CPU_COUNT,
                          VISUAL_LLC_OCCUPENCY])

###############################################################################
# Perf
###############################################################################
VIEW_SQL_EVALUATION = VMsView(
               name = "sql-evaluation-view",
               visuals = [VISUAL_SQL_TPS_WITH_COMM, VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN, VISUAL_LOAD_U("Clients")])

VIEW_SQL_PERFORMANCE = VMsView(
               name = "sql-performance-view",
               visuals = [VISUAL_SQL_TPS_WITH_COMM, VISUAL_SQL_TPS_WITHOUT_COMM,
                          VISUAL_MEMORY, VISUAL_LOAD_U("Clients")])

VIEW_SQL_PERF_FOR_LOAD = VMsView(
               name = "sql-performance-for-load-view",
               visuals = [VISUAL_SQL_PERF_FOR_LOAD])

VIEW_SQL_PERF_FOR_LOAD_COMPARE = VMsView(
               name = "sql-performance-for-load-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_SQL_PERF_FOR_LOAD])

VIEW_SQL_PERF_FOR_MEM = VMsView(
               name = "sql-performance-for-mem-view",
               visuals = [VISUAL_SQL_PERF_FOR_MEM])

VIEW_SQL_PERF_FOR_MEM_COMPARE = VMsView(
               name = "sql-performance-for-mem-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_SQL_PERF_FOR_MEM])

VIEW_SQL_PERF_TO_MEM_FOREACH_LOAD = VMsView(
               name = "sql-performance-to-mem-foreach-load-view",
               visuals = [VISUAL_SQL_PERF_TO_MEM_FOREACH_LOAD],
               compare_visuals = [])

VIEW_MCD_HITRATE_TO_CACHE_FOREACH_MEM = VMsView(
               name = "mcd-hitrate-to-cache-foreach-mem-view",
               visuals = [],
               compare_visuals = [VISUAL_MCD_HITRATE_TO_CACHE_FOREACH_MEM])

VIEW_MCD_THROUGHPUT_TO_CACHE_FOREACH_MEM = VMsView(
               name = "mcd-throughput-to-cache-foreach-mem-view",
               visuals = [],
               compare_visuals = [VISUAL_MCD_THROUGHPUT_TO_CACHE_FOREACH_MEM])

VIEW_SQL_PERF_TO_MEM_FOREACH_LOAD_COMPARE = VMsView(
               name = "sql-performance-to-mem-foreach-load-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_SQL_PERF_TO_MEM_FOREACH_LOAD])

VIEW_SQL_COMPARE_PERFORMANCE = VMsView(
               name = "sql-compare-performance-view",
               visuals = [VISUAL_LOAD_U("Clients")],
               compare_visuals = [VISUAL_SQL_TPS_WITH_COMM])

VIEW_SQL_QUERIES = VMsView(
               name = "sql-queries-view",
               visuals = [VISUAL_SQL_QUERY_LATENCY(n) for n in range(1,15)] + [VISUAL_MEMORY, VISUAL_LOAD_U("Clients")],
               size = (40,8))
