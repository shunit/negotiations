'''
Created on Feb 26, 2013

@author: eyal
'''

import matplotlib
import sys
matplotlib.use('Agg')

import os
from numpy import *  #@UnusedWildImport
import pylab as pl
import numpy as np
from etc.plots import draw_n_save, nice_surf2, pl_apply_defaults
from out.common.common import copy_files

if __name__ == '__main__':
    sim_name = "dynamic-p0-mcd-oc-1.50-1"
    pl_apply_defaults()

    if len(sys.argv) > 1:
        sim_name = sys.argv[1]

    prefix = "sim-" + ("mcd" if sim_name.find("mcd") != -1 else "mc") + "-dynamic-"

    remotedir = "%s/moc-output/sim/%s" % (os.path.expanduser("~"), sim_name)
    localdir = "%s/sim-output/%s" % (os.path.expanduser("~"), sim_name)
    #localdir = "/home/eyal/moc-output/sim/" + sim_name
    outdir = os.path.join(localdir, "plots")
    pngoutdir = os.path.join(localdir, "png-plots")
    datadir = os.path.join(localdir, "data")

    fig_pos = (0.2, 0.17, 0.6, 0.75)  #[left, bottom, width, height]
    cbar_pos = (0.82, 0.17, 0.1, 0.75)  #[left, bottom, width, height]

    if not os.path.exists(localdir): os.mkdir(localdir)
    if not os.path.exists(outdir): os.mkdir(outdir)
    if not os.path.exists(pngoutdir): os.mkdir(pngoutdir)
    if not os.path.exists(datadir): os.mkdir(datadir)

    copy_files(remotedir, datadir, ["sim-*", "info-*"])
    datadir = os.path.join(datadir, sim_name)

    results = eval("".join(file(os.path.join(datadir, "all-results")).readlines()))
    p0s = results['p0s']
    T = 1. / array(results['Ts'])

    sw_max = np.max(np.max(results['social-welfare']))
    print sw_max

    for name in (
                 'ties',
                 'utility',
                 'host-revenue',
                 'social-welfare',
                 ):

        vals = array(results[name])
        cbar_ticks = None  #np.arange(0.0, 1.01, 0.1)
        cbar_formatter = "%.2f"
        if sim_name.find("mcd") != -1:
            if name == "social-welfare":
                vals /= sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "utility":
                vals /= sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "host-revenue":
                vals /= sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "ties":
                cbar_ticks = np.arange(0, 1.01, 0.2)
                cbar_formatter = "%.1f"
        else:
            if name == "social-welfare":
                vals /= sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "utility":
                vals /= sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "host-revenue":
                vals /= sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            if name == "ties":
                cbar_ticks = np.arange(0, .02, 0.01)
                cbar_formatter = "%.2f"

        pl.clf()
        nice_surf2(p0s, T, vals, cbar_pos = cbar_pos,
                   cbar_formatter = cbar_formatter,
#                   cbar_ticks = cbar_ticks
                   )
        pl.gca().set_position(fig_pos)
        pl.xlabel("p0_mem")
        pl.ylabel("Dynamicity (" + r"$T_{auction} / T_{load}$" + ")")
        pl.gca().set_yscale("log")
        pl.xlim([min(p0s), max(p0s)])
        pl.gca().set_xticks(np.arange(min(p0s), max(p0s) + 0.1, 0.2))

        pl.gca().yaxis.set_major_formatter(pl.FormatStrFormatter("%.2f"))
        pl.gca().xaxis.set_major_formatter(pl.FormatStrFormatter("%.1f"))
        draw_n_save(os.path.join(outdir, prefix + name + ".eps"), (3.5, 2.5))
        draw_n_save(os.path.join(pngoutdir, prefix + name + ".png"), (3.5, 2.5))


    for p0 in p0s:
        pl.clf()
        for name, vals in results.iteritems():
            if name not in ("social-welfare", "utility", "host-revenue"):
                continue
            vals = vals / sw_max
            pl.plot(T, vals[:, p0s.index(p0)], label = name)

        pl.legend(loc = "best")
        pl.xlabel("Dynamicity (" + r"$T_{auction} / T_{load}$" + ")")
        pl.ylabel(r"% of $\max{SW}$")
        draw_n_save(os.path.join(outdir, prefix + ("p0=%i" % p0) + ".eps"), (3.5, 2.5))
        draw_n_save(os.path.join(pngoutdir, prefix + ("p0=%i" % p0) + ".png"), (3.5, 2.5))



