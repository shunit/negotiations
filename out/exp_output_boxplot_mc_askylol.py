'''
Created on Mar 4, 2013

@author: eyal
'''

import os

from etc.plots import pl_apply_defaults
from out.common.common import sync_files
from out.exp_output_boxplot import boxplot_create_file, boxplot_plot_file

pl_apply_defaults()

sync_data = False
create_files = True

filenames = ["negotiations-%i" % i for i in [181, 186, 188]]
localdir = "/home/sharonkl/moc-output/exp/"
remotedir = "%s/moc-output/exp" % os.path.expanduser("~")

profiler_file = "/home/sharonkl/moc/doc/profiler-mc-spare-50-satur-2000-tapuz21.xml"
field = "hits_rate"

start_min = 0
end_min = 50

limit = 100
step = 5
func = lambda x: x
k = ""
boxplot_width = 1

if __name__ == "__main__":

    paths = [os.path.join(localdir, f) for f in filenames]

    if sync_data:
        if not os.path.exists(localdir): os.mkdir(localdir)
        for f in filenames:
            sync_files(os.path.join(remotedir, f), os.path.join(localdir, f),
                       ["bidder-*", "exp-out*", "prog-*"])
    if create_files:
        for f in paths:
            boxplot_create_file(f, profiler_file, field, start_min, end_min, main_exp="p0-0.00-freq-60")
    boxplot_plot_file(paths=paths, outdir=localdir, axis_max=limit, axis_step=step, func=func, k=k, name="mc")
