'''
Created on Mar 5, 2012

@author: eyal
'''
import matplotlib
import sys
matplotlib.use('Agg')
import os
import pylab as pl
import numpy as np
from numpy import array
from etc.plots import draw_n_save, pl_apply_defaults
from mom.Plotter import HOST
from mpl_toolkits.mplot3d import Axes3D
from mom.Profiler3d import fromxml
from collections import OrderedDict 

# load all markers
markers = []
for m in matplotlib.lines.Line2D.markers:
    try:
        if len(m) == 1 and m != ' ':
            markers.append(m)
    except TypeError:
        pass

# set output directory
out_dir = None
base_dir = None
try:
    # throws exception "AttributeError" on windows    
    os.uname()
    out_dir = os.environ['HOME'] + "/sim-output/traces/%s"
    base_dir = os.environ['HOME'] + "/moc/%s"
except Exception:
    out_dir = os.environ['HOMEPATH'] + r"\Documents\moc-output\sim-output\traces\%s"
    base_dir = os.environ['HOMEPATH'] + r"\Documents\moc-network\%s"

size = (10, 5)  #(2.5, 2)
t0 = 0
start = 8
end = 12

auction_types = ['mem', 'bw']

pl_apply_defaults({
         'axes.labelsize': 6,
         'text.fontsize': 6,
         'legend.fontsize': 5,
         'font.size': 8,
         'xtick.labelsize': 6,
         'ytick.labelsize': 6,
         })

colors = ["r", "g", "b", "k"]

pol_time = lambda lst: [t - lst['time'][0] for t in lst['time']]

def exp_plot_valuation(profiler, load):
    fig = pl.figure()
    ax = fig.gca(projection='3d')
    # mem
    X = np.asarray(profiler.y)
    # bw
    Y = np.asarray(profiler.z)
    X1, Y1 = np.meshgrid(X, Y)
    Z = profiler.interpolate([load * np.ones(X1.shape), np.copy(X1), np.copy(Y1)])
    surf = ax.plot_surface(X1, Y1, Z, rstride=1, cstride=1, cmap=matplotlib.cm.coolwarm,
        linewidth=0, antialiased=False)
    ax.set_xlabel('Memory')
    ax.set_ylabel('Bandwidth')
    ax.set_zlabel('Value')
    max_val = profiler.interpolate([profiler.x[-1], X[-1], Y[-1]])
    max_val *= 1.1
    ax.set_zlim(profiler.interpolate([0,0,0]), max_val)

def exp_plot(keys, data, func, ylabel, marker = "None", linestyle = "-", xlabel = "round"):
    ret_y = {}
    #guests = sorted(set(data.keys()).difference(set((HOST,))))
    #n = len(guests) + 1
    n = len(keys) + 1
    sp = 1
    maxy = 0
    lines = []

    # draw total load in first subplot
    _xmin = 100000000
    _xmax = -1

    ys = []
    xs = []
    xfunc, yfunc = func
    #for vm in guests:
    for vm in keys:
        try:
            xi = xfunc(data[vm])
            yi = yfunc(data[vm])
            xi = xi[-len(yi):]
            _xmin = min(_xmin, xi[0])
            _xmax = max(_xmax, xi[-1])
            maxy = max(maxy, max(yi))
            ymin = min(min(yi), 0)
            ret_y.setdefault("test", []).extend(yi)
        except Exception as ex:
            continue
        ys.append(yi)
        xs.append(xi)

    #print len(xs), xs
    #print len(ys), ys

    #for i in map(int, guests):
    for i,_ in enumerate(keys):
        print i    
#        l = pl.plot(xs[i], ys[i], markersize = 4, alpha = 0.5,
        l = pl.plot(xs[i], ys[i], markersize = 4, alpha = 1.0,
                    linestyle = linestyle,
                    marker = markers[i % len(markers)],
                    color = pl.cm.jet(float(i) / len(ys)),  #@UndefinedVariable
                    label = "%s-%i" % (ylabel, i))
        lines.append(l[0])
        pl.grid(True)

    pl.legend(lines, [l.get_label() for l in lines])
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    pl.ylim(ymin = ymin, ymax = float(maxy) * 1.1)
    return ret_y

def build_cur_bid(_bids):
    bids = OrderedDict(_bids)
    value = [(0 , 0)]
    cur_val = 0
    cur_q = 0
    keys = sorted(bids.keys())
    for p in keys[::-1]:
        q = bids[p]
        t = (q, cur_val + p * (q - cur_q))
        #print value
        value.append(t)
        cur_q = q
        cur_val = value[-1][1]
    return value

def draw_exp(fname_exp, fname_info):
    data = eval(file(fname_exp, "rb").read())
    name = os.path.split(fname_exp)[1]
    info = eval(file(fname_info, "rb").read())
    global out_dir
    global base_dir
    out_dir = out_dir % name
    # create output directory if it does not already exist
    if not (os.path.exists(out_dir)):
        os.makedirs(out_dir)

    pl.clf()
    for auction_type in auction_types:
        guests = sorted(set(data[auction_type].keys()).difference(set((HOST,))))
        host = [HOST]
        exp_plot(guests, data[auction_type],
                 (pol_time, lambda lst: lst['adviser_time']),
                 "adviser_time_%s" % auction_type, "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-%s-adviser-time.png" % (name, auction_type)), size)


        exp_plot(guests, data[auction_type],
                 (pol_time, lambda lst: lst['perf']),
                 "performance_%s" % auction_type, "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-%s-perf.png" % (name, auction_type)), size)

        pl.clf()
        exp_plot(guests, data[auction_type],
                 (pol_time, lambda lst: lst['rev']),
                 "revenue_%s" % auction_type, "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-%s-rev.png" % (name, auction_type)), size)

        # memory view
        pl.clf()
        exp_plot(guests, data[auction_type],
                 (pol_time, lambda lst: lst['control_%s' % auction_type]),
                 "%s" % auction_type, "o", "-")
        draw_n_save(os.path.join(out_dir, "%s-%s.png" % (name, auction_type)), size)

        # bills
        pl.clf()
        exp_plot(guests, data[auction_type],
                 (pol_time, lambda lst: lst['control_bill_%s' % auction_type]),
                 "bill_%s" % auction_type, "o", "-")
        draw_n_save(os.path.join(out_dir, "%s-%s-bill.png" % (name, auction_type)), size)

        # auction time
        pl.clf()
        exp_plot(host, data[auction_type],
                 (pol_time, lambda lst: lst['auction_time']),
                 "auction_time_%s" % auction_type, "o", "-")
        draw_n_save(os.path.join(out_dir, "%s-%s-auction-time.png" % (name, auction_type)), size)

        # bills
        #pl.clf()
        #exp_plot(data[auction_type],
        #         (pol_time, lambda lst: lst['p_target_%s' % auction_type]),
        #         "p_target_%s" % auction_type, "o", "-")
        #draw_n_save(os.path.join(out_dir, "%s-%s-p-target.png" % (name, auction_type)), size)

    # memory bids
    pl.clf()
    for i, (label, func) in enumerate([
            ("p_mem", lambda lst: lst['p_mem']),
            ("r_mem", lambda lst: lst['r_mem']),
            ("q_mem", lambda lst: lst['q_mem'])]):
        pl.subplot(3, 1, i + 1)
        keys = sorted(set(data['mem'].keys()).difference(set((HOST,))))
        exp_plot(keys, data['mem'], (pol_time, func), label, "None", "-")
    draw_n_save(os.path.join(out_dir, "%s-pqr.png" % name), size)

    # bandwidth bids
    # bids_bw structure is for each guest [round_number][array of pairs(p,q)]
    start_round = 0
    # the bids array for each guest for the first round
    cur_bid = {g : [(p,q) for p,q in data['bw'][g]['bids_bw'][0] ] for g in data['bw'].keys() if g != 'Host'}
    cur_bid_value = {g : build_cur_bid(data['bw'][g]['bids_bw'][0]) for g in data['bw'].keys() if g != 'Host'}
    #print cur_bid['0']
    #print cur_bid_value['0']
    for cur_round in data['bw']['Host']['auction_round']:
        # an array representing the bids for rounds starting from start_round upto cur_round
        #print 'starting round %d' % cur_round
        # for #number_of_guests
        for g in data['bw'].keys():
            if g == 'Host':
                continue
            bid = data['bw'][g]['bids_bw'][cur_round]
            if cur_bid[g] == bid:
                continue
            # there is at least on bid that is new 
            #print 'Bid %s is new in round %i' % (g, cur_round)
            func_val = lambda l: [val for q, val in l]
            func_quantity = lambda l: [q for q, val in l]
            pl.clf()
            keys = sorted(set(cur_bid_value.keys()).difference(set((HOST,))))
            exp_plot(keys, cur_bid_value, (func_quantity, func_val), 'value', "None", "-", 'bandwidth')
            draw_n_save(os.path.join(out_dir, "bw-bids-%d-%d.png" % (start_round, cur_round)), size)
            # update cur_bid to represent the bid that has changed
            cur_bid = {_g : [(p,q) for p,q in data['bw'][_g]['bids_bw'][cur_round] ] for _g in data['bw'].keys() if _g != 'Host'}
            cur_bid_value = {_g : build_cur_bid(data['bw'][_g]['bids_bw'][cur_round]) for _g in data['bw'].keys() if _g != 'Host'}
            start_round = cur_round + 1
    func_val = lambda dic: [val for q, val in dic ] 
    func_quantity = lambda dic: [q for q, val in dic]
    pl.clf()
    keys = sorted(set(cur_bid_value.keys()).difference(set((HOST,))))
    exp_plot(keys, cur_bid_value, (func_quantity, func_val), 'value', "None", "-", 'bandwidth')
    draw_n_save(os.path.join(out_dir, "bw-bids-%d-%d.png" % (start_round, cur_round)), size)

    if 'opt' in data.keys():
        opt = data['opt']
        guests = sorted(set(data['opt'].keys()).difference(set((HOST,))))
        host = [HOST]
        index_time_bw = lambda lst: [i for i in xrange(len(lst['opt-alloc-bw']))]
        index_time_mem = lambda lst: [i for i in xrange(len(lst['opt-alloc-mem']))]
        index_time_perf = lambda lst: [i for i in xrange(len(lst['opt-perf']))]
        index_time_val = lambda lst: [i for i in xrange(len(lst['opt-val']))]
        index_auction_time = lambda lst: [i for i in xrange(len(lst['opt-auction-time']))]
        pl.clf()
        exp_plot(guests, opt,
                 (index_time_bw, lambda lst: lst['opt-alloc-bw']),
                 "opt-alloc-bw", "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-opt-alloc-bw.png" % (name)), size)

        pl.clf()

        exp_plot(guests, opt,
                 (index_time_mem, lambda lst: lst['opt-alloc-mem']),
                 "opt-alloc-mem", "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-opt-alloc-mem.png" % (name)), size)

        pl.clf()
        exp_plot(guests, opt,
                 (index_time_perf, lambda lst: lst['opt-perf']),
                 "opt-perf", "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-opt-perf.png" % (name)), size)

        pl.clf()
        exp_plot(guests, opt,
                 (index_time_val, lambda lst: lst['opt-val']),
                 "opt-perf", "None", "-")
        draw_n_save(os.path.join(out_dir, "%s-opt-val.png" % (name)), size)

        pl.clf()
        exp_plot(host, opt,
                 (index_auction_time, lambda lst: lst['opt-auction-time']),
                 "opt-auction-time" , "o", "-")
        draw_n_save(os.path.join(out_dir, "%s-opt-auction-time.png" % (name)), size)

    if not info is None:
        # valuation function
        pl.clf()
        profiler = fromxml(base_dir % info['adviser_args']['profiler'].replace('/', os.path.sep), 'hits_rate')
        for load in profiler.x:
            exp_plot_valuation(profiler, load)
            draw_n_save(os.path.join(out_dir, "valuation_load%d.png" % load), size)


if __name__ == '__main__':
    if len(sys.argv)>4:
        print "Usage: sim_output_trace.py <simulation_output_file> <simulation_info_file>\
                \nAssume we are running from the base directory.\n"
        exit() 
    print "plotting results for %s. Output will be available in %s.\n" % (sys.argv[1],out_dir)
    arg1 = sys.agrv[1]
    arg2 = None if len(sys.argv) < 3 else sys.argv[2]
    draw_exp(arg1, arg2)
