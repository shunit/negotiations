import argparse
import itertools

ROUND_NUMBER_INDEX = 0
HOST_CSV_FIELDS = 3
GUEST_ID_INDEX = 1
EXPERIMENTS_FOLDER_FORMAT = "/home/shunita/moc-output/exp/coalitions-%d/"

TOTAL_MEM = 3333

def read_csv_from_logs(log_files):
  # Open the file and read only CSV lines
  lines = []
  for fname in log_files:
    f = open(fname,"r")
    file_lines = f.readlines()
    #print "lines in %s: %d" % (fname,len(file_lines))
    csv_lines = [l for l in file_lines if "CSV" in l]
    #print "CSV lines in %s: %d" % (fname, len(csv_lines))
    lines += csv_lines
    f.close()
  return lines

def extract_host_data(lines, guest_ids):
  coalitions = {}
  for line in lines:
    fields = [a for a in line.split("CSV")[1].strip("\n ").split("\t")]
    if len(fields) < HOST_CSV_FIELDS:
      continue
    round_number = int(fields[ROUND_NUMBER_INDEX])
    active_coalitions = eval(fields[1])
    coalitions[round_number] = {}
    for guest_id in guest_ids:
      coalitions[round_number][guest_id] = {guest_id:1}
      for c in active_coalitions:
        #print "Active coalition: %s" % c
        c_new_keys = {}
        for k in c.keys():
          new_k = int(k.split("-")[1])
          c_new_keys[new_k] = c[k]
        if "vm-%d" % guest_id in c:
          coalitions[round_number][guest_id] = c_new_keys
  return coalitions




def extract_guest_data(lines, negparams_file, exp_id):
  guests = {}
  max_round_number = 0
  for line in lines:
    if "round_number" in line:
      continue
    if "PBELIEF" in line or "NEGPARAMS" in line:
      continue
    else:
      fields = [float(a) for a in line.split("CSV")[1].strip("\n ").split(",")]

    guest_id = int(fields[GUEST_ID_INDEX])
    round_number = int(fields[ROUND_NUMBER_INDEX])
    if round_number > max_round_number:
      max_round_number = round_number
    if round_number not in guests:
      guests[round_number] = {}
    if guest_id not in guests[round_number]:
      guests[round_number][guest_id] = {}
    
    guests[round_number][guest_id]["valuation"] = fields[5]
    guests[round_number][guest_id]["bill"] = fields[6]
    guests[round_number][guest_id]["bill_wo_coalition"] = fields[7]
    guests[round_number][guest_id]["notify_mem"] = fields[4]
    guests[round_number][guest_id]["requested_mem"] = fields[8]
    guests[round_number][guest_id]["bid_p"] = fields[11]
    guests[round_number][guest_id]["offer_value"] = fields[9]
    guests[round_number][guest_id]["accept_threshold"] = fields[10]

  rounds = int(max_round_number)+1
  #print "Rounds: %d" % rounds
  return guests, rounds


def get_all_orders(group):
  return itertools.permutations(group)

def calc_bill(allocated_q, bill_steps):
  bill = 0
  q = allocated_q
  for amount,p in bill_steps:
    if q < 0:
      return bill
    if q > amount:
      q = q - amount
      bill += amount*p
    else:
      bill += q*p
  return bill

def calculate_bills_per_subgroup(group, bid_qs, bid_ps):
  data = zip(group, bid_qs, bid_ps)
  data.sort(key=lambda x: x[2], reverse=True)
  remain = TOTAL_MEM
  bill_steps = []
  for (guest_id, q, p) in data:
    if remain>q:
      remain = remain-q
    else:
      amount = q-remain
      bill_steps.append((amount, p))
  
  bills_per_subgroup = {}
  for size in range(1, len(group)+1):
    for subset_as_tuple in itertools.combinations(group, size):
      total_q = sum([bid_qs[group.index(member)] for member in subset_as_tuple])
      bills_per_subgroup[frozenset(subset_as_tuple)] = calc_bill(total_q, bill_steps)
  bills_per_subgroup[frozenset([])] = 0
  return bills_per_subgroup
  
      
def shapley_for_coalition(bills_per_subgroup, coalition_members):
  d = {}
  for member in coalition_members:
    member_bill = bills_per_subgroup[frozenset([member])]
    #print "member bill: %s" % member_bill
    sum_of_contributions = 0
    number_of_orders = 0
    for order in get_all_orders(coalition_members):
      number_of_orders += 1
      member_index = order.index(member)
      sum_of_contributions += (bills_per_subgroup[frozenset(order[:member_index])] + # all the players before i
                               member_bill - # i's bill
                               bills_per_subgroup[frozenset(order[:member_index+1])]) # all players up to i, including i
    member_discount = sum_of_contributions*1.0/number_of_orders
    #print("guest: %s, shapley: %s, sum contrib: %s, num_orders: %s" %(member, member_discount, sum_of_contributions, number_of_orders))
    d[member] = member_discount
  return d

def best_coalition_for_guest_in_round(guest, bills_per_subgroup, all_guests, debug_mode=False):
  best_coalition = []
  highest_profit = 0
  # go over all coalitions that include the guest
  all_other_guests = [g for g in all_guests if g != guest]
  max_bill_for_coalition_size = {}
  need_to_increase_size = True

  for r in range(len(all_other_guests)+1):
    if not need_to_increase_size:
      return best_coalition, highest_profit
    if debug_mode:
      if r>6:
        continue
    print "calc best coalition for %s: coalitions of size %s. n = %s" % (guest, r, len(all_other_guests))
    # Only check larger coalitions if there is a coalition of size r that does not cover the entire "outside".
    need_to_increase_size = False
    max_bill_for_coalition_size[r] = 0
    for comb in itertools.combinations(all_other_guests, r):
      coalition = set(comb)
      coalition.add(guest)
      total_bill = bills_per_subgroup[frozenset(coalition)]

      if total_bill > max_bill_for_coalition_size[r]:
        max_bill_for_coalition_size[r] = total_bill
      if r<1 or total_bill > max_bill_for_coalition_size[r-1]:
        need_to_increase_size = True

      # calc shapley for the guest in each of them
      d = shapley_for_coalition(bills_per_subgroup, coalition)
      if d[guest]> highest_profit:
        best_coalition = coalition
        highest_profit = d[guest] 
  # return the best one
  return best_coalition, highest_profit
  
def should_calc_best_coalition(guest, rnd):
  return True
  #return r % 5 == 1 and guest == 1

def calculate_shapley_for_experiment(guest_data, rounds_num, coalitions):
  guests = guest_data[0].keys()
  for r in range(rounds_num):
    round_coalitions = coalitions[r]
    print round_coalitions

    qs = [guest_data[r][g]["requested_mem"] for g in guests]
    ps = [guest_data[r][g]["bid_p"] for g in guests]
    bills_per_subgroup = calculate_bills_per_subgroup(guests, qs, ps)

    handled_guests = []
    for g in round_coalitions:
      if g in handled_guests:
        continue
      coalition = round_coalitions[g]
      d = shapley_for_coalition(bills_per_subgroup, coalition.keys())
      for member in d.keys():
        guest_data[r][member]["shapley_profit"] = d[member]
        guest_data[r][member]["actual_coalition"] = ".".join([str(i) for i in list(coalition.keys())])
        if should_calc_best_coalition(member, r):
          best_coalition, highest_profit = best_coalition_for_guest_in_round(member, bills_per_subgroup, guests, debug_mode=False)
          guest_data[r][member]["best_coalition"] = ".".join([str(i) for i in list(best_coalition)])
          guest_data[r][member]["highest_shapley_profit"] = highest_profit
        else:
          guest_data[r][member]["best_coalition"] = ""
          guest_data[r][member]["highest_shapley_profit"] = ""
        handled_guests.append(member)
  return guest_data
          

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Create graphs from experiments.')
  parser.add_argument("--exp_ids", nargs='+', type=int,
                        help='experiment ids to take data from. E.g., --exp_ids 202 204')
  parser.add_argument("--num_guests", type=int, help='The number of guests in the experiment, e.g., 5.')
  parser.add_argument("--min_exp_id", type=int, default=0, help="define a range of exp ids to work on. Use with max_exp_id.")
  parser.add_argument("--max_exp_id", type=int, default=0, help="define a range of exp ids to work on. Use with min_exp_id.")
  args = parser.parse_args()
  num_guests = args.num_guests
  guest_ids = [i for i in range(1, num_guests+1)]
  exp_ids = args.exp_ids
  if not exp_ids:
    exp_ids = range(args.min_exp_id, args.max_exp_id)
  for exp in exp_ids:
    bidder_files = ["/home/shunita/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"%(exp, i) for i in guest_ids]
    host_file = "/home/shunita/moc-output/exp/coalitions-%d/p0-0.00-freq-60/exp-out" % exp
    guest_data, rounds_num = extract_guest_data(read_csv_from_logs(bidder_files), None, exp)
    coalitions = extract_host_data(read_csv_from_logs([host_file]), guest_ids)
    new_guest_data = calculate_shapley_for_experiment(guest_data, rounds_num, coalitions)
  #print new_guest_data
  # TODO: make it work with multiple experiments
  f = open("/home/shunita/moc-output/exp/coalitions-%d/shapley1.csv" % exp, "w")
  f.write("round,guest,OV,AT,p,q,bill_wo_coalition,profit,shapley_profit,best_coalition,highest_shapley_profit,best_coalition_size,actual_coalition,actual_coalition_size\n")
  for rnd in new_guest_data:
    for g in new_guest_data[rnd]:
      data = new_guest_data[rnd][g]
      shapley = data["shapley_profit"] if "shapley_profit" in data else 0
      if "best_coalition" not in data.keys():
        print "round: %s, data: %s" % (rnd,data)
      f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (
              rnd, g, data["offer_value"], data["accept_threshold"], 
              data["bid_p"], data["requested_mem"], data["bill_wo_coalition"],
              data["bill_wo_coalition"] - data["bill"], shapley,
              data["best_coalition"], data["highest_shapley_profit"], len(data["best_coalition"].split(".")), 
              data["actual_coalition"], len(data["actual_coalition"].split("."))))
  f.close()

