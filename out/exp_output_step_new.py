'''
Created on Jun 4, 2014

@author: dl8
'''
from out.common.EvaluationOutput import EvaluationOutputPlotter
from out.common.OutputView import VIEW_MEMORY, VIEW_TRACE, \
    VIEW_PERFORMANCE
from etc.Settings import Settings
    
import pylab as pl
from etc.plots import draw_n_save
import os

def draw_memcached(remotedir, localdir, duration, load = None):
    
    for i in range(1, 1 + 1):
        data = {}
        path = ""
        if load is None:
            path = os.path.join(remotedir, "mcd-step-%i" % (duration), "prog-vm-%i" % i)
        else:
            path = os.path.join(remotedir, "mcd-step-%i-load-%i" % (duration, load), "prog-vm-%i" % i)

        for line in file(path):
            if line.find("target") == -1 or line.find("usage") == -1:
                continue
            for tup in line.split(" - ")[-1].split(", "):
                if not tup.strip():
                    continue
                try:
                    k, v = tup.split(": ")
                    data.setdefault(k, []).append(v)
                except:
                    pass

        interval = 1 # plot every interval-th point
        pl.figure(figsize = (60, 20))
#        data['fix'] = [int(x) - int(y) for x, y in zip(data['total_malloced'], data['statm'])]
        for k, v in data.iteritems():
#            if k not in ["fix"]: continue
            if k not in ["target", "total", "usage", "diff", "statm"]:
                continue
            pl.plot([float(j) / 60 for j in range(len(v[::interval]))], v[::interval], label = k)
        pl.legend(loc = 0)
        pl.grid()

#        pl.ylim(-50, 0)
#        pl.xlim(0, 18800)
        pl.show()

        pl.ylabel("MB")
        pl.xlabel("Approx. time [min]")
        draw_n_save(os.path.join(localdir, "vm-%i-memcached.png" % (i)), (40, 8))

if __name__ == '__main__':
    exps = ["step-mcd-15", "step-mcd-16"]

    views = [VIEW_MEMORY, VIEW_TRACE, VIEW_PERFORMANCE]

    for exp in exps:
        print "Plotting experiment %s" % (exp)
        EvaluationOutputPlotter("step", exp, views).plotAll()
    
        remotedir = os.path.join(Settings.user_home(), Settings.output_dir(), "step", exp)
        localdir = os.path.join(Settings.user_home(), Settings.output_dir(), "-".join(["step", Settings.hostname()]), exp)
        print "remote: ", remotedir
        print "local: ", localdir
        print os.listdir(remotedir)
        for dirname in os.listdir(remotedir):
            print dirname
            if not os.path.isdir(os.path.join(localdir, dirname)): continue
            split_dir = dirname.split("-")
            duration = 1200
            load = 8

            if "step" in split_dir:
                duration = int(split_dir[split_dir.index("step") + 1])
            if "load" in split_dir:
                load = int(split_dir[split_dir.index("load") + 1])


            print "MCD duration ", duration
            print "MCD load ", load

            draw_memcached(remotedir, os.path.join(localdir, dirname), duration, load)
