#from time import strptime, strftime
from datetime import datetime
import os
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import numpy as np

LOG_FILE_FORMAT = "/home/shunita/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"
OUT_DIR_FORMAT = "/home/shunita/moc-output/exp/coalitions-%d/decisions"
OUT_FILE_FORMATS = {"VISDEC": "/home/shunita/moc-output/exp/coalitions-%d/decisions/vm-%d-round-%d.png",
                    "VISPROFIT": "/home/shunita/moc-output/exp/coalitions-%d/decisions/vm-%d-round-%d-coalition_fear.png",
                    "VISPQ": "/home/shunita/moc-output/exp/coalitions-%d/decisions/vm-%d-round-%d-p_debug.png",
                    "VISSIM": "/home/shunita/moc-output/exp/coalitions-%d/decisions/vm-%d-round-%d-whatif-%s.png"}


def process_file(log_file, prefix):
    f = open(log_file, "r")
    lines = f.readlines()
    f.close()
    round_to_tuples = {}
    for line in lines:
        parts = line.split(prefix)
        if len(parts) < 2: # The prefix is not in there
            continue
        rnd_search = re.search("round ([0-9]*)", parts[1])
        if not rnd_search or len(rnd_search.groups())<1:
            continue
        rnd = int(rnd_search.groups()[0])
        if rnd not in round_to_tuples:
            round_to_tuples[rnd] = {}
        if prefix == "VISDEC":
            if "minimal p" in parts[1]:
                min_p = float(parts[1].split(":")[1].strip())
                round_to_tuples[rnd]["min_p"] = min_p
                continue
            # tuples are of the form (q, V, estimated_bill)
            tuples = eval(parts[1].split(":")[1].strip())
            round_to_tuples[rnd]["tuples"] = tuples
        elif prefix == "VISPROFIT":
            # tuple is of the form (q, delta_V, delta_bill, profit_delta, q_prev)
            single_tuple = eval(parts[1].split(":")[1].strip())
            round_to_tuples[rnd][single_tuple[0]] = single_tuple
        elif prefix == "VISSIM":
            # tuple is of the form (q, delta_V, delta_bill, profit_delta, q_prev, guest profit part)
            single_tuple = eval(parts[1].split(":")[1].strip())
            profit_part = single_tuple[-1]
            t = single_tuple[:-1]
            if profit_part not in round_to_tuples[rnd]:
                round_to_tuples[rnd][profit_part] = {}
            round_to_tuples[rnd][profit_part][single_tuple[0]] = t
        elif prefix=="VISPQ":
            # tuples are of the form (q, p, U)
            if "good" in parts[1]:
                round_to_tuples[rnd]["good"] = eval(parts[1].split(":")[1].strip())
            if "bad" in parts[1]:
                round_to_tuples[rnd]["bad"] = eval(parts[1].split(":")[1].strip())
            if "minimal" in parts[1]:
                min_p,max_p,bid_p = eval(parts[1].split(":")[1].strip())
                round_to_tuples[rnd]["min_p"] = min_p
                round_to_tuples[rnd]["max_p"] = max_p
                round_to_tuples[rnd]["bid_p"] = bid_p
            
    if prefix == "VISPROFIT":
        for rnd in round_to_tuples.keys():
            round_to_tuples[rnd] = {"tuples": sorted(round_to_tuples[rnd].values(), key=lambda x: x[0])}
    if prefix == "VISSIM":
        for rnd in round_to_tuples.keys():
            for pp in round_to_tuples[rnd].keys():
                round_to_tuples[rnd][pp] = sorted(round_to_tuples[rnd][pp].values(), key=lambda x: x[0])
    return round_to_tuples



if __name__ == "__main__":
    matplotlib.rcParams.update({'font.size': 7})
    parser = argparse.ArgumentParser(description='Create a graph of valuation and estimated bill as a function of q for an adviser in a certain round.')
    parser.add_argument("--exp_id", type=int, dest='exp_id', help='Experiment id, e.g., 202.')
    parser.add_argument("--guest_ids", nargs='+', type=int, dest='guest_ids', 
                        help='The ids of the guests for which the graphs should be created, e.g., --guest_ids 2 3 5.')
    parser.add_argument("--rounds", nargs='+', type=int, dest='rounds',
                        help='The auction rounds for which the graphs should be created, e.g., --rounds 1 4 15.' )
    parser.add_argument("--prefix", type=str, dest='prefix', help='Which prefix to look for in a log line, e.g., VISDEC')
    args = parser.parse_args()

    experiment_id = args.exp_id
    guest_ids = args.guest_ids
    rounds = args.rounds
    prefix = args.prefix
    if guest_ids and rounds and not os.path.exists(OUT_DIR_FORMAT % experiment_id):
        os.mkdir(OUT_DIR_FORMAT % experiment_id)
    for i in guest_ids:
        fname = LOG_FILE_FORMAT % (experiment_id, i)
        rnd_to_tuples = process_file(fname, prefix)
        for r in rounds:
            if prefix == "VISDEC":
                tuples = rnd_to_tuples[r]["tuples"]
                min_p = rnd_to_tuples[r]["min_p"]
                plt.figure()
                plt.grid()
                plt.title("Decision Making Agent %d Round %d" % (i,r))
                q, v, bill = zip(*tuples)
                profit = [v[j]-bill[j] for j in range(len(q))]
                v_line, = plt.plot(q, v, label="Valuation")
                bill_line, = plt.plot(q, bill, label="Bill")
                profit_line, = plt.plot(q, profit, label="Profit")
                min_p_line, = plt.plot(q, [min_p*q1 for q1 in q], "--", label="min_p")
                plt.xlabel("Memory Quantity [MB]")
                plt.ylabel("$/Sec")
                plt.legend(handles=[v_line, bill_line, profit_line, min_p_line])
                plt.savefig(OUT_FILE_FORMATS[prefix] % (experiment_id, i, r), format="png")
            elif prefix == "VISPROFIT":
                tuples = rnd_to_tuples[r]["tuples"]
                plt.figure()
                plt.grid()
                plt.title("Decision Making Agent %d Round %d: Coalitions Considered" % (i,r))
                q, v_delta, bill_delta, profit_delta, q_prev = zip(*tuples)
                v_line, = plt.plot(q, v_delta, label="Valuation Delta", linewidth=3)
                bill_line, = plt.plot(q, bill_delta, label="Bill Delta", linewidth=3)
                profit_line, = plt.plot(q, profit_delta, label="Profit Delta", linewidth=3)
                q_prev_line = plt.axvline(x=q_prev[0], linewidth=2, color='k')
                plt.text(x=q_prev[0], y=20, s=str(q_prev[0])) 
                plt.xlabel("Memory Quantity [MB]", fontsize=24)
                plt.ylabel("delta($)", fontsize=24)
                #plt.axes().set_xlim(left = 0, right=6000)
                #plt.axes().set_ylim(bottom = -1000, top=1000)
                lgd = plt.legend(handles=[v_line, bill_line, profit_line], bbox_to_anchor=(1.05, 1), loc=2,  borderaxespad=0.)
                plt.savefig(OUT_FILE_FORMATS[prefix] % (experiment_id, i, r), format="png", bbox_extra_artists=(lgd,), bbox_inches='tight')
            elif prefix == "VISSIM":
                for pp in rnd_to_tuples[r]:
                    tuples = rnd_to_tuples[r][pp]
                    plt.figure()
                    plt.title("Decision Agent %d Round %d Profit Part %s" % (i,r,pp))
                    q, v_delta, bill_delta, profit_delta, q_prev = zip(*tuples)
                    v_line, = plt.plot(q, v_delta, label="Valuation Delta")
                    bill_line, = plt.plot(q, bill_delta, label="Bill Delta")
                    profit_line, = plt.plot(q, profit_delta, label="Profit Delta")
                    q_prev_line = plt.axvline(x=q_prev[0], linewidth=1, color='k')
                    plt.text(x=q_prev[0], y=20, s=str(q_prev[0]))
                    plt.xlabel("Memory Quantity [MB]")
                    plt.ylabel("delta($)")
                    plt.axes().set_xlim(left = 0, right=6000)
                    #plt.axes().set_ylim(bottom = -100, top=100)
                    lgd = plt.legend(handles=[v_line, bill_line, profit_line], bbox_to_anchor=(1.05, 1), loc=2,  borderaxespad=0.)
                    plt.savefig(OUT_FILE_FORMATS[prefix] % (experiment_id, i, r, pp), format="png", bbox_extra_artists=(lgd,), bbox_inches='tight')
            elif prefix == "VISPQ":
                good = rnd_to_tuples[r]["good"]
                bad = rnd_to_tuples[r]["bad"]
                min_p = rnd_to_tuples[r]["min_p"]
                max_p = rnd_to_tuples[r]["max_p"]
                bid_p = rnd_to_tuples[r]["bid_p"]
                fig, ax = plt.subplots()
                plt.title("Decision Making Agent %d Round %d: debugging" % (i,r))
                qg, pg, Ug = zip(*good)
                Ug_line = ax.scatter(qg, Ug, label="Good Utility", marker="o", color="g", alpha=0.5)
                pg_line = ax.scatter(qg, pg, label="Good p", marker="o", color="b", alpha=0.5)
                if bad:
                    qb, pb, Ub = zip(*bad)
                    Ub_line = ax.scatter(qb, Ub, label="Bad Utility", marker="x", color="k",alpha=0.5)
                    pb_line = ax.scatter(qb, pb, label="Bad p", marker="x", color="r", alpha=0.5)
                    all_q = sorted(np.append(qb,qg))
                    gb_line = ax.axvline(x=qb[0], linewidth=1, color='k')
                    ax.text(x=qb[0], y=100, s=str(qb[0]))
                else:
                    all_q = qg
                
                min_p_line = ax.plot(all_q, [min_p for q in all_q], label="min_p", color="m")
                max_p_line = ax.plot(all_q, [max_p for q in all_q], "c--", label="max_p")
                lgd = ax.legend(bbox_to_anchor=(1.05, 1), loc=2,  borderaxespad=0.)
                plt.savefig(OUT_FILE_FORMATS[prefix] % (experiment_id, i, r), format="png", bbox_extra_artists=(lgd,), bbox_inches='tight')
