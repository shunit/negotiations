'''
Created on Mar 5, 2012

@author: eyal
'''
import os
import pylab as pl
import numpy as np
from etc.plots import draw_n_save
from mom.Plotter import POL, HOST
from out.common.common import sync_files
#from numpy import *

sim_name = "dynamic_mc-estimator-oc-1.50-5"
localdir = "/home/eyal/moc-output/sim-dshad4/" + sim_name
remotedir = "%s/moc-output/sim/%s" % (os.path.expanduser("~"), sim_name)
size = (5, 10)  #(2.5, 2)
t0 = 0
start = 8
end = 12

pl.rcParams.update({
         'axes.labelsize': 6,
         'text.fontsize': 6,
         'legend.fontsize': 5,
         'font.size': 8,
         'xtick.labelsize': 6,
         'ytick.labelsize': 6,
         })

colors = ["r", "g", "b", "k"]

pol_time = lambda lst: [t - lst[POL]['time'][0] for t in lst[POL]['time']]

def exp_plot(data, funcs, labels, markers, linestyles, _ymax = None, _ymin = None):
    ret_y = {}
    guests = sorted(set(data.keys()).difference(set((HOST,))))
    n = len(guests) + 1
    sp = 1
    maxy = [0] * len(funcs)
    axss = []
    lines = []

    # draw total load in first subplot
    min_size = 10000000
    _xmin = 100000000
    _xmax = -1
    for vm in guests:
        min_size = min(min_size, len(gdata[vm][POL]["load"]))
    sum_load = np.zeros(min_size)
    for vm in guests:
        sum_load += np.array(gdata[vm][POL]['load'][:min_size])

#    pl.subplot(n, 1, 1)
#    pl.plot(mon_time(gdata[0])[:min_size], sum_load, "k")

    for vm in guests:
        lines = []
        pl.subplot(n - 1, 1, sp)
        axs = [pl.gca()]
        for i in range(len(funcs) - 1):
            axs.append(pl.twinx(axs[0]))
        axss.append(axs)
        axl = pl.twinx(axs[0])
        try:
            y = []
            x = []
            for i in range(len(funcs)):
                xfunc, yfunc = funcs[i]
                xi = xfunc(data[vm])
                yi = yfunc(data[vm])
                xi = xi[-len(yi):]
                _xmin = min(_xmin, xi[0])
                _xmax = max(_xmax, xi[-1])
                maxy[i] = max(maxy[i], max(yi))
                x.append(xi)
                y.append(yi)
            ret_y.setdefault("test", []).extend(y[0])
        except Exception as ex:
            continue

        for i in range(len(y)):
#            l = axs[i].plot(x[i], y[i], markersize = 4, alpha = 0.5,
            l = axs[i].plot(x[i], y[i], markersize = 4, alpha = 1.0,
                            linestyle = linestyles[i],
                            marker = markers[i],
                            c = colors[i],
                            label = "%s-%i" % (labels[i], sp))
            lines.append(l[0])

        try:
            axl.plot(pol_time(gdata[vm]),
                     data[vm][POL]['load'], linestyle = "--",
                     color = "k", label = "load-%i" % sp)
        except KeyError:
            pass
        axl.set_ylim((0, 20))
        axs[0].set_ylabel(vm)

        axs[0].grid(True)

        if sp != n - 1:
            for ax in axs:
                ax.get_xaxis().set_ticklabels([])
        sp += 1

    if len(maxy) > 2:
        y2max = max(maxy[1:])
        for i in range(1, len(maxy)):
            maxy[i] = y2max

    for axs in axss:
        for i in range(len(funcs)):
            axs[i].set_ylim((0 if _ymin is None else _ymin,
                             (maxy[i] * 1.1) if _ymax is None else _ymax))
            axs[i].set_xlim((_xmin, _xmax))

    pl.legend(lines, [l.get_label() for l in lines])
    try:
        axss[-1][0].set_xlabel("round")
    except IndexError:pass
    return ret_y

def draw_exp(_oc, _a):
    global gdata
    read_dir = localdir  # os.path.join(dir, "T-%i-t-%i" % (_oc, _a))
    results_file = "sim-oc-%.2f-a-%.2f" % (_oc, _a)

    out_dir = os.path.join(read_dir, "details")
    try: os.mkdir(out_dir)
    except: pass

    gdata = eval(file(os.path.join(read_dir, results_file), "r").readline())

    data = gdata

    for p in ["auction_mem", "ties"]:  # , 'p_in_min_mem', 'p_out_max_mem']:
        pl.clf()
        pl.plot([x - data["Host"][POL]["time"][0] for x in data["Host"][POL]["time"]],
                data["Host"][POL][p], color = "g",
#                marker = "None", alpha = 0.5, label = "bla", linestyle = "-")
                marker = "None", alpha = 1.0, label = "bla", linestyle = "-")
        pl.legend(loc = "best")
        pl.grid(True)
        pl.ylim((0, pl.ylim()[1]))
        draw_n_save(os.path.join(out_dir, "host-%s-oc-%.2f-a-%.2f.png" % (p, _oc, _a)), size)

    pl.clf()
    exp_plot(data,
             [(pol_time, lambda lst: lst[POL]['control_mem']),
              (pol_time, lambda lst: lst[POL]['perf'])],
             ["memory", "throughput"], ["None", "None"], ["-", "-"])
    draw_n_save(os.path.join(out_dir, "mem-perf-oc-%.2f-a-%.2f.png" % (_oc, _a)), size)

    # memory view
    pl.clf()
    exp_plot(data,
             [(pol_time, lambda lst: lst[POL]['control_mem'])],
             ["memory"], ["None"], ["-"])
    draw_n_save(os.path.join(out_dir, "memory-oc-%.2f-a-%.2f.png" % (_oc, _a)), size)

    # bills
    pl.clf()
    exp_plot(data,
             [(pol_time, lambda lst: lst[POL]['control_bill'])],
             ["bill"], ["o"], ["-"])
    draw_n_save(os.path.join(out_dir, "bill-oc-%.2f-a-%.2f.png" % (_oc, _a)), size)

    pl.clf()
    exp_plot(data,
             ((pol_time, lambda lst: lst[POL]['p']),
              (pol_time, lambda lst: [x[0][0] for x in lst[POL]['rq']]),
              (pol_time, lambda lst: [x[0][1] for x in lst[POL]['rq']])),
             ["p", "r", "q"], ["None", "^", "v"], ["-"] * 3)
    draw_n_save(os.path.join(out_dir, "prq-oc-%.2f-a-%.2f.png" % (_oc, _a)), size)

if __name__ == '__main__':
    if not os.path.exists(localdir): os.mkdir(localdir)
    sync_files(remotedir, localdir, ["bidder-*", "exp-out*", "prog-*"])

    for f in os.listdir(localdir):
        if f.endswith("png") or not f.startswith("sim-oc"):
            continue
        vals = f.split("-")
        draw_exp(eval(vals[2]), eval(vals[4]))
