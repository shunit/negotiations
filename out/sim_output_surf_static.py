'''
Created on Feb 11, 2013

@author: eyal
'''
import os
import collections
from sim.SimResultsProcess import get_tools
from sim.simulate import HOST
import numpy as np
import pylab as pl
from etc.plots import nice_surf, draw_n_save, pl_apply_defaults
from out.common.common import sync_files
from numpy import array  #@UnusedImport

sim_name = "sim-mc-4"
localdir = "/home/eyal/moc-output/sim-dshad4/" + sim_name
remotedir = "%s/moc-output/sim/%s" % (os.path.expanduser("~"), sim_name)
file_type = "eps"
fig_size = (2.5, 2)
fig_pos = (0.17, 0.15, 0.6, 0.8)  #[left, bottom, width, height]
cbar_pos = (0.8, 0.15, 0.1, 0.8)  #[left, bottom, width, height]

pl_apply_defaults()

parse_oc = lambda fname: round(float(fname.split("-")[2]), 2)
parse_a = lambda fname: round(float(fname.split("-")[4]), 2)
outdir = os.path.join(localdir, "plots-eps")
pngoutdir = os.path.join(localdir, "plots-png")
datadir = os.path.join(localdir, "data")

if __name__ == '__main__':

    if not os.path.exists(localdir): os.mkdir(localdir)
    if not os.path.exists(outdir): os.mkdir(outdir)
    if not os.path.exists(pngoutdir): os.mkdir(pngoutdir)
    if not os.path.exists(datadir): os.mkdir(datadir)
    sync_files(remotedir, datadir, ["bidder-*", "exp-out*", "prog-*", "plots"])

    ## read data from files
    opt = {}
    res = collections.defaultdict(dict)
    mem = set()
    alpha = set()

    for f in os.listdir(datadir):
        if f.startswith("sim"):
            oc = parse_oc(f)
            a = parse_a(f)
            res[oc][a] = eval(file(os.path.join(datadir, f)).readline())
            mem.add(oc)
            alpha.add(a)
        elif f.startswith("opt"):
            oc = parse_oc(f)
            opt[oc] = eval(file(os.path.join(datadir, f)).readline())

    mem = sorted(list(mem))
    alpha = sorted(list(alpha))

    ## prepare main results dictionary, contains matrices of values:
    results = {}
    sz = (len(mem), len(alpha))
    tools = get_tools()
    for name, t in tools.items():
        results[name] = np.ndarray(sz)

    # iterate all evaluation points
    for i, m in enumerate(mem):
        for j, a in enumerate(alpha):

            if set(alpha) != set(res[m].keys()):
                continue

            tools = get_tools()
            for name, data in res[m][a].iteritems():
                # process the host and guests
                if name == HOST:
                    for t in tools.itervalues(): t.process_host(data)
                else:
                    # add the optimized results to the simulation results
                    data.update(opt[m][name])
                    for t in tools.itervalues(): t.process_guest(data)

            # add the simulation value to the result:
            for name, t in tools.items():
                results[name][i, j] = t.finish()

    sw_max = np.max(results["social-welfare"])
    file(os.path.join(localdir, "sw_max.txt"), "w").write(str(sw_max))

    for name, vals in results.iteritems():
        if name == "perf_waste":
            continue
        colorbar_suffix = ""
        zlim = None
        cbar_ticks = None
        cbar_formatter = "%i"

        if sim_name.find("mcd") != -1:
            if name == "social-welfare":
                vals = vals / sw_max
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "utility":
                vals = vals / sw_max
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "host-revenue":
                vals = vals / sw_max
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "ties-avg":
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "poa":
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 0.06, 0.05)
            elif name == "waste":
                cbar_ticks = range(0, 1501, 300)
            elif name == "ties":
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.2)
        else:
            if name == "social-welfare":
                vals = vals / sw_max
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "utility":
                vals = vals / sw_max
                cbar_formatter = "%.1f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "host-revenue":
                vals = vals / sw_max
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 1.01, 0.1)
            elif name == "poa":
                cbar_formatter = "%.2f"
                cbar_ticks = np.arange(0.0, 0.06, 0.05)
            elif name == "waste":
                cbar_ticks = range(0, 1501, 300)
            elif name == "ties":
                cbar_formatter = "%.2f"
                cbar_ticks = [0, 1e-5]

        pl.clf()
        nice_surf(alpha, mem, vals, zlim = zlim,
                  cbar_pos = cbar_pos,
                  cbar_formatter = cbar_formatter,
                  cbar_ticks = cbar_ticks)
        pl.gca().set_position(fig_pos)
        pl.xlim([min(alpha), max(alpha)])
        pl.ylim([min(mem), max(mem)])
        pl.xlabel("Reclaim Factor")
        pl.ylabel("Memory Overcommitment")
        pl.grid(True)
        draw_n_save(os.path.join(outdir, name + ".eps"), fig_size)
        draw_n_save(os.path.join(pngoutdir, name + ".png"), fig_size)

