import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse
import sys
import numpy as np
import pprint as pp
import csv
import os
import re
import getpass

#experiment_id = 202
#num_guests = 5

GUEST_ID_INDEX = 1
ROUND_NUMBER_INDEX = 0
HOST_CSV_FIELDS = 3
USER = getpass.getuser()
EXPERIMENTS_FOLDER_FORMAT = "/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/"
#EXPERIMENTS_FOLDER_FORMAT = "E:\\University\\CloudProject2019\\EXPs_NEW\\coalitions-%d\\"

COLORS = ['r', # red
          '#8BC34A', # light green
          '#FFEB3B', # yellow
          'b', # blue
          '#F06292', # pink
          '#FF5722', # orange
          '#9C27B0', # purple
          '#6D4C41', # brown
          '#546E7A', # gray
          '#2E7D32', #dark green
         ]
BASE_MEM = 653

#SW
STYLE_SET1 = [{"marker":"v", "c":"b", "alpha":0.8},
              {"marker":"d", "c": "r", "alpha":1},
              {"marker":"s", "c": "g","alpha":0.8}]
# profit
STYLE_SET2 = [{"marker":"o", "c":"b", "alpha":0.8},
              {"marker":"^", "c": "r", "alpha":1},
              {"marker":"s", "c": "g","alpha":0.8}]



def ecdf(x):
    xs = np.sort(x)
    ys = np.arange(1, len(xs)+1)/float(len(xs))
    return xs, ys

def read_csv_from_logs(log_files):
  # Open the file and read only CSV lines
  lines = []
  for fname in log_files:
    f = open(fname,"r")
    file_lines = f.readlines()
    #print "lines in %s: %d" % (fname,len(file_lines))
    csv_lines = [l for l in file_lines if "CSV" in l]
    #print "CSV lines in %s: %d" % (fname, len(csv_lines))
    lines += csv_lines
    f.close()
  return lines

def extract_host_data(lines, guest_ids):
  coalitions = {}
  profit_parts = []
  profit_parts_with_guest_id = []
  guests_in_coalition_count = []
  for line in lines:
    fields = [a for a in line.split("CSV")[1].strip("\n ").split("\t")]
    if len(fields) < HOST_CSV_FIELDS:
      continue
    round_number = int(fields[ROUND_NUMBER_INDEX])
    coalitions[round_number] = {"largest_coalition_size": int(fields[2])}
    active_coalitions = eval(fields[1])
    guest_count = 0
    for guest_id in guest_ids:
      coalitions[round_number][guest_id] = guest_id
      for c in active_coalitions:
        if "vm-%d" % guest_id in c:
          # Each coalition is identified by the smallest ID in it.
          coalitions[round_number][guest_id] = min([int(k.split("-")[1]) for k in c.keys()])
          profit_parts.append(c["vm-%d"%guest_id])
          profit_parts_with_guest_id.append((guest_id, c["vm-%d"%guest_id]))
          guest_count += 1
    guests_in_coalition_count.append(guest_count)
  return coalitions,profit_parts, guests_in_coalition_count, profit_parts_with_guest_id

def order_guest_ids_by_bid_price(guests, rnd):
  guest_ids = guests.keys()
  return sorted(guest_ids, key=lambda g:guests[g][rnd]["bid_p"], reverse=True)

def extract_guest_data(lines, negparams_file, exp_id):
  guests = {}
  max_round_number = 0
  for line in lines:
    if "round_number" in line:
      continue

    if "PBELIEF" in line:
      fields = line.split("PBELIEF")[1].strip("\n ").split(",")
      coalition = (fields[2] == "True")
      broke = (fields[3] == "True")
    elif "NEGPARAMS" in line:
      if negparams_file is None:
        continue
      negparams_file.write("%s,%s\r\n" % (exp_id, line.split("NEGPARAMS")[1].strip("\n ")))
      continue
    else:
      fields = [float(a) for a in line.split("CSV")[1].strip("\n ").split(",")]

    guest_id = int(fields[GUEST_ID_INDEX])
    round_number = int(fields[ROUND_NUMBER_INDEX])
    if round_number > max_round_number:
      max_round_number = round_number
    if guest_id not in guests:
      guests[guest_id] = {}
    if round_number not in guests[guest_id]:
      guests[guest_id][round_number] = {"had_coalition": False, "broke_coalition": False}

    if "PBELIEF" in line:
      guests[guest_id][round_number]["had_coalition"] = coalition
      guests[guest_id][round_number]["broke_coalition"] = (coalition and broke)
    elif "NEGPARAMS" in line:
      continue
    else:
      guests[guest_id][round_number]["valuation"] = fields[5]
      guests[guest_id][round_number]["bill"] = fields[6]
      guests[guest_id][round_number]["bill_wo_coalition"] = fields[7]
      guests[guest_id][round_number]["notify_mem"] = fields[4]
      guests[guest_id][round_number]["requested_mem"] = fields[8]
      guests[guest_id][round_number]["bid_p"] = fields[11]
      guests[guest_id][round_number]["offer_value"] = fields[9]
      guests[guest_id][round_number]["accept_threshold"] = fields[10]

  rounds = int(max_round_number)+1
  #print "Rounds: %d" % rounds
  return guests, rounds

def draw_multi_experiment_graphs(exp_ids, guest_ids, labels = ["satisfied only", "all allowed", "no coalitions"]):
  exp_labels = {}
  SW_styles = {}
  profit_styles = {}
  for i in range(len(labels)):
    exp_labels[exp_ids[i]] = labels[i]
    SW_styles[exp_ids[i]] = STYLE_SET1[i]
    profit_styles[exp_ids[i]] = STYLE_SET2[i]
  #exp_labels = {exp_ids[0]:"satisfied only", exp_ids[1]: "all allowed", exp_ids[2]:"no coalitions"}
  #exp_labels = {exp_ids[0]:"Increase Allowed", exp_ids[1]: "No-Increase Policy", exp_ids[2]:"No Coalitions"}
  #styles = {exp_ids[0]:"-", exp_ids[1]: "-.", exp_ids[2]:":"}
  
  #SW_styles = {exp_ids[0]:{"marker":"v", "c":"b", "alpha":0.8}, 
  #             exp_ids[1]: {"marker":"d", "c": "r", "alpha":1}, 
  #             exp_ids[2]:{"marker":"s", "c": "g","alpha":0.8}}
  #profit_styles = {exp_ids[0]:{"marker":"o", "c":"b", "alpha":0.8},
  #                 exp_ids[1]: {"marker":"^", "c": "r", "alpha":1},
  #                 exp_ids[2]:{"marker":"s", "c": "g","alpha":0.8}}
  exp_to_data = {}
  SW_handles = []
  profit_handles = []
  
  fig1, ax1 = plt.subplots()
  #ax2 = ax1.twinx()
  fig2, ax2 = plt.subplots()
  for exp_id in exp_ids:
    bidder_files = ["/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"%(exp_id, i) for i in guest_ids]
    host_file = "/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/exp-out" % exp_id
    negparams_file = open((EXPERIMENTS_FOLDER_FORMAT % exp_id)+"negparams.csv", "w")
    negparams_file.write(
      "exp,round,vm_number,offer_value,accept_threshold,normalized_offer_value,normalized_accept_threshold,bill,bill_wo_coalition,was_leader,leader,ov_or_at_effect\r\n")
    guest_data, rounds_num = extract_guest_data(read_csv_from_logs(bidder_files), negparams_file, exp_id)
    negparams_file.close()
    rounds = range(rounds_num)
    SW = {}
    avg_profit = {}
    profits = {1:{}}
    for rnd in rounds:
      SW[rnd] = sum([guest_data[i][rnd]["valuation"] for i in guest_ids])
      avg_profit[rnd] = sum([guest_data[i][rnd]["valuation"]-guest_data[i][rnd]["bill"] for i in guest_ids])*1.0/len(guest_ids)
      profits[1][rnd] = guest_data[1][rnd]["valuation"] - guest_data[1][rnd]["bill"]
    exp_to_data[exp_id] = {"SW": SW, "avg_profit": avg_profit}
    SW_handle, = ax1.plot(rounds, [SW[rnd] for rnd in rounds], 
                          linestyle="dashed",
                          linewidth=2,
                          color=SW_styles[exp_id]["c"], 
                          label=("SW %s" % exp_labels[exp_id]))
    SW_handles.append(SW_handle)
    profit_handle, = ax2.plot(rounds, [avg_profit[rnd] for rnd in rounds], 
                              marker=profit_styles[exp_id]["marker"],
                              markerfacecolor='none',
                              markeredgecolor=profit_styles[exp_id]["c"],
                              markevery=3,
                              markeredgewidth=2,
                              linewidth=2,
                              color=profit_styles[exp_id]["c"],
                              label=("profit %s" % exp_labels[exp_id]))
    profit_handles.append(profit_handle)
    if exp_id == exp_ids[0]: # Only once
        best_collusion_handle, = ax2.plot(rounds, [SW[rnd]/len(guest_ids) for rnd in rounds],
                              linestyle="dashed",
                              linewidth=2,
                              color="k",
                              label=("Max. Collusion Profit")) 
        profit_handles.append(best_collusion_handle)
  #### Max profit improvement ####
  if len(exp_ids) > 2:  
    max_profit_improvement = 1
    profit_improvements_inside = []
    profit_improvements_all = []
    for rnd in rounds:
      max_profit_improvement = max(exp_to_data[exp_ids[1]]["avg_profit"][rnd]/exp_to_data[exp_ids[2]]["avg_profit"][rnd], 
                                   exp_to_data[exp_ids[0]]["avg_profit"][rnd]/exp_to_data[exp_ids[2]]["avg_profit"][rnd],
                                   max_profit_improvement)
      profit_improvements_inside.append(exp_to_data[exp_ids[0]]["avg_profit"][rnd]/exp_to_data[exp_ids[2]]["avg_profit"][rnd])
      profit_improvements_all.append(exp_to_data[exp_ids[1]]["avg_profit"][rnd]/exp_to_data[exp_ids[2]]["avg_profit"][rnd])
    print "Max profit improvement: %f" % max_profit_improvement
    print "Average improvement inside only: %f" % np.mean(profit_improvements_inside)
    print "Average improvement all allowed: %f" % np.mean(profit_improvements_all)
  #########################

  ax1.set_ylabel("Social Welfare [$/Sec]", fontsize=24)
  #ax1.set_ylim(bottom = 0, top=3400)
  ax1.set_xlim(left=0,right=40)
  ax1.set_xlabel("Rounds", fontsize=24)
  ax1.tick_params(labelsize=24)
  ax1.grid()

  ax2.set_ylabel("Mean Profit [$/Sec]", fontsize=24)
  ax2.set_ylim(bottom = 0, top = 400)
  ax2.set_xlim(left=0,right=40)
  ax2.set_xlabel("Rounds", fontsize=24)
  ax2.tick_params(labelsize=24)
  ax2.grid()
  
  
  lgd1 = ax1.legend(handles = (SW_handles), loc=4, fontsize=24)
  lgd2 = ax2.legend(handles = (profit_handles), loc=4, fontsize=24)
  fig1.savefig(("/home_nfs/" + USER + "/tmp/%s" % "-".join([str(e) for e in exp_ids])) + "social_welfare.png",
              format="png", 
              bbox_extra_artists=(lgd1,), 
              bbox_inches='tight'
             )
  fig2.savefig(("/home_nfs/" + USER + "/tmp/%s" % "-".join([str(e) for e in exp_ids])) + "avg_profit.png",
              format="png",
              bbox_extra_artists=(lgd2,),
              bbox_inches='tight'
             )


def get_profit_from_guest_data(guest_data, rounds_num, guest_ids):
  """ Profit[#Round][GuestId] """
  profit = {}
  rounds = range(rounds_num)
  for rnd in rounds:
    profit[rnd] = {}
    profit[rnd]["avg"] = sum([guest_data[i][rnd]["valuation"]-guest_data[i][rnd]["bill"] for i in guest_ids])*1.0/len(guest_ids)
    for i in guest_ids:
      profit[rnd][i] = guest_data[i][rnd]["valuation"]-guest_data[i][rnd]["bill"]
  return profit

def get_coalition_profit_from_guest_data(guest_data, rounds_num, guest_ids):
  """ Profit[#Round][GuestId] """
  profit = {}
  rounds = range(rounds_num)
  for rnd in rounds:
    profit[rnd] = {}
    #profit[rnd]["avg"] = sum([guest_data[i][rnd]["valuation"]-guest_data[i][rnd]["bill"] for i in guest_ids])*1.0/len(guest_ids)
    for i in guest_ids:
      profit[rnd][i] = guest_data[i][rnd]["bill_wo_coalition"]-guest_data[i][rnd]["bill"]
  return profit

def get_SW_from_guest_data(guest_data, rounds_num, guest_ids):
  SW = {}
  rounds = range(rounds_num)
  for rnd in rounds:
    SW[rnd] = sum([guest_data[i][rnd]["valuation"] for i in guest_ids])
  return SW



def draw_graphs(guests, rounds_num, coalitions, experiment_id, guest_ids, profit_parts, guests_in_coalition_count, coalitions_with_leaders):
  plot_num = 1
  matplotlib.rcParams.update({'font.size': 7})
  # SW is the sum of all valuations
  SW = {}
  # Profit is valuation-bill
  profit = get_profit_from_guest_data(guests, rounds_num, guest_ids)
  SW = get_SW_from_guest_data(guests, rounds_num, guest_ids)
  rounds = range(rounds_num)
  #for rnd in rounds:
  #  SW[rnd] = sum([guests[i][rnd]["valuation"] for i in guest_ids])
  #  profit[rnd] = {}
  #  profit[rnd]["avg"] = sum([guests[i][rnd]["valuation"]-guests[i][rnd]["bill"] for i in guest_ids])*1.0/len(guest_ids)
  #  for i in guest_ids:
  #    profit[rnd][i] = guests[i][rnd]["valuation"]-guests[i][rnd]["bill"]

  # Plot All the things
  # First: SW
  plt.figure(plot_num)
  plt.title("Social Welfare")
  plt.plot(rounds, [SW[rnd] for rnd in rounds])
  plt.xlabel("Rounds")
  plt.ylabel("$/Sec")
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "social_welfare.png", format="png")
  plot_num += 1

  # All profits + average profit
  plt.figure(plot_num)
  plt.title("Profits and Avg profit")
  avg, = plt.plot(rounds, [profit[rnd]["avg"] for rnd in rounds], 'b--', label="avg profit")
  handles = [avg]
  for i in guest_ids:
    handle, = plt.plot(rounds, [profit[rnd][i] for rnd in rounds], label=("vm-%d"%i))
    handles.append(handle)
  plt.legend(handles=handles)
  plt.xlabel("Rounds")
  plt.ylabel("$/Sec")
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "profits_and_avg_profit.png", format="png")
  plot_num += 1

  # Valuation for each guest
  plt.figure(plot_num)
  plt.title("Valuation")
  handles = []
  for i in guest_ids:
    handle, = plt.plot(rounds, [guests[i][rnd]["valuation"] for rnd in rounds], label=("vm-%d"%i))
    handles.append(handle)
  plt.xlabel("Rounds")
  plt.ylabel("$/Sec")
  plt.legend(handles=handles)
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "valuation.png", format="png")
  plot_num += 1

  # Won memory for each guest
  plt.figure(plot_num)
  plt.title("Won Mem")
  handles = []
  for i in guest_ids:
    handle, = plt.plot(rounds, [guests[i][rnd]["notify_mem"] for rnd in rounds], label=("vm-%d"%i))
    handles.append(handle)
  plt.xlabel("Rounds")
  plt.ylabel("Memory [MB]")
  plt.legend(handles=handles)
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "won_mem.png", format="png")
  plot_num+=1
 
  # Bill with and without coalitions for each guest
  plt.figure(plot_num)
  plt.title("Bill")
  handles = []
  for i in guest_ids:
    handle, = plt.plot(rounds, [guests[i][rnd]["bill"] for rnd in rounds],
                       COLORS[i-1], label=("bill vm-%d"%i))
    handles.append(handle)
    handle, = plt.plot(rounds, [guests[i][rnd]["bill_wo_coalition"] for rnd in rounds], 
                       color=COLORS[i-1] , linestyle='--', label=("no coalition bill vm-%d"%i))
    handles.append(handle)
  lgd = plt.legend(handles=handles, bbox_to_anchor=(1.05, 1), loc=2,  borderaxespad=0.)
  plt.xlabel("Rounds")
  plt.ylabel("Bill [$/Sec]")
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "bill.png", format="png", bbox_extra_artists=(lgd,), bbox_inches='tight')
  plot_num+=1

  # Coalitions formed over the experiment
  plt.figure(plot_num)
  #plt.title("Coalitions")
  plt.grid()
  handles = []
  for i in guest_ids:
    h, =plt.step(rounds, [coalitions_with_leaders[rnd][i]["leader"]+i*0.05 for rnd in rounds], color=COLORS[i-1], label=str(i))
    handles.append(h)
  #plt.legend(handles=handles, fontsize=24)
  plt.xlabel("Rounds", fontsize=24)
  plt.ylabel("Leader ID in coalition", fontsize=24)
  plt.tick_params(labelsize=24)
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "coalitions.png", format="png", bbox_inches='tight')
  plot_num+=1

  # Won memory for each guest, as a bar graph
  plt.figure(plot_num)
  plt.title("Stacked Won Mem")
  sample_rounds = rounds
  bottom = [0 for rnd in sample_rounds]
  handles = []
  labels = []
  for i in guest_ids:
    h = plt.bar(sample_rounds, 
            [guests[i][rnd]["notify_mem"] - BASE_MEM for rnd in sample_rounds],
            bottom = bottom,
            color=COLORS[i-1])
    handles.append(h[0])
    labels.append(str(i))
    for (j,rnd) in enumerate(sample_rounds):
        bottom[j] += (guests[i][rnd]["notify_mem"] - BASE_MEM)
  plt.legend(handles, labels)
  plt.xlabel("Rounds")
  plt.ylabel("Memory [MB]")
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "won_mem_stacked.png", format="png")
  plot_num+=1

  # Requested memory for each guest, as a bar graph.
  # TODO: Possible improvement: order by height of p and not by id.
  plt.figure(plot_num)
  plt.title("Stacked Requested Mem")
  bottom = [0 for rnd in sample_rounds]
  handles = []
  labels = []
  for i in guest_ids:
    h = plt.bar(sample_rounds,
            [guests[i][rnd]["requested_mem"] for rnd in sample_rounds],
            bottom = bottom,
            color=COLORS[i-1])
    handles.append(h[0])
    labels.append(str(i))
    for (j,rnd) in enumerate(sample_rounds):
        bottom[j] += guests[i][rnd]["requested_mem"]
  plt.legend(handles, labels)
  plt.xlabel("Rounds")
  plt.ylabel("Memory [MB]")
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "requested_mem_stacked.png", format="png")
  plot_num+=1

  # AVG profit as a result of offer value and accept threshold.
  # Calculate the average profit over time for each player.
  avg_profit_per_player = [np.mean([guests[i][rnd]["bill_wo_coalition"] - guests[i][rnd]["bill"] 
                           for rnd in rounds]) 
                           for i in guest_ids]
  #print "Avg profit per player: %s" % avg_profit_per_player
  #print "Avg profit over all players: %s" % np.mean(avg_profit_per_player)
  #print "Avg profit over positive players: %s" % np.mean([a for a in avg_profit_per_player if a > 0])

  # Get the offer value and accept threshold for each player.
  #offer_values = [guests[i][1]["offer_value"] for i in guest_ids]
  #accept_thresholds = [guests[i][1]["accept_threshold"] for i in guest_ids]
  #max_profit = max(avg_profit_per_player)
  #if max_profit == 0:
  #  max_profit = 1
  #area = [np.pi * (((r*1.0/max_profit)*25)**2) for r in avg_profit_per_player]
  
  #plt.figure(plot_num)
  #plt.title("Avg Profit Depending on Negotiation Parameters")
  #plt.scatter(offer_values, accept_thresholds, s=area, alpha=0.5)
  #plt.xlabel("Offer Value")
  #plt.ylabel("Accept Threshold")
  #plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "negotiation_parameters.png", format="png")
  #plot_num+=1

  # TODO: fix and enable
  #profit parts ecdf
  #plt.figure(plot_num)
  #plt.title("Effective CDF for profit parts %s" % experiment_id)
  #xs,ys = ecdf(profit_parts)
  #plt.plot(xs, ys)
  #plt.xlabel("X")
  #plt.ylabel("Probability of (Profit part <X)")
  #plt.axes().set_xlim(left = 0, right=1.1)
  #plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "profit_parts_ecdf.png", format="png")
  #plot_num += 1

  plt.figure(plot_num)
  plt.title("Effective CDF for discount size %s" % experiment_id)
  discounts = []
  for i in guest_ids:
    for rnd in rounds:
      discounts.append(guests[i][rnd]["bill_wo_coalition"]-guests[i][rnd]["bill"])
  xs,ys = ecdf(discounts)
  plt.plot(xs, ys)
  plt.xlabel("X[$]")
  plt.ylabel("Probability of (Discount size <X)")
  plt.axes().set_xlim(left = 0, right=2)
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "discount_ecdf.png", format="png")
  plot_num += 1

  plt.figure(plot_num)
  plt.title("Effective CDF for #guests in coalition %s" % experiment_id)
  xs,ys = ecdf(guests_in_coalition_count)
  plt.plot(xs, ys)
  plt.xlabel("X[#guests]")
  plt.ylabel("Probability of (#guests in coalition <X)")
  #plt.axes().set_xlim(left = 0, right=)
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "guests_in_coalition_ecdf.png", format="png")
  plot_num += 1


  plt.figure(plot_num)
  plt.title("Effective Coalition Break Probability")
  avg = np.mean([float(sum([guests[guest_id][rnd]["broke_coalition"] for guest_id in guest_ids]))/
                  max(1.0,sum([guests[guest_id][rnd]["had_coalition"] for guest_id in guest_ids])) 
                  for rnd in rounds])
  print "Average Probability for coalition break: %s" % avg
  
  plt.plot(rounds, 
           [1.0*sum([guests[guest_id][rnd]["broke_coalition"] for guest_id in guest_ids])/
            max(1, sum([guests[guest_id][rnd]["had_coalition"] for guest_id in guest_ids])) for rnd in rounds])
  plt.plot(rounds, [avg for r in rounds])
  plt.xlabel("rounds")
  plt.ylabel("#guests broken/#guests coalition")
  plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "coalition_break_prob.png", format="png")
  plot_num+=1

  # Profit for chosen guests
  chosen_ids = []
  for guest_id in chosen_ids:
    plt.figure(plot_num)
    plt.title("Profit for vm-%s"% guest_id)
    handles = []
    handle1, = plt.plot(rounds, 
                        [guests[guest_id][rnd]["valuation"] - guests[guest_id][rnd]["bill"] for rnd in rounds],
                        label=("with coalitions"), color="b")
    handle2, = plt.plot(rounds,
                        [guests[guest_id][rnd]["valuation"] - guests[guest_id][rnd]["bill_wo_coalition"] for rnd in rounds], 
                        label=("without coalitions"), color="r")
    handles = [handle1, handle2]
    plt.legend(handles=handles)
    plt.xlabel("Rounds")
    plt.ylabel("$/Sec")
    plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + ("profit-vm-%s.png" % guest_id), format="png")
    plot_num+=1

def get_coalition_leaders(negparams_path, rounds, guest_ids):
    f = open(negparams_path, "r")
    reader = csv.reader(f)
    coalitions = {}
    for row in reader:
        rnd = row[1]
        if rnd == "round":
            continue
        rnd = int(row[1])
        guest = int(row[2])
        leader = row[10]
        if leader == 'None':
            continue
        leader = int(row[10])
        profit = float(row[8])-float(row[7])
        ov = float(row[3])
        at = float(row[4])
        if rnd not in coalitions:
            coalitions[rnd] = {guest: {"leader": leader, "profit": profit, "ov":ov, "at":at}}
        if guest not in coalitions[rnd]:
            coalitions[rnd][guest] = {"leader": leader, "profit": profit, "ov":ov, "at":at}
        
    f.close()
    for rnd in range(rounds):
        if rnd not in coalitions:
            print "rnd not in coalitions: %s" % rnd
            coalitions[rnd] = {}
        for guest in guest_ids:
            if guest not in coalitions[rnd]:
                coalitions[rnd][guest] = {"leader": guest, "profit": 0, "ov": None, "at": None}
    # deduce the actual coalition of each guest
    rich_coalitions = coalition_stats(coalitions)
    # rewrite negparams
    exp_dir = os.path.dirname(negparams_path)
    f = open(os.path.join(exp_dir,"coalition_stats.csv"), "w")
    f.write("round, vm_number,ov,at, profit, coalition_size, is_with_1-4, coalition\n")
    for r in rich_coalitions:
        if r == 0:
            continue
        for g in rich_coalitions[r]:
            c = rich_coalitions[r][g]["coalition"]
            f.write("%s,%s,%s,%s,%s,%s,%s,%s\n"%(
                     r, g, rich_coalitions[r][g]["ov"], rich_coalitions[r][g]["at"], 
                     rich_coalitions[r][g]["profit"], len(c), intersect([1,2,3,4], c), 
                     " ".join([str(i) for i in c])))
    f.close()
    return coalitions

def coalition_stats(coalitions):
    """input is a dictionary of the form: {rnd: {guest: {"leader": leader, "profit": profit}}}.
       output: {rnd:{guest: {"leader": leader, "profit": profit, "coalition": coalition}}"""
    #print "exp_id, round, vm_number, coalition_size, is_with_1-4, coalition"
    #guest_to_coalition = {}
    for rnd in coalitions:
        guests = coalitions[rnd].keys()
        for main_guest in guests:
            coalitions[rnd][main_guest]["coalition"] = []
            leader = coalitions[rnd][main_guest]["leader"]
            for g in guests:
                if coalitions[rnd][g]["leader"] == leader and g not in coalitions[rnd][main_guest]["coalition"]: # same coalition as main guest
                    coalitions[rnd][main_guest]["coalition"].append(g)
    return coalitions

def intersect(A,B):
    for i in A:
        if i in B:
            return True
    return False

def get_smart_agents_ids(exp_path):
  INFO_FILE_NAME = "info_2"

  # first create info_2 file
  generate_info2_file(exp_path)

  file_path = os.path.join(exp_path, INFO_FILE_NAME)
  with open(file_path, "r") as f:
      lines = f.readlines()
  smarts = []
  for i,line in enumerate(lines):
      class_name = line.split(', ')[1].split(' ')[1]
      if class_name == "AdviserCoalitionsAdaptive":
          smarts.append(i+1)    # guest id starts from 1
  return smarts

def generate_info2_file(exp_path):
  info_file = os.path.join(exp_path, "info")
  vms_info = get_vms_info(info_file)
  with open("%s_2" % (info_file), 'w') as outfile:  # save json as "info_2"
    for k in vms_info.keys():
      vm = vms_info[k]
      line = 'vm: %s, name: %s, at: %s, ov: %s\n' % (str(k), str(vm.name), str(vm.at), str(vm.ov))
      outfile.writelines(line)

def get_vms_info(info_file):
   vms_info = {}
   # read vms info
   f = open(info_file, "r")
   info_dictionary = eval_info_file_content(f.readlines()[0])
   for key in info_dictionary['vms'].keys():
     vm = info_dictionary['vms'][key]
     vm_id = int(vm['adviser_args']['vm_number'])
     vm_name = str(vm['adviser_args']['name'])
     vm_at = float(vm['adviser_args']['accept_threshold'])
     vm_ov = float(vm['adviser_args']['offer_value'])
     vms_info[vm_id] = VmInfo(vm_id, vm_name, vm_ov, vm_at)
   f.close()
   return vms_info

def eval_info_file_content(info):
  '''
  Serialize 'info' file with the dictionary data
  Remove objects from info text. Something like "< Bla Bla >"
  '''
  DICT_OBJ_PATTERN = re.compile("<[^<>]*>")
  count = -1
  while count != 0:
      info, count = DICT_OBJ_PATTERN.subn("None", info)
  return eval(info)

class VmInfo:
  def __init__(self, id, name, ov, at):
    self.id = id
    self.name = name
    self.ov = ov
    self.at = at

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Create graphs from experiments.')
  parser.add_argument("--exp_ids", nargs='+', type=int,
                        help='experiment ids to take data from. E.g., --exp_ids 202 204')
  parser.add_argument("--num_guests", type=int, help='The number of guests in the experiment, e.g., 5.')
  parser.add_argument("--op", type=int, default=1, 
                      help="operation to perform. \n1 - generate all graphs\n"+
                           "2 - generate a file with #guests in coalition statistics.\n"+
                           "3 - generate average total profit. \n4 - profit parts by guest id."+
                           "4 - ?. \n"+
                           "5 - generate avg profit of Smart & Static populations (). \n"
                      )
  parser.add_argument("--min_exp_id", type=int, default=0, help="define a range of exp ids to work on. Use with max_exp_id.")
  parser.add_argument("--max_exp_id", type=int, default=0, help="define a range of exp ids to work on. Use with min_exp_id.")
  parser.add_argument("--labels", nargs='+', type=str, help="labels for multi experiment graph.")
  #parser.add_argument("--smart_agents_count", type=int, help='The number of smart guests in the experiment, e.g., 5.')

  args = parser.parse_args()
  num_guests = args.num_guests
  guest_ids = [i for i in range(1, num_guests+1)]

  # A range of experiments
  if args.min_exp_id >=0 and args.max_exp_id>0:
    if args.op == 2:
      print "exp_id,average #guests in coalition,std, max #guests in coalition"
      for experiment_id in range(args.min_exp_id, args.max_exp_id + 1):
        host_file = "/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/exp-out" % experiment_id
        coalitions, profit_parts, guests_in_coalition_count, pp_with_guest_id = \
            extract_host_data(read_csv_from_logs([host_file]), guest_ids)
        print "%s,%s,%s,%s" % (experiment_id,np.mean(guests_in_coalition_count),np.std(guests_in_coalition_count),max(guests_in_coalition_count))
    if args.op == 4:
      print "exp_id,guest_id,profit_part"
      for experiment_id in range(args.min_exp_id, args.max_exp_id + 1):
        host_file = "/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/exp-out" % experiment_id
        coalitions, profit_parts, guests_in_coalition_count, pp_with_guest_id = \
            extract_host_data(read_csv_from_logs([host_file]), guest_ids)
        print "\n".join(["%s,%s,%s"%(experiment_id, pair[0], pair[1]) for pair in pp_with_guest_id])
    if args.op == 3:
      print "exp_id, average total profit"
      for experiment_id in range(args.min_exp_id, args.max_exp_id + 1):
        bidder_files = ["/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"%(experiment_id, i) for i in guest_ids]
        guest_data, rounds_num = extract_guest_data(read_csv_from_logs(bidder_files), None, experiment_id)
        coalition_profit = get_profit_from_guest_data(guest_data, rounds_num, guest_ids)
        print "%s,%s" % (experiment_id, np.mean([coalition_profit[rnd]["avg"] for rnd in range(rounds_num)]))
    if args.op == 5:

      smart_profits = []
      static_profits = []
      coalition_sizes = []

      smart_profit_vs_largest_coalition_size = []
      static_profit_vs_largest_coalition_size = []

      smart_profit_vs_been_in_coalition_count = []
      static_profit_vs_been_in_coalition_count = []

      # iterate over experiments folders
      for experiment_id in range(args.min_exp_id, args.max_exp_id + 1):
        #bidder_files = ["/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"%(experiment_id, i) for i in guest_ids]

        exp_path = (EXPERIMENTS_FOLDER_FORMAT + r"p0-0.00-freq-60") % (experiment_id)
        bidder_files = [
            (EXPERIMENTS_FOLDER_FORMAT + r"/p0-0.00-freq-60/bidder-vm-%d-mem") % (experiment_id, i) for i in guest_ids]

        guest_data, rounds_num = extract_guest_data(read_csv_from_logs(bidder_files), None, experiment_id)

        coalition_profit = get_coalition_profit_from_guest_data(guest_data, rounds_num, guest_ids)  # Profit[  # Round][GuestId]

        host_file = os.path.join(exp_path, "exp-out")
        coalitions, profit_parts, guests_in_coalition_count, pp_with_guest_id = \
            extract_host_data(read_csv_from_logs([host_file]), guest_ids)

        smarts = get_smart_agents_ids(exp_path)  # [1,2]
        smarts_agents_count = len(smarts)
        static_agents_count = num_guests - smarts_agents_count

        largest_coalition_size = 0
        agents_coalition_count_hash = {} # Ex: {1: 3} - agent 1 was 3 times in a coalition

        #iterate over coalitions
        for i in range(len(coalitions)):
            # save largest coalition size
            if largest_coalition_size < coalitions[i]['largest_coalition_size']:
                largest_coalition_size = coalitions[i]['largest_coalition_size']
            coalition_sizes.append(coalitions[i]['largest_coalition_size'])

            # save for each agent the number of rounds he was in coalition
            # first build coal_size_hash - hash from coalition_id(leader) to coalition_size
            # convert:{1: 1, 2: 2, 3: 3, 4: 1, 5: 5}
            # to:     {1: 2, 2: 1, 3: 1, 5: 1}
            coal_size_hash = {}
            for j in range(1, num_guests + 1):
                if coalitions[i][j] in coal_size_hash:
                    coal_size_hash[coalitions[i][j]] = coal_size_hash[coalitions[i][j]] + 1
                else:
                    coal_size_hash[coalitions[i][j]] = 1
            # run again over agents and save
            for j in range(1, num_guests + 1):
                # only if agent j was in a coalition (not alone)
                if coal_size_hash[coalitions[i][j]] > 1:
                    if j in agents_coalition_count_hash:
                        agents_coalition_count_hash[j] = agents_coalition_count_hash[j] + 1
                    else:
                        agents_coalition_count_hash[j] = 1

        current_smart_profit = []
        current_static_profit = []
        # iterate over rounds at current exp.
        for rnd in range(rounds_num):
            for i in range(1, num_guests+1):
                c_profit = coalition_profit[rnd][i]
                if i in smarts:
                    smart_profits.append(c_profit)
                    current_smart_profit.append(c_profit)
                else:
                    static_profits.append(c_profit)
                    current_static_profit.append(c_profit)

        smart_profit_vs_largest_coalition_size_item = {
            "profit": np.sum(current_smart_profit) / smarts_agents_count,
            "max_coalition_size": largest_coalition_size
        }
        smart_profit_vs_largest_coalition_size.append(smart_profit_vs_largest_coalition_size_item)

        static_profit_vs_largest_coalition_size_item = {
            "profit": np.sum(current_static_profit) / static_agents_count,
            "max_coalition_size": largest_coalition_size
        }
        static_profit_vs_largest_coalition_size.append(static_profit_vs_largest_coalition_size_item)

        # calc profit vs number of rounds that agent in coalition:
        smart_sum_count = 0
        static_sum_count = 0
        for j in range(1, num_guests + 1):
            if j in smarts:
                smart_sum_count += agents_coalition_count_hash[j]
            else:
                static_sum_count += agents_coalition_count_hash[j]

        if smarts_agents_count > 0:
            smart_profit_vs_number_of_rounds_in_coalition_item = {
                "profit": np.sum(current_smart_profit) / smarts_agents_count,
                "smart_sum_count": smart_sum_count / smarts_agents_count
            }
            smart_profit_vs_been_in_coalition_count.append(smart_profit_vs_number_of_rounds_in_coalition_item)

        if static_agents_count > 0:
            static_profit_vs_number_of_rounds_in_coalition_item = {
                "profit": np.sum(current_static_profit) / static_agents_count,
                "static_sum_count": static_sum_count / static_agents_count
            }
            static_profit_vs_been_in_coalition_count.append(static_profit_vs_number_of_rounds_in_coalition_item)

      # end of exp iteration



      print("num smarts: %s smart avg profit: %s static avg profit: %s   (mean_largest_coalition_size: %s)" %
            (len(smarts), np.mean(smart_profits), np.mean(static_profits), np.mean(coalition_sizes)))

      # GRAPH 1.1
      sorted_static_profit_vs_largest_coalition_size = \
          sorted(static_profit_vs_largest_coalition_size, key=lambda x: x["max_coalition_size"], reverse=False)
      # Coalitions formed over the experiment
      plt.figure(1)
      plt.title("StaticProfit vs LargestCoalitionSize")
      plt.grid()
      sizes = []
      profits = []
      for item in sorted_static_profit_vs_largest_coalition_size:
          sizes.append(item["max_coalition_size"])
          profits.append(item["profit"])
      # plt.legend(handles=handles, fontsize=24)
      plt.xlabel("LargestCoalitionSize", fontsize=24)
      plt.ylabel("StaticProfit", fontsize=24)
      # plt.tick_params(labelsize=24)
      plt.plot(sizes, profits, 'ro')
      plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "StaticProfit vs LargestCoalitionSize.png",
                  format="png", bbox_inches='tight')

      # GRAPH 1.2
      sorted_smart_profit_vs_largest_coalition_size = \
          sorted(smart_profit_vs_largest_coalition_size, key=lambda x: x["max_coalition_size"], reverse=False)
      # Coalitions formed over the experiment
      plt.figure(2)
      plt.title("SmartProfit vs LargestCoalitionSize")
      plt.grid()
      sizes = []
      profits = []
      for item in sorted_smart_profit_vs_largest_coalition_size:
          sizes.append(item["max_coalition_size"])
          profits.append(item["profit"])
      # plt.legend(handles=handles, fontsize=24)
      plt.xlabel("LargestCoalitionSize", fontsize=24)
      plt.ylabel("SmartProfit", fontsize=24)
      #plt.tick_params(labelsize=24)
      plt.plot(sizes, profits, 'ro')
      plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "SmartProfit vs LargestCoalitionSize.png",
                  format="png", bbox_inches='tight')

      # GRAPH 2.1
      sorted_smart_profit_vs_been_in_coalition_count = \
          sorted(smart_profit_vs_been_in_coalition_count, key=lambda x: x["smart_sum_count"], reverse=False)
      # Coalitions formed over the experiment
      plt.figure(3)
      plt.title("SmartProfit vs #RoundsAgentInCoalition")
      plt.grid()
      sizes = []
      profits = []
      for item in sorted_smart_profit_vs_been_in_coalition_count:
          sizes.append(item["smart_sum_count"])
          profits.append(item["profit"])
      # plt.legend(handles=handles, fontsize=24)
      plt.xlabel("#RoundsAgentInCoalition", fontsize=24)
      plt.ylabel("SmartProfit", fontsize=24)
      # plt.tick_params(labelsize=24)
      plt.plot(sizes, profits, 'ro')
      #plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "SmartProfit vs NumRoundsAgentInCoalition.png",
                  #format="png", bbox_inches='tight')

      # GRAPH 2.2
      sorted_static_profit_vs_been_in_coalition_count = \
          sorted(static_profit_vs_been_in_coalition_count, key=lambda x: x["static_sum_count"], reverse=False)
      # Coalitions formed over the experiment
      #plt.figure(4)
      plt.title("StaticProfit vs #RoundsAgentInCoalition")
      plt.grid()
      sizes = []
      profits = []
      for item in sorted_static_profit_vs_been_in_coalition_count:
          sizes.append(item["static_sum_count"])
          profits.append(item["profit"])
      # plt.legend(handles=handles, fontsize=24)
      plt.xlabel("#RoundsAgentInCoalition", fontsize=24)
      plt.ylabel("StaticProfit", fontsize=24)
      # plt.tick_params(labelsize=24)
      plt.plot(sizes, profits, 'bo')
      plt.savefig((EXPERIMENTS_FOLDER_FORMAT % experiment_id) + "Smart_StaticProfit vs NumRoundsAgentInCoalition.png",
                  format="png", bbox_inches='tight')







    sys.exit()
 
  # multiple specified experiments
  if args.exp_ids is not None and len(args.exp_ids)>1:
    draw_multi_experiment_graphs(args.exp_ids, guest_ids, args.labels)
    sys.exit()

  # Just one experiment
  experiment_id = args.exp_ids[0]
  bidder_files = ["/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/bidder-vm-%d-mem"%(experiment_id, i) for i in guest_ids]
  host_file = "/home_nfs/" + USER + "/moc-output/exp/coalitions-%d/p0-0.00-freq-60/exp-out" % experiment_id
  negparams_file_path = (EXPERIMENTS_FOLDER_FORMAT % experiment_id)+"negparams.csv"
  coalitions, profit_parts, guests_in_coalition_count,pp_with_guest_id = extract_host_data(read_csv_from_logs([host_file]), guest_ids)
  if args.op == 2:
    print "average #guests in coalition,std, max #guests in coalition"
    print np.mean(guests_in_coalition_count),",",np.std(guests_in_coalition_count),",",max(guests_in_coalition_count)
    sys.exit()
  negparams_file = open(negparams_file_path, "w")
  negparams_file.write(
      "exp,round,vm_number,offer_value,accept_threshold,normalized_offer_value,normalized_accept_threshold,bill,bill_wo_coalition,was_leader,leader,ov_or_at_effect\r\n")
  guest_data, rounds = extract_guest_data(read_csv_from_logs(bidder_files), negparams_file, experiment_id)
  negparams_file.close()
  coalitions_with_leaders = get_coalition_leaders(negparams_file_path, rounds, guest_ids)
  draw_graphs(guest_data, rounds, coalitions, experiment_id, guest_ids, profit_parts, guests_in_coalition_count, coalitions_with_leaders)
