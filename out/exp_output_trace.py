'''
Created on Mar 5, 2012
@author: eyal
'''

from matplotlib import use
use('cairo')
import pylab as pl
import numpy as np

import os
from etc.plots import draw_n_save, pl_apply_defaults, multifunction
from mom.Plotter import Plotter, PERF, MON, HOST, POL
import sys
from matplotlib.transforms import Bbox
from out.common.common import sync_files, get_vals, perf_time, iter_vms, pol_time, \
    mon_time, get_exp_data, process_description_file
from etc.Settings import Settings

# filename = "mcd-test-piecewise1400-pareto"
# filename = "mcd-test2-secondorder-pareto"
# filename = "mcd-test3-secondorder-pareto"
# filename = "mcd-test4-secondorder-pareto"
# filename = "mcd-test6-secondorder-pareto"
# filename = "mcd-test7-secondorder-pareto"
# filename = "mcd-test8-secondorder-pareto"
# filename = "mcd-test9-secondorder-pareto"
# filename = "mcd-test10-secondorder-pareto"
# filename = "mcd-test11-secondorder-pareto"
# filename = "mcd-test13-secondorder-pareto"
# filename = "mcd-test15-secondorder-pareto-2"
# filename = "mcd-test16-secondorder-pareto"

# 7-9 VMs experiments
# filename = "mcd-1-secondorder-pareto"
# filename = "mcd-1-piecewise1400-pareto"
# filename = "mcd-2-secondorder-pareto"
# filename = "mcd-2-piecewise1400-pareto"
# filename = "mcd-3-piecewise1400-pareto"

# 9-11 VMs experiments
# filename = "mcd-4-piecewise1400-pareto"
# filename = "mcd-4-secondorder-pareto"

# killer 23
#filename = "mcd-5-piecewise1400-pareto"
#filename = "mcd-5-secondorder-pareto"

#filename = "mcd-6-piecewise1400-pareto"

#filename = "mcd-7-piecewise1400-pareto-8"
#filename = "mcd-10-piecewise1400-pareto"
#filename = "mcd-4-piecewise1400-pareto"
#filename = "mcd-11-secondorder-pareto"
#filename = "mcd-11-piecewise1400-pareto"
#filename = "mcd-11-secondorder-pareto-ginseng-0.0"
#filename = "mcd-11-secondorder-pareto-ginseng-0.1"

# mixed workload
#filename = "batch-mix-linear-pareto-17"
#filename = "batch-mix-linear-pareto-18"
#filename = "batch-mix-linear-pareto-19"
#filename = "random-seed-mcd"
#filename = "batch-Memcached-25"
#filename = "batch-MemoryConsumer-0"

# adaptive affine maximizer
#filename = "mcd-dead-secondorder-pareto-2"
#filename = "mcd-dead-secondorder-pareto-4"
#filename = "mcd-dead-secondorder-pareto-6"
#filename = "mcd-dead-secondorder-pareto-36"
#filename = "mc-dead-secondorder-pareto"
#filename = "mcd-v-change-secondorder-pareto"
#filename = "mcd-v-change-secondorder-pareto-7"
#filename = "mcd_noisy"
filename = "batch-mcd-others"

debug_mode = True

prefix = os.path.join(Settings.user_home(), Settings.output_dir())
remotedir = os.path.join(prefix, "exp", filename)
localdir = os.path.join(prefix, "exp-%s" % Settings.hostname(), filename)
outdir = os.path.join(localdir, "details")

discard_min = 5
end_min = 25

if not os.path.exists(localdir): os.mkdir(localdir)
if not os.path.exists(outdir): os.mkdir(outdir)
position = (0.1, 0.1, 0.85, 0.85)

pl_apply_defaults({'legend.fontsize': 8})
paper_mode = False

perf_field = "hits_rate"

exclude_tests = set()  # set(["mom"])

#all_tests = ["divided", "ginseng", "hinted-host-swapping", "hinted-mom"]
# all_tests = ["ginseng"]
# all_tests = ["hinted-mom", "ginseng"]
#all_tests = ["ginseng", "hinted-mom"] + ["ginseng-%02f" % (i / 10.) for i in range(0, 10 + 1)]
#all_tests = ["ginseng-0.%i" % (i) for i in range(0, 10)]
#all_tests = ["divided"]
#all_tests = ["ginseng-%.01f" % (i / 10.) for i in (0, 2, 4, 5, 8, 10)] + ["hinted-mom"]#range(0, 10 + 1, 2)] #+ ["hinted-mom", "divided", "hinted-host-swapping"]
#all_tests = ["ginseng-0.0"] + ["hinted-mom", "divided"]
all_tests = ["ginseng-%.02f" % (i / 10.) for i in range(0, 11)] + ["hinted-mom", "divided", "hinted-host-swapping"]
#all_tests = ["ginseng-0.0"]

shortcuts = ["D", "G", "HH", "HM"]

#test_color = lambda i: [(195 / 255.0, 195 / 255.0, 195 / 255.0), (192 / 255.0, 0 / 255.0, 0 / 255.0),
#                        (0 / 255.0, 128 / 255.0, 0 / 255.0), (0 / 255.0, 0 / 255.0, 255 / 255.0),
#                        (255 / 255.0, 255 / 255.0, 0 / 255.0), (0 / 255.0, 255 / 255.0, 255 / 255.0),
#                        (255 / 255.0, 0 / 255.0, 255 / 255.0), (255 / 255.0, 128 / 255.0, 0 / 255.0),
#                        (89 / 255.0, 0 / 255.0, 0 / 255.0), (240 / 255.0, 130 / 255.0, 176 / 255.0)][i]
test_color = lambda i: ['b', 'r', 'c', 'm', 'g', 'gray'][i]

is_auction = lambda name: name.find("ginseng") != -1

size = (20, 4) if not paper_mode else (7.5, 1.5)
# size = (20, 8) if not paper_mode else (7.5, 1.5)

def_markers = lambda n: [pl.Line2D.filled_markers[i] for i in range(n)]

raw_perf_time = lambda dct: [float(t['time']) / 60 for t in dct['raw_perf']]

def exp_plot(data, funcs, plot_kwargs, axes=None, tests=None, ylim=None, add_load=True, sum_values=False):
    if not data:
        return

    print "starting exp_plot()"

    if not axes:
        axes = range(len(funcs))

    if add_load:
        funcs.append((perf_time, lambda dct: get_vals(dct[PERF], 'load')))
        plot_kwargs.append({"label": "Load", "marker": "None", "color": "k",
                            "alpha": 0.2, "linestyle": "-", "linewidth": 2})
        axes.append(max(axes) + 1)

    multi_axes = []
    multi_kw = []

    y_labels = [kw["label"] for kw in plot_kwargs]

    if tests is None:
        tests = all_tests

    for test in tests:
        i = all_tests.index(test)
        for ax in axes:
            multi_axes.append(ax)

        for kw in plot_kwargs:
            multi_kw.append(kw.copy())
            if "color" not in multi_kw[-1]:
                multi_kw[-1]["color"] = test_color(i)
            multi_kw[-1]["label"] = test + " " + kw["label"]

    xmin = sys.maxint
    xmax = 0

    all_xy = []

    print "aggregating data"

    if sum_values:
        temp_x = {}
        temp_y = {}
        keys_added_sorted = []
        all_xy.append([])
        temp_key = lambda test, index: "%s-%s" % (test, index)
        for vm_i, vm in enumerate(iter_vms(data.values()[0])):
            for test in tests:
                for func_i, xy_func in enumerate(funcs):
                    cur_key = temp_key(test, func_i)
                    try:
                        x = xy_func[0](data[test][vm])
                        y = xy_func[1](data[test][vm])
                    except Exception as ex:
                        print ("error in guest: %i, test: %s, %s" % (vm_i, test, ex))
                        y = x = [0]
                        raise
                    if len(x) != 0 and len(y) != 0:
                        xmin = min(xmin, min(x))
                        xmax = max(xmax, max(x))
                    if cur_key not in temp_x:
                        keys_added_sorted.append(cur_key)
                        temp_x[cur_key] = x
                        temp_y[cur_key] = y
                    else:
                        for point_i, (x_val, y_val) in enumerate(zip(x, y)):
                            if len(temp_x[cur_key]) == point_i:
                                temp_x[cur_key].append(x_val)
                                temp_y[cur_key].append(y_val)
                            else:
                                temp_y[cur_key][point_i] += y_val

        for key in keys_added_sorted:
            all_xy[0].append((temp_x[key], temp_y[key]))
    else:
        for vm_i, vm in enumerate(iter_vms(data.values()[0])):
            all_xy.append([])
            for test in tests:
                if test not in data:
                    print "skipping missing test result %s" % test
                    continue
                for xy_func in funcs:
                    try:
                        x = xy_func[0](data[test][vm])
                        y = xy_func[1](data[test][vm])
                    except Exception as ex:
                        print ("error in guest: %i, test: %s, %s" % (vm_i, test, ex))
                        y = x = [0]
                        raise
                    if len(x) != 0 and len(y) != 0:
                        xmin = min(xmin, min(x))
                        xmax = max(xmax, max(x))
                    all_xy[-1].append((x, y))

    print "processing multifunction plot"
    multifunction(all_xy, multi_axes, multi_kw, xlim=(xmin, xmax), ylim=ylim,
                  ylabels=y_labels)

def norm_perf_func(prop):
    def y(vm_data):
        px = perf_time(vm_data)
        py = get_vals(get_vals(vm_data[PERF], 'perf'), prop)
        mx = mon_time(vm_data)
        my = np.array(get_vals(vm_data[MON], 'mem_available'))

        norm = []
        for i in range(len(px) - 1):
            inds = [j for j, x in enumerate(mx) if px[i] <= x <= px[i + 1]]
            m = np.average(my[inds])
            norm.append(float(py[i]) / m)

        return norm

    def x(vm_data):
        px = perf_time(vm_data)
        xmid = []
        for i in range(len(px) - 1):
            xmid.append(float(px[i] + px[i + 1]) / 2)

        return xmid

    return x, y

def draw_exp(data, info, num):
    print "calculating experiment %i" % num

    ###== performance view ===================================
    k_per_sec = lambda x: float(x) / 1000
####
#    exp_plot(data,
#             [(perf_time, lambda dct: map(k_per_sec, get_vals(get_vals(dct[PERF], 'perf'), "get_total"))),
#              (perf_time, lambda dct: map(k_per_sec, get_vals(get_vals(dct[PERF], 'perf'), perf_fields[0]))),
#              ], [
#              {"label": "total", "linestyle": "-", "marker": "None", "linewidth": 2, "alpha": 0.5},
#              {"label": "hits", "linestyle": "--", "marker": "None", "linewidth": 2, "alpha": 0.5},
#              ])
#    draw_n_save(os.path.join(outdir, "check-rate-num-%i" % (num)), size,
#                num = num)
#
#    exp_plot(data,
#             [(perf_time, lambda dct: map(k_per_sec, get_vals(get_vals(dct[PERF], 'perf'), "get_total"))),
#              (mon_time, lambda dct: map(k_per_sec, get_vals(dct[MON], "prog_get_hits"))),
#              ], [
#              {"label": "total", "linestyle": "-", "marker": "None", "linewidth": 2, "alpha": 0.5},
#              {"label": "hits", "linestyle": "None", "marker": "o", "linewidth": 2, "alpha": 0.5},
#              ])
#    draw_n_save(os.path.join(outdir, "check2-num-%i" % (num)), size,
#                num = num)
####
    check_com = lambda d: (d['p'] == 0 and len(d['rq']) == 1
                           and d['rq'][0][0] == 0 and d['rq'][0][1] == 0)

    def no_communication_markers(data):
        data = data[POL]
        if not data[0].has_key("rq"):
            return []
        ret = []
        for d in data:
            if check_com(d):
                ret.append(0)
        return ret

    def no_communication_time(data):
        data = data[POL]
        if not data[0].has_key("rq"):
            return []
        ret = []
        for d in data:
            if check_com(d):
                ret.append(d['time'] / 60.0)
        return ret

    def extra_mem(dct):
        base = get_vals(dct[POL], 'mem_base')
        mem = get_vals(dct[POL], 'control_mem')
        assert len(base) == len(mem)
        return [_mem - _base for _mem, _base in zip(mem, base)]
 
    exp_plot(data,
             [(perf_time, lambda dct: map(k_per_sec, get_vals(dct[PERF], 'rev'))),
              (perf_time, lambda dct: map(k_per_sec, get_vals(dct[PERF], 'perf'))),
              (mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
              (mon_time, lambda lst: get_vals(lst[MON], 'major_fault')),
              (no_communication_time, no_communication_markers),
              ], [
               {"label": "SW [K/s]", "linestyle": "-", "marker": "None", "linewidth": 3, "alpha": 0.5},
               {"label": "performance[Khits/s]", "linestyle": "--", "marker": "None", "linewidth": 2, "alpha": 0.5},
               {"label": "memory", "linestyle": ":", "marker": "None", "linewidth": 2, "alpha": 0.5},
               {"label": "page faults", "linestyle": "None", "marker": "o", "alpha": 0.5},
               {"label": "No comm", "linestyle": "None", "marker": "x"}
              ])
    draw_n_save(os.path.join(outdir, "exp-view-num-%i" % (num)), size,
                num=num)

    pl.clf()
    exp_plot(data,
             [
              (perf_time, lambda dct: map(k_per_sec, get_vals(dct[PERF], 'rev'))),
              (perf_time, lambda dct: map(k_per_sec, get_vals(dct[PERF], 'perf')))
              ], [
               {"label": "revenue", "linestyle": "-", "marker": "None", "linewidth": 2, "alpha": 0.5},
               {"label": "performance", "linestyle": "--", "marker": "None", "linewidth": 2, "alpha": 0.5},
              ])
    draw_n_save(os.path.join(outdir, "perf-rev-num-%i" % (num)), size,
                num=num)

    ###== host views ===================================

    for p in ["mem_available", "mem_unused", "cache_and_buff", "major_fault"]:
        pl.clf()
        for i, test in enumerate(all_tests):
            try:
                pl.plot(mon_time(data[test][HOST]),
                    get_vals(data[test][HOST][MON], p),
                    color=test_color(i),
                    marker="None", alpha=0.5, label=test)
            except KeyError:
                pass
#            if p in ("mem_unused", "cache_and_buff"):
#                pl.ylim((0, 1000))
        pl.legend(loc="best")
        pl.grid(True)
        pl.xlabel("Time [min]")
        pl.ylabel("[MB]")
        draw_n_save(os.path.join(outdir, "host-%s-num-%i" % (p, num)), size, 1)

    pl.clf()
    for i, test in enumerate(all_tests):
        pl.subplot(len(all_tests), 1, i + 1)
        try:
            keys = sorted([k for k in data[test][HOST][MON][0].keys()
                if k.startswith("cpu")])

            for j, k in enumerate(keys):
                pl.plot(mon_time(data[test][HOST]),
                    get_vals(data[test][HOST][MON], k),
                    color=pl.cm.jet(float(j) / len(keys)),  # @UndefinedVariable
                    marker="None", alpha=0.5, label=k)
                pl.grid(True)
                pl.ylabel(test)
            if i == 0:
                pl.legend(loc="best")
            if i == len(keys) - 1:
                pl.xlabel("Time [min]")
        except KeyError:
            pass
    draw_n_save(os.path.join(outdir, "host-cpu-num-%i" % (num)), size, 1)
#
    for p in ["auction_mem"]:
        pl.clf()
        for i, test in enumerate(all_tests):
            if not is_auction(test): continue
            pl.plot(pol_time(data[test][HOST]), get_vals(data[test][HOST][POL], p),
                    color=test_color(i),
                    marker="s", alpha=0.5, label=test)
            pl.legend(loc="best")
            pl.grid(True)
            pl.xlabel("Time [min]")
            draw_n_save(os.path.join(outdir, "host-%s-num-%i" % (p, num)), size, 1)

    # p values
    pl.clf()

    for p, marker in zip(['p_in_min_mem', 'p_out_max_mem', 'p0_mem', 'p_reduction_mem'],
                          ["^", "v", "*", "o"]):
        for i, test in enumerate(all_tests):
            if not is_auction(test): continue
            pl.plot(pol_time(data[test][HOST]), get_vals(data[test][HOST][POL], p),
                    color=test_color(i),
                    marker=marker, alpha=0.5, label=test + "-" + p)
        pl.legend(loc="best")
        pl.grid(True)
        pl.xlabel("Time [min]")
    draw_n_save(os.path.join(outdir, "host-p-vals-num-%i" % (num)), size, 1)

    # host total sold memory
    pl.clf()
    for i, test in enumerate(all_tests):
        if not is_auction(test): continue
        total_mem = pl.np.zeros(len(data[test]['vm-1'][POL]))
        for vm in iter_vms(data[test]):
            vals = get_vals(data[test][vm][POL], 'control_mem')
            sz = min(len(vals), len(total_mem))
            total_mem[:sz] += vals[:sz]
        t = pol_time(data[test][HOST])
        sz = min(len(t), len(total_mem))
        pl.plot(t[:sz], total_mem[:sz],
                label=test, marker="s",
                color=test_color(i)
                )
        pl.ylabel('total-memory-available [MB]')
        pl.grid(True)
        pl.legend(loc="best")
        draw_n_save(os.path.join(outdir, "host-total-sold-num-%i" % (num)), size, 1)

        ###== split guest view ===================================

    # memory view
    pl.clf()
    exp_plot(data,
             [(mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
              (mon_time, lambda lst: get_vals(lst[MON], 'mem_unused')),
              (mon_time, lambda lst: get_vals(lst[MON], 'cache_and_buff'))],
             [{"label": "Total Memory", "linestyle": "-", "marker": "None", "linewidth": 4, "alpha": 0.5},
              {"label": "Unused Memory", "linestyle": "--", "marker": "None", "linewidth": 2},
              {"label": "Cache and Buffers", "linestyle": ":", "marker": "None", "linewidth": 2}],
             [0, 1, 1])
    draw_n_save(os.path.join(outdir, "memory-num-%i" % num), size, num)

    # memory compare
    pl.clf()
    exp_plot(data,
             [(mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
              (pol_time, lambda lst: np.array(get_vals(lst[POL], 'control_mem')) - 235)],
             [{"label": "Total Memory", "linestyle": "-", "color":"r", "marker": "None", "linewidth": 2},
              {"label": "Control Memory", "linestyle": "-", "color":"b", "marker": "None", "linewidth": 2,
               "drawstyle": "steps-post"}, ],
             [0, 0])
    draw_n_save(os.path.join(outdir, "memory-compare-num-%i" % num), size, num)



    # bills
    pl.clf()
    exp_plot(data,
         [(pol_time, lambda lst: get_vals(lst[POL], 'control_bill'))],
         [{"label": "bill", "linestyle": "-", "marker": "None"}])
    draw_n_save(os.path.join(outdir, "bill-num-%i" % num), size, num)

    # maj falts cpu
    pl.clf()
    exp_plot(data,
             [(mon_time, lambda lst: get_vals(lst[MON], 'major_fault')),
              (mon_time, lambda lst: get_vals(lst[MON], 'cpu_usage'))],
             [{"label": "Major Faults", "linestyle": "None", "marker": "o"},
              {"label": "CPU", "linestyle": "-", "marker": "None"}])
    draw_n_save(os.path.join(outdir, "faults-cpu-num-%i" % num), size, num)

    # cpu
    pl.clf()
    exp_plot(data,
             [(mon_time, lambda lst: get_vals(lst[MON], 'cpu_usage')),
              (mon_time, lambda lst: get_vals(lst[MON], 'python1_usage')),
              (mon_time, lambda lst: get_vals(lst[MON], 'python2_usage')),
              (mon_time, lambda lst: get_vals(lst[MON], 'prog_usage'))],
             [{"label": "CPU", "linestyle": "-", "marker": "None"},
              {"label": "p1-cpu", "linestyle": "-", "marker": "^"},
              {"label": "p2-cpu", "linestyle": "-", "marker": "v"},
              {"label": "prog-cpu", "linestyle": "-", "marker": "s"}],
             [0, 0, 0, 0],
             ylim=[0, 1])
    draw_n_save(os.path.join(outdir, "cpu-num-%i" % num), size, num)

    # swap
    pl.clf()
    exp_plot(data,
             [(mon_time, lambda lst: get_vals(lst[MON], 'swap_in')),
              (mon_time, lambda lst: get_vals(lst[MON], 'swap_out'))],
             [{"label": "swap in", "linestyle": "None", "marker": "^"},
              {"label": "swap out", "linestyle": "None", "marker": "v"}],
             [0, 0])
    draw_n_save(os.path.join(outdir, "swap-num-%i" % num), size, num)

    pl.clf()
    prq_tests = [t for t in all_tests if is_auction(t)]
    if len(prq_tests) > 0:
        exp_plot(data,
                 [
                  (pol_time, lambda lst: get_vals(lst[POL], 'p')),
#                  (pol_time, lambda lst: get_vals(lst[POL], 'mem_base')),
                  (pol_time, lambda lst: [x[0][0] for x in get_vals(lst[POL], 'rq')]),
                  (pol_time, lambda lst: [x[0][1] for x in get_vals(lst[POL], 'rq')]),
                  (pol_time, lambda dct: extra_mem(dct)),
                 ],
                 [{"label": "p", "linestyle": "-", "linewidth": 2, "marker": "None", "alpha": 0.5},
#                  {"label": "base", "linestyle": "None", "marker": "o", "alpha": 0.5},
                  {"label": "r", "linestyle": "None", "marker": "^", "alpha": 0.5},
                  {"label": "q", "linestyle": "None", "marker": "v", "alpha": 0.5},
                  {"label": "mem", "linestyle": "--", "marker": "None", "alpha": 0.5}
                 ],
                 [0, 1, 1, 1],
                 tests=prq_tests)
        draw_n_save(os.path.join(outdir, "prq-num-%i" % num), size, num)

    pl.clf()
    exp_plot(data,
             [
              (mon_time, lambda lst: get_vals(lst[MON], 'host_major_faults')),
              (mon_time, lambda lst: get_vals(lst[MON], 'host_minor_faults')),
              (mon_time, lambda lst: get_vals(lst[MON], 'rss')),
              ], [
              {"label": "Major faults", "linestyle": "None", "marker": "o", "alpha": 0.5},
              {"label": "Minor faults", "linestyle": "None", "marker": "^", "alpha": 0.5},
              {"label": "RSS", "alpha": 0.5},
              ])
    draw_n_save(os.path.join(outdir, "proc-mem-num-%i.png" % num), size, num)

    # host process monitor
    pl.clf()
    exp_plot(data,
             [
              (raw_perf_time, lambda dct: get_vals(get_vals(dct['raw_perf'], 'perf'), 'get_total')),
              (raw_perf_time, lambda dct: get_vals(get_vals(dct['raw_perf'], 'perf'), "get_hits_total")),
              (raw_perf_time, lambda dct: get_vals(get_vals(dct['raw_perf'], 'perf'), 'get_misses')),
#              (raw_perf_time, lambda dct: map(k_per_sec, get_vals(get_vals(dct['raw_perf'], 'perf'), 'miss_rate'))),
              (raw_perf_time, lambda dct: get_vals(get_vals(dct['raw_perf'], 'perf'), 'hits_rate')),
              (raw_perf_time, lambda dct: get_vals(get_vals(dct['raw_perf'], 'perf'), 'get_hits_percent')),
#              (mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
              (mon_time, lambda lst: get_vals(lst[MON], 'major_fault')),
              ], [
              {"label": "Total gets", "linestyle": "-", "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "y"},
              {"label": "Total hits", "linestyle": "-", "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "r"},
              {"label": "Total misses", "linestyle": "-", "marker": "None", 'linewidth': 1, "alpha": 0.5, 'color': 'c'},
#              {"label": "Miss rate", "linestyle": "--", "marker": "None", "linewidth": 2, "alpha": 0.5, "color": "b"},
              {"label": "Hits rate", "linestyle": "--", "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "r"},
              {"label": "Hits percent", "linestyle": "-", "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "b"},
#              {"label": "Memory", "linestyle": "-", "marker": "None", "linewidth": 2, "alpha": 0.5, "color": "g"},
              {"label": "Major faults", "linestyle": "None", "marker": "o", "alpha": 0.5},
              ],
              [0, 0, 0, 1, 2, 3])
#              [0, 0, 0, 1, 1, 2, 3, 4])
    draw_n_save(os.path.join(outdir, "perf-view-num-%i.png" % num), size, num)

    # social welfare (sum of all guests)
    '''
    pl.clf()
    exp_plot(data,
             [(perf_time, lambda dct: map(k_per_sec, get_vals(dct[PERF], 'rev'))),
             (perf_time, lambda dct: map(k_per_sec, get_vals(dct[PERF], 'perf')))],
             [{"label": "SW[K/s]", "linestyle": "-", "marker": "None", "linewidth": 2, "alpha": 0.5},
              {"label": "Performance[Khits/s]", "linestyle": "--", "marker": "None", "linewidth": 2, "alpha": 0.5}],
             tests=[t for t in all_tests if is_auction(t)],
             sum_values=True, add_load=False)
    draw_n_save(os.path.join(outdir, "sw-perf-sum"), size, num = 2)
    '''
    
#    # calculate RSS ratio
#    try:
#        pl.clf()
#        for e, d in data.iteritems():
#            A = get_vals(d[HOST][MON], 'mem_available')[0]
#            B = A - 1024
#            C = {}
#            for name in set(d.keys()) - set([HOST,]):
#                memfree = get_vals(d[name][MON], 'mem_free')
#                for t, v in memfree.items():
#                    C.setdefault(t, 0)
#                    C[t] += v
#            rss = [float(A - c) / B for c in get_vals(C)]
#            t = [x - d["vm-1"][MON][0]['time'] for x in get_vals(d["vm-1"][MON], 'time')]
#            pl.plot(rss, label = e)
#        pl.xlabel("time [min]")
#        pl.ylabel("RSS ratio")
#        pl.title("RSS ratio for configured ratio of %.2f" % (float(A) / B))
#        pl.grid(True, 'both')
#        pl.legend(loc = "lower right")
#        draw_n_save(os.path.join(outdir, "num-%i-rss-ratio.png" % num), (12, 9))
#    except:
#        print "ERROR with calculating RSS"

def draw_bars(data, info, field):

    # collect the performance data
    all_perf = {}
    all_rev = {}

    for n in data.iterkeys():
        all_perf[n] = {}
        all_rev[n] = {}
        for test in all_tests:
            all_perf[n][test] = {}
            all_rev[n][test] = {}
            for vm in iter_vms(data[n].values()[0]):
                try:
                    vals = get_vals(get_vals(data[n][test][vm][PERF], 'perf'), field)
                    times = perf_time(data[n][test][vm])
                    vals = [float(v) for t, v in zip(times, vals) if discard_min <= t <= end_min]
                    all_perf[n][test][vm] = vals[1:]

                    vals = get_vals(get_vals(data[n][test][vm][PERF], 'rev'), field)
                    vals = [float(v) for t, v in zip(times, vals) if discard_min <= t <= end_min]
                    all_rev[n][test][vm] = vals[1:]

                    print len(vals), " - ", n, test, vm

                    ls = get_vals(data[n][test][vm][PERF], 'load')
                except KeyError as ex:
                    pass

    nums = sorted(all_perf.keys())
    if paper_mode:
        axs = [pl.gca()]
        axs[0].set_position(Bbox.from_bounds(0.06, 0.11, 0.93, 0.65))
        size = (7.5, 1.2)
        filetype = ".eps"
    else:
        axs = [pl.subplot(311), pl.subplot(312)]
        axs[0].set_position(Bbox.from_bounds(0.1, 0.53, 0.88, 0.33))
        axs[1].set_position(Bbox.from_bounds(0.1, 0.15, 0.88, 0.33))
        size = (16, 6)
        filetype = ".png"

if __name__ == '__main__':
    sync_files(remotedir, localdir, ["bidder-*", "exp-out*", "prog-*"])
    process_description_file([localdir], localdir, exp_name="ginseng-0.00")
    rev_funcs = eval("".join(file(os.path.join(localdir, "valuations.txt"))))

    print "processing data"

    data, info = get_exp_data(localdir, perf_field, rev_funcs)

    for value in data.values():
        for test in reversed(all_tests):
            if test not in value and test in all_tests:
                print "removing missing test %s" % test
                all_tests.remove(test)

    print "Plotting traces for " + filename

    '''
    all_tests_orig = all_tests
    outdir_orig = outdir
    for test in all_tests_orig:
        outdir = outdir_orig + "-%s" % (test)
        if not os.path.exists(outdir): os.mkdir(outdir)
        all_tests = [test]
        for i in sorted(data.iterkeys(), reverse=True):
            draw_exp(data[i], info[i], i)
    '''
    for test in all_tests:
        if not os.path.exists(outdir): os.mkdir(outdir)
        for i in sorted(data.iterkeys(), reverse=True):
            draw_exp(data[i], info[i], i)
