import matplotlib
#matplotlib.use('cairo')
matplotlib.use('agg') # agg has no point limit, unlike cairo
import pylab as pl
import numpy as np

from etc.plots import draw_n_save, plot_mean, pl_apply_defaults, plot_scatter, \
    plot_up_down
import os
from mom import Profiler
#from exp.exp_output import sync_files
from out.common.common import sync_files
from etc.Settings import Settings

pl_apply_defaults(fontsize = 10)

paper_mode = False
ax_position = (0.15, 0.15, 0.78, 0.8) #(0.14, 0.12, 0.78, 0.8)

#test_dir = "Memcached-47"
#test_dir = "Memcached-157"
#test_dir = "MemoryConsumer"
#test_dir = "MemoryConsumer-4"
test_dir = "Memcached-76"
remote_dir = os.path.join(Settings.user_home(), Settings.output_dir(), "testbed", test_dir)
local_dir = os.path.join(Settings.user_home(), Settings.output_dir(), "-".join(["testbed", Settings.hostname()]), test_dir)

out_dir = local_dir

## BAD TESTBED EXAMPLE FOR PAPER
#test_dir = "Memcached-90"
#local_dir = os.path.join("/home/eyal/moc-output/testbed-bad/", test_dir)
#out_dir = "/home/eyal/moc-output/testbed-bad/"

if test_dir.find("Memcached") != -1:
    prog = "Memcached"
    dmem = 49
    perf_field = 'hits_rate'
    V = lambda p: p  #lambda p:(1800 * p if p > 3400 else 100 * p)
    reduction = 0
    xlim = (400, 2400)
elif test_dir.find("MemoryConsumer") != -1:
    prog = "MemoryConsumer"
    dmem = 49
    perf_field = 'hits_rate'
    V = lambda p: p  #lambda p:np.array(p) ** 2
    reduction = 0
    xlim = (400, 2400)


def save_image(out_dir, name, size = (10, 8)):
    ending = ".png" if not paper_mode else ".eps"
    draw_n_save(os.path.join(out_dir, name + ending), size)

def get_xyz(dct, key, reduction = 3):
    load = dct['tb_load']
    mem = dct['mem_available']

    if key in ['major_fault', 'minor_fault', 'swap_out', 'swap_in']:
        z = [(float(x) / t if t != 0 and t is not None else 0)
             if x is not None else x
             for x, t in zip(dct[key], dct['test_time'])]
        key += '_per_sec'
    else:
        z = dct[key]

    xx = []
    yy = []
    zz = []
    for a, b, c in zip(load, mem, z):
        if a > 10:
            continue;
        if None not in (a, b, c):
            xx.append(a)
            yy.append(b)
            zz.append(c / (10 ** reduction))
    return xx, yy, zz

def draw_testbed():
    files = [f for f in os.listdir(local_dir) if f.startswith("testbed-results-")]

    dcts = [eval(file(os.path.join(local_dir, f)).readline())
            for f in files]
    num = len(dcts)

    # calculate hits_per_sec

    if prog == "Memcached":
        for dct in dcts:
            dct['val'] = []
            get_total = []
            duration = []
            percent = []
            load = []
            for a, b, c, d in zip(dct['get_total'], dct['test_time'],
                                  dct['get_hits_percent'], dct['tb_load']):
                if None in (a, b, c, d):
                    continue
                get_total.append(a)
                duration.append(b)
                percent.append(c)
                load.append(d)

            get_total = np.array(get_total)
            duration = np.array(duration)
            percent = np.array(percent)
            load = np.array(load)

            for i in range(len(load)):
                ind = load == load[i]
                avg = np.average(get_total[ind] / duration[ind])
                dct['val'].append(V(percent[i] * avg))
            #dct['hits_rate'] = list(np.array([x if x is not None else 0 for x in dct['get_hits_total']]) / 200)
    else:
        for dct in dcts:
            dct['val'] = []
            for i in range(len(dct['tb_load'])):
                dct['val'].append(V(dct[perf_field][i]))

#    dct['hits_rate'] = list(np.array([x if x is not None else 0 for x in dct['get_hits_total']]) / 200)

    dct = {}
    for k in dcts[0].keys():
        dct[k] = []
        for d in dcts:
            dct[k] += d[k]

    keys = dct.keys() if not paper_mode else ["val"]

    for k in keys:

#        if k in ("tb_load", "tb_mem"):  # or k.startswith("cpu") or k.startswith("io"):
#            continue
        x, y, z = get_xyz(dct, k, reduction)

        label = k.replace("_", " ").capitalize()
        suffix = "-".join((prog.lower(), k.replace("_", "-")))

        if paper_mode:
            pl.clf()
            plot_mean(x, y, z,
                      x_label = "Allocation [MB]", legend_label = "Load",
                      y_label = "Performance [Khits/s]",  # % reduction,  #"Performance [Khits/s]",
                      normalize = False)
            pl.gca().set_position(ax_position)
#            pl.gca().set_xticks(np.arange(600, 2401, 200))
#            pl.gca().set_xlim((600, 2400))
#            pl.gca().set_ylim((0, 5))
            save_image(out_dir, "-".join((test_dir, "avg", suffix)), size = (3.5, 3.5))
            continue
        pl.clf()
        plot_mean(x, y, z,
             x_label = "Allocation [MB]", legend_label = "Load", y_label = label,
             normalize = False, xlim = xlim)
        save_image(out_dir, "-".join(("avg", suffix)))

#
        pl.clf()
        plot_scatter(x, y, z,
             x_label = "Allocation [MB]", y_label = label, normalize = False, xlim = xlim)
        save_image(out_dir, "-".join(("scatter", suffix)), size = (20, 16))
#

#
        pl.clf()
        plot_up_down(x, y, z,
             x_label = "Allocation [MB]", y_label = label, normalize = False, xlim = xlim)
        save_image(out_dir, "-".join(("up-down", suffix)), size = (20, 16))

    if paper_mode:
        return

    pl.clf()
    for i, dct in enumerate(dcts):
        pl.subplot(num, 1, i + 1)
        ax1 = pl.gca()
        ax2 = pl.twinx(ax1)
        ax3 = pl.twinx(ax1)
        ax4 = pl.twinx(ax1)
        lines = []
        lines += ax2.plot(dct['tb_load'], "b", label = "TB Load")
        lines += ax3.plot(dct['mem_available'], "k", label = "Mem Available")
        lines += ax3.plot(dct['tb_mem'], "--k", label = "TB Mem")
        lines += ax4.plot(dct['major_fault'], "g", label = "Major Fault")
        lines += ax1.plot(dct[perf_field], "r", linewidth = 2, label = (perf_field.replace("_", " ").capitalize()))
        labels = [l.get_label() for l in lines]
        pl.legend(lines, labels, loc = 1)


    save_image(out_dir, "along-time", size = (40, 20))

    pl.clf()
    for i, dct in enumerate(dcts):
        lines = []
        dct = dcts[i]
        pl.subplot(num, 1, i + 1)
        ax1 = pl.gca()
        consumption = [1 - float(a) / b if a and b else 0
                       for a, b in zip(dct['mem_free'], dct['mem_available'])]
        lines += ax1.plot(consumption, "r", linewidth = 2, label = "Consumption")
        maxmem = max(dct['mem_available'])
        lines += pl.twinx(ax1).plot(dct['mem_available'], "k", label = "Mem Available")
        pl.ylim((0, maxmem))
        lines += pl.twinx(ax1).plot(dct['mem_free'], "--k", label = "Mem Free")
        pl.ylim((0, maxmem))
        lines += pl.twinx(ax1).plot(dct['major_fault'], "b", label = "Major Fault")
        labels = [l.get_label() for l in lines]
        pl.legend(lines, labels, loc = 1)

    save_image(out_dir, "consumption-along-time", size = (40, 20))

    # major-faults-along-time (to see what's with those major faults)
    pl.clf()
    for i, dct in enumerate(dcts):
        pl.subplot(num, 1, i + 1)
        ax1 = pl.gca()
        ax3 = pl.twinx(ax1)
        lines = []
        lines += ax3.plot(dct['mem_available'], "k", label = "Mem Available")
        lines += ax1.plot(dct['major_fault'], "-or", linewidth = 2, ms = 0.5, label = "Major Fault")
        labels = [l.get_label() for l in lines]
        pl.legend(lines, labels, loc = 1)

    save_image(out_dir, "major-faults-along-time", size = (40, 20))

    # major-faults-and-cpu-along-time (to see what's with those major faults)
    pl.clf()
    for i, dct in enumerate(dcts):
        pl.subplot(num, 1, i + 1)
        ax1 = pl.gca()
        ax2 = pl.twinx(ax1)
        ax3 = pl.twinx(ax1)
        ax4 = pl.twinx(ax1)
        ax5 = pl.twinx(ax1)
        lines = []
        lines += ax3.plot(dct['mem_available'], "k", label = "Mem Available")
        lines += ax5.plot(dct['tb_load'], "-oy", ms = 0.8, alpha = 0.75, label = "TB Load")
        lines += ax4.plot(dct['io_percent_%i' % (i + 1) ], "--g", ms = 0.5, label = ("IO %i" % (i + 1)))
        lines += ax2.plot(dct['cpu_%i' % (i + 1)], "b", label = ("CPU %i" % (i + 1)))
        lines += ax1.plot(dct['major_fault'], "-or", linewidth = 2, ms = 0.5, label = "Major Fault")
        labels = [l.get_label() for l in lines]
        pl.legend(lines, labels, loc = 1)

    save_image(out_dir, "major-faults-and-cpu-along-time", size = (40, 20))

    # major-faults-and-swap-along-time (to see what's with those major faults)
    pl.clf()
    for i, dct in enumerate(dcts):
        pl.subplot(num, 1, i + 1)
        ax1 = pl.gca()
        ax2 = pl.twinx(ax1)
        ax3 = pl.twinx(ax1)
        ax4 = pl.twinx(ax1)
        ax5 = pl.twinx(ax1)
        lines = []
        lines += ax3.plot(dct['mem_available'], "k", label = "Mem Available")
        lines += ax5.plot(dct['tb_load'], "-oy", ms = 0.8, alpha = 0.75, label = "TB Load")
        lines += ax2.plot(dct['swap_in'], "g", ms = 0.5, label = ("Swap in"))
        lines += ax4.plot(dct['swap_out'], "b", label = ("Swap out"))
        lines += ax1.plot(dct['major_fault'], "-or", linewidth = 2, ms = 0.5, label = "Major Fault")
        labels = [l.get_label() for l in lines]
        pl.legend(lines, labels, loc = 1)

    save_image(out_dir, "major-faults-and-swap-time", size = (40, 20))

    # this doesn't omit the beginning. uncomment if you trust this
    '''
    # creating average profiler for testbed
    x = np.array(dct['tb_load'])
    y = np.array([v for v in dct['tb_mem']])
    l = sorted(set(x))
    m = sorted(set(y))
    data = {}
    n = len(dct[perf_field]) / (len(l) * len(m))
    data[perf_field] = np.ndarray((len(l), len(m), 1))
    for i in range(len(l)):
        for j in range(len(m)):
            # take only the last measurement from each round
            # we can tell it's the last of the round if the load or memory have been changed in the next round
            places = (x == l[i]) & (y == m[j])
            vals = [dct[perf_field][k] for k in range(len(places)) if places[k] and ((k == len(places) - 1) or not places[k+1])]
            data[perf_field][i, j] = (np.average([v for v in vals if v is not None])
                               if len(vals) > 0 else 0)

    profiler = Profiler.Profiler(data, [l, m], perf_field)
    profiler.save_xml(os.path.join(local_dir, "profiler.xml"), round_digits = 2)
    pl.clf()
#    profiler.draw_2d(entry = perf_field)
#    save_image(out_dir, ("profiler"))
    '''


def draw_memcached():
    for i in range(1, 4 + 1):
        data = {}
        fname = os.path.join(local_dir, "remote-vm-%i" % i)
        if not os.path.isfile(fname): break
        for line in file(fname):
            if line.find("target") == -1 or line.find("usage") == -1:
                continue
            for tup in line.split(" - ")[-1].split(", "):
                if not tup.strip():
                    continue
                try:
                    k, v = tup.split(": ")
                    data.setdefault(k, []).append(v)
                except:
                    pass

        interval = 1 # plot every interval-th point
        pl.figure(figsize = (60, 20))
#        data['fix'] = [int(x) - int(y) for x, y in zip(data['total_malloced'], data['statm'])]
        for k, v in data.iteritems():
#            if k not in ["fix"]: continue
            if k not in ["target", "total", "usage", "diff", "statm"]:
                continue
            pl.plot(v[::interval], label = k)
        pl.legend(loc = 0)
        pl.grid()

#        pl.ylim(-50, 0)
#        pl.xlim(0, 18800)
        pl.show()

        pl.ylabel("MB")
        pl.xlabel("sample #")
        draw_n_save(os.path.join(local_dir, "vm-%i-memcached" % (i)), (60, 30))

if __name__ == "__main__":
    if not os.path.exists(local_dir): os.makedirs(local_dir)
    sync_files(remote_dir, local_dir, ["*.png", "*.xml", "testbed-out*"])  #"remote-*"

    print "drawing testbed"
    draw_testbed()
    print "drawing memcached"
    draw_memcached()

    print "plots generated in %s" % out_dir
