#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/param.h>
#include <sys/syscall.h>    /* For SYS_write etc */
#include <sys/reg.h>
#include <ctype.h>

#define _GNU_SOURCE
#include <getopt.h>

#include "util.h"

#define WORDSIZE (sizeof(unsigned long))

enum syscall_register {
        scnum = 0, /* syscall number */ 
        scret,     /* syscall retval */ 
        scarg1,    /* syscall first arg */
        scarg2,    /* syscall second arg */
};

enum syscall_status {
        stentry = 0, 
        stexit
};

#ifdef i386
unsigned long register_offsets[] = {
        ORIG_EAX << 2, /* num */
        EAX << 2, 
        EBX << 2,
        ECX << 2
};
#elif defined (__x86_64)
unsigned long register_offsets[] = {
        ORIG_RAX << 3, /* num */
	RAX << 3,
        RDI << 3,
        RSI << 3
};
#else
#error unknown ARCH
#endif /* arch support */

#define BAD_SYSCALL ~0UL

/* make a ptrace request and barf if it fails */
static unsigned long 
xptrace(enum __ptrace_request req, pid_t proc, void* addr, void* data)
{
        unsigned long res;

        /* global errno */
        errno = 0;
        res = ptrace(req, proc, addr, data);
        if (res == (unsigned long)-1 && errno)
                die(errno, "ptrace(proc %d, req %d): %s\n", proc, req, strerror(errno));

        return res;
}

/* get the data at 'addr */
unsigned long getmem(pid_t proc, unsigned long addr)
{
        return xptrace(PTRACE_PEEKDATA, proc, (void*)addr, NULL);
}

unsigned long putmem(pid_t proc, unsigned long addr, void* data)
{
        return xptrace(PTRACE_POKEDATA, proc, (void*)addr, data);
}

unsigned long getscreg(pid_t proc, enum syscall_register reg)
{
        unsigned long addr = register_offsets[reg];

        return xptrace(PTRACE_PEEKUSER, proc, (void*)addr, NULL);
}

unsigned long putscreg(pid_t proc, enum syscall_register reg, void* data)
{
        unsigned long addr =  register_offsets[reg];
        
        return xptrace(PTRACE_POKEUSER, proc, (void*)addr, data);
}

/* copied from syscalltrack's sctrace/main.cpp. I can do that, I wrote it */
static char* find_in_path(const char* name)
{
        char* ret = NULL;
        char* env = getenv("PATH");
        char* beg, *end;
        char buf[MAXPATHLEN + 256];

        if (!env)
                return NULL;

        if (!(env = strdup(env)))
                return NULL;

        beg = env;

        /* find the first : character. */
        while (1) {
                end = strchr(beg, ':');
                if (end)
                        *end = '\0';

                (void)snprintf(buf, sizeof(buf), "%s/%s", beg, name);

                if (!access(buf, X_OK)) {
                        ret = strdup(buf);
                        break;
                }
                if (!end)
                        break;
                ++end;
                beg = end;
        }
        free(env);

        /* check if name references an executable relative to cwd */
        if (!access(name, X_OK)) {
                ret = strdup(name);
                return ret;
        }

        return ret;
}

void exec_child(const char* exec, char** argv)
{
        const char* fullpath;

        xptrace(PTRACE_TRACEME, 0, NULL, NULL);

        if (*exec == '/')
                fullpath = exec;
        else if (!(fullpath = find_in_path(exec)))
                die(ENOENT, "could not find file to execute '%s'\n", exec);

        sleep(1);
        printf("starting");
        execv(fullpath, argv);
        die(errno, "execv %s: %s", fullpath, strerror(errno));
}

struct tracefd {
        int waitforme; 
        int fd;
        unsigned long readbuf; /* address of read buffer in user memory */
	char* rbuf; /* our local copy of the user's buffer */
	int rbufsz; /* size of user buffer */
};

struct tracedata { 
        pid_t pid;
        enum syscall_status status;
        unsigned long syscall;
        struct tracefd vmstat;
        struct tracefd meminfo;
        unsigned long time;
};

static const char* vmstat = "/proc/vmstat";
static const char* meminfo = "/proc/meminfo";

static inline int vmstat_active(struct tracedata* td)
{
        return td->vmstat.fd;
}

static inline int meminfo_active(struct tracedata* td)
{
        return td->meminfo.fd;
}

static int get_read_buf(struct tracefd *fd, pid_t pid)
{
	int i;

	/* what did read return? */
	fd->rbufsz = getscreg(pid, scret);
	if (fd->rbufsz <= 0) {
		log("read returned %d\n", fd->rbufsz);
		return fd->rbufsz;
	}
		
	fd->readbuf = getscreg(pid, scarg2);
	log("readbuf = 0x%lx [size %d]\n", fd->readbuf, fd->rbufsz);

	fd->rbuf = malloc(fd->rbufsz + WORDSIZE);
	if (!fd->rbuf)
		return -ENOMEM;
	memset(fd->rbuf, 0, fd->rbufsz + WORDSIZE);

	/* copy the user's buffer */
	unsigned int words = (fd->rbufsz / WORDSIZE) + 1;
	for (i = 0; i < words; ++i) {
		unsigned long data = getmem(pid, fd->readbuf + (i * WORDSIZE));
		memcpy(fd->rbuf + (i * WORDSIZE), &data, sizeof(data));
	}

	//log("readbuf: 0x%lx, data '%s'\n", fd->readbuf, fd->rbuf);
	return fd->rbufsz;;
}

static void put_read_buf(struct tracefd *fd, pid_t pid)
{
	int i;

	/* copy the user's buffer */
	unsigned int words = (fd->rbufsz / WORDSIZE) + 1;
	for (i = 0; i < words; ++i) {
		unsigned long data;
		memcpy(&data, fd->rbuf + (i * WORDSIZE), sizeof(data));
		putmem(pid, fd->readbuf + (i * WORDSIZE), (void*)data);
	}
}

static void update_meminfo(struct tracefd* fd, const char* needle, unsigned int val)
{
	char* p = strstr(fd->rbuf, needle);
	if (!p) {
		log("could not find '%s' in '%s'\n", needle, fd->rbuf);
		return;
	}

	p += strlen(needle);

	// find the first digit after the needle
	while (*p && !isdigit(*p))
		p++;

 	// and convert it to 0
	while (*p && isdigit(*p))
		*p++ = '0';
}

static void fudge_meminfo(struct tracefd* fd, pid_t pid)
{
	int ret;

	ret = get_read_buf(fd, pid);
	if (ret <= 0) {
		log("could not get read buf, bailing\n");
		return;
	}

	/* find the interesting stuff and change it in place */
	update_meminfo(fd, "MemFree", 0);
	update_meminfo(fd, "Buffers", 0);
	update_meminfo(fd, "Cached", 0);

	/* copy the user's buffer back */
	put_read_buf(fd, pid);
	
}

static void handle_read(struct tracedata* td)
{
        /* do we care about reads at this point in time? */
        if (!(vmstat_active(td) || meminfo_active(td)))
                return;

        if (td->status == stexit) {
		log("doing read\n");
		unsigned int fd = getscreg(td->pid, scarg1);

		// get the file descriptor so we know which file is being read
		if (meminfo_active(td) && fd == td->meminfo.fd) {
			log("meminfo: fooling the read\n");
			fudge_meminfo(&td->meminfo, td->pid);
                } else if (vmstat_active(td) && fd == td->vmstat.fd) {
                        log("read got vmstat, skipping\n");
		}
        }
}

static void handle_close(struct tracedata* td)
{
        int fd; 

        if (td->status == stentry) {
                if (!(vmstat_active(td) || meminfo_active(td)))
                        return;

                fd = getscreg(td->pid, scarg1);
                log("close: fd %d\n", fd);
                if (fd == td->meminfo.fd) {
                        log("closing meminfo\n");
                        td->meminfo.fd = 0;
                } else if (fd == td->vmstat.fd) {
                        log("closing vmstat\n");
                        td->vmstat.fd = 0;
                }
        }
}

static void handle_open(struct tracedata* td)
{
        int i;
        unsigned long nameaddr;
        unsigned long data;
        char name[256];
        int fd;

        if (td->status == stentry) {
                nameaddr = getscreg(td->pid, scarg1);
                for (i = 0; i < 12; ++i) {
                        data = getmem(td->pid, nameaddr + i * 4);
                        memcpy(name + i * 4, &data, sizeof(data));
                }
                name[i * 4 + 1] = '\0'; 
                // log("nameaddr: 0x%lx, data '%s'\n", nameaddr, name);
                if (!strncmp(name, vmstat, strlen(vmstat))) {
                        struct tracefd* pfd = &td->vmstat;
                        pfd->waitforme = 1;
                        log("vmstat: settincg %p->waitforme to 1\n", pfd);
                } else if (!strncmp(name, meminfo, strlen(meminfo))) {
                        struct tracefd* pfd = &td->meminfo;
                        pfd->waitforme = 1;
                        log("meminfo: setting %p->waitforme to 1\n", pfd);
                }
        } else { /* stexit */
                if (td->vmstat.waitforme) {
                        fd = getscreg(td->pid, scret);
                        td->vmstat.waitforme = 0;
                        td->vmstat.fd = fd;
                        log("vmstat: setting %p->waitforme to 0, fd %d\n", 
                            &td->vmstat, fd);
                } else if (td->meminfo.waitforme) {
                        fd = getscreg(td->pid, scret);
                        td->meminfo.waitforme = 0;
                        td->meminfo.fd = fd;
                        log("meminfo: setting %p->waitforme to 0, fd %d\n", 
                            &td->meminfo, fd);
                }
        }       
}

static int run_tracer(pid_t child)
{
        int status;
        unsigned long tmp;
        int res;
        int loop = 0;
        struct tracedata td = { 
                .pid  = 0,
                .status = stexit,
                .syscall = BAD_SYSCALL,
                .vmstat = { 0, },
                .meminfo = { 0, },
                .time = 0
        };

        td.pid = child;

        while (1) {
                loop++;
                res = waitpid(child, &status, 0);
                if (res != child)
                        die(errno, "waitpid");
                
                if (WIFEXITED(status))
                        return WEXITSTATUS(status);

		if (WIFSIGNALED(status))
			return WTERMSIG(status);

                 if (td.status == stentry || td.syscall == BAD_SYSCALL) { /* syscall entry */
                       td.syscall = tmp = getscreg(child, scnum);
                } else 
                        tmp = getscreg(child, scnum);

#if 0
                log("loop %d: %s: got syscall %lu\n", 
                    loop, (td.status == stentry ? "entry" : "exit"),
                    td.syscall);
                if (td.syscall != tmp) {
                        log("loop %d: syscall %lu, but tmp %lu\n", 
                            loop, td.syscall, tmp);
                }
#endif /* 0 */

                switch (td.syscall) {
                case SYS_open:
                        handle_open(&td);
                        break;
                case SYS_read:
                        handle_read(&td);
                        break;
                case SYS_close:
                        handle_close(&td);
                        break;
                case BAD_SYSCALL:
                        die(EINVAL, "bad_syscall caught!\n");
                }

                xptrace(PTRACE_SYSCALL, child, NULL, NULL);
                td.status = (td.status == stentry ? stexit : stentry);
        }
}

int trace_new_proc(const char* cmd, char* args[])
{
        pid_t child = fork();
        if (child == -1)
                die(errno, "fork failed\n");

        if (child == 0) {
                /* child code */
                exec_child(cmd, args);
        } else
                return run_tracer(child);

        /* not reached */
        return 0;
}

/* main */
int main(int argc, char* argv[])
{
        int ret;

	if (argc < 2) 
		die(EINVAL, "usage: %s <cmd>\n", argv[0]);

        ret = trace_new_proc(argv[1], &argv[1]);

        return ret;
}
