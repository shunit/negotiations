#!/usr/bin/python2

# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import re
import logging
import sys

def open_datafile(filename):
    """
    Open a data file for reading.
    """
    try:
        filevar = open(filename, 'r')
    except IOError, (errno, strerror):
        logger = logging.getLogger('mom.Collector')
        logger.error("Cannot open %s: %s" % (filename, strerror))
        sys.exit(1)
    return filevar

def parse_int(regex, src):
    """
    Parse a body of text according to the provided regular expression and return
    the first match as an integer.
    """
    m = re.search(regex, src, re.M)
    if m:
        return int(m.group(1))
    else:
        return None

def parse_float(regex, src):
    m = re.search(regex, src, re.M)
    if m:
        return float(m.group(1))
    else:
        return None

class HostMemory():
    """
    This Collctor returns memory statistics about the host by examining
    /proc/meminfo and /proc/vmstat.  The fields provided are:
        mem_available - The total amount of available memory (MB)
        mem_unused    - The amount of memory that is not being used for any purpose (MB)
        mem_free      - The amount of free memory including some caches (MB)
        anon_pages    - The amount of memory used for anonymous memory areas (MB)
        swap_in       - The amount of memory swapped in since boot (pages)
        swap_out      - The amount of memory swapped out since boot (pages)
        major_fault   - The amount of major page faults since boot (pages)
        minor_fault   - The amount of minor page faults since boot (pages)
    """
    meminfo = vmstat = None

    def __init__(self):
        self.meminfo = open_datafile("/proc/meminfo")
        self.vmstat = open_datafile("/proc/vmstat")
#         self.swap_in_prev = None
#         self.swap_in_cur = None
#         self.swap_out_prev = None
#         self.swap_out_cur = None
#         self.minflt = None
#         self.majflt = None

    def __del__(self):
        if self.meminfo is not None:
            self.meminfo.close()
        if self.vmstat is not None:
            self.vmstat.close()

    def collect(self):
        self.meminfo.seek(0)
        self.vmstat.seek(0)

        contents = self.meminfo.read()
        avail = parse_int("^MemTotal: (.*) kB", contents) >> 10
        anon = parse_int("^AnonPages: (.*) kB", contents) >> 10
        unused = parse_int("^MemFree: (.*) kB", contents) >> 10
        buffers = parse_int("^Buffers: (.*) kB", contents) >> 10
        cached = parse_int("^Cached: (.*) kB", contents) >> 10
        free = unused + buffers + cached

        # /proc/vmstat reports cumulative statistics so we must subtract the
        # previous values to get the difference since the last collection.
        contents = self.vmstat.read()
        swap_in = parse_int("^pswpin (.*)", contents)
        swap_out = parse_int("^pswpout (.*)", contents)
        minflt = parse_int("^pgfault (.*)", contents)
        majflt = parse_int("^pgmajfault (.*)", contents)

###############################################################################
# Previously reported swap and faults difference.
# Now we will report data since boot and use gradient for plot.
# Liran.
###############################################################################
#         self.swap_in_prev = self.swap_in_cur
#         self.swap_out_prev = self.swap_out_cur
#         self.swap_in_cur = parse_int("^pswpin (.*)", contents)
#         self.swap_out_cur = parse_int("^pswpout (.*)", contents)
#         minflt_cur = parse_int("^pgfault (.*)", contents)
#         majflt_cur = parse_int("^pgmajfault (.*)", contents)
#
#         if self.swap_in_prev is None:
#             self.swap_in_prev = self.swap_in_cur
#         if self.swap_out_prev is None:
#             self.swap_out_prev = self.swap_out_cur
#
#         swap_in = self.swap_in_cur - self.swap_in_prev
#         swap_out = self.swap_out_cur - self.swap_out_prev
#
#         minflt = (minflt_cur - self.minflt) if self.minflt is not None else 0
#         majflt = (majflt_cur - self.majflt) if self.majflt is not None else 0
#         self.minflt = minflt_cur
#         self.majflt = majflt_cur
###############################################################################

#        fields = [
#              'vm.min_free_kbytes',
#              'vm.mmap_min_addr',
#              'vm.overcommit_memory',
#              'vm.swappiness',
#              'vm.lowmem_reserve_ratio',
#              ]
#        Popen("sysctl")

        data = { 'mem_available': avail, 'mem_unused': unused, 'mem_free': free,
                 'swap_in': swap_in, 'swap_out': swap_out,
                 'anon_pages': anon , 'major_fault': majflt,
                 'minor_fault': minflt, 'cache_and_buff': free - unused
                 }
        return data

    @staticmethod
    def getFields():
        return set(['mem_available', 'mem_unused', 'mem_free', 'swap_in',
                   'swap_out', 'anon_pages', 'major_fault', 'minor_fault', 'cache_and_buff'])

if __name__ == "__main__":
    hc = HostMemory()
    print hc.collect()

