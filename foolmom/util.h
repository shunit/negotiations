#ifndef INC_UTIL_H
#define INC_UTIL_H

#include <unistd.h>

void _log(const char *func, char *format, ...)
__attribute__ ((format (printf, 2, 3)));
void _warn(const char *func, char *format, ...)
__attribute__ ((format (printf, 2, 3)));
void _die(int retcode, const char *func, char *format, ...)
__attribute__ ((format (printf, 3, 4))) __attribute__ ((noreturn));

#define log(format, args...) \
	_log(__func__, format , ## args)
#define warn(format, args...) \
	_warn(__func__, format , ## args)
#define die(retcode, format, args...)			\
	_die(retcode, __func__, format , ## args)

#endif /* INC_UTIL_H */
