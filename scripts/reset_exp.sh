# A script to destory all virtual machines and kill experiment processes running.
# Prints the list of virtual machines and python processes after it's done - so 
# the user can verify all is well.


# Kill all python
kill $(pgrep python -U $USER)
 
# Kill all ssh
kill $(ps -C ssh -o pid --no-headers)
 
for vm in $(virsh list --name); do
        virsh destroy $vm
done
 
echo "Killed python, pgbench and destroyed vm-1,2,3 - VALIDATE"
virsh list
ps -f -C python
