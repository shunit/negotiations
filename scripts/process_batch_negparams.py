first_exp = 2585
last_exp = 2659
fname = "/home/shunita/tmp/mixed_env_4_changes_memcached_negparams_%s_%s.csv" % (first_exp, last_exp)
fname_coalition_stats = "/home/shunita/tmp/opt_coalition_sizes_%s_%s.csv" % (first_exp, last_exp)
# negparams format
ov_index = 3
at_index = 4
bill_index = 7
bill_wo_coalition_index = 8
draw_per_partner = False


def map_to_bucket(value):
    if value < 0.025:
        return 0
    if value < 0.075:
        return 0.05
    if value < 0.125:
        return 0.1
    if value < 0.175:
        return 0.15
    if value < 0.225:
        return 0.2
    if value < 0.275:
        return 0.25
    if value < 0.325:
        return 0.3
    if value < 0.375:
        return 0.35
    if value < 0.425:
        return 0.4
    if value < 0.475:
        return 0.45
    if value < 0.525:
        return 0.5
    if value < 0.575:
        return 0.55
    if value < 0.625:
        return 0.6
    if value < 0.675:
        return 0.65
    if value < 0.725:
        return 0.7
    if value < 0.775:
        return 0.75
    if value < 0.825:
        return 0.8
    if value < 0.875:
        return 0.85
    if value < 0.925:
        return 0.9
    if value < 0.975:
        return 0.95
    return 1

# Get those negparams!
out = open(fname, "w")
for i in range(first_exp, last_exp+1):
    print "opening file " + "/home/shunita/moc-output/exp/coalitions-%i/negparams.csv" % i
    f = open("/home/shunita/moc-output/exp/coalitions-%i/negparams.csv" % i,"r")
    lines = f.readlines()
    print len(lines), "lines"
    f.close()
    if i == first_exp:
        header_fields = lines[0].strip("\r\n ").split(",")
        header_fields+=["profit","ov_bucket","at_bucket"]
        header_line = ",".join(header_fields)
        out.write(header_line+"\r\n")
    
    lines = lines[1:]
    for line in lines:
        fields = line.strip("\r\n ").split(",")
        if fields[10] == "None" and not draw_per_partner:
            continue
        # Add profit
        profit = float(fields[bill_wo_coalition_index])-float(fields[bill_index])
        if profit < 10**(-5):
            profit = 0
        fields.append(str(profit))
	# Add ov_bucket
        fields.append(str(map_to_bucket(float(fields[ov_index]))))
        # Add at_bucket
        fields.append(str(map_to_bucket(float(fields[at_index]))))
        pack_line = ",".join(fields)
        out.write(pack_line+"\r\n")
out.close()

# Now get the coalition stats.
out = open(fname_coalition_stats, "w")
for i in range(first_exp, last_exp+1):
    print "opening file " + "/home/shunita/moc-output/exp/coalitions-%i/coalition_stats.csv" % i
    f = open("/home/shunita/moc-output/exp/coalitions-%i/coalition_stats.csv" % i,"r")
    lines = f.readlines()
    print len(lines), "lines"
    f.close()
    if i == first_exp:
        header_fields = lines[0].strip("\r\n ").split(",")
        #header_fields+=["profit","ov_bucket","at_bucket"]
        header_line = ",".join(header_fields)
        out.write(header_line+"\r\n")

    lines = lines[1:]
    for line in lines:
        fields = line.strip("\r\n ").split(",")
        pack_line = ",".join(fields)
        out.write(pack_line+"\r\n")


out.close()

    
	

