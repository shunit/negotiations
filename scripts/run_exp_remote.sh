#! /bin/bash

# Setting option to exit the script if a command exits with a non-zero status
set -e

# Constants:
EXPERIMENT="exp/exp_negotiations.py"
NUM_OF_MACHINES=2
SOURCE_DIR="/home/alex/workspace/moc/."
DEST_DIR="/home/alexbousso/moc/"
MACHINE="csl-tapuz2a"
REMOTE_OUTPUT_DIR="/home/alexbousso/moc-output/exp"
LOCAL_OUTPUT_DIR="/home/alex/workspace/moc-output"
CSL_USERNAME="alexbousso"

# Functions:
usage() {
cat << EOF

usage: $0 OPTIONS

                               *** ATTENTION ***
Please notice that you must set up your personal parameters in the script, in
the "Constants" section before using the script!

OPTIONS:
    -r, --run-experiment        Uploads the code from SOURCE_DIR to DEST_DIR
                                and runs the experiment from EXPERIMENT in
                                MACHINE. The flag -k is called implicitly.
    -g, --get-data              Downloads the last data from REMOTE_OUTPUT_DIR
                                to LOCAL_OUTPUT_DIR.
    -k, --kill-machines         Kills NUM_OF_MACHINES virtual machines.
    -s, --restart-service       Restarts libvirt service on MACHINE.
    -v, --verbose               Show verbose (mostly for debugging).
    --sync                      Synchronize code with server.

EOF
}

# get_data() deletes the old log files from the local machine and then retrieves the most recent log files (that means,
# the last updated) from the remote machine.
get_data() {
    echo "Downloading data from $REMOTE_OUTPUT_DIR to $LOCAL_OUTPUT_DIR"
    COMMAND="cd ${REMOTE_OUTPUT_DIR}; cd \$(ls --sort=time -1 | head -1); pwd"
    # Sending stderr to null to reduce ssh verbosity
    LAST_EXP=$(ssh -q ${CSL_USERNAME}@ds-out0.cs.technion.ac.il $COMMAND 2> /dev/null)
    rm -rf ${LOCAL_OUTPUT_DIR}/*
    nice scp -rp ${CSL_USERNAME}@ds-out0.cs.technion.ac.il:$LAST_EXP/* $LOCAL_OUTPUT_DIR 2> /dev/null
}

run_ssh() {
    COMMAND=$1
    ARGS=$2
    verbose_print "run_ssh() - Executing the command:"
    verbose_print "ssh $ARGS ${CSL_USERNAME}@ds-out0.cs.technion.ac.il ssh $MACHINE $COMMAND"
    ssh $ARGS ${CSL_USERNAME}@ds-out0.cs.technion.ac.il ssh ${MACHINE} ${COMMAND}
}

verbose_print() {
    if [[ -n $VERBOSE_FLAG ]]; then
        echo $1
    fi
}

# Main:
RUN_EXPERIMENT_FLAG=
GET_DATA_FLAG=
KILL_MACHINES_FLAG=
RESTART_SERVICE_FLAG=
VERBOSE_FLAG=
SYNC_FLAG=

while [[ $# > 0 ]]; do
	key="$1"
    
	case $key in
		-r|--run-experiment)
			RUN_EXPERIMENT_FLAG=1
			;;
		-g|--get-data)
			GET_DATA_FLAG=1
			;;
		-k|--kill-machines)
			KILL_MACHINES_FLAG=1
			;;
		-s|--restart-service)
			RESTART_SERVICE_FLAG=1
			;;
        -v|--verbose)
            VERBOSE_FLAG=1
            ;;
        --sync)
        	SYNC_FLAG=1
        	;;
		*)
			usage
			exit 1
			;;
	esac
	shift  # past argument or value
done

if [[ -z $RUN_EXPERIMENT_FLAG ]] && [[ -z $GET_DATA_FLAG ]] && 
		[[ -z $KILL_MACHINES_FLAG ]] && [[ -z $RESTART_SERVICE_FLAG ]] &&
		[[ -z $SYNC_FLAG ]]; then
	usage
	exit 1
fi

# Order of actions:
#  1. Restart libvirt service
#  2. Kill the machines
#  3. Run the experiment
#  4. Download results

REMOTE_SCRIPT="cd ${DEST_DIR}"
EXEC_RUN_SSH=

if [[ -n $RESTART_SERVICE_FLAG ]]; then
    REMOTE_SCRIPT="${REMOTE_SCRIPT}; sudo service libvirt-bin restart"
    EXEC_RUN_SSH=1
fi

if [[ -n $KILL_MACHINES_FLAG ]] || [[ -n $RUN_EXPERIMENT_FLAG ]]; then
    REMOTE_SCRIPT="${REMOTE_SCRIPT}; scripts/mc_destroy.py $NUM_OF_MACHINES"
    EXEC_RUN_SSH=1
fi

if [[ -n $SYNC_FLAG ]] || [[ -n $RUN_EXPERIMENT_FLAG ]]; then
	OPTS="-a -v -u --rsh=ssh --progress --delete-during"
	echo "Syncing $SOURCE_DIR to ${CSL_USERNAME}@ds-out0.cs.technion.ac.il:${DEST_DIR}"
    nice rsync $OPTS $SOURCE_DIR ${CSL_USERNAME}@ds-out0.cs.technion.ac.il:${DEST_DIR}
fi

if [[ -n $RUN_EXPERIMENT_FLAG ]]; then
    REMOTE_SCRIPT="${REMOTE_SCRIPT}; python $EXPERIMENT"
    EXEC_RUN_SSH=1
fi

if [[ -n $EXEC_RUN_SSH ]]; then
    REMOTE_SCRIPT="'${REMOTE_SCRIPT}'"
    verbose_print "The REMOTE_SCRIPT variable is:"
    verbose_print "$REMOTE_SCRIPT"
    run_ssh "$REMOTE_SCRIPT"
fi

if [[ -n $GET_DATA_FLAG ]]; then
    get_data
fi

exit 0
