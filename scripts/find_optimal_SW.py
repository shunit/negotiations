import sys
sys.path.append("/home/shunita/cjoinval")

import cjoinval
import numpy as np


#execfile("/home/shunita/moc/scripts/valuations_10_guests_5_outside_same.txt")
execfile("/home/shunita/moc/scripts/evenly_sampled_10_guests.txt")

valuations = [None]*10
for i in range(10):
    valuations[i] = np.asarray(v[i], dtype=np.float64) - v[i][0]

#v1_array = np.asarray(v1, dtype=np.float64) -v1[0]
#v2_array = np.asarray(v2, dtype=np.float64) -v2[0]
#v3_array = np.asarray(v3, dtype=np.float64)

#valuations = [v1_array, v1_array, v1_array, v1_array, v1_array, 
#              v2_array, v2_array, v2_array, v2_array, v2_array]

#valuations = [v3_array]
total_mem = 6333 #6333
#total_mem = 633
d = 10

print cjoinval.optimize_sw(valuations, total_mem/d, 0)
