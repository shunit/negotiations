'''
Created on Aug 5, 2012

@author: eyal
'''
from exp.core.Experiment import Experiment
from exp.core.ExpMachine import ExpMachine
import threading
from exp.remote.remote_prog import remote_prog
from exp.remote.remote_bidder import remote_bidder

class LocalExperiment(Experiment):
    class LocalExpMachine(ExpMachine):
        def start_domain(self): return
        def destroy_domain(self): return
        def copy_code(self): return
        def set_mem(self, mb): return

    def get_vms(self, load_interval, start_cpu, vms_desc):
        vm = self.LocalExpMachine("localhost", None, 0, load_interval,
                                  self.out_dir, **vms_desc["localhost"])
        return [vm]

    def destroy_vms(self): return

class ReallyLocalExperiment(LocalExperiment):

    class LocalExpMachine(LocalExperiment.LocalExpMachine):
        def prog_start(self, verbosity):
            self._ssh_prog = threading.Thread(
                        target = remote_prog,
                        args = [verbosity] + list(self.prog_args),
                        name = "RemoteProgram")
            self._ssh_prog.start()

        def bidder_start(self, verbosity):
            self._ssh_bidder = threading.Thread(
                target = remote_bidder,
                args = (self.desc['moc_dir'], verbosity, self.prog_args[0],
                        self.adviser_args),
                name = "RemoteBidder")
            self._ssh_bidder.start()

        def prog_join(self, timeout = None):
            self._ssh_prog.join(timeout)

        def bidder_join(self, timeout = None):
            self._ssh_bidder.join(timeout)

        def log_output(self, out_dir): return
