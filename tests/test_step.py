#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: eyal
'''
#! /usr/bin/env python
from exp.core.Experiment import confirm_moc_path
'''
Created on Mar 9, 2012

@author: eyal
'''
from exp.prog.Programs import Guest_MemoryConsumer, Guest_Memcached
from exp.common.StepExperiments import StepExperiments

if __name__ == '__main__':

    confirm_moc_path()

    prog = "mc"

    if prog == "mc":
        prog_args = Guest_MemoryConsumer(
                        consume_percent = 0.3,
                        saturation = 600,
                        update_interval = 2,
                        sleep_after_write = 0.1
                        ).command_args
        name = "test-mc-step"

    elif prog == "memcached":
        prog_args = Guest_Memcached(
                        consume_percent = 0.3,
                        init_mem_size = 100,
                        update_interval = 2
                        ).command_args
        name = "test-memcached-step"

    StepExperiments(
                    name = name,
                    vm_name = "localhost",

                    step_timings = [[10, 30, 60]],
                    load_intervals = [2],
                    loads = [10, 20],

                    mem_bases = [1000],
                    mem_steps = [100, 500, 1000],

                    prog_args = prog_args,

                    start_cpu = 0,
                    verbosity = "info").start()

