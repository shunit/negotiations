'''
Created on Jul 28, 2012

@author: eyal
'''
from exp.prog.Programs import Guest_MemoryConsumer, Guest_Memcached, HostProgram
from tests.LocalTestbed import LocalTestbed, ReallyLocalTestbed

if __name__ == '__main__':

    prog = "mc"
    test_type = "really-local"

    if prog == "mc":
        prog_args = Guest_MemoryConsumer(
                        consume_percent = 0.5,
                        saturation = 800,
                        update_interval = 2,
                        sleep_after_write = 0.1
                        ).command_args

    elif prog == "memcached":
        prog_args = Guest_Memcached(
                        consume_percent = 0.5,
                        init_mem_size = 100,
                        update_interval = 2
                        ).command_args

    if test_type == "local":
        testbed_class = LocalTestbed
    elif test_type == "really-local":
        testbed_class = ReallyLocalTestbed

    testbed_class(vm_names = ["localhost"],
            prog_args = prog_args,
            mem_range = range(1100, 1201, 100),
            load_range = [1, 30],
            entries = ["throughput", "test_time"],
            samples_num = 1,
            repetitions = 1,
            length = 5,
            cpu_start = 3,
            mem_scan_up_and_down = True,
            verbosity = "info").start()
