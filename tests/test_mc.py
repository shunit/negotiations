'''
Created on Aug 4, 2012

@author: eyal
'''
from etc.PySock import Client
from exp.prog.MemoryConsumer import MemoryConsumerServer, MemoryConsumer
import pylab as pl
import sys
import signal

class FakeMonitor():
    def __init__(self, mc):
        self.mc = mc
        self.t = 0
        self.mem_total = 0
        self.total = []
        self.free = []
        self.load = []
        self.consumption = []
        self.perf = []
        self.client = Client("localhost", MemoryConsumerServer.port)

    def __del__(self):
        self.client.close()

    def collect(self):
        perf = self.client.send_recv("get-perf")
        curr_mc_size = self.mc.mem_size

        if self.t > 0 and perf is not None:
            self.total.append(self.mem_total)
            self.free.append(self.mem_total - curr_mc_size)
            self.load.append(self.mc.load)
            self.consumption.append(1 - float(self.free[-1]) / self.total[-1]
                                    if self.total[-1] != 0 else 0)
            self.perf.append(eval(perf))

        # set next round workload
        self.mem_total = mem_func(self.t)
        print ">> total_mem = ", self.mem_total
        self.client.send_recv("set-load:%i" % load_func(self.t))
        mem_free = self.mem_total - curr_mc_size
        self.t += 1

        return {"mem_available": self.mem_total, "mem_free": mem_free}

    def plot(self):
        ax1 = pl.gca()
        ax2 = ax1.twinx()
        ax3 = ax1.twinx()
        ax4 = ax1.twinx()
        lines = [ax1.plot(self.total, label = "total", c = "g")[0],
                 ax1.plot(self.free, label = "free", c = "g", linestyle = "--")[0],
                 ax2.plot(self.load, label = "load", c = "b")[0],
                 ax3.plot(self.perf, label = "performance", c = "r")[0],
                 ax4.plot(self.consumption, label = "consumption", c = "k", linestyle = "--")[0]]
        pl.legend(lines, [l.get_label() for l in lines], loc = "best")
        pl.xlabel("time")

        pl.figure()
        for i in range(len(self.perf)):
            perf = float(self.perf[i]) / max(self.perf)
            pl.plot(self.total[i], self.load[i],
                    linestyle = "None", marker = 'o', mew = 0,
                    mfc = pl.cm.jet(perf), #@UndefinedVariable
#                     ms = 20, alpha = 0.5)
                     ms = 20, alpha = 1.0)
        pl.grid(True)
        pl.xlabel("memory [MB]")
        pl.ylabel("load")

        pl.show()


class TestMemoryConsumer(MemoryConsumer):
    def __init__(self, *args, **kwrds):
        MemoryConsumer.__init__(self, *args, **kwrds)
        self.mem_monitor = FakeMonitor(self)

    def start(self):
        MemoryConsumer.start(self)
        self.logger.debug("Printing results")
        self.mem_monitor.plot()
        return None, None

if __name__ == "__main__":

    test_func = "scan"

    print "STARTING TEST MODE"

    if test_func == "scan":
        load_start = 10
        load_inc = 10
        load_steps = 5
        mem_start = 100
        mem_inc = 100
        load_func = lambda t: load_start + (t % load_steps) * load_inc
        mem_func = lambda t: mem_start + (t / load_steps) * mem_inc

    elif test_func == "sine":
        import math
        mem_avg = 400
        mem_amp = 350
        mem_T = 30
        mem_p = 0
        load_avg = 10
        load_amp = 9
        load_T = 20
        load_p = 0
        mem_func = lambda t: int(mem_avg + mem_amp * math.sin(math.pi * 2 / mem_T * (float(t) - mem_p)))
        load_func = lambda t: int(load_avg + load_amp * math.sin(math.pi * 2 / load_T * (float(t) - load_p)))

    from mom.LogUtils import LogUtils
    LogUtils("debug")
    mc = TestMemoryConsumer(spare_mem = 100,
                            saturation_mem = 500,
                            update_interval = 2,
                            sleep_after_write = 0.1)

    def signal_quit(signum, frame):
        print ">> TERMINATING MC!"
        mc.terminate()
    signal.signal(signal.SIGINT, signal_quit)
    signal.signal(signal.SIGTERM, signal_quit)

    mc.start()
