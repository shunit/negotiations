'''
Created on Jul 19, 2012

@author: eyal
'''
import exp.core.Loads
from exp.core.Loads import gen_random_sine_const_sum
from mom.LogUtils import LogUtils
import os
from exp.core.VMDesc import get_vm_desc
from exp.prog.Programs import Guest_Memcached, Guest_MemoryConsumer, \
    Host_Memcached
from exp.core.ExpMachine import ExpMachineProps
from exp.core.ExpMachine import ExpMachine
from exp.core.Experiment import confirm_moc_path
from exp.core.Experiment import Experiment
import threading
from tests.LocalExperiment import LocalExperiment, ReallyLocalExperiment

local = True
prog = "mc"
verbosity = "info"
exp_type = "local"

name = "localhost" if local else "vm-1"
out_dir = "../moc-output/exp-test-%s/" % prog

if __name__ == "__main__":
    LogUtils(verbosity)
    confirm_moc_path()

    print "Starting %s@%s" % (prog, name)

    if prog == "mc":
        spare = 800
        satur = 1600
        prog_args = Guest_MemoryConsumer(spare_mem = spare, saturation_mem = satur,
                update_interval = 2, sleep_after_write = 0.1).command_args

        prifiler_file = 'doc/profiler-mc-spare-%s-satur-%s.xml' % (spare, satur)
        profiler_entry = 'throughput'
        base_mem = 1100
        load = exp.core.Loads.loadupto40

    elif prog == "memcached":
        prog_args = Guest_Memcached(consume_percent = 0.5, init_mem_size = 100,
                update_interval = 2).command_args
        bm_args = Host_Memcached(
                keys_dist = ((16, 245, 1),),
                vals_dist = ((1024, 1024, 1),),
                cmd_get_percent = 0.5,
                win_size = "10m"
                ).command_args

        prifiler_file = 'doc/profiler-memcached-10vms.xml'
        profiler_entry = 'throughput'
        base_mem = 1100
        load = exp.core.Loads.loadupto20

    adviser_args = {'name': 'AdviserProfit', 'profiler': prifiler_file,
                    'entry': profiler_entry, 'rev_func': 'lambda x: 10*x',
                    'base_mem': base_mem, 'memory_delta': 10, 'load_smooth': 0,
                    'memory_smooth': 0, 'load_d': 5, 'memory_d':10}

    func = gen_random_sine_const_sum(1, T_avg = 50, **load)[0][0]

    vm_desc = {name: ExpMachineProps(adviser_args = adviser_args,
                                  prog_args = prog_args, bm_args = bm_args,
                                  load_func = func, load_interval = 2,
                                   desc = get_vm_desc(name))}

    moc_args = ("MocAuctioneer", [],
                {"host_mem": 0, "alpha": 1.0,
                 "t_auction": 10, "local_test": local})

    if exp_type == "local": experiment = LocalExperiment
    elif exp_type == "really-local": experiment = ReallyLocalExperiment

    experiment(
               moc_args = moc_args,
               vms_desc = vm_desc,
               out_dir = out_dir,
               duration = 30,
               start_cpu = 0,
               join_timeout = None,
               verbosity = verbosity
               ).start()

