'''
Created on Aug 22, 2012

@author: eyal
'''
from mom.Policy.Allocator import allocate, Bid
import mom.Policy.Allocator as Allocator
import random

def sort_and_allocate(bids, total):
    order = bids.keys()
    random.shuffle(order)
    lst = sorted([(i, bids[i]) for i in order],
                 key = lambda x:x[1].p, reverse = True)
    alloc = allocate([x[1] for x in lst], total)

    return dict([(lst[i][0], {"p":lst[i][1].p, "alloc": alloc[i]})
                 for i in range(len(lst))])

def check(result, expected):
    alloc = [result[k]['alloc'] for k in sorted(result.keys())]

    if all(a == e for a, e in zip(alloc, expected)):
        return True
    else:
        alloc_w = sum([result[k]['p'] * result[k]['alloc'] for k in result.keys()])
        expected_w = sum([result[i]['p'] * expected[i] for i in range(len(expected))])
        print ("got: %s (%i), expected: %s (%i)" %
               (alloc, alloc_w, expected, expected_w))
        return alloc_w == expected_w

def tests():
    bids = dict(enumerate([
            Bid(1, [(0, 10)]),
            Bid(2, [(0, 10)]),
            Bid(3, [(0, 10)])
            ]))

    assert check(sort_and_allocate(bids, 10), [0, 0, 10])
    assert check(sort_and_allocate(bids, 15), [0, 5, 10])
    assert check(sort_and_allocate(bids, 20), [0, 10, 10])
    assert check(sort_and_allocate(bids, 25), [5, 10, 10])

    bids = dict(enumerate([
            Bid(1, [(0, 10)]),
            Bid(2, [(0, 10)]),
            Bid(3, [(0, 3), (7, 10)])
            ]))

    assert check(sort_and_allocate(bids, 5), [0, 2, 3])
    assert check(sort_and_allocate(bids, 7), [0, 0, 7])
    assert check(sort_and_allocate(bids, 10), [0, 0, 10])
    assert check(sort_and_allocate(bids, 15), [0, 5, 10])
    assert check(sort_and_allocate(bids, 20), [0, 10, 10])
    assert check(sort_and_allocate(bids, 25), [5, 10, 10])

    bids = dict(enumerate([
            Bid(1, [(0, 10)]),
            Bid(2, [(0, 5), (7, 10)]),
            Bid(3, [(0, 2), (8, 10)])
            ]))
    assert check(sort_and_allocate(bids, 7), [0, 5, 2])
    assert check(sort_and_allocate(bids, 8), [0, 0, 8])
    assert check(sort_and_allocate(bids, 16), [1, 5, 10])
    assert check(sort_and_allocate(bids, 17), [0, 7, 10])

    bids = dict(enumerate([
            Bid(1, [(0, 10)]),
            Bid(2, [(0, 2), (8, 10)]),
            Bid(3, [(0, 2), (8, 10)])
            ]))
    assert check(sort_and_allocate(bids, 7), [3, 2, 2])
    assert check(sort_and_allocate(bids, 8), [0, 0, 8])
    assert check(sort_and_allocate(bids, 11), [0, 1, 10])
    assert check(sort_and_allocate(bids, 13), [1, 2, 10])

    bids = dict(enumerate([
            Bid(1, [(0, 5), (8, 10)]),
            Bid(2, [(6, 10)]),
            Bid(3, [(0, 2), (8, 10)])
            ]))
    assert check(sort_and_allocate(bids, 15), [0, 6, 9])
    assert check(sort_and_allocate(bids, 16), [0, 6, 10])

    bids = dict(enumerate([
            Bid(1, [(2, 5), (8, 10)]),
            Bid(2, [(6, 7)]),
            Bid(3, [(9, 10)])
            ]))
    assert check(sort_and_allocate(bids, 5), [5, 0, 0])
    assert check(sort_and_allocate(bids, 7), [0, 7, 0])
    assert check(sort_and_allocate(bids, 8), [0, 7, 0])
    assert check(sort_and_allocate(bids, 9), [0, 0, 9])
    assert check(sort_and_allocate(bids, 18), [0, 7, 10])
    assert check(sort_and_allocate(bids, 19), [2, 7, 10])

if __name__ == '__main__':
    tests()
    print "counter:", Allocator.counter, "stopped:", Allocator.stopped_by_lower_bound
    Allocator.reversed = True
    tests()
    print "counter:", Allocator.counter, "stopped:", Allocator.stopped_by_lower_bound
