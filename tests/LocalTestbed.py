'''
Created on Aug 5, 2012

@author: eyal
'''
from testbed.core.Testbed import Testbed
from testbed.core.TestbedMachine import TestbedMachine
from etc.Machine import Machine
from exp.prog.Programs import HostProgram
import threading
from testbed.remote_testbed import remote_testbed

class LocalTestbed(Testbed):
    """
    Runs Testbed on the host
    """
    class LocalTestbedMachine(TestbedMachine):
        def memory_wait(self, target_mem): return
        def copy_code(self): return
        def set_mem(self, mb): return

    def get_vms(self):
        vm = self.LocalTestbedMachine("localhost", None, 0,
                                      prog_args = self.prog_args)
        vm.start()
        HostProgram.min_cpu = self.cpu_start + 1
        return [vm]

class ReallyLocalTestbed(LocalTestbed):
    """
    Runs the remote testbed as a function in a thread,
    instead through ssh popen
    """
    class LocalTestbedMachine(LocalTestbed.LocalTestbedMachine):
        def remote_testbed_start(self, verbosity):
            self.ssh_testbed = threading.Thread(target = remote_testbed,
                                   args = [self.verbosity] + self.prog_args,
                                   name = "Remote-Testbed")
            self.ssh_testbed.start()

        def remote_testbed_join(self, timeout = None):
            self.ssh_testbed.join(timeout)
