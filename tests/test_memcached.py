#! /usr/bin/env python
'''
Created on Jun 26, 2012

@author: eyal
'''
from exp.prog.Programs import get_host_program, get_guest_program, Guest_Memcached, \
    Host_Memcached
import time
import signal
from etc.PySock import Client
import re
from threading import Thread
import socket

ip = "localhost"
load_duration = 10

spare_mem = 100

load_start = 10
load_inc = 10
load_steps = 5
mem_start = 100
mem_inc = 100
load_func = lambda t: load_start + (t % load_steps) * load_inc
mem_func = lambda t: mem_start + (t / load_steps) * mem_inc

class FakeMonitor():
    def __init__(self, host_memcached, guest_memcached):

        if (not isinstance(host_memcached, Host_Memcached) or
            not isinstance(guest_memcached, Guest_Memcached)):
            raise ValueError

        self.h_memcached = host_memcached
        self.g_memcached = guest_memcached
        self.t = 0
        self.total = []
        self.free = []
        self.load = []
        self.throughput = []
        self.get_hits = []
        self.actual = []
        self.target = []
        self.consumption = []
        self.g_memcached.mem_ctrl.mem_monitor = self
        self.client = Client("localhost", self.g_memcached.memcached_port)

    def __del__(self):
        self.client.close()

    def collect(self):

        if self.t > 0:
            self.load.append(load_func(self.t - 1))
            print "set load: ", self.load[-1]
            perf = self.h_memcached.apply_load(self.load[-1], load_duration)
            if not isinstance(perf, int):
                self.throughput.append(perf['throughput'])
                self.get_hits.append(perf['get_hits'])

        self.target.append(self.g_memcached.mem_ctrl.current_mem)
        self.total.append(mem_func(self.t))
        self.free.append(max(0, self.total[-1] - self.target[-1]))
        self.consumption.append(1 - float(self.free[-1]) / self.total[-1]
                                if self.total[-1] != 0 else 0)

        try:
            output = self.client.send_recv("stats slabs")
        except socket.error:
            self.actual.append(0)
        else:
            match = re.search(r"STAT total_malloced (\d*)\r", output)
            self.actual.append(int(match.groups(1)[0]) >> 20)

        print "total memory: ", self.total[-1], "current memory: ", self.actual[-1]
        self.t += 1

        return {"mem_available":self.total[-1], "mem_free": self.free[-1]}

    def plot(self):
        import pylab as pl
        ax1 = pl.gca()
        ax2 = ax1.twinx()
        ax3 = ax1.twinx()
        ax4 = ax1.twinx()
        lines = [ax1.plot(self.total, label = "total", c = "g")[0],
                 ax1.plot(self.free, label = "free", c = "g", linestyle = "--")[0],
                 ax1.plot(self.actual, label = "actual", c = "y")[0],
                 ax1.plot(self.target, label = "target", c = "y", linestyle = "--")[0],
                 ax2.plot(self.load, label = "load", c = "b")[0],
                 ax3.plot(self.throughput, label = "throughput", c = "r")[0],
                 ax3.plot(self.get_hits, label = "get_hits", c = "r", linestyle = "--")[0],
                 ax4.plot(self.consumption, label = "consumption", c = "k", linestyle = "--")[0]]
        pl.legend(lines, [l.get_label() for l in lines], loc = "best")
        pl.xlabel("time")

        pl.figure()
        for i in range(len(self.throughput)):
            perf = float(self.throughput[i]) / max(self.throughput)
            pl.plot(self.target[i], self.load[i],
                    linestyle = "None", marker = 'o', mew = 0,
                    mfc = pl.cm.jet(perf),  #@UndefinedVariable
                    ms = 20, alpha = 1)
        pl.grid(True)
        pl.xlabel("memory [MB]")
        pl.ylabel("load")
        pl.title("throughput")

        pl.figure()
        for i in range(len(self.get_hits)):
            perf = float(self.get_hits[i]) / max(self.get_hits)
            pl.plot(self.target[i], self.load[i],
                    linestyle = "None", marker = 'o', mew = 0,
                    mfc = pl.cm.jet(perf),  #@UndefinedVariable
                    ms = 20, alpha = 1)
        pl.grid(True)
        pl.xlabel("memory [MB]")
        pl.ylabel("load")
        pl.title("get-hits")

        pl.show()

if __name__ == '__main__':
    guest_args = Guest_Memcached(spare_mem = spare_mem,
                           init_mem_size = 1,
                           update_interval = 2,
                           ).command_args

    bm_args = Host_Memcached(
                cmd_get_percent = 0.5,
                keys_dist = ((16, 249, 1),),
                vals_dist = ((1024, 1024, 1),),
                win_size = "10m"
                ).command_args

    g_memcached = get_guest_program(args = guest_args)
    h_memcached = get_host_program(args = bm_args, ip = "localhost")

    Thread(target = g_memcached.start).start()
    time.sleep(1)
    fake_monitor = FakeMonitor(h_memcached, g_memcached)

    try:
        print ">> waiting for init..."
        h_memcached.wait_for_program_init()
        signal.pause()
    except KeyboardInterrupt:
        pass
    finally:
        g_memcached.terminate()
        fake_monitor.plot()

