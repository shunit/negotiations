﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CloudLogReader
{
    public class ListBoxItemEx : ListBoxItem
    {
        public string FullText { get; set; }

        public ListBoxItemEx(string str)
        {
            FullText = str;
            Content = GetShortLine();
        }

        private string GetShortLine()
        {
            var eolIdx = FullText.IndexOf(Environment.NewLine);
            return (eolIdx == -1) ?
                FullText : $"{FullText.Substring(0, eolIdx)}...";
        }

        public override string ToString()
        {
            const string MY_LOG_PREFIX = "[myLog]";

            var idx = FullText.IndexOf(MY_LOG_PREFIX);
            if (idx < 1)
            {
                return FullText;
            }
            else
            {
                var start = idx;    // Show [MyLog]
                return FullText.Substring(start, FullText.Length - start);
            }
        }
    }
}
