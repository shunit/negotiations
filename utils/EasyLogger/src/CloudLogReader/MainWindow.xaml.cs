﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;

namespace CloudLogReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static List<T> GetVisualChildCollection<T>(object parent) where T : Visual
        {
            List<T> visualCollection = new List<T>();
            GetVisualChildCollection(parent as DependencyObject, visualCollection);
            return visualCollection;
        }
        private static void GetVisualChildCollection<T>(DependencyObject parent, List<T> visualCollection) where T : Visual
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child is T)
                {
                    visualCollection.Add(child as T);
                }
                else if (child != null)
                {
                    GetVisualChildCollection(child, visualCollection);
                }
            }
        }

        private static List<ListBox> _listBoxes = new List<ListBox>();

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowVM();
            ImpoerFolderButton_Click(null, null);
        }
        
        private void InitTabsFromFiles(string folderPath)
        {
            var path = folderPath;

            this.Title = $"EasyLogger - {path}";

            var files = Directory.GetFiles(path, "bidder-vm-*-mem");

            tabControl.Items.Clear();
            VisTextBox.Text = "";

            foreach (var fpath in files)
            {
                var fname = System.IO.Path.GetFileName(fpath);

                // Build Vis python code - old code - moved to /out/SMART_graph_generator.py
                //if (fpath.Contains("-1-"))
                //{
                //    var snapshots = File.ReadAllLines(fpath)
                //        .Where(l => l.Contains("XXXS") && l.Contains("|"))
                //        .Select(l => 
                //        {
                //            var FIRST_ANCHOR = "XXXS";
                //            var idx = l.IndexOf(FIRST_ANCHOR);
                //            var startIdx = idx + FIRST_ANCHOR.Length;
                //            var sub = l.Substring(startIdx, l.Length - startIdx);
                //            var tokens = sub.Split('|');
                //            var senderId = tokens[0].ToString();
                //            Console.WriteLine(senderId);
                //            var round = tokens[1].ToString();
                //            var obj = tokens[2];
                //            var pyArrItem = "dict(vm=" + senderId + ",round=" + round + ", obj=" + obj + ")";
                //            return pyArrItem;
                //        });
                //    var arrayItemsCode = String.Join(",", snapshots);
                //    VisTextBox.Text = $"snapshots = [{arrayItemsCode}]";
                //}

                var lstBox = new ListBox();
                _listBoxes.Add(lstBox);
                lstBox.MouseDoubleClick += (o, e) =>
                {
                    var item = ((ListBox)o).SelectedItem;
                    if (item != null)
                    {
                        System.Windows.Clipboard.SetText(item.ToString());
                    }
                };

                lstBox.SelectionChanged += (o, e) =>
                {
                    var item = ((ListBox)o).SelectedItem;
                    txtBox.Text = $"{item}";
                    HighlightText();
                };

                var lines = File.ReadAllLines(fpath)
                    .Where(l => l.Contains("- Adviser -") && !l.Contains("VIS"))
                    .Select(l => l.Replace("|||", Environment.NewLine))
                    .ToList();

                lines.ForEach(l => lstBox.Items.Add(new ListBoxItemEx(l)));

                var tabItem = new TabItem();
                tabItem.Header = fname.Replace("bidder-", "").Replace("-mem", "");
                tabItem.Content = lstBox;
                tabControl.Items.Add(tabItem);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            return;

            foreach (var listBox in _listBoxes)
            {
                List<ScrollBar> scrollBarList = GetVisualChildCollection<ScrollBar>(listBox);
                foreach (ScrollBar scrollBar in scrollBarList)
                {
                    if (scrollBar.Orientation == Orientation.Vertical)
                    {
                        scrollBar.ValueChanged += new RoutedPropertyChangedEventHandler<double>((o, p) =>
                        {
                            //_listBoxes.ForEach(l => l.scr)
                        });
                    }
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var txt = (sender as TextBox).Text;

            foreach (var listBox in _listBoxes)
            {    
                ICollectionView view = CollectionViewSource.GetDefaultView(listBox.Items);
                
                if (string.IsNullOrEmpty(txt))
                {
                    view.Filter = null;
                }
                else
                {
                    view.Filter = (obj) => { return obj.ToString().Contains(txt); };
                }
            }
        }

        private void DescriptionTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void HighlightText()
        {
            try
            {
                var txt = DescriptionTextBox.Text;
                var idx = txtBox.Text.IndexOf(txt);
                txtBox.Focus();
                txtBox.Select(idx, txt.Length);
            }
            catch { }
        }

        private void txtBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                HighlightText();
            }
        }

        private void FilesListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var selectedItem = (sender as ListBox).SelectedItem.ToString();
            OpenFileListGrid.Visibility = Visibility.Hidden;
            InitTabsFromFiles($"{selectedItem}\\p0-0.00-freq-60");
        }

        private void ImpoerFolderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FilesListBox.Items.Clear();
                Directory.GetDirectories(BasePathTextbox.Text).ToList()
                    .ForEach(d => FilesListBox.Items.Add(d));
                OpenFileListGrid.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateBasePathButton_Click(object sender, RoutedEventArgs e)
        {
            ImpoerFolderButton_Click(sender, e);
        }
    }

}
