'''
Created on Oct 21, 2011

@author: eyal
'''

import os
import numpy as np
import signal
from mom.LogUtils import LogUtils
from mom.Collectors.HostMemory import HostMemory
from mom.Collectors.GuestCpu import GuestCpu
from etc.TerminatableThread import Terminatable
from etc.ParallelInvoker import ParallelInvoker
import logging
from testbed.core.TestbedMachine import TestbedMachine
from exp.core.Experiment import set_output_path
import time
from mom.Collectors.HostCpu import HostCpu

from etc.FaultyFlag import FaultyFlag
from etc.HostMachine import HostMachine
from etc.Machine import MachineException

class Testbed(FaultyFlag):
    '''
    Run a testbed
    Must have an appropriate setup inside the VM: a remote-testbed.py and the
    checked program
    '''
    def __init__(self, vm_names, prog_args, bm_args,
                 mem_range, load_range, entries,
                 samples_num, repetitions, length, set_notify_sleep,
                 mem_scan_up_and_down = True, verbosity = "info"):
        FaultyFlag.__init__(self)
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.set_notify_sleep = set_notify_sleep
        self.vm_names = vm_names
        self.verbosity = verbosity
        self.mem_scan_up_and_down = mem_scan_up_and_down
        self.samples_num = samples_num
        self.length = length
        self.repetitions = repetitions
        self.entries = entries
        self.prog = prog_args[0]

        self.output_dir = set_output_path("testbed", self.prog)
        stdout_file = os.path.join(self.output_dir, "testbed-out")
        LogUtils(verbosity)
        LogUtils.start_file_logging(stdout_file, logging.DEBUG)
        self.logger = logging.getLogger("Testbed")

        self.host_machine = HostMachine()

        estimated_duration = (len(load_range) + 1) * length * repetitions * samples_num
        if self.mem_scan_up_and_down:
            estimated_duration *= 2 * len(mem_range) - 1
        else:
            estimated_duration *= len(mem_range)
        estimated_duration /= 3600.0
        self.logger.info("Estimated testbed duration: %.1fh", estimated_duration)

        self.terminator = Terminatable()

        def signal_quit(signum, frame):
            self.terminator.terminate()

        signal.signal(signal.SIGINT, signal_quit)
        signal.signal(signal.SIGTERM, signal_quit)

        mem_range.sort()
        load_range.sort()
        #self.params = [load_range, mem_range]
        self.load_range = load_range
        self.mem_range = mem_range
        self.warmup_load = load_range[len(load_range) / 2]

        self.vms = self.get_vms()
        self.invoker = ParallelInvoker(self.vms)

        self.host_machine.begin_experiment(self)

        file(os.path.join(self.output_dir, "info"), "w").write(str(
            {"vm_names": vm_names, "prog_args": prog_args, "bm_args": bm_args,
             "mem_range":mem_range, "load_range":load_range,
             "entries":entries, "samples_num":samples_num,
             "repetitions": repetitions, "length":length,
             "set_notify_sleep": set_notify_sleep
             }))

    def start(self):
        self.logger.info("Starting VMs...")

        # libvirt need to be serial - start domain in serial, all following
        # operations in parallel
        for vm in self.vms:
            vm.start_domain()

        self.invoker.start_and_join(lambda vm: vm.start)

        ## initialize output data structure
        self.fields = reduce(set.union,
                [HostMemory.getFields(), GuestCpu.getFields(),
                 set(["tb_load", "tb_mem"]), self.vms[0].fields,
                 HostCpu.getFields()])

        self.logger.info("Using fields: %s", ", ".join(self.fields))

        self.test_results = dict((n, dict((p, []) for p in self.fields))
                                 for n in self.vm_names)

#         array_size = (len(self.load_range), len(self.mem_range),
#                       self.samples_num * self.repetitions)
#        self.profiler_data = dict([(name, dict([(key, np.ndarray(array_size))
#                                 for key in self.entries]))
#                     for name in self.vm_names])

        mem_ind = range(len(self.mem_range))
        if self.mem_scan_up_and_down:
            first_iteration = True
            for i in reversed(mem_ind):
                if first_iteration:
                    first_iteration = False
                    continue
                mem_ind.append(i)
#            mem_ind.reverse()

        self.logger.debug("mem_ind = " + str(mem_ind))

        try:
            for s in range(self.samples_num):
#                if self.mem_scan_up_and_down:
#                    mem_ind.reverse()
                prev_mem = None
                for m in mem_ind:
                    mem = self.mem_range[m]
                    if mem == prev_mem:
                        continue
                    prev_mem = mem

                    # set memory allocation for current tests
                    self.notify_mem(mem)
                    time.sleep(self.set_notify_sleep)
                    self.set_mem(mem)

                    self.logger.info("wait for all vms to achieve target memory")
                    self.invoker.start_and_join(
                            lambda vm: vm.memory_wait,
                            args = lambda vm: [mem])
                    self.logger.info("all vms reached target memory")

                    # warmup
                    self.logger.info("warm up...")
                    self.logger.debug("warm up load: %i", self.warmup_load)
                    try:
                        self.collect_data(self.warmup_load, self.length)
                    except MachineException as ex:
                        self.logger.error("Error during warmup: %s", ex)
                        self.setFaulty()

                    for l in range(len(self.load_range)):
                        load = self.load_range[l]

                        for r in range(self.repetitions):

                            self.logger.info("starting: sample: %i, load: %i, " +
                                        "memory: %i, repeat: %i",
                                        s, load, mem, r)

                            i = s * self.repetitions + r

                            # run benchmark
                            try:
                                collect_data = self.collect_data(load, self.length)
                            except MachineException:
                                collect_data = {}
                                self.setFaulty()
                                self.logger.warn("Data collection failed," +
                                    "sample: %i, load: %i, memory: %i, repeat: %i", s, load, mem, r)

                            # output
                            self.collect_output(l, m, i, collect_data)
                            self.logger.info("Ended: load: %i, memory: %i", load, mem)
                            self.print_statistics(collect_data)

                            # check termination
                            if not self.terminator.should_run:
                                raise KeyboardInterrupt

        except KeyboardInterrupt:
            pass
        finally:
            try:
                self.logger.info("Saving output")
                self.save_output()
            except Exception as ex:
                self.logger.warn("Error saving output: %s", ex)
            try:
                self.destroy_vms()
            except Exception as ex:
                self.logger.warn("Error while ending: %s", ex)
            self.logger.info("Ended. Output written to %s", self.output_dir)

        # print warning if faulty
        if self.isFaulty:
            self.logger.warn("Testbed results may be faulty")


    def collect_output(self, l, m, i, collected):
        load = self.load_range[l]
        mem = self.mem_range[m]

        # copy data to result variables
        for name in self.vm_names:
            # add to profiler data:
#            for entry in self.entries:
#                try:
#                    self.profiler_data[name][entry][l, m, i] = \
#                                            collected[name][entry]
#                except KeyError:
#                    self.profiler_data[name][entry][l, m, i] = 0
#                except ValueError:
#                    self.logger.error("assignment data: %s", str(self.profiler_data[name][entry][l, m, i]))
#                    self.logger.error("tried to assign: %s", str(collected[name][entry]))
#                    raise

            max_len = 0
            for prop in self.fields:
                if name not in collected or prop not in collected[name]:
                    continue
                if isinstance(collected[name][prop], list):
                    max_len = max(max_len, len(collected[name][prop]))

            # add test parameters (for an entire list if we have a list):
            if max_len == 0:
                collected[name]['tb_load'] = load
                collected[name]['tb_mem'] = mem
            else:
                collected[name]['tb_load'] = [load] * max_len
                collected[name]['tb_mem'] = [mem] * max_len

            for prop in self.fields:
                try:
                    if isinstance(collected[name][prop], list):
                        for sample in collected[name][prop]:
                            self.test_results[name][prop].append(sample)
                    else:
                        self.test_results[name][prop].append(collected[name][prop])
                except KeyError:
                    self.test_results[name][prop].append(None)

    def print_statistics(self, collected):
        # print test statistics
        system_stat = {}
        for prop in self.fields:
            vals = []
            for name in self.vm_names:
                try:
                    vals.append(collected[name][prop])
                except KeyError: pass
            system_stat[prop] = (np.average(vals), np.std(vals))
        self.logger.info("\n".join(["system stat:"] +
                                   ["    %s: %s (%s)" % (k, str(v[0]), str(v[1]))
                                    for k, v in system_stat.iteritems()]))

    def get_vms(self):
        import libvirt
        self.logger.info("connecting to libvirt")
        self.conn = libvirt.open('qemu:///system')

        ret = []

        for name in self.vm_names:
            ret.append(TestbedMachine(name, self.conn, None,
                               self.output_dir,
                               prog_args = self.prog_args,
                               bm_args = self.bm_args,
                               base_mem = self.mem_range[0]))

        for vm in ret:
            vm_cpus = self.host_machine.auto_select_cpus( vm.get_cpu_count() )
            bm_cpus = self.host_machine.auto_select_cpus( vm.get_benchmark_cpu_count() )
            vm.set_guest_cpus(vm_cpus)
            vm.set_benchmark_cpus(bm_cpus)

        return ret

    def destroy_vms(self):
        self.logger.info("Ending...")
        self.invoker.start_and_join(lambda vm: vm.destroy)
        self.logger.info("Destroying VMs...")
        for vm in self.vms:
            time.sleep(0.1)
            vm.destroy_domain()
            if vm.isFaulty:
                self.logger.warn("VM %s had faults, results may be invalid", vm.name)
                self.setFaulty()
        self.conn.close()

    def collect_data(self, load, length):
        output = {}
        self.invoker.start_and_join(
            lambda vm: vm.collect_data,
            args = lambda vm: (load, length, output))
        return output

    def remote_testbed_start(self):
        self.logger.info("invoking remote-testbed")
        for vm in self.vms:
            vm.remote_testbed_start(self.verbosity)

    def remote_testbed_end(self):
        for vm in self.vms:
            vm.remote_testbed_end()

    def remote_testbed_join(self):
        self.invoker.start_and_join(lambda vm: vm.remote_testbed_join,
                                   args = lambda vm: [30])

    def notify_mem(self, mb):
        self.invoker.start_and_join(lambda vm: vm.memory_notify,
                                    args = lambda vm: [mb])

    def set_mem(self, mb):
        self.logger.info("set new memory allocation: %i MB", mb)
        for vm in self.vms:
            vm.set_mem(mb)

    def save_output(self):
        try:
            for name, val in self.test_results.iteritems():
                res_file = os.path.join(self.output_dir,
                        "testbed-results-%s-%s" % (self.prog, name))
                file(res_file, 'w').write(str(val))
        except Exception as ex:
            self.logger.info("Error while saving results: %s", ex)

