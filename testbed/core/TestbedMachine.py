'''
Created on Jul 27, 2012

@author: eyal
'''
from exp.deprecated.QuitServer import send_quit_request
import socket
from etc.Machine import Machine, MachineException
from exp.prog.Programs import get_host_program
from etc.PySock import Client
import os
from testbed.remote import remote_testbed
from testbed.remote.remote_testbed import RemoteTestbedServer
from etc.shell import TimeoutError
import time
from mom.Collectors.HostCpu import HostCpu

mb_diff_ok = 10

class TestbedMachine(Machine):
    def __init__(self, vm_name, conn, cpu_pin, output_dir, **props):
        Machine.__init__(self, vm_name, conn, cpu_pin)
        self.prog_args = props['prog_args']
        self.bm_args = props['bm_args']
        self.init_mem = props['base_mem']
        self.max_mem = 3000
        self.output_dir = output_dir
        self.ssh_testbed = None
        self._bm = None
        self._client = None
        self.host_cpu = HostCpu(None)
        self.alloc_diff = 0

    def start_domain(self):
        if self.dom.isActive():
            return
        self.set_libvirt_mem(self.init_mem, self.max_mem)
        Machine.start_domain(self)

    def start(self):
        self.logger.info("Starting...")
        self.start_domain()
        self.logger.info("Waiting for SSH...")
        self.wait_for_ssh_server()
        self.logger.info("Copy moc code...")
        self.copy_code()
        self.set_vm_props()
        self.log_info()
        # starting all the stuff
        self.logger.info("Initiating benchmark...")
        self.benchmark_init()
        self.logger.info("Starting remote testbed...")
        self.remote_testbed_start("info")
        self.logger.info("Waiting for remote initialization...")
        self.prog_wait_for_init()

        guest_mem = self.get_guest_memory()
        self.logger.info("Initial memory allocation: %i MB", self.init_mem)
        self.set_mem(self.init_mem, correct_diff = False)
        self.alloc_diff = self.init_mem - guest_mem
        self.logger.info("Detected memory difference: %i", self.alloc_diff)
        self.logger.info("Expected memory: %i", self.init_mem)
        self.logger.debug("Actual memory: %i", guest_mem)
#        self.logger.info("Correcting memory allocation")

    def destroy(self):
        self.logger.info("Ending benchmark...")
        self.benchmark_end()
        self.logger.info("Ending remote testbed...")
        self.remote_testbed_end()
        self.logger.info("Waiting for remote testbed...")
        self.remote_testbed_join(20)

    @property
    def fields(self):
        return self._bm.getFields()

    def benchmark_init(self):
        self._bm = get_host_program(args = self.bm_args, ip = self.ip, alias = self.name)
        self._client = Client(self.ip, RemoteTestbedServer.port,
                               timeout = None,
                               name = "%s-testbed-client" % self.name)

    def benchmark_end(self):
        self._client.close()
        self._bm.terminate()

    def memory_notify(self, target_mem):
        self._client.send_recv("total-mem:%i" % target_mem)

    def get_guest_memory(self):
        try:
            curr_mem = int(self._client.send_recv("get-mem"))
        except (socket.error, SyntaxError):
            raise MachineException("vm %s doesn't report memory" %
                                   self.name)
        return curr_mem

    def set_mem(self, mb, correct_diff = True):
        amount = mb
        if correct_diff:
            amount += max(0, self.alloc_diff)
        self.logger.info("Allocating %s with %i MB (target memory is %i MB)", self.name, amount, mb)
#       if amount != mb:
#           self.logger.debug("Expected allocation: %i, actual allocation: %i", mb, amount)
        Machine.set_mem(self, amount)

    def memory_wait(self, target_mem):
        while True:
            curr_mem = self.get_guest_memory()
            diff = target_mem - curr_mem

            if abs(diff) <= mb_diff_ok:
                break
            self.logger.info("VM %s not yet reached memory (%i MB to go. Target is %i MB)",
                             self.name, diff, target_mem)
            # try to re-allocate with the new diff
            self.alloc_diff += diff
            self.logger.debug("Trying to reallocate. Target: %i, diff: %i",
                              target_mem, self.alloc_diff)
            self.set_mem(target_mem)
            time.sleep(3)

    def collect_data(self, load, length, ret_dict):
        # start the remote collector, and wait for reply
        ret_dict[self.name] = {}
        try:
            self._client.send_recv("collect-start")
        except socket.error:
            raise MachineException("Problem sending collect-start")

        self.host_cpu.collect()
        # apply load
        try:
            data = self._bm.benchmark(load, length)
        except Exception as ex:
            raise MachineException("Problem applying load: %s", ex)

        # get reply from the remote collector
        try:
            answer = self._client.send_recv("collect-end")
            remote = eval(answer)
        except (socket.error, SyntaxError) as ex:
            raise MachineException("Problem in collect-end: %s", ex)

        host_cpu_data = self.host_cpu.collect()

        # combine the results
        # choose if property is aggregated (like page faults)
        # or can be averaged.
        for key, val in remote.iteritems():
            if key in ('major_fault', 'minor_fault', 'swap_in', 'swap_out'):
                ret_dict[self.name][key] = val['sum']
            else:
                ret_dict[self.name][key] = val['avg']

        ret_dict[self.name].update(host_cpu_data)

        max_len = 0

        if isinstance(data, list):
            max_len = len(data)
            for measurement in data:
                for k, v in measurement.iteritems():
                    if k not in ret_dict[self.name]:
                        ret_dict[self.name][k] = []
                    ret_dict[self.name][k].append(v)
        else:
            ret_dict[self.name].update(data)

        # make all of the measurements synchronized in size (if we have multiple measurements)
        if max_len > 0:
            for key, val in ret_dict[self.name].iteritems():
                if not isinstance(val, list):
                    ret_dict[self.name][key] = [val] * max_len
                else:
                    if len(val) < max_len: # pad if needed (hopefully will never happen)
                        for i in range(max_len - len(val)):
                            ret_dict[self.name].append(None)

        # insert into a global variable
        #ret_dict[self.name] = data

    def prog_wait_for_init(self):
        if not self.ssh_testbed.is_alive():
            raise MachineException("Remote testbed was terminated: %s" %
                            str(self.ssh_testbed.err))
        while True:
            out = {}
            try:
                self.collect_data(1, 1, out)
            except MachineException:
                time.sleep(3)
            else:
                if len(out[self.name]) > 0: break

        self.logger.info("Program initiated")

    def remote_testbed_start(self, verbosity):
        executable = self.local_path('testbed/remote/remote_testbed.py')
        remote_testbed_args = [verbosity] + list(self.prog_args)
        args = [executable] + remote_testbed_args
        out_file = os.path.join(self.output_dir, 'remote-%s' % self.name)
        self.ssh_testbed = self.ssh(*args, name = self.name + "-Remote-Testbed",
                                    output_file = out_file)
        self.ssh_testbed.start()

    def remote_testbed_end(self):
        try:
            send_quit_request(self.ip, remote_testbed.RemoteTestbedServer.port)
        except socket.error:
            self.logger.error("No connection to bidder's quit server")
            self.setFaulty()

    def remote_testbed_join(self, timeout):
        if not self.ssh_testbed:
            return
        if self.ssh_testbed.is_alive():
            try:
                # try to join the ssh_trd with timeout
                self.ssh_testbed.join(timeout)
            except TimeoutError:
                # if timeout, kill the thread.
                self.logger.warn("Join %s was timeout, terminating...",
                                 self.ssh_testbed.name)
                self.ssh_testbed.terminate()
                self.ssh_testbed.join()
            else:
                self.logger.info("Program joined")

