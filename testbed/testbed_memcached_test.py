#! /usr/bin/env python
'''
Created on Dec 30, 2011

@author: eyal
'''
from testbed.core.Testbed import Testbed
from exp.prog.Programs import Guest_Memcached, Host_Memcached

def testbed():
    n = 2

    prog_args = Guest_Memcached(
            spare_mem = 50,
            init_mem_size = 10,
            update_interval = 1,
            ).command_args

    bm_args = Host_Memcached(
            cmd_get_percent = 0.3,
            keys_dist = ((249, 249, 1),),
            vals_dist = ((1024, 1024, 1),),
            win_size = "500k",
            ).command_args

    Testbed(vm_names = ["vm-%i" % i for i in range(1, n + 1)],
            prog_args = prog_args,
            bm_args = bm_args,
            mem_range = [600, 1200, 2000], #range(600, 2201, 200),
            load_range = [1, 8], #range(1, 10 + 1),
            entries = ['test_time', 'get_hits_percent', 'hits_rate',
                       'samples_num', 'get_total', 'throughput',
                       'get_hits_total', 'measure_duration'],
            samples_num = 2,
            repetitions = 1,
            length = 1200,
            set_notify_sleep = 5,
            mem_scan_up_and_down = True,
            verbosity = "debug").start()

if __name__ == "__main__":
    testbed()
