#! /usr/bin/env python
'''
Created on Dec 30, 2011

@author: eyal
'''

from testbed.runtestbed import testbed
import time

if __name__ == "__main__":

    n = 8

    mem_offset = 348

    heap_size = 800
    dt_max_users = 2000
    dt_max_quotes = 40000
    ab_length = 60
    repeatitions = 5
    mem_range = range(1000, 2001, 100)
    load_range = [1, 5, 10, 15]
    samples_num = 4

    prog_args = ["DayTrader", heap_size, dt_max_users, dt_max_quotes]
    entries = ["req_sec", "ms_req", "ms_req_across", "percent95", "test_time"]
    vm_names = ["dt-%i" % i for i in range(1, n + 1)]

    start_time = time.time()

    testbed(vm_names, prog_args, mem_range, load_range, entries, samples_num,
            repeatitions, ab_length, mem_offset, length_per_load = False)

    print "testbed duration: %.2f sec" % (time.time() - start_time)
