#! /usr/bin/env python
'''
Created on Dec 30, 2011

@author: eyal
'''
from testbed.core.Testbed import Testbed
from exp.prog.Programs import Guest_MemcachedStatic, Host_MemcachedStatic

def testbed():
    n = 4

    prog_args = Guest_MemcachedStatic(
            spare_mem = 50,
            init_mem_size = 500,
            update_interval = 1,
            memcached_port = 1234
            ).command_args

    bm_args = Host_MemcachedStatic(
            cmd_get_percent = 0.3,
            keys_dist = ((249, 249, 1),),
            vals_dist = ((1024, 1024, 1),),
            win_size = "100k",
            remote_ip = "csl-tapuz26",
            memcached_port = 1234
            ).command_args

    Testbed(vm_names = ["vm-%i" % i for i in range(1, n + 1)],
            prog_args = prog_args,
            bm_args = bm_args,
            mem_range = range(300, 1000, 100),
            load_range = [1, 2, 4, 6, 8, 10],
            entries = ["throughput", "get_hits_percent", "get_total",
                       "get_hits_total", "samples_num", "test_time",
                       "hits_rate"
                       ],
            samples_num = 2,
            repetitions = 1,
            length = 200,
            set_notify_sleep = 5,
            mem_scan_up_and_down = True,
            verbosity = "info").start()

if __name__ == "__main__":
    testbed()
